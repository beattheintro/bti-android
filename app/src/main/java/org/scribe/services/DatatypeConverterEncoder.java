package org.scribe.services;


public class DatatypeConverterEncoder extends Base64Encoder
{
  @Override
  public String encode(byte[] bytes)
  {
    //return DatatypeConverter.printBase64Binary(bytes);
    return Base64Encoder.getInstance().encode(bytes);
  }

  @Override
  public String getType()
  {
    return "DatatypeConverter";
  }
}
