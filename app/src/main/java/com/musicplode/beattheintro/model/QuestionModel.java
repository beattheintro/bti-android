package com.musicplode.beattheintro.model;

import android.widget.ImageView;

import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.OfflineHelpers.Track;
import com.musicplode.beattheintro.modelhelpers.OfflineTrackModel;
import com.musicplode.beattheintro.modelhelpers.TrackModel;
import com.musicplode.beattheintro.storage.Storage;

import org.json.JSONException;
import org.json.JSONObject;

public class QuestionModel {
	private TrackModel[] tracks;
	private int correctTrack;
	private ImageView imageView;
	private String answerId;
	private String answerTrackId;


    public QuestionModel(Track track0, Track track1, Track track2) {
		tracks = new TrackModel[3];
		tracks[0] = new OfflineTrackModel(track0);
		tracks[1] = new OfflineTrackModel(track1);
		tracks[2] = new OfflineTrackModel(track2);
		answerId = "";
		
		shuffleTrackModels(tracks);
		
		for (int i = 0; i < 3; i++) {
			if (tracks[i].getStreamURL().equals(track0.getPath()))
				correctTrack = i;
		}
		
	}
	
	public QuestionModel(JSONObject o) {
		tracks = new TrackModel[3];
		try {
			answerId = o.getString("AnswerId");
			tracks[0] = new TrackModel(o.getJSONObject("TrackCorrect"));
			tracks[1] = new TrackModel(o.getJSONObject("TrackWrong1"));
			tracks[2] = new TrackModel(o.getJSONObject("TrackWrong2"));

			answerTrackId = o.getString("AnswerTrackId");
			//noinspection StatementWithEmptyBody
			if (answerTrackId.contentEquals(o.getJSONObject("TrackCorrect").getString("Id"))) {
				//getImageView().setImageResource(R.drawable.gameplay_correct);
			}
			else //noinspection StatementWithEmptyBody
				if (answerTrackId.contentEquals(o.getJSONObject("TrackWrong1").getString("Id")) || answerTrackId.contentEquals(o.getJSONObject("TrackWrong2").getString("Id"))) {
				//getImageView().setImageResource(R.drawable.gameplay_incorrect);
			}
			else {
				TrackModel corrTrack = tracks[0];
				tracks = shuffleTrackModels(tracks);
				
				for (int i = 0; i < 3; i++) {
					if (tracks[i] == corrTrack)
						correctTrack = i;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	

	public TrackModel[] shuffleTrackModels(TrackModel[] trackModels) {
		if (trackModels == null || trackModels.length == 0)
			return null;
		
		int size = trackModels.length;
		int sw;
		for (int i = 0; i < trackModels.length; i++) {
			TrackModel tmp = trackModels[i];
			sw = Storage.random.nextInt(size);
			
			trackModels[i] = trackModels[sw];
			trackModels[sw] = tmp;
		}
		
		return trackModels;
	}
	
	public void setResult(boolean guessedCorrect) {
		if (guessedCorrect)
			getImageView().setImageResource(R.drawable.gameplay_correct);
		else
			getImageView().setImageResource(R.drawable.gameplay_incorrect);
	}
	public String getStreamURL() {
		return tracks[correctTrack].getStreamURL();
	}
	public String getCorrectTrackId() {
		return tracks[correctTrack].getId();
	}
	public TrackModel getTrack(int i) {
		return tracks[i];
	}

	public int getCorrectTrackNumber() {
		return correctTrack;
	}
	
	@Override
	public String toString() {
		String out = "QM, ";
		if (tracks != null)
			out += "l:" + tracks.length;
		
		for (int i = 0; i < tracks.length; i++) {
			if (tracks[i] != null)
				out += " | Track " + i + " " + tracks[i];
			else
				out += " | Track " + i + " == null";
		}
		
		return out;
	}

	public int getWrongTrackNumber() {
		if (correctTrack == 0)
			return 1;
		return 0;
	}

	public ImageView getImageView() {
		return imageView;
	}

	public void setImageView(ImageView imageView) {
		this.imageView = imageView;
		
		if (answerTrackId == null)
			return;
		
		if (answerTrackId.contentEquals(getCorrectTrackId())) {
			getImageView().setImageResource(R.drawable.gameplay_correct);
		}
		else if (answerTrackId.contentEquals( getTrack(0).getId()) || answerTrackId.contentEquals( getTrack(1).getId()) || answerTrackId.contentEquals( getTrack(2).getId())) {  
			getImageView().setImageResource(R.drawable.gameplay_incorrect);
		}
	}

	public String getAnswerId() {
		return answerId;
	}

    public TrackModel getCorrectTrackModel() {
        return tracks[getCorrectTrackNumber()];
    }
}
