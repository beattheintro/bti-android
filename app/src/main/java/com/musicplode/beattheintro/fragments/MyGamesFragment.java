package com.musicplode.beattheintro.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.Sounds;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.DialogHelpers.OnOkButton;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.modelhelpers.EarnDiscTypes;
import com.musicplode.beattheintro.modelhelpers.GamePlayer;
import com.musicplode.beattheintro.modelhelpers.IGame;
import com.musicplode.beattheintro.modelhelpers.OnlineGame;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

/**
 * Contains the list of current & previous Games
 */
public class MyGamesFragment extends SocialHelperFragment implements IUpdateableFragment {
	@Override
	public String getName() {
		return "MyGamesList";
	}
	private ListView lv;
	private GamesAdapter ga;
	private View v;
	private boolean previousGames = false;
	
	@Override
	public void onResume() {
		super.onResume();
		
		updateListView();

		((LoggedInMenu)getActivity()).disableLowerChart();
		((LoggedInMenu)getActivity()).enableAds();

        ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.OPEN);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		uiHelper = new UiLifecycleHelper(getActivity(), callback);
	    uiHelper.onCreate(savedInstanceState);
	}

    private StatusCallback callback = new StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {

		}
	};
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
    }
	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
//        	System.out.println("FBSHARE Error: " + error.toString());
        }
        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
			String gesture = FacebookDialog.getNativeDialogCompletionGesture(data);
			//noinspection StatementWithEmptyBody
			if (gesture != null) {
				if ("post".equals(gesture)) {
					EarnDiscTypes earn = Storage.getEarnDiscTypes(getActivity());
					String id = earn.getNameToIdMap().get("FbShareScore");
					
					//Send liked request to BTI server
					JSONObject jsonReq = JSONRequestHelpers.getFacebookShareJSON(getActivity(), id);
					Storage.addRequestToQueue(getActivity(), Storage.EARNDISC_URL, jsonReq, new OnNetworkResult() {
						@Override
						public void okResult(JSONObject response) {
						}
						@Override
						public void badResult(String errorMessage) {
						}
					});
				} 
				else //noinspection StatementWithEmptyBody
					if ("cancel".equals(gesture)) {
				} 
				else {
				}
			} else {
			}
        }
    };
    
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.my_games, container, false);
		lv = (ListView) v.findViewById(R.id.listView1);
		lv.setEmptyView(v.findViewById(android.R.id.empty));

		/** View jv = v.findViewById(R.id.backButtonRing);
		if (jv == null) {
			Helpers.setupBackButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backSwirl));
		}
		else {
			Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
		} */

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));

		isTablet = getResources().getBoolean(R.bool.tablet);
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (!previousGames) {
					IGame apa = (IGame) lv.getAdapter().getItem(arg2);

					if (apa.getPlayer().isFinished() && apa.getOpponents().size() > 0) {
						GamePlayer opponent = apa.getOpponents().get(0);
						
						JSONObject jsonReq = JSONRequestHelpers.getNudgeJSON(getActivity(), opponent.getUserId());
						Storage.addRequestToQueue(getActivity(), Storage.NUDGE_URL, jsonReq, new OnNetworkResult() {
							@Override
							public void okResult(JSONObject response) {
								DialogHelpers.showHardCodedDialog(getActivity(), "Nudge", "Nudge nudge");
							}
							@Override
							public void badResult(String errorMessage) {
								DialogHelpers.showStandardBadResult(getActivity(), errorMessage);
							}
						});
						
						return;
					}
					final ProgressDialog pd = new ProgressDialog(getActivity());
					pd.setMessage(getString(R.string.LOADING));
					pd.setCancelable(false);
					pd.show();
					
					Helpers.startGameById((LoggedInMenu) getActivity(), container.getId(), pd, apa.getId());
				}
			}
		});
		
		/**
		 * Add a delete menu on longclicking an item
		 */
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				final int arg2, long arg3) {
				DialogHelpers.showDeleteDialog(getActivity(), R.string.MY_GAMES_DELETE_TITLE, R.string.MY_GAMES_DELETE_BUTTON_TEXT, R.string.MY_GAMES_DELETE_CANCEL_BUTTON, new OnOkButton() {
					@Override
					public void onOk() {
						try {
							IGame apa = (IGame) lv.getAdapter().getItem(arg2);
							JSONObject o = JSONRequestHelpers.getCloseGameJSON(Storage.getUserProfile(getActivity()), apa.getId());
							Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
								@Override
								public void okResult(JSONObject response) {
									if (getActivity() != null) {
										Storage.updateUserProfile(getActivity(), response, true);
										((LoggedInMenu)getActivity()).updateUI();
									}
								}
								@Override
								public void badResult(String errorMessage) {
									
								}
							});
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
				return true;
			}
		});
		
		
		Helpers.addArrows(lv, v.findViewById(R.id.myGamesArrowUp), v.findViewById(R.id.myGamesArrowDown));
		
		/**
		 * Switch between previous and current games on button press
		 */
		v.findViewById(R.id.mygamesPreviousButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v2) {
				previousGames = !previousGames;
				TextView emptyTextView = (TextView)v.findViewById(android.R.id.empty);
				
				if (previousGames) {
					((Button)v.findViewById(R.id.mygamesPreviousButton)).setText(R.string.CURRENT_GAMES);
					((TextView)v.findViewById(R.id.mygamesTitleTextView)).setText(R.string.MY_PREVIOUS_GAMES);
					
					if (emptyTextView != null)
						emptyTextView.setText(R.string.MY_GAMES_EMPTY_PREVIOUS);
				}
				else {
					((Button)v.findViewById(R.id.mygamesPreviousButton)).setText(R.string.PREVIOUS_GAMES);
					((TextView)v.findViewById(R.id.mygamesTitleTextView)).setText(R.string.MY_CURRENT_GAMES);
					if (emptyTextView != null)
						emptyTextView.setText(R.string.MY_GAMES_EMPTY_CURRENT);
				}
				
				updateListView();
			}
		});
		
		Button newGame = (Button) v.findViewById(R.id.mygamesNewGame);
		// Only aplicable for tablet version
		if (newGame != null) {
			newGame.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					ft.replace(container.getId(), PackSelectFragment.newInstance(false));
					ft.addToBackStack(null);
					ft.commit();
				}
			});
		}
		// Helpers.addSwirl(v.findViewById(R.id.myGamesSwirl));
		
		Helpers.setAllFonts(v, getActivity());
		
		return v;
	}
	
	/**
	 * Called from the activity on userprofile updates
	 */
	@Override
	public void onUpdate() {
		updateListView();
	}
	
	private void updateListView() {
		int visiblePos = -1;
		
		if (ga == null)
			ga = new GamesAdapter(getActivity(), 0);
		else {
			visiblePos = lv.getFirstVisiblePosition();
			ga.clear();
		}
		
		
		try {
			JSONObject userProfile = Storage.getUserProfile(getActivity());
			JSONArray gamesArray = userProfile.getJSONObject("UserProfile").getJSONArray("Games");
			
			for (int i = 0; i < gamesArray.length(); i++) {
				IGame g = new OnlineGame(gamesArray.getJSONObject(i));
				if (g.isFinished() == previousGames)
					ga.add(g);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		lv.setAdapter(ga);
		ga.notifyDataSetChanged();
		
		if (visiblePos >= 0 && visiblePos < lv.getCount())
			lv.setSelection(visiblePos);
	}

	private UiLifecycleHelper uiHelper;
    private boolean isTablet;
	
	public enum MyGameCellTypes {
		CURRENT, PREVIOUS, EARN
	}
	
	public class GamesAdapter extends ArrayAdapter<IGame> {
		private Set<Integer> earnCells = new HashSet<Integer>();
		
		public GamesAdapter(Context context, int resource) {
			super(context, resource);
		}
		
		@Override
		public int getViewTypeCount() {
			return MyGameCellTypes.values().length;
		}
		
		@Override
		public int getItemViewType(int position) {
			if (previousGames) {
				if (earnCells.contains(position))
					return MyGameCellTypes.EARN.ordinal();
				else
					return MyGameCellTypes.PREVIOUS.ordinal();
			}
			return MyGameCellTypes.CURRENT.ordinal();
		}
		
		public View getEarnView(final int position, View view, ViewGroup parent) {
			if (view == null || (Integer) view.getTag() != getItemViewType(position)) {
				LayoutInflater inflater =
	                    (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.mygamesearncell, null);
				Helpers.setAllFonts(view, getContext());
			}
			
			Button facebookShare = (Button) view.findViewById(R.id.shareScoreFacebookButton);
			Button twitterShare = (Button) view.findViewById(R.id.shareScoreTwitterButton);
			
			Helpers.changeStringToSpannable(getActivity(), facebookShare);
			Helpers.changeStringToSpannable(getActivity(), twitterShare);
			
			Helpers.addPulse(view.findViewById(R.id.mygamesCellBackSwirl), 0); // adds small pulsing animation
			
			/**
			 * Setup the left side of the layout (shared between earn and normal cells
			 */
			setupLeftLayout(view, position);
			
			
			/**
			 * Back button
			 */
			View mygamesCellBackButton = view.findViewById(R.id.mygamesCellBackButton);
			mygamesCellBackButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					earnCells.remove(position);
					notifyDataSetChanged();
				}
			});
			
			/**
			 * FB button
			 */
			if (facebookShare != null) {
				facebookShare.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						String facebookTitle;
						String facebookDesc;
						String facebookCaption;
						
						if (getItem(position).getNumberOfPlayers() >= 2) {
							if (getItem(position).didWin()) {
								facebookTitle = getString(R.string.FACEBOOK_SHARE_TITLE_MULTI_PLAYER_I_WON);
								facebookDesc = getString(R.string.FACEBOOK_SHARE_DESCRIPTION_MULTI_PLAYER_I_WON);
								facebookCaption = getString(R.string.FACEBOOK_SHARE_CAPTION_MULTI_PLAYER_I_WON);
							}
							else {
								facebookTitle = getString(R.string.FACEBOOK_SHARE_TITLE_MULTI_PLAYER_I_LOST);
								facebookDesc = getString(R.string.FACEBOOK_SHARE_DESCRIPTION_MULTI_PLAYER_I_LOST);
								facebookCaption = getString(R.string.FACEBOOK_SHARE_CAPTION_MULTI_PLAYER_I_LOST);
							}
						}
						else {
							facebookTitle = getString(R.string.FACEBOOK_SHARE_TITLE_SINGLE_PLAYER);
							facebookDesc = getString(R.string.FACEBOOK_SHARE_DESCRIPTION_SINGLE_PLAYER);
							facebookCaption = getString(R.string.FACEBOOK_SHARE_CAPTION_SINGLE_PLAYER);
						}
						
						
						facebookTitle = replaceString(facebookTitle, getItem(position));
						facebookDesc = replaceString(facebookDesc, getItem(position));
						facebookCaption = replaceString(facebookCaption, getItem(position));
						
						
						
						String link = getString(R.string.FACEBOOK_SHARE_URL);
						
						String picture = Helpers.getBigSquarePackArtURL(getItem(position).getPackId());
						
						if (FacebookDialog.canPresentShareDialog(getActivity(), FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
							// Publish the post using the Share Dialog
							FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(getActivity())
							.setLink(link)
							.setName(facebookTitle)
							.setDescription(facebookDesc)
							.setCaption(facebookCaption)
							.setPicture(picture)
							.setFragment(MyGamesFragment.this)
							.build();
							
							uiHelper.trackPendingDialogCall(shareDialog.present());
						}
						else {
							facebookShareWithoutApp(true, facebookTitle, facebookDesc, facebookCaption, link, picture);
						}
					}
				});
			}
			/**
			 * Twitter button
			 */
			//TODO: Readd twitter button when twitter is readded to server
			if (twitterShare != null) {	
				twitterShare.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						String s;
						
						if (getItem(position).getNumberOfPlayers() >= 2) {
							if (getItem(position).didWin()) {
								s = getString(R.string.TWEET_MESSAGE_MULTI_PLAYER_I_WON);
							}
							else {
								s = getString(R.string.TWEET_MESSAGE_MULTI_PLAYER_I_LOST);
							}
						}
						else {
							s = getString(R.string.TWEET_MESSAGE_SINGLE_PLAYER);
						}
						
						tweetScore(true, replaceString(s, getItem(position)));
					}
				});
			}
			view.setTag(getItemViewType(position));
			
			return view;
		}
		
		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			if (getItemViewType(position) == MyGameCellTypes.EARN.ordinal())
				return getEarnView(position, view, parent);

			if (view == null || (Integer) view.getTag() != getItemViewType(position)) {
				LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.mygamescell, null);
				Helpers.setAllFonts(view, getContext());
			}
			
			boolean isSinglePlayer = getItem(position).getOpponents().size() == 0;
			boolean isStarted = getItem(position).isStarted();
			boolean isPlayerFinished = getItem(position).getPlayer().isFinished();
			
			String playerName = getItem(position).getPlayer().getName();
			int playerScore = getItem(position).getPlayer().getScore();
			String opponentName = isSinglePlayer ? "" : getItem(position).getOpponents().get(0).getName();
			
			
			/**
			 * Setup the left side of the layout (shared between earn and normal cells
			 */
			setupLeftLayout(view, position);
			
			
			/**
			 * Middle texts, 
			 * Name & New Game if newgame
			 * vs if started
			 */
			TextView mygamesCellMiddleNameTextView = (TextView) view.findViewById(R.id.mygamesCellMiddleNameTextView);
			TextView mygamesCellMiddleNewGameTextView = (TextView) view.findViewById(R.id.mygamesCellMiddleNewGameTextView);
			TextView mygamesCellVSTextView = (TextView) view.findViewById(R.id.mygamesCellVSTextView);

			setVisible(mygamesCellMiddleNameTextView, mygamesCellMiddleNewGameTextView, mygamesCellVSTextView);
			
			mygamesCellMiddleNameTextView.setTextColor(getResources().getColor(R.color.white));
			setText(mygamesCellMiddleNameTextView, opponentName);

			if (isStarted) {
				if (mygamesCellMiddleNameTextView != null) {
                    mygamesCellMiddleNameTextView.setVisibility(View.INVISIBLE);
                    mygamesCellMiddleNameTextView.setText("");
                }
				
				if (mygamesCellMiddleNewGameTextView != null) {
                    mygamesCellMiddleNewGameTextView.setVisibility(View.INVISIBLE);
                    mygamesCellMiddleNameTextView.setText("");
                }
			}
			else if (! isSinglePlayer) {
				mygamesCellMiddleNewGameTextView.setVisibility(View.INVISIBLE);
			}
			if (isSinglePlayer) {
				if (mygamesCellVSTextView != null)
					mygamesCellVSTextView.setVisibility(View.INVISIBLE);
			}
			if (!isSinglePlayer && !isStarted) {
				mygamesCellMiddleNewGameTextView.setVisibility(View.VISIBLE);
				mygamesCellVSTextView.setVisibility(View.INVISIBLE);
			}
			
			/**
			 * You won / you lost text
			 */
			if (getItem(position).isFinished() && !isSinglePlayer) {
				if (getItem(position).getPlayer().didWin()) {
					mygamesCellMiddleNameTextView.setTextColor(getResources().getColor(R.color.greenneon));
					mygamesCellMiddleNameTextView.setText(R.string.WON);
					mygamesCellMiddleNameTextView.setVisibility(View.VISIBLE);
				}
				else if (getItem(position).getOpponents().get(0).didWin()) {
					mygamesCellMiddleNameTextView.setTextColor(getResources().getColor(R.color.redneon));
					mygamesCellMiddleNameTextView.setText(R.string.LOST);
					mygamesCellMiddleNameTextView.setVisibility(View.VISIBLE);
				}
			}
			
			/**
			 * Right name & score
			 */
			TextView mygamesCellRightNameTextView = (TextView) view.findViewById(R.id.mygamesCellRightNameTextView);
			TextView mygamesCellRightScoreTextView = (TextView) view.findViewById(R.id.mygamesCellRightScoreTextView);
			ImageView mygamesCellRightScoreImageView = (ImageView) view.findViewById(R.id.mygamesCellRightScoreImageView);
			
			setText(mygamesCellRightNameTextView, playerName);
			setText(mygamesCellRightScoreTextView, "" + playerScore);
			
			if (isSinglePlayer || !isStarted) {
				mygamesCellRightNameTextView.setVisibility(View.INVISIBLE);
				mygamesCellRightScoreTextView.setVisibility(View.INVISIBLE);
				mygamesCellRightScoreImageView.setVisibility(View.INVISIBLE);
			}
			else
				setVisible(mygamesCellRightNameTextView, mygamesCellRightScoreTextView, mygamesCellRightScoreImageView);
				
			/**
			 * Button
			 */
			Button mygamesCellButton = (Button) view.findViewById(R.id.mygamesCellButton);
			setVisible(mygamesCellButton);
			if (mygamesCellButton != null) {
				if (previousGames) {
					setText(mygamesCellButton, getString(R.string.MY_GAMES_BUTTON_EARN));
					Helpers.changeStringToSpannable(getActivity(), mygamesCellButton);
					mygamesCellButton.setBackgroundResource(R.drawable.games_earn_button);
					mygamesCellButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							earnCells.add(position);
							notifyDataSetChanged();
						}
					});
				}
				else if (isPlayerFinished && !isSinglePlayer) {
					setText(mygamesCellButton, getString(R.string.MY_GAMES_BUTTON_NUDGE));
					mygamesCellButton.setBackgroundResource(R.drawable.games_nudge_button);
				}
				else if (isStarted) {
					setText(mygamesCellButton, getString(R.string.MY_GAMES_BUTTON_PLAY));
					mygamesCellButton.setBackgroundResource(R.drawable.games_play_button);
				}
				else {
					setText(mygamesCellButton, getString(R.string.MY_GAMES_BUTTON_START));
					mygamesCellButton.setBackgroundResource(R.drawable.games_start_button);
				}
			}
			
			view.setTag(getItemViewType(position));
			return view;
		}

		
		private void setVisible(View... view) {
			if (view != null && view.length > 0) {
				for (int i = 0; i < view.length; i++)
					if (view[i] != null)
						view[i].setVisibility(View.VISIBLE);
			}
		}

		private void setText(TextView view, String text) {
			if (view != null && text != null)
				view.setText(text);
		}


        /**
         * Sets up the left side of a cell (pack image etc)
         */
		public void setupLeftLayout(View view, int position) {
			boolean isSinglePlayer = getItem(position).getOpponents().size() == 0;
			boolean isStarted = getItem(position).isStarted();
			
			String playerName = getItem(position).getPlayer().getName();
			int playerScore = getItem(position).getPlayer().getScore();
			String opponentName = isSinglePlayer ? "" : getItem(position).getOpponents().get(0).getName();
			int opponentScore = isSinglePlayer ? 0 : getItem(position).getOpponents().get(0).getScore();	
			
			int chartPosition;
			if (isSinglePlayer)
				chartPosition = getItem(position).getPlayer().getChartPosition();
			else
				chartPosition = getItem(position).getOpponents().get(0).getChartPosition();
			
			/**
			 * Pack image
			 */
			ImageView packImageView = (ImageView) view.findViewById(R.id.mygamesCellPackImageView);
			if (packImageView != null) {
				packImageView.setImageResource(R.drawable.bti_default_pack);
				
				
				if (isTablet)
					Helpers.displayPackImage(getContext(), getItem(position).getPackId(), packImageView);
				else
					Helpers.displayImage(getContext(), Helpers.getSmallSquarePackArtURL(getItem(position).getPackId()), packImageView);
					
				
			}
			
			/**
			 * Level image
			 */
			ImageView mygamesCellLevelImageView = (ImageView) view.findViewById(R.id.mygamesCellLevelImageView);
			if (mygamesCellLevelImageView != null) {
				int level = getItem(position).getLevelNumber(getActivity());
				if (level == 0)
					mygamesCellLevelImageView.setImageResource(R.drawable.level1);
				if (level == 1)
					mygamesCellLevelImageView.setImageResource(R.drawable.level2);
				if (level == 2)
					mygamesCellLevelImageView.setImageResource(R.drawable.level3);
			}
			
			/**
			 * Online indicator
			 */
			ImageView mygamesCellOnlineIndicatorImageView = (ImageView) view.findViewById(R.id.mygamesCellOnlineIndicatorImageView);
			//noinspection StatementWithEmptyBody
			if (mygamesCellOnlineIndicatorImageView != null) {
				
			}
			
			/**
			 * Profile image
			 */
			ImageView profileImageView = (ImageView) view.findViewById(R.id.mygamesCellProfileImageView);
			if (profileImageView != null) {
				profileImageView.setImageResource(R.drawable.profile_unknown);
				
				String profileImageUrl = getItem(position).getProfileURL();
				
				if (profileImageUrl != null)
					Helpers.displayImage(getContext(), profileImageUrl, profileImageView);
			}
			
			/**
			 * Chart pos indicator (gold dics etc)
			 */
			int imageResource = Helpers.getImageForChartPos(chartPosition);
			ImageView mygamesCellProfileChartImageView = (ImageView) view.findViewById(R.id.mygamesCellProfileChartImageView);
			if (mygamesCellProfileChartImageView != null) {
				mygamesCellProfileChartImageView.setImageResource(imageResource);
			}
			
			
			/**
			 * Firstname and score
			 */
			TextView mygamesCellLeftNameTextView = (TextView) view.findViewById(R.id.mygamesCellLeftNameTextView);
			TextView mygamesCellLeftScoreTextView = (TextView) view.findViewById(R.id.mygamesCellLeftScoreTextView);
			ImageView mygamesCellLeftScoreImageView = (ImageView) view.findViewById(R.id.mygamesCellLeftScoreImageView);

			setVisible(mygamesCellLeftNameTextView, mygamesCellLeftScoreTextView, mygamesCellLeftScoreImageView);
			if (isStarted) {
				if (isSinglePlayer) {
					setText(mygamesCellLeftNameTextView, playerName);
					setText(mygamesCellLeftScoreTextView, "" + playerScore);
				}
				else {
					setText(mygamesCellLeftNameTextView, opponentName);
					setText(mygamesCellLeftScoreTextView, "" + opponentScore);
				}
			}
			else {
				if (mygamesCellLeftNameTextView != null) 
					mygamesCellLeftNameTextView.setVisibility(View.INVISIBLE);
				if (mygamesCellLeftScoreTextView != null) 
					mygamesCellLeftScoreTextView.setVisibility(View.INVISIBLE);
				if (mygamesCellLeftScoreImageView != null)
					mygamesCellLeftScoreImageView.setVisibility(View.INVISIBLE);
			}
		}
		
		
		private String replaceString(String s, IGame game) {
			String packName = Helpers.getPackNameFromPackId(Storage.getUserProfile(getActivity()), game.getPackId());
			String levelName = "Level " + (game.getLevelNumber(getActivity()) + 1);
			String opponentName = "";
			
			if (game.getOpponents().size() > 0)
				opponentName = game.getOpponents().get(0).getName();
			
			int points = game.getPlayer().getScore();
			
			s = s.replace("[X]", "" + points);
			s = s.replace("[packName]", "" + packName);
			s = s.replace("[levelName]", "" + levelName);
			s = s.replace("[opponentName]", "" + opponentName);
			s = s.replace("[noOfOpponents]", "" + (game.getNumberOfPlayers() - 1));
			
			return s;
		}
		
	}
	
	@Override
	public void onTweetFail() { 
		DialogHelpers.showErrorDialog(getActivity(), R.string.TWITTER_ERROR_DUPLICATE_TWEET_TITLE, R.string.TWITTER_ERROR_DUPLICATE_TWEET_TEXT, R.string.OK);
	}

    /**
     * Callback that is called when a tweet has successfully been tweeted
     */
	@Override
	public void onTweeted() { 
		EarnDiscTypes earn = Storage.getEarnDiscTypes(getActivity());
		String id = earn.getNameToIdMap().get("TwitterShareScore");
		
		JSONObject jsonReq = JSONRequestHelpers.getFacebookShareJSON(getActivity(), id);
		Storage.addRequestToQueue(getActivity(), Storage.EARNDISC_URL, jsonReq, new OnNetworkResult() {
			@Override
			public void okResult(JSONObject response) {
				if (getActivity() != null) {
					Helpers.refreshProfile(getActivity());
				}
			}
			@Override
			public void badResult(String errorMessage) {
			}
		});
	}
}
