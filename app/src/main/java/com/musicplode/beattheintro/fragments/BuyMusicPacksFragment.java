package com.musicplode.beattheintro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.modelhelpers.Pack;
import com.musicplode.beattheintro.storage.Storage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BuyMusicPacksFragment extends BaseFragment {
	private int containerId;
	@Override
	public String getName() { return "BuyMusicPacksList"; }
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.buymusicpacks, container, false);
		
		containerId = container.getId();
		
		ListView lv = (ListView) v.findViewById(R.id.buyMusicPacksListView);
		PackAdapter pa = new PackAdapter(getActivity(), 0);
		
		try {
			JSONArray arr = Storage.getUserProfile(getActivity()).getJSONObject("UserProfile").getJSONArray("Packs");
			
			for (int i = 0; i < arr.length(); i++) {
                pa.add(new Pack(arr.getJSONObject(i)));
			}
			lv.setAdapter(pa);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		/**
		 * Removed Unlock text from submenu header, as now part of main header view
		TextView tv = (TextView) v.findViewById(R.id.unlockText);
		if (tv != null) {
			String s = tv.getText().toString();
			s = s.replace("[X]", "" + Helpers.getNumberOfUnlocks(Storage.getUserProfile(getActivity())));
			tv.setText(s);
		}
		 */

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
        Helpers.setAllFonts(v, getActivity());
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				PackAdapter pa = (PackAdapter) arg0.getAdapter();
				JSONObject packJSON = pa.getItem(arg2).getJSON();
				
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				Helpers.setFragmentAnimation(ft);
				ft.replace(container.getId(), BuySinglePackFragment.newInstance(packJSON));
				ft.addToBackStack(null);
				ft.commit();
			}
		});

		/**
		 * The info button is 'hidden' behind the menu bar left-hand icon.
		 * Put another way, simply press the icon to get some help
		 */
		View info = v.findViewById(R.id.mycoinsInfoSwirl);
		if (info != null) {
			info.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					DialogHelpers.showInfoDialog(getActivity(), R.string.UNLOCK_PACK_INFO_TITLE, R.string.UNLOCK_PACK_INFO_TEXT); 
				}
			});
		}
		
		// Helpers.addPulse(v.findViewById(R.id.mycoinsInfoSwirl),0); // No need to pulse an invisible button!
		
		return v;
	}
	
	public class PackAdapter extends ArrayAdapter<Pack> {
		public PackAdapter(Context context, int resource) {	super(context, resource); }
		
		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}
		
		@Override
		public boolean isEnabled(int position) {
			return !getItem(position).isPlayable();
		}
		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.unlockpackscell, null);
				Helpers.setAllFonts(view, getContext());
			}
			
			
			TextView tvPackTitle = (TextView) view.findViewById(R.id.buyPackTitle);
            TextView tvPackPrice = (TextView) view.findViewById(R.id.buyPackPrice);
			ImageView packImageView = (ImageView) view.findViewById(R.id.buyPackImageView);
			ImageView greenCheck = (ImageView) view.findViewById(R.id.greenCheckImageView);
			ImageView padlockView = (ImageView) view.findViewById(R.id.padlockImageView);

			tvPackPrice.setVisibility(View.VISIBLE);
			greenCheck.setVisibility(View.INVISIBLE);
			padlockView.setVisibility(View.VISIBLE);
			
			if (getItem(position).isPlayable()) {
				tvPackPrice.setVisibility(View.INVISIBLE);
				greenCheck.setVisibility(View.VISIBLE);
				padlockView.setVisibility(View.INVISIBLE);
			}
			
			tvPackPrice.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v2) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					ft.replace(containerId, BuySinglePackFragment.newInstance(getItem(position).getJSON()));
					ft.addToBackStack(null);
					ft.commit();
				}
			});
			
			packImageView.setImageResource(R.drawable.bti_default_pack);
			
			tvPackTitle.setText(getItem(position).getName());
			
			Helpers.displayPackImage(getContext(), getItem(position).getId(), packImageView);
			
			return view;
		}
	}
}
