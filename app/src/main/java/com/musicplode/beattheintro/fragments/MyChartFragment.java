package com.musicplode.beattheintro.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
// import android.widget.ProgressBar;
import android.widget.TextView;

import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.Sounds;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.modelhelpers.UserProfile;
import com.musicplode.beattheintro.storage.Storage;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains the randomized animations, the number of bars is different in the tablet and mobile version
 */
public class MyChartFragment extends BaseFragment {
	private View v;
	private List<List<View>> bars = new ArrayList<List<View>>();

	private int[] barImages = {R.drawable.mychart__test_bar1, R.drawable.mychart__test_bar2, R.drawable.mychart__test_bar3, R.drawable.mychart__test_bar4,
			R.drawable.mychart__test_bar5, R.drawable.mychart__test_bar6, R.drawable.mychart__test_bar7, R.drawable.mychart__test_bar8,
			R.drawable.mychart__test_bar9, R.drawable.mychart__test_bar10, R.drawable.mychart__test_bar11, R.drawable.mychart__test_bar12,
			R.drawable.mychart__test_bar13, R.drawable.mychart__test_bar14, R.drawable.mychart__test_bar15, R.drawable.mychart__test_bar16};
	private int maxBars = 26;
	private CountDownTimer cdt;
	private boolean isTablet = false;
	private int upTo;

	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.mychart, container, false);
		
		isTablet = getResources().getBoolean(R.bool.tablet);
		if (isTablet)
			maxBars = 12;
		
		LinearLayout ll = (LinearLayout) v.findViewById(R.id.mychartBarLinearLayout);
		
		LayoutParams lp = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 1);
		lp.setMargins(2, 2, 2, 2);
		
		for (int x = 0; x < barImages.length; x++) {
			bars.add(new ArrayList<View>());
			LinearLayout lv = new LinearLayout(getActivity());
			lv.setOrientation(LinearLayout.VERTICAL);
			lv.setWeightSum(maxBars);

			// getResources().getDrawable(R.drawable.mychart__test_bar1, null); // This is the newer call for API 21
			getResources().getDrawable(R.drawable.mychart__test_bar1);
			ll.addView(lv, new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.MATCH_PARENT, 1));
			
			for (int y = 0; y < maxBars; y++) {
				ImageView iv = new ImageView(getActivity());
				iv.setImageResource(barImages[x]);
				
				iv.setLayoutParams(lp);
				
				lv.addView(iv);
				bars.get(x).add(iv);
			}
		}
		
		Helpers.changeStringToSpannable(getActivity(), (TextView) v.findViewById(R.id.textView2));
		
		Helpers.setAllFonts(v, getActivity());
		
		
		TextView tv = (TextView) v.findViewById(R.id.textView2);
		UserProfile up = new UserProfile(Storage.getUserProfile(getActivity()));
		int discs = up.getNumberOfDiscs();
		String s = "#" + Helpers.getChartPositionFromDiscs(discs);
		String packLevelString = Helpers.getLevelStringFromDiscs(getActivity(), discs);
		
		if (packLevelString != null) {
			if (!isTablet)
				s += " /\n";
			else
				s += " / ";
			s += packLevelString;
			
		}
			
		tv.setText(s);
		
		Helpers.changeStringToSpannable(getActivity(), (TextView) v.findViewById(R.id.milestoneTextView1));
		Helpers.changeStringToSpannable(getActivity(), (TextView) v.findViewById(R.id.milestoneTextView2));
		Helpers.changeStringToSpannable(getActivity(), (TextView) v.findViewById(R.id.milestoneTextView3));
		Helpers.changeStringToSpannable(getActivity(), (TextView) v.findViewById(R.id.milestoneTextView4));

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
		//TODO this call draws a button (without a swirl) on a non-Amazon device, which it shouldn't
		
		return v;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		if (cdt != null) {
			cdt.cancel();
			cdt = null;
		}
	}
	@Override
	public void onResume() {
		super.onResume();
		UserProfile up = new UserProfile(Storage.getUserProfile(getActivity()));
		int discs = up.getNumberOfDiscs();
		startAnimation((int) ((barImages.length * Helpers.getPercentageCompletedFromDiscs(discs)) / 100f));

        if (isVisible()) {
            ((LoggedInMenu) getActivity()).enableLowerChart();
            ((LoggedInMenu) getActivity()).disableAds();
			((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.OPEN);
        }
	}
	
	public void startAnimation(final int upTo) {
		this.upTo = upTo;
		
		cdt = new CountDownTimer(300000, 100) {
			@Override
			public void onTick(long millisUntilFinished) {
				for (int i = 0; i < barImages.length; i++) {
					animateBar(i);
				}
			}
			
			@Override
			public void onFinish() {
				for (List<View> l : bars) {
					if (l == null)
						return;
					for (View iv : l) {
						if (iv == null)
							return;
						iv.setVisibility(View.INVISIBLE);
					}
				}
			}
		};
		
		cdt.start();
	}
	
	private List<BarAnimationState> barStates = new ArrayList<BarAnimationState>();

	public class BarAnimationState {
		int currentBar = 0;
		float visibilityLevel = 0f;
		
		public void step() {
			if (currentBar == 0) {
				currentBar = 1;
				visibilityLevel = 0.5f;
			}
			else if (currentBar == maxBars - 1) {
				currentBar = maxBars - 2;
				visibilityLevel = 0.5f;
			}
			else {
				if (visibilityLevel == 0.5f) {
					if (Storage.random.nextBoolean())
						visibilityLevel = 1;
					else
						visibilityLevel = 0;
				}
				else {
					if (Storage.random.nextBoolean())
						currentBar = currentBar+1;
					else
						currentBar = currentBar-1;
					visibilityLevel = 0.5f;
				}
			}
		}

		public float getAlpha(int i) {
			if (currentBar == i)
				return visibilityLevel;
			else if (i > currentBar)
				return 0;
			return 1;
		}
	}
	
	public void applyStateToBar(int bar) {
		BarAnimationState state = barStates.get(bar);
		List<View> b = bars.get(bar);
		
		if (bar == 0) {
			for (int i = 0; i < b.size(); i++ ) {
				b.get(b.size() - i - 1).setAlpha(state.getAlpha(i));
			}
		}
		else {
			for (int i = 0; i < b.size(); i++ ) {
				if (bar >= upTo) {
					b.get(b.size() - i - 1).setAlpha(Math.min(0.2f, state.getAlpha(i)));
				}
				else
					b.get(b.size() - i - 1).setAlpha(state.getAlpha(i));
			}
		}
	}
	
	public void animateBar(int barNo) {
		if (barStates.size() == 0) {
			for (int i = 0; i < barImages.length; i++) {
				barStates.add(new BarAnimationState());
			}
		}

		barStates.get(barNo).step();
		applyStateToBar(barNo);
	}
	
	@Override
	public String getName() {
		return "MyChart";
	}
}
