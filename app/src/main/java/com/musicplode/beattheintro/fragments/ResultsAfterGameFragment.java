package com.musicplode.beattheintro.fragments;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.LoggedInMenu.OnSongCompletion;
import com.musicplode.beattheintro.LoggedInMenu.OnSongReadyListener;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.helpers.TrackerHelpers;
import com.musicplode.beattheintro.modelhelpers.IGame;
import com.musicplode.beattheintro.modelhelpers.OnlineGame;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ResultsAfterGameFragment extends BaseFragment {
	@Override
	public String getName() {
		return "ResultsAfterGame";
	}
	
	private AdapterView<ResultsAfterGameAdapter> lv;
	private IGame game;
	private View v;
	private JSONObject trackToArtist;
	private boolean playerResults = true;
	private WhoWonFragment whoWonContainerFragment;
	public void updateUI() {
		if (getActivity() != null)
			((LoggedInMenu)getActivity()).updateUI();
	}

    @Override
    public void onStop() {
        super.onStop();

        if (getActivity() != null && getActivity() instanceof LoggedInMenu) {
            ((LoggedInMenu)getActivity()).stopMediaPlayers();
        }
    }

	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.resultsaftergame, container, false);

		lv = (AdapterView<ResultsAfterGameAdapter>) v.findViewById(R.id.adapterView1);
		
		boolean singlePlayer = game.getNumberOfPlayers() == 1;
		
		Helpers.setAllFonts(v, getActivity());
		
		for (Fragment f : ((LoggedInMenu)getActivity()).getVisibleFragments()) {
			if (f instanceof WhoWonFragment) {
				whoWonContainerFragment = (WhoWonFragment) f;
				
				whoWonContainerFragment.setResultButton(this);
				
				if (singlePlayer)
					whoWonContainerFragment.hideResultButton();
			}
		}
		
		setUser(playerResults);
		
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		((LoggedInMenu)getActivity()).disableLowerChart();
		((LoggedInMenu)getActivity()).enableAds();
	}
	
	public void onResultButtonSwitch() {
		playerResults = !playerResults;
		setUser(playerResults);
	}
	
	public void setResultButtonText(int textResource) {
		whoWonContainerFragment.setResultText(textResource);
	}
	
	private void setUser(boolean isPlayer) {
		if (isPlayer)
			setResultButtonText(R.string.OPONENTS_SCORE);
		else
			setResultButtonText(R.string.YOUR_SCORE);
		
		int correctTracks = 0;
		int outOf = 0;
		ResultsAfterGameAdapter raga = lv.getAdapter();
		
		if (lv.getAdapter() == null) {
			raga = new ResultsAfterGameAdapter(getActivity(), 0);
			lv.setAdapter(raga);
		}

		raga.clear();
		
		try {
			JSONArray questionArray;
			if (isPlayer)
				questionArray = game.getJSON().getJSONArray("Players").getJSONObject(0).getJSONArray("Questions");
			else
				questionArray = game.getJSON().getJSONArray("Players").getJSONObject(1).getJSONArray("Questions");
			
			if (questionArray != null) {
				for (int i = 0; i < questionArray.length(); i++) {
					outOf++;
					JSONObject o = questionArray.getJSONObject(i);
					JSONObject trackInfo = trackToArtist.getJSONArray("Tracks").getJSONObject(i);
					
					raga.add(new ResultEntry(o, trackInfo));
					
					if (raga.correctGuess(o))
						correctTracks++;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		raga.notifyDataSetChanged();

		if (isPlayer)
			((TextView) v.findViewById(R.id.resultsAfterGameYouGotTextView)).setText(getString(R.string.YOU_GOT) + " " + correctTracks + "/" + outOf);
		else
			((TextView) v.findViewById(R.id.resultsAfterGameYouGotTextView)).setText(getString(R.string.OPPONENT_GOT) + " " + correctTracks + "/" + outOf);
		
		
	}
	
	public class ResultsAfterGameAdapter extends ArrayAdapter<ResultEntry> {
		private String currentlyPlayingTrack;
		private boolean playing;
		private String lastTrackId;

		public ResultsAfterGameAdapter(Context context, int resource) {
			super(context, resource);
		}
		
		private boolean correctGuess(JSONObject o) {
			try {
				String correctAnswer = o.getString("TrackCorrect");
				String guessedAnswer = o.getString("AnswerTrackId");
				return correctAnswer.contentEquals(guessedAnswer);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return false;
		}
		private boolean incorrectGuess(JSONObject o) {
			try {
				String wrong1 = o.getString("TrackWrong1");
				String wrong2 = o.getString("TrackWrong2");
				String guessedAnswer = o.getString("AnswerTrackId");
				return wrong1.contentEquals(guessedAnswer) || wrong2.contentEquals(guessedAnswer);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return false;
		}

		public void setCurrentTrack(String currentTrack, boolean playing) {
			currentlyPlayingTrack = currentTrack;
			this.playing = playing;
		
			notifyDataSetChanged();
		}
		
		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater =
	                    (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.resultaftergamecell, null);
				Helpers.setAllFonts(view, getContext());
			}

			TextView trackNumberView = (TextView) view.findViewById(R.id.resultAfterGameCellTrackNumberTextView);
			TextView artist = (TextView) view.findViewById(R.id.resultAfterGameCellArtistTextView);
			TextView track = (TextView) view.findViewById(R.id.resultAfterGameCellTrackTextView);
			ImageView iv = (ImageView) view.findViewById(R.id.resultAfterGameCellImageView);
			ImageView indicator = (ImageView) view.findViewById(R.id.resultAfterGameCellIndicatorImageView);
			ImageView basketButton  = (ImageView) view.findViewById(R.id.resultAfterGameCellBasketButton);
			final ImageView basketButtonSwirl  = (ImageView) view.findViewById(R.id.resultAfterGameCellBasketButtonSwirl);
			ImageView listen = (ImageView) view.findViewById(R.id.resultAfterGameCellListenButton);
			ImageView listenSwirl = (ImageView) view.findViewById(R.id.resultAfterGameCellListenButtonSwirl);
			
			Helpers.addPulse(basketButtonSwirl,0);
			Helpers.addPulse(listenSwirl, 0);
			
			if (playing) {
				String mp3Path = null;
				try {
					mp3Path = Helpers.getIntroMp3URL(getItem(position).getJSON().getString("TrackCorrect"));
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (mp3Path != null && mp3Path.equals(currentlyPlayingTrack))
					listen.setImageResource(R.drawable.gameplay_pause);
				else
					listen.setImageResource(R.drawable.basket_listen);
			}
			else
				listen.setImageResource(R.drawable.basket_listen);
			
			
			
			basketButton.setVisibility(View.VISIBLE);
			indicator.setImageResource(0);
			iv.setImageResource(0);
			
			try {
				if (Helpers.doesLikeTrackId(Storage.getUserProfile(getContext()), getItem(position).getJSON().getString("TrackCorrect"))) {
					view.findViewById(R.id.resultAfterGameCellBasketButton).setVisibility(View.INVISIBLE);
					
					view.findViewById(R.id.resultAfterGameCellBasketButtonSwirl).clearAnimation();
					view.findViewById(R.id.resultAfterGameCellBasketButtonSwirl).setVisibility(View.INVISIBLE);
				}
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			
			
			basketButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					v.setVisibility(View.INVISIBLE);
					basketButtonSwirl.clearAnimation();
					basketButtonSwirl.setVisibility(View.INVISIBLE);
					
					try {
						TrackerHelpers.addTrackToBasket(getActivity(), getItem(position).getJSON().getString("TrackCorrect"));
						
						JSONObject o = JSONRequestHelpers.getLikeTrackJSON(getContext(), Storage.getUserProfile(getContext()), getItem(position).getJSON().getString("TrackCorrect"));
						
						Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
							@Override
							public void okResult(JSONObject response) {
								Storage.updateUserProfile(getContext(), response, true);
							}
							
							@Override
							public void badResult(String errorMessage) {
							}
						});
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
				}
			});
			
			listen.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					playSong(position);
				}
			});
			
			try {
				String artistName = getItem(position).getTrackInfo().getString("Artist");
				String trackName = getItem(position).getTrackInfo().getString("Title");
				
				String albumCoverURL = getItem(position).getTrackInfo().getString("AlbumCoverURL");
				
				Helpers.displayImage(getContext(), albumCoverURL, iv);
				
				artist.setText(artistName);
				track.setText(trackName);
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			if (correctGuess(getItem(position).getJSON())) {
				indicator.setVisibility(View.VISIBLE);
				indicator.setImageResource(R.drawable.gameplay_correct);
			}
			else if (incorrectGuess(getItem(position).getJSON())) {
				indicator.setVisibility(View.VISIBLE);
				indicator.setImageResource(R.drawable.gameplay_incorrect);
			}
			else
				indicator.setVisibility(View.INVISIBLE);
			
			trackNumberView.setText("" + (position+1) + ".");
			
			return view;
		}

		/**
		 * Start playback of song at position. If song at position is currently
		 * playing it will be paused
		 * 
		 * @param position
		 */
		public void playSong(int position) {
			if (getActivity() == null)
				return;
			String trackId = "";
			
			if (getItem(position) != null && getItem(position).getJSON() != null) {
				try {
					trackId = getItem(position).getJSON().getString("TrackCorrect");
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (playing) {
					((LoggedInMenu)getActivity()).stopMediaPlayers();
					playing = false;
					notifyDataSetChanged();
					
					if (lastTrackId != null && trackId.equals(lastTrackId)) 
						return;
				}
			}

			lastTrackId = trackId;
			final String mp3Path = Helpers.getIntroMp3URL(trackId);
			
			LoggedInMenu mainActivity = (LoggedInMenu)getActivity();
			if (mainActivity != null) {
				mainActivity.stopPreparingSongs();
				mainActivity.prepareSong(mp3Path, new OnErrorListener() {
					@Override
					public boolean onError(MediaPlayer mp, int what, int extra) {
						
						return false;
					}
				});
				mainActivity.playIfReady(mp3Path, new OnSongReadyListener() {
					@Override
					public void onReady(MediaPlayer mediaplayer) {
						mediaplayer.start();
						setCurrentTrack(mp3Path, true);
						mediaplayer.setOnCompletionListener(new OnCompletionListener() {
							@Override
							public void onCompletion(MediaPlayer mp) {
								setCurrentTrack(mp3Path, false);
								notifyDataSetChanged();
							}
						});
					}
				}, new OnSongCompletion() {
					@Override
					public void onSongComplete(MediaPlayer mediaplayer, String songPath) {
						setCurrentTrack(mp3Path, false);
						notifyDataSetChanged();
					}
				}, 0);
			}
		}

		
	}
	
	public class ResultEntry {
		private JSONObject json;
		private JSONObject trackInfo;
		
		public ResultEntry(JSONObject json, JSONObject trackInfo) {
			this.json = json;
			this.trackInfo = trackInfo;
		}
		
		public JSONObject getJSON() {
			return json;
		}
		public JSONObject getTrackInfo() {
			return trackInfo;
		}
	}
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        try {
			game = new OnlineGame(new JSONObject(getArguments().getString("json")));
			trackToArtist = new JSONObject(getArguments().getString("trackToArtist"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
    }
	public static Fragment newInstance(String json, JSONObject trackToArtist, int containerId, String opponentName, String levelTypeId) {
        ResultsAfterGameFragment f = new ResultsAfterGameFragment();

        
        Bundle args = new Bundle();
        args.putString("json", json);
        args.putString("trackToArtist", trackToArtist.toString());
        args.putInt("containerId", containerId);
        args.putString("levelTypeId", levelTypeId);
        args.putString("opponentName", opponentName);
        
        f.setArguments(args);
		
		return f;
	}
}
