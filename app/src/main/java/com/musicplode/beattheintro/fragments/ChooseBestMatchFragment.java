package com.musicplode.beattheintro.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChooseBestMatchFragment extends BaseFragment {
	@Override
	public String getName() {
		return "ChooseBestMatchOpponent";
	}
	private View v;
	private int separatorStartY;
	private int separatorHeight;
	private String packId;
	private String levelId;
	private List<BestMatchPerson> bestMatches = new ArrayList<BestMatchPerson>();
	private BestMatchPerson currentBestMatch;
	
	private int containerId;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.choosebestmatch, container, false);
		
		//Tablet version of this fragment does not cover entire content area
		if (containerId == 0)
			containerId = container.getId();
		
		/**
		 * Fix tablet divider position between bestmatch and choose opponent menu
		 */
		if (v.findViewById(R.id.chooseBestMatchSeparator) != null) {
			ImageView iv = (ImageView) v.findViewById(R.id.chooseBestMatchSeparator);
			
			LayoutParams lp = (LayoutParams) iv.getLayoutParams();
			lp.topMargin = separatorStartY;
			lp.height = separatorHeight;
			iv.setLayoutParams(lp);
		}

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
		Helpers.setAllFonts(v, getActivity());
		
		// Try to load the users facebook image
		try {
			String fbID = Storage.getUserProfile(getActivity()).getJSONObject("UserProfile").getString("FacebookId");
			if (fbID.length() > 0)
				Helpers.displayImage(getActivity(), Helpers.getFacebookProfileImage(fbID), (ImageView) v.findViewById(R.id.chooseBestMatchPlayer1ImageView));
		} catch (JSONException e) {
			e.printStackTrace();
		}

        /**
         * Init the opponent array
         */
		searchForBestMatch();
		
		v.findViewById(R.id.chooseBestMatchSearchAgainButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (bestMatches.size() > 1) {
					bestMatches.remove(0);
					setCurrentBestMatch(bestMatches.get(0));
				}
				else
					searchForBestMatch();
			}
		});
		
		v.findViewById(R.id.chooseBestMatchAcceptButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (currentBestMatch != null) {
					Helpers.startGame((LoggedInMenu) getActivity(), containerId, currentBestMatch.getName(), packId, levelId);
				}
			}
		});
		
		// Helpers.addSwirl(v.findViewById(R.id.chooseOpponentSwirl));
		
		return v;
	}
	
	
	/**
	 * Searches for best matches and will call updateBestMatch when done
	 */
	private void searchForBestMatch() {
		int howMany = 10;
		
		JSONObject jsonReq = JSONRequestHelpers.getBestMatchJSON(Storage.getUserProfile(getActivity()), packId, levelId, howMany);
		
		Storage.addRequestToQueue(getActivity(), Storage.BESTMATCH_URL, jsonReq, new OnNetworkResult() {
			@Override
			public void okResult(JSONObject response) {
				try {
					updateBestMatch(response.getJSONArray("Players"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			
			@Override
			public void badResult(String errorMessage) {
			}
		});
	}

	/**
	 * Populate the internal list with the best matches
	 * @param arr
	 */
	protected void updateBestMatch(JSONArray arr) {
		bestMatches.clear();
		for (int i = 0; i < arr.length(); i++) {
			BestMatchPerson bmp = new BestMatchPerson();
			JSONObject o;
			try {
				o = arr.getJSONObject(i);String name = o.optString("Name", null);
				String facebookId = o.optString("FacebookId", null);
				String id = o.optString("Id", null);
				String email = o.optString("Email", null);
				String profilePicture = o.optString("ProfilePicture", null);

				bmp.setName(name);
				bmp.setFacebookId(facebookId);
				bmp.setId(id);
				bmp.setEmail(email);
				bmp.setProfilePicture(profilePicture);
			
				bestMatches.add(bmp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		setCurrentBestMatch(bestMatches.get(0));
	}

    /**
     * Updates the UI with a given person as the current BesstMatch
     */
	private void setCurrentBestMatch(BestMatchPerson bestMatchPerson) {
		currentBestMatch = bestMatchPerson;
		((TextView)v.findViewById(R.id.multiplayerBestMatchPlayer2)).setText(bestMatchPerson.getName());
		v.findViewById(R.id.chooseBestMatchPlayer2ProgressBar).setVisibility(View.INVISIBLE);
		
		ImageView image = (ImageView) v.findViewById(R.id.chooseBestMatchPlayer2ImageView);
		image.setImageResource(R.drawable.profile_unknown);
		image.setVisibility(View.VISIBLE);
		
		String profileImageURL = bestMatchPerson.getProfilePicture();
		if (profileImageURL != null && profileImageURL.length() > 0) {
			Helpers.displayImage(getActivity(), profileImageURL, image);
		}
	}
	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
		if (getArguments() != null) {
			separatorStartY = getArguments().getInt("separatorStartY");
			separatorHeight = getArguments().getInt("separatorHeight");
			packId = getArguments().getString("packId");
			levelId = getArguments().getString("levelId");
			containerId = getArguments().getInt("containerId");
		}
	}
	
	public static Fragment newInstance(String packId, String levelId, int separatorStartY, int separatorHeight, int containerId) {
		ChooseBestMatchFragment f = new ChooseBestMatchFragment();
		Bundle b = new Bundle();
		b.putInt("separatorStartY", separatorStartY);
		b.putInt("separatorHeight", separatorHeight);
		b.putInt("containerId", containerId);
		b.putString("packId", packId);
		b.putString("levelId", levelId);
		f.setArguments(b);
		
		return f;
	}
	public static Fragment newInstance(String packId, String levelId) {
		return newInstance(packId, levelId, 0, 0, 0);
	}

	/**
	 * Container class for a Best-match person
	 */
	public class BestMatchPerson {
		private String name;
		private String facebookId;
		private String id;
		private String email;
		private String profilePicture;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getFacebookId() {
			return facebookId;
		}
		public void setFacebookId(String facebookId) {
			this.facebookId = facebookId;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getProfilePicture() {
			if (profilePicture == null && getFacebookId() != null && getFacebookId().length() > 0) 
				return Helpers.getFacebookProfileImage(getFacebookId());
			
			return profilePicture;
		}
		public void setProfilePicture(String profilePicture) {
			if (profilePicture != null && profilePicture.length() > 0)
				this.profilePicture = profilePicture;
		}
	}
	
}
