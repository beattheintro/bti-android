package com.musicplode.beattheintro.fragments;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.PartyPlayActivity;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.Sounds;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.model.QuestionModel;
import com.musicplode.beattheintro.modelhelpers.OnlinePartyPlayGame;
import com.musicplode.beattheintro.modelhelpers.TrackModel;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.support.PieProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 20/06/2014.
 */
@SuppressWarnings("DefaultFileTemplate")
public class PartyPlayGameFragment extends BaseFragment {
    private View v = null;


    private String[] playerNames;
    private int numberOfPlayers = 2;
    private PieProgressBar pb;

    private View playerAnswerBarsLayout[][];
    private ImageView playerAnswerBarsAlbumArt[][];
    private TextView playerAnswerBarsArtistTextView[][];
    private TextView playerAnswerBarsTrackTextView[][];

    private int numberOfQuestions;

    // Denotes the current question number (0...numberOfQuestions-1)
    private int question;

    private List<View> currentAnimations = new ArrayList<View>();
    private TextView playerScoreViews[];


    /**
     * Contains the time to answer for each player, answerArray[player][question]
     * if the content is -1 it denotes that the player answered incorrectly
     */
    private int answerTimeArray[][];
    private boolean answeredCorrectArray[][];
    private int answeredBarArray[][];

    private boolean isNextButtonPressed = false;
    private boolean isMusicPrepared = false;

    private int containerId;
    private OnlinePartyPlayGame game;

    private String correctTrack = null;
    private String correctArtist = null;
    private QuestionModel[] questionModels;
    private MediaPlayer[] mediaPlayers;
    private double maxScore = 1500;
    private int maxTimeToAnswer = 15000;
    private SongBlaTimer songTimer;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        this.containerId = container.getId();

        numberOfPlayers = playerNames.length;

        if (numberOfPlayers >= 5) {
            v = inflater.inflate(R.layout.partyplay_six_players, container, false);

            if (numberOfPlayers == 5) {
                v.findViewById(R.id.partyPlayTopLeftCorner).setVisibility(View.INVISIBLE);
                v.findViewById(R.id.partyPlayBottomLeftCorner).setVisibility(View.INVISIBLE);
                v.findViewById(R.id.partyPlayPlayer6TopLayout).setVisibility(View.INVISIBLE);
                v.findViewById(R.id.partyPlayPlayer6StarLayout).setVisibility(View.INVISIBLE);
                v.findViewById(R.id.partyPlayPlayer6Layout).setVisibility(View.INVISIBLE);
                v.findViewById(R.id.partyPlayPlayer6Score).setVisibility(View.INVISIBLE);
                v.findViewById(R.id.partyPlayPlayer6Name).setVisibility(View.INVISIBLE);
            }
        }
        else if (numberOfPlayers >= 3) {
            v = inflater.inflate(R.layout.partyplay_four_players, container, false);

            if (numberOfPlayers == 3) {
                v.findViewById(R.id.partyPlayPlayer4Layout).setVisibility(View.INVISIBLE);
                v.findViewById(R.id.partyPlayPlayer4Name).setVisibility(View.INVISIBLE);
                v.findViewById(R.id.partyPlayPlayer4Score).setVisibility(View.INVISIBLE);
                v.findViewById(R.id.partyPlayPlayer4StarLayout).setVisibility(View.INVISIBLE);
                v.findViewById(R.id.partyPlayBarTopLeftCorner).setVisibility(View.INVISIBLE);
            }
        }
        else
            v = inflater.inflate(R.layout.partyplay_two_players, container, false);

        setupNames(playerNames);

        setupAnswerButtons();

        setupPauseButtons();

        setupNextButtons();

        setupPlayerScoreViews();

        setupLevelNameAndImages();

        pb = (PieProgressBar) v.findViewById(R.id.partyPlayProgressBar);
        if (pb != null) {
            songTimer = new SongBlaTimer(maxTimeToAnswer/numberOfQuestions, 30, pb, (TextView) v.findViewById(R.id.partyPlayProgressBarTextView), this);
            songTimer.start();
        }

        Helpers.setAllFonts(v, getActivity());

        startPulsing();

        onNextPressed();

        ((PartyPlayActivity)getActivity()).setListenerOnLoaded(getQuestionModels()[question].getCorrectTrackModel().getStreamURL(), new PartyPlayActivity.OnSongReadyListener() {
            @Override
            public void onReady(MediaPlayer mediaPlayer) {
                System.out.println("mediaplayer reported ready");
                onMusicPrepared(mediaPlayer, 0);
            }
        });

        return v;
    }

    private void setupLevelNameAndImages() {
        TextView tv = (TextView) v.findViewById(R.id.partyPlayPackName1TextView);
        if (tv != null)
            tv.setText(getGameModel().getPack(getActivity()).getName());
        tv = (TextView) v.findViewById(R.id.partyPlayPackName2TextView);

        if (tv != null)
            tv.setText(getGameModel().getPack(getActivity()).getName());

        int imageResource;
        switch (getGameModel().getLevelNumber(getActivity())) {
            case 0:
                imageResource = R.drawable.level1;
                break;
            case 1:
                imageResource = R.drawable.level2;
                break;
            default:
                imageResource = R.drawable.level3;
        }

        ImageView iv = (ImageView) v.findViewById(R.id.partyPlayLevelImageView1);
        if (iv != null)
            iv.setImageResource(imageResource);

        iv = (ImageView) v.findViewById(R.id.partyPlayLevelImageView2);
        if (iv != null)
            iv.setImageResource(imageResource);
    }

    private void setupPlayerScoreViews() {
        playerScoreViews = new TextView[playerNames.length];

        playerScoreViews[0] = (TextView) v.findViewById(R.id.partyPlayPlayer1Score);
        playerScoreViews[1] = (TextView) v.findViewById(R.id.partyPlayPlayer2Score);

        if (numberOfPlayers >= 3)
            playerScoreViews[2] = (TextView) v.findViewById(R.id.partyPlayPlayer3Score);
        if (numberOfPlayers >= 4)
            playerScoreViews[3] = (TextView) v.findViewById(R.id.partyPlayPlayer4Score);
        if (numberOfPlayers >= 5)
            playerScoreViews[4] = (TextView) v.findViewById(R.id.partyPlayPlayer5Score);
        if (numberOfPlayers >= 6)
            playerScoreViews[5] = (TextView) v.findViewById(R.id.partyPlayPlayer6Score);
    }

    private void setupNextButtons() {
        Helpers.setClickListenerToViews(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNextPressed();
            }
        }, v.findViewById(R.id.partyPlayNextButton1), v.findViewById(R.id.partyPlayNextButton2));
    }

    private void setupPauseButtons() {
        Helpers.setClickListenerToViews(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPausePressed();
            }
        }, v.findViewById(R.id.partyPlayPauseButton1), v.findViewById(R.id.partyPlayPauseButton2));
    }

    private void setupAnswerButtons() {
        playerAnswerBarsLayout = new View[numberOfPlayers][3];
        playerAnswerBarsAlbumArt = new ImageView[numberOfPlayers][3];
        playerAnswerBarsArtistTextView = new TextView[numberOfPlayers][3];
        playerAnswerBarsTrackTextView = new TextView[numberOfPlayers][3];

        /**
         * Player 1
         */
        playerAnswerBarsLayout[0][0] = v.findViewById(R.id.partyPlayPlayer1QuestionBar1);
        playerAnswerBarsLayout[0][1] = v.findViewById(R.id.partyPlayPlayer1QuestionBar2);
        playerAnswerBarsLayout[0][2] = v.findViewById(R.id.partyPlayPlayer1QuestionBar3);

        playerAnswerBarsAlbumArt[0][0] = (ImageView) v.findViewById(R.id.partyPlayPlayer1QuestionBar1AlbumImageView);
        playerAnswerBarsAlbumArt[0][1] = (ImageView) v.findViewById(R.id.partyPlayPlayer1QuestionBar2AlbumImageView);
        playerAnswerBarsAlbumArt[0][2] = (ImageView) v.findViewById(R.id.partyPlayPlayer1QuestionBar3AlbumImageView);

        playerAnswerBarsArtistTextView[0][0] = (TextView) v.findViewById(R.id.partyPlayPlayer1QuestionBar1TrackArtist);
        playerAnswerBarsArtistTextView[0][1] = (TextView) v.findViewById(R.id.partyPlayPlayer1QuestionBar2TrackArtist);
        playerAnswerBarsArtistTextView[0][2] = (TextView) v.findViewById(R.id.partyPlayPlayer1QuestionBar3TrackArtist);

        playerAnswerBarsTrackTextView[0][0] = (TextView) v.findViewById(R.id.partyPlayPlayer1QuestionBar1TrackTitle);
        playerAnswerBarsTrackTextView[0][1] = (TextView) v.findViewById(R.id.partyPlayPlayer1QuestionBar2TrackTitle);
        playerAnswerBarsTrackTextView[0][2] = (TextView) v.findViewById(R.id.partyPlayPlayer1QuestionBar3TrackTitle);


        /**
         * Player 2
         */
        playerAnswerBarsLayout[1][0] = v.findViewById(R.id.partyPlayPlayer2QuestionBar1);
        playerAnswerBarsLayout[1][1] = v.findViewById(R.id.partyPlayPlayer2QuestionBar2);
        playerAnswerBarsLayout[1][2] = v.findViewById(R.id.partyPlayPlayer2QuestionBar3);

        playerAnswerBarsAlbumArt[1][0] = (ImageView) v.findViewById(R.id.partyPlayPlayer2QuestionBar1AlbumImageView);
        playerAnswerBarsAlbumArt[1][1] = (ImageView) v.findViewById(R.id.partyPlayPlayer2QuestionBar2AlbumImageView);
        playerAnswerBarsAlbumArt[1][2] = (ImageView) v.findViewById(R.id.partyPlayPlayer2QuestionBar3AlbumImageView);

        playerAnswerBarsArtistTextView[1][0] = (TextView) v.findViewById(R.id.partyPlayPlayer2QuestionBar1TrackArtist);
        playerAnswerBarsArtistTextView[1][1] = (TextView) v.findViewById(R.id.partyPlayPlayer2QuestionBar2TrackArtist);
        playerAnswerBarsArtistTextView[1][2] = (TextView) v.findViewById(R.id.partyPlayPlayer2QuestionBar3TrackArtist);

        playerAnswerBarsTrackTextView[1][0] = (TextView) v.findViewById(R.id.partyPlayPlayer2QuestionBar1TrackTitle);
        playerAnswerBarsTrackTextView[1][1] = (TextView) v.findViewById(R.id.partyPlayPlayer2QuestionBar2TrackTitle);
        playerAnswerBarsTrackTextView[1][2] = (TextView) v.findViewById(R.id.partyPlayPlayer2QuestionBar3TrackTitle);



        /**
         * Player 3
         */
        if (numberOfPlayers >= 3) {
            playerAnswerBarsLayout[2][0] = v.findViewById(R.id.partyPlayPlayer3QuestionBar1);
            playerAnswerBarsLayout[2][1] = v.findViewById(R.id.partyPlayPlayer3QuestionBar2);
            playerAnswerBarsLayout[2][2] = v.findViewById(R.id.partyPlayPlayer3QuestionBar3);

            playerAnswerBarsAlbumArt[2][0] = (ImageView) v.findViewById(R.id.partyPlayPlayer3QuestionBar1AlbumImageView);
            playerAnswerBarsAlbumArt[2][1] = (ImageView) v.findViewById(R.id.partyPlayPlayer3QuestionBar2AlbumImageView);
            playerAnswerBarsAlbumArt[2][2] = (ImageView) v.findViewById(R.id.partyPlayPlayer3QuestionBar3AlbumImageView);

            playerAnswerBarsArtistTextView[2][0] = (TextView) v.findViewById(R.id.partyPlayPlayer3QuestionBar1TrackArtist);
            playerAnswerBarsArtistTextView[2][1] = (TextView) v.findViewById(R.id.partyPlayPlayer3QuestionBar2TrackArtist);
            playerAnswerBarsArtistTextView[2][2] = (TextView) v.findViewById(R.id.partyPlayPlayer3QuestionBar3TrackArtist);

            playerAnswerBarsTrackTextView[2][0] = (TextView) v.findViewById(R.id.partyPlayPlayer3QuestionBar1TrackTitle);
            playerAnswerBarsTrackTextView[2][1] = (TextView) v.findViewById(R.id.partyPlayPlayer3QuestionBar2TrackTitle);
            playerAnswerBarsTrackTextView[2][2] = (TextView) v.findViewById(R.id.partyPlayPlayer3QuestionBar3TrackTitle);
        }


        /**
         * Player 4
         */
        if (numberOfPlayers >= 4) {
            playerAnswerBarsLayout[3][0] = v.findViewById(R.id.partyPlayPlayer4QuestionBar1);
            playerAnswerBarsLayout[3][1] = v.findViewById(R.id.partyPlayPlayer4QuestionBar2);
            playerAnswerBarsLayout[3][2] = v.findViewById(R.id.partyPlayPlayer4QuestionBar3);

            playerAnswerBarsAlbumArt[3][0] = (ImageView) v.findViewById(R.id.partyPlayPlayer4QuestionBar1AlbumImageView);
            playerAnswerBarsAlbumArt[3][1] = (ImageView) v.findViewById(R.id.partyPlayPlayer4QuestionBar2AlbumImageView);
            playerAnswerBarsAlbumArt[3][2] = (ImageView) v.findViewById(R.id.partyPlayPlayer4QuestionBar3AlbumImageView);

            playerAnswerBarsArtistTextView[3][0] = (TextView) v.findViewById(R.id.partyPlayPlayer4QuestionBar1TrackArtist);
            playerAnswerBarsArtistTextView[3][1] = (TextView) v.findViewById(R.id.partyPlayPlayer4QuestionBar2TrackArtist);
            playerAnswerBarsArtistTextView[3][2] = (TextView) v.findViewById(R.id.partyPlayPlayer4QuestionBar3TrackArtist);

            playerAnswerBarsTrackTextView[3][0] = (TextView) v.findViewById(R.id.partyPlayPlayer4QuestionBar1TrackTitle);
            playerAnswerBarsTrackTextView[3][1] = (TextView) v.findViewById(R.id.partyPlayPlayer4QuestionBar2TrackTitle);
            playerAnswerBarsTrackTextView[3][2] = (TextView) v.findViewById(R.id.partyPlayPlayer4QuestionBar3TrackTitle);
        }

        /**
         * Player 5
         */
        if (numberOfPlayers >= 5) {
            playerAnswerBarsLayout[4][0] = v.findViewById(R.id.partyPlayPlayer5QuestionBar1);
            playerAnswerBarsLayout[4][1] = v.findViewById(R.id.partyPlayPlayer5QuestionBar2);
            playerAnswerBarsLayout[4][2] = v.findViewById(R.id.partyPlayPlayer5QuestionBar3);

            playerAnswerBarsAlbumArt[4][0] = (ImageView) v.findViewById(R.id.partyPlayPlayer5QuestionBar1AlbumImageView);
            playerAnswerBarsAlbumArt[4][1] = (ImageView) v.findViewById(R.id.partyPlayPlayer5QuestionBar2AlbumImageView);
            playerAnswerBarsAlbumArt[4][2] = (ImageView) v.findViewById(R.id.partyPlayPlayer5QuestionBar3AlbumImageView);

            playerAnswerBarsArtistTextView[4][0] = (TextView) v.findViewById(R.id.partyPlayPlayer5QuestionBar1TrackArtist);
            playerAnswerBarsArtistTextView[4][1] = (TextView) v.findViewById(R.id.partyPlayPlayer5QuestionBar2TrackArtist);
            playerAnswerBarsArtistTextView[4][2] = (TextView) v.findViewById(R.id.partyPlayPlayer5QuestionBar3TrackArtist);

            playerAnswerBarsTrackTextView[4][0] = (TextView) v.findViewById(R.id.partyPlayPlayer5QuestionBar1TrackTitle);
            playerAnswerBarsTrackTextView[4][1] = (TextView) v.findViewById(R.id.partyPlayPlayer5QuestionBar2TrackTitle);
            playerAnswerBarsTrackTextView[4][2] = (TextView) v.findViewById(R.id.partyPlayPlayer5QuestionBar3TrackTitle);
        }


        /**
         * Player 6
         */
        if (numberOfPlayers >= 6) {
            playerAnswerBarsLayout[5][0] = v.findViewById(R.id.partyPlayPlayer6QuestionBar1);
            playerAnswerBarsLayout[5][1] = v.findViewById(R.id.partyPlayPlayer6QuestionBar2);
            playerAnswerBarsLayout[5][2] = v.findViewById(R.id.partyPlayPlayer6QuestionBar3);

            playerAnswerBarsAlbumArt[5][0] = (ImageView) v.findViewById(R.id.partyPlayPlayer6QuestionBar1AlbumImageView);
            playerAnswerBarsAlbumArt[5][1] = (ImageView) v.findViewById(R.id.partyPlayPlayer6QuestionBar2AlbumImageView);
            playerAnswerBarsAlbumArt[5][2] = (ImageView) v.findViewById(R.id.partyPlayPlayer6QuestionBar3AlbumImageView);

            playerAnswerBarsArtistTextView[5][0] = (TextView) v.findViewById(R.id.partyPlayPlayer6QuestionBar1TrackArtist);
            playerAnswerBarsArtistTextView[5][1] = (TextView) v.findViewById(R.id.partyPlayPlayer6QuestionBar2TrackArtist);
            playerAnswerBarsArtistTextView[5][2] = (TextView) v.findViewById(R.id.partyPlayPlayer6QuestionBar3TrackArtist);

            playerAnswerBarsTrackTextView[5][0] = (TextView) v.findViewById(R.id.partyPlayPlayer6QuestionBar1TrackTitle);
            playerAnswerBarsTrackTextView[5][1] = (TextView) v.findViewById(R.id.partyPlayPlayer6QuestionBar2TrackTitle);
            playerAnswerBarsTrackTextView[5][2] = (TextView) v.findViewById(R.id.partyPlayPlayer6QuestionBar3TrackTitle);
        }



        for (int i = 0; i < numberOfPlayers; i++) {
            for (int j = 0; j < 3; j++) {
                addAnswerButton(playerAnswerBarsLayout[i][j], i, j);


                if (playerAnswerBarsAlbumArt[i][j] == null)
                    System.out.println("playerAnswerBarsAlbumArt " + i + " " + j);

                if (playerAnswerBarsTrackTextView[i][j] == null)
                    System.out.println("playerAnswerBarsTrackTextView " + i + " " + j);

                if (playerAnswerBarsArtistTextView[i][j] == null)
                    System.out.println("playerAnswerBarsTrackTextView " + i + " " + j);
            }
        }
    }

    private void addAnswerButton(View v, final int player, final int answerId) {
        if (v != null)
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onPlayerAnswer(player, answerId);
                }
            });
        else
            System.out.println("Player " + player + " answer id " + answerId + " has no view");
    }

    private void onPlayerAnswer(int player, int answerId) {
        disablePlayerInterface(player);

        int timeToAnswer = 15000;
        if (songTimer != null)
            timeToAnswer = (int) songTimer.getElapsedTime();

        answerTimeArray[player][question] = timeToAnswer;
        answeredBarArray[player][question] = answerId;

        if (timeToAnswer != 0 && didPlayerGuessCorrect(player, answerId)) {
            answeredCorrectArray[player][question] = true;
            updatePlayerScore(player);
            ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.CORRECT);
        }
        else {
            answeredCorrectArray[player][question] = false;
            ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.WRONG);
        }

        if (hasAllPlayersAnsweredCurrentQuestion()) {
            onAllPlayersHaveAnswered();
        }
    }

    private boolean hasAllPlayersAnsweredCurrentQuestion() {
        for (int i = 0; i < numberOfPlayers; i++) {
            if (answerTimeArray[i][question] == 0)
                return false;
        }
        return true;
    }

    private void updatePlayerScore(int player) {
        int oldScore = 0;
        for (int i = 0; i < question; i++) {
            if (answeredCorrectArray[player][i]) {
                int tta = answerTimeArray[player][i];

                if (tta > 0)
                    oldScore += calculateScore(tta);
            }
        }

        int newScore = oldScore;
        if (answeredCorrectArray[player][question])
            newScore = oldScore + calculateScore(answerTimeArray[player][question]);

        if (oldScore != newScore)
            startScoreTravelAnimation(player, oldScore, newScore);
    }

    private void startScoreTravelAnimation(int player, int oldScore, int newScore) {
        animatePointIncrease(oldScore, newScore, player);
    }

    //private int getStarsFromScore(int score) {
    //    return 0;
    //}

    private int calculateScore(int timeToAnswer) {
        double maxScorePerQuestion = maxScore / (double) (numberOfQuestions);
        double scorePerTimeUnit = maxScorePerQuestion
                / (maxTimeToAnswer / (numberOfQuestions));

        return (int) (maxScorePerQuestion - timeToAnswer * scorePerTimeUnit);
    }

    private boolean didPlayerGuessCorrect(int player, int answerId) {
        String artist = playerAnswerBarsArtistTextView[player][answerId].getText().toString();
        String track = playerAnswerBarsTrackTextView[player][answerId].getText().toString();

        if (correctArtist == null) {
            QuestionModel questions[] = getQuestionModels();
            TrackModel correctTrackModel = questions[question].getTrack(questions[question].getCorrectTrackNumber());

            correctTrack = correctTrackModel.getTitle();
            correctArtist = correctTrackModel.getArtist();
        }

        return (artist.contentEquals(correctArtist) && track.contentEquals(correctTrack));
    }

    private void onAllPlayersHaveAnswered() {
        songTimer.cancel();


        for (int i = 0; i < numberOfPlayers; i++) {
            if (answeredBarArray[i][question] >= 0) {
                if (didPlayerGuessCorrect(i, answeredBarArray[i][question]))
                    playerAnswerBarsLayout[i][answeredBarArray[i][question]].setBackgroundResource(R.drawable.partyplay_sixplayer_questionbar_correct);
                else
                    playerAnswerBarsLayout[i][answeredBarArray[i][question]].setBackgroundResource(R.drawable.partyplay_sixplayer_questionbar_incorrect);
            }
        }

        question++;

        correctTrack = correctArtist = null;

        if (question == numberOfQuestions){
            onAllQuestionsPlayed();
        }
        else {
            startPreparingSong();
            showPauseButton();
        }

        showNextButton();
    }

    private QuestionModel[] getQuestionModels(){
        if (questionModels == null)
            questionModels = getGameModel().getQuestions(getActivity());

        return questionModels;
    }

    private void startPreparingSong() {
        final int nextQuestion = question;

        if (nextQuestion < numberOfQuestions) {
            final String songPath = getQuestionModels()[nextQuestion].getCorrectTrackModel().getStreamURL();

            ((PartyPlayActivity) getActivity()).prepareSong(songPath, new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                    onMusicLoadingError();
                    return false;
                }
            }, true);

            ((PartyPlayActivity)getActivity()).setListenerOnLoaded(songPath, new PartyPlayActivity.OnSongReadyListener() {
                @Override
                public void onReady(MediaPlayer mediaplayer) {
                    onMusicPrepared(mediaplayer, nextQuestion);
                }
            });
        }
    }

    private void clearAnimations() {
        for (View v : currentAnimations)
            v.clearAnimation();
    }

    private synchronized void onNextPressed() {
        isNextButtonPressed = true;
        
        hideNextButton();

        if (question >= numberOfQuestions)
            showResultScreen();

        if (isMusicPrepared)
            onMusicPreparedAndNextButtonPressed(mediaPlayers[question]);
    }


    private void showResultScreen() {
        ((PartyPlayActivity)getActivity()).stopCurrentSong();

        Bundle b = new Bundle();
        b.putInt("numberOfPlayers", numberOfPlayers);
        b.putInt("numberOfQuestions", numberOfQuestions);
        for (int i = 0; i < numberOfPlayers; i++) {
            b.putIntArray("playerTime" + i, answerTimeArray[i]);
            b.putBooleanArray("playerGuessedCorrect" + i, answeredCorrectArray[i]);
        }
        b.putStringArray("playerNames", playerNames);

        b.putInt("maxTimeToAnswer", maxTimeToAnswer);
        b.putInt("maxScore", (int) maxScore);

        b.putString("packId", getGameModel().getPackId());
        b.putString("levelId", getGameModel().getLevelTypeId());
        b.putInt("levelNumber", getGameModel().getLevelNumber(getActivity()));

        Intent i = new Intent();
        i.putExtras(b);

        getActivity().setResult(Activity.RESULT_OK, i);
        getActivity().finish();
    }

    private void onTimerEnd() {
        for (int i = 0; i < numberOfPlayers; i++) {
            if (answerTimeArray[i][question] == 0) {
                disablePlayerInterface(i);

                answerTimeArray[i][question] = maxTimeToAnswer/numberOfQuestions;
                answeredCorrectArray[i][question] = false;
                answeredBarArray[i][question] = -1;
            }
        }

        onAllPlayersHaveAnswered();
    }

    private void onPausePressed() {
        getActivity().finish();
    }

    private void onMusicLoadingError() {

    }

    private void onMusicPreparedAndNextButtonPressed(MediaPlayer mediaPlayer) {
        hideNextButton();
        hidePauseButton();

        isMusicPrepared = false;
        isNextButtonPressed = false;

        clearAnimations();

        prepareQuestionBarsForNextTrack();

        resetAndStartCounter();

        startPlayback(mediaPlayer);

        updateQuestionNumber();
    }

    private void updateQuestionNumber() {
        String text = (question+1) + "/" + numberOfQuestions;

        TextView tv = (TextView) v.findViewById(R.id.partyPlayQuestionTextView1);
        if (tv != null)
            tv.setText(text);

        tv = (TextView) v.findViewById(R.id.partyPlayQuestionTextView2);
        if (tv != null)
            tv.setText(text);
    }

    private void startPlayback(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
    }

    private void resetAndStartCounter() {
        if (songTimer != null) {
            songTimer.cancel();
        }
        songTimer = new SongBlaTimer(maxTimeToAnswer/numberOfQuestions, 30, pb, (TextView) v.findViewById(R.id.partyPlayProgressBarTextView), this);
        songTimer.start();
    }

    private synchronized void onMusicPrepared(MediaPlayer mediaPlayer, int questionNumber) {
        isMusicPrepared = true;

        System.out.println("On Music prepared " + questionNumber);

        if (isNextButtonPressed)
            onMusicPreparedAndNextButtonPressed(mediaPlayer);
        else {
            mediaPlayers[questionNumber] = mediaPlayer;
        }
    }

    private void hidePauseButton() {
        v.findViewById(R.id.partyPlayPauseButton1).clearAnimation();
        v.findViewById(R.id.partyPlayPauseButton2).clearAnimation();

        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayPauseButton1), View.INVISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayPauseButton2), View.INVISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayPauseButton1Image), View.INVISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayPauseButton2Image), View.INVISIBLE);
    }

    private void hideNextButton() {
        v.findViewById(R.id.partyPlayNextButton1).clearAnimation();
        v.findViewById(R.id.partyPlayNextButton2).clearAnimation();

        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayNextButton1), View.INVISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayNextButton2), View.INVISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayNextButton1Image), View.INVISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayNextButton2Image), View.INVISIBLE);
    }

    private void onAllQuestionsPlayed() {
        // Report question answers to server
    }

    private void showNextButton() {
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayNextButton1), View.VISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayNextButton2), View.VISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayNextButton1Image), View.VISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayNextButton2Image), View.VISIBLE);

        Helpers.addPulse(v.findViewById(R.id.partyPlayNextButton1),0);
        Helpers.addPulse(v.findViewById(R.id.partyPlayNextButton2),0);
    }

    private void showPauseButton() {
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayPauseButton1), View.VISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayPauseButton2), View.VISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayPauseButton1Image), View.VISIBLE);
        Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayPauseButton2Image), View.VISIBLE);

        Helpers.addPulse(v.findViewById(R.id.partyPlayPauseButton1),0);
        Helpers.addPulse(v.findViewById(R.id.partyPlayPauseButton2),0);
    }

    private void disablePlayerInterface(int player) {
        for (int i = 0; i < 3; i++) {
            playerAnswerBarsLayout[player][i].setEnabled(false);
            playerAnswerBarsLayout[player][i].setBackgroundResource(R.drawable.partyplay_sixplayer_questionbar_disabled);
            playerAnswerBarsArtistTextView[player][i].setTextColor(getResources().getColor(R.color.whitefiftyaplha));
            playerAnswerBarsTrackTextView[player][i].setTextColor(getResources().getColor(R.color.whitefiftyaplha));
        }
    }

    private OnlinePartyPlayGame getGameModel() {
        return game;
    }

    private void prepareQuestionBarsForNextTrack() {
        QuestionModel questions[] = getQuestionModels();

        String artists[] = new String[3];
        String tracks[] = new String[3];
        String trackIds[] = new String[3];

        for (int i = 0; i < 3; i++) {
            artists[i] = questions[question].getTrack(i).getArtist();
            tracks[i] = questions[question].getTrack(i).getTitle();
            trackIds[i] = questions[question].getTrack(i).getId();
        }

        for (int i = 0; i < numberOfPlayers; i++) {
            shuffleArrays(artists, tracks, trackIds);
            for (int j = 0; j < artists.length; j++) {
                Helpers.displayImage(getActivity(), Helpers.getAlbumArtURL(trackIds[j]), playerAnswerBarsAlbumArt[i][j]);
                playerAnswerBarsArtistTextView[i][j].setText(artists[j]);
                playerAnswerBarsTrackTextView[i][j].setText(tracks[j]);
                playerAnswerBarsArtistTextView[i][j].setTextColor(getResources().getColor(R.color.white));
                playerAnswerBarsTrackTextView[i][j].setTextColor(getResources().getColor(R.color.white));
            }

            for (int j = 0; j < 3; j++) {
                playerAnswerBarsLayout[i][j].setBackgroundResource(R.drawable.partyplay_sixplayer_questionbar);
                playerAnswerBarsLayout[i][j].setEnabled(true);
                playerAnswerBarsLayout[i][j].setVisibility(View.VISIBLE);
            }
        }
    }

    private void shuffleArrays(String a[], String t[], String id[]) {
        for (int i = 0; i < a.length; i++) {
            int randomPlace = Storage.random.nextInt(3);

            swap(a, i, randomPlace);
            swap(t, i, randomPlace);
            swap(id, i, randomPlace);
        }
    }

    private void swap(String[] stringArray, int i, int j) {
        String tmp = stringArray[i];
        stringArray[i] = stringArray[j];
        stringArray[j] = tmp;
    }


    private void setupNames(String[] playerNames) {
        ((TextView)v.findViewById(R.id.partyPlayPlayer1Name)).setText(playerNames[0]);
        ((TextView)v.findViewById(R.id.partyPlayPlayer2Name)).setText(playerNames[1]);
        if (numberOfPlayers > 2)
            ((TextView)v.findViewById(R.id.partyPlayPlayer3Name)).setText(playerNames[2]);
        if (numberOfPlayers > 3)
            ((TextView)v.findViewById(R.id.partyPlayPlayer4Name)).setText(playerNames[3]);
        if (numberOfPlayers > 4)
            ((TextView)v.findViewById(R.id.partyPlayPlayer5Name)).setText(playerNames[4]);
        if (numberOfPlayers > 5)
            ((TextView)v.findViewById(R.id.partyPlayPlayer6Name)).setText(playerNames[5]);
    }


    private void startPulsing() {
        Helpers.addPulse(v.findViewById(R.id.partyPlayNextButton1),0);
        Helpers.addPulse(v.findViewById(R.id.partyPlayNextButton2),0);
        Helpers.addPulse(v.findViewById(R.id.partyPlayPauseButton1),0);
        Helpers.addPulse(v.findViewById(R.id.partyPlayPauseButton2),0);
    }


    /**
     * Timer class that represents the main timer bar in a game
     */
    public static class SongBlaTimer extends CountDownTimer {
        private final TextView tv;
        private final PartyPlayGameFragment gameFragment;
        private long maxTime;
        private ProgressBar pb;
        private long curr;

        public SongBlaTimer(long millisInFuture, long countDownInterval, ProgressBar pb, TextView tv, PartyPlayGameFragment gameFragment) {
            super(millisInFuture, countDownInterval);
            maxTime = millisInFuture;
            this.pb = pb;
            this.tv = tv;
            this.gameFragment = gameFragment;
            curr = maxTime;
        }

        public long getElapsedTime() {
            return maxTime - curr;
        }

        @Override
        public void onFinish() {
            gameFragment.onTimerEnd();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (pb != null) {
                double p = (double) millisUntilFinished / (double) maxTime;

                if (tv != null)
                    tv.setText("" +(int) (p * gameFragment.maxScore / (gameFragment.numberOfQuestions)));

                p *= pb.getMax();
                pb.setProgress(pb.getMax() - (int) p);
            }
            curr = millisUntilFinished;
        }
    }


    private void animatePointIncrease(final int from, final int to, int player) {
        final View fromTextView;
        final TextView toTextView;
        fromTextView = v.findViewById(R.id.partyPlayMiddle);
        toTextView = playerScoreViews[player];

        ValueAnimator va = ValueAnimator.ofInt(from, to);
        va.setDuration(1000);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                toTextView.setText("" + value);
            }
        });
        va.start();

        for (int i = 1; i < 6; i++) {
            ImageView iv = new ImageView(getActivity());
            iv.setImageResource(R.drawable.gameplay_ball);
            startTravelAnimation(fromTextView, toTextView, iv, 300, i * 100);
        }
    }

    @SuppressLint("NewApi")
    private void startTravelAnimation(View src, View target, final ImageView iv, int travelTime, int timeStartOffset) {
        int[] mepa = new int[2];
        target.getLocationOnScreen(mepa);

        int width = target.getWidth();
        int height = target.getHeight();

        if (target.getRotation() == 90f || target.getRotation() == 270f) {
            mepa[0] += height/2;
            mepa[1] += width/2;
        }

        TranslateAnimation ta = new TranslateAnimation(src.getX(),
                mepa[0], src.getY(), mepa[1]);
        ta.setDuration(travelTime);
        ta.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                iv.clearAnimation();
                RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.partyplayContent);
                if (rl != null)
                    rl.removeView(iv);
            }
        });
        RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.partyplayContent);
        rl.addView(iv);
        iv.setVisibility(View.INVISIBLE);
        ta.setFillEnabled(true);
        ta.setFillBefore(true);
        ta.setFillAfter(false);
        ta.setStartOffset(timeStartOffset);
        iv.startAnimation(ta);
    }





    @Override
    public String getName() {
        return "PartyPlay Game";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playerNames = getArguments().getStringArray("playerNames");

        try {
            game = new OnlinePartyPlayGame(new JSONObject(getArguments().getString("gameJSON")));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        numberOfQuestions = getQuestionModels().length;

        answerTimeArray = new int[playerNames.length][numberOfQuestions];
        answeredCorrectArray = new boolean[playerNames.length][numberOfQuestions];
        answeredBarArray = new int[playerNames.length][numberOfQuestions];

        mediaPlayers = new MediaPlayer[numberOfQuestions];

        maxTimeToAnswer = getGameModel().getLevelType(getActivity()).getMaxTimeToAnswer();
        maxScore = getGameModel().getLevelType(getActivity()).getMaxScoreReward();
    }

    public static Fragment newInstance(String json, String playerNames[]) {
        PartyPlayGameFragment f = new PartyPlayGameFragment();
        Bundle b = new Bundle();
        b.putStringArray("playerNames", playerNames);
        b.putString("gameJSON", json);
        f.setArguments(b);
        return f;
    }
}
