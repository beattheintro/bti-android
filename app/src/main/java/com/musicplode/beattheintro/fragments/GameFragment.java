package com.musicplode.beattheintro.fragments;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.LoggedInMenu.OnSongReadyListener;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.R.color;
import com.musicplode.beattheintro.Sounds;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.DialogHelpers.OnOkButton;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.helpers.TrackerHelpers;
import com.musicplode.beattheintro.model.QuestionModel;
import com.musicplode.beattheintro.modelhelpers.GamePlayer;
import com.musicplode.beattheintro.modelhelpers.IGame;
import com.musicplode.beattheintro.modelhelpers.OfflineGame;
import com.musicplode.beattheintro.modelhelpers.OnlineGame;
import com.musicplode.beattheintro.modelhelpers.Pack;
import com.musicplode.beattheintro.modelhelpers.TrackModel;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * The fragment that holds everything regarding a game
 */
public class GameFragment extends BaseFragment {
	public static final String TAG = "Gameplay";

	private boolean offlineMode = false;

	private ArrayList<View> gamesBar = new ArrayList<View>();
	private int itemsInBasket = 0;
	private ImageView ivs[] = new ImageView[3];

	private ImageView basketImageViews[] = new ImageView[3];

	private int question = 0;
	private View v;
	private ProgressBar pb;
	private SongBlaTimer timer;
	private int player0Score;
	private int maxQuestions = 15;
	private int correctAnswer;
	private TextView questionTextViews[] = new TextView[3];
	private boolean lastGameInfoReceived = false;
	private Drawable[] pbs = new Drawable[3];

	private QuestionModel[] questions;

	private IGame currentGame;

	private List<View> questionBarViews = new ArrayList<View>();

	private int numberOfPlayers = 1;
	private View pauseSwirlImageView, nextSwirlImageView;
	private ImageView pauseImageView;

	private int maxTimeToAnswer = 150000;
	private int maxScore = 1500;

	private List<Animation> currentAnimations = new ArrayList<Animation>();
	private List<ValueAnimator> valueAnimations = new ArrayList<ValueAnimator>();
	private ArrayList<CountDownTimer> animatePoints = new ArrayList<CountDownTimer>();
	private int containerId;
	private int levelNumber;
	private String packId;

	private int prevStars[] = new int[3];
	
	@Override
	public void onStop() {
		super.onStop();

		if (getActivity() != null && getActivity() instanceof LoggedInMenu) {
			((LoggedInMenu)getActivity()).stopMediaPlayers();
		}
	}
	
	/**
	 * Starts ball traveling animation
	 */
	private void animatePointIncrease(final int from, final int to, int player) {
		final TextView fromTextView;
		final TextView toTextView;
		if (player == 0) {
			fromTextView = (TextView) v.findViewById(R.id.currentScore);
			toTextView = (TextView) v.findViewById(R.id.player1ScoreTextView);


            final ImageView scoreImageView = (ImageView) v.findViewById(R.id.currentScoreImageView);
            final TextView scoreTextView = (TextView) v.findViewById(R.id.currentScore);

            scoreTextView.setVisibility(View.INVISIBLE);
            scoreImageView.setImageResource(R.drawable.gameplay_neon_correct);

            Animation anim = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.scaleandfadeout);
            scoreImageView.setAnimation(anim);

            scoreTextView.setVisibility(View.VISIBLE);
            scoreImageView.setVisibility(View.INVISIBLE);

            ValueAnimator va = ValueAnimator.ofInt(from, to);
            va.setDuration(1000);
            va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer value = (Integer) animation.getAnimatedValue();
                    fromTextView.setText("" + (to - value));
                    toTextView.setText("" + value);
                }
            });
            va.start();
            valueAnimations.add(va);


            if (Build.VERSION.SDK_INT >= 17) {
                for (int i = 1; i < 6; i++) {
                    ImageView iv = new ImageView(getActivity());
                    iv.setImageResource(R.drawable.gameplay_ball);
                    startTravelAnimation(fromTextView, toTextView, iv, 300, i * 100);
                }
            }
		} else if (player == 1) {
			fromTextView = (TextView) v.findViewById(R.id.currentScore);
			toTextView = (TextView) v.findViewById(R.id.player2ScoreTextView);

			ValueAnimator va = ValueAnimator.ofInt(from, to);
			va.setDuration(1000);
			va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
				@Override
				public void onAnimationUpdate(ValueAnimator animation) {
					Integer value = (Integer) animation.getAnimatedValue();
					toTextView.setText("" + value);
				}
			});
			va.start();
			valueAnimations.add(va);

            if (Build.VERSION.SDK_INT >= 17) {
                for (int i = 1; i < 6; i++) {
                    ImageView iv = new ImageView(getActivity());
                    iv.setImageResource(R.drawable.gameplay_ball);
                    startTravelAnimation(fromTextView, toTextView, iv, 300, i * 100);
                }
            }
		}
	}

	private IGame getCurrentGame() {
        return currentGame;
	}

	/**
	 * Enables/disables the answer bars
	 */
	private void showAnswerBars(boolean value) {
		if (value) {
			for (View v : questionBarViews) {
				v.setVisibility(View.VISIBLE);
				Helpers.setVisibilityIfExists(v, View.VISIBLE);
			}
		} else {
			for (View v : questionBarViews) {
				Helpers.setVisibilityIfExists(v, View.GONE);
			}
		}
	}

	private void hideMainProgressBar() {
		Helpers.setVisibilityIfExists(v.findViewById(R.id.imageView9), View.GONE);
		Helpers.setVisibilityIfExists(v.findViewById(R.id.progressBar1), View.GONE);
		Helpers.setVisibilityIfExists(v.findViewById(R.id.imageView8), View.GONE);
		Helpers.setVisibilityIfExists(v.findViewById(R.id.imageView6), View.GONE);
		Helpers.setVisibilityIfExists(v.findViewById(R.id.currentScore), View.GONE);
		Helpers.setVisibilityIfExists(v.findViewById(R.id.currentScoreImageView), View.GONE);
		Helpers.setVisibilityIfExists(v.findViewById(R.id.currentScoreDiscImage), View.GONE);

		hidePause();

		hideNextAnimation();
	}

	private void showWhoWonFragment(JSONObject gameInfo) {
		if (getActivity() == null)
			return;

		if (v.findViewById(R.id.frameLayout11) == null)
			return;

		Helpers.setVisibilityIfExists(v.findViewById(R.id.frameLayout11), View.VISIBLE);

		FragmentTransaction ft = getFragmentManager().beginTransaction();
		Helpers.setFragmentAnimation(ft);

		/*
		 * gameJSON packid, scores, etc
		 */
		JSONObject trackToArtist = new JSONObject();
		JSONArray arr = new JSONArray();
		try {
			trackToArtist.put("Tracks", arr);
			for (QuestionModel q : questions) {
				if (q != null) {
					JSONObject o = new JSONObject();
					o.put("Artist", q.getTrack(q.getCorrectTrackNumber()).getArtist());
					o.put("Title", q.getTrack(q.getCorrectTrackNumber()).getTitle());
					o.put("AlbumCoverURL", q.getTrack(q.getCorrectTrackNumber()).getAlbumCoverURL());
					arr.put(o);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		((LoggedInMenu) getActivity()).publishAmazonLeaderBoardScore(getLeaderboardId(), player0Score);

		try {
			Fragment f = WhoWonFragment.newInstance(gameInfo.toString(), trackToArtist, containerId, prevStars);
			ft.add(R.id.frameLayout11, f);
			ft.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    /**
     * Creates an id that Amazon gameCircle can tolerate.
     * @return The packs Id with its dashes changed to underscore and add the levelnumber
     */
	public String getLeaderboardId() {
		String s = "" + packId;

		s = s.replace("-", "_");
		s += "_" + levelNumber;

		return s;
	}

	private void hideTrackScrollView() {
		v.findViewById(R.id.horizontalLinearLayout1).setVisibility(View.GONE);
		v.findViewById(R.id.horizontalScrollView1).setVisibility(View.GONE);
	}

	private void setStar(View v, boolean filled) {
		if (v == null)
			return;
		if (v instanceof ImageView) {
			if (filled)
				((ImageView) v).setImageResource(R.drawable.gameplay_star);
			else
				((ImageView) v).setImageResource(R.drawable.gameplay_star_empty);
		}
	}

	private void setStars(int stars, int player) {
		if (player == 0) {
			setStar(v.findViewById(R.id.player1Star1), stars > 0);
			setStar(v.findViewById(R.id.player1Star2), stars > 1);
			setStar(v.findViewById(R.id.player1Star3), stars > 2);
		}
		if (player == 1) {
			setStar(v.findViewById(R.id.player2Star1), stars > 0);
			setStar(v.findViewById(R.id.player2Star2), stars > 1);
			setStar(v.findViewById(R.id.player2Star3), stars > 2);
		}
	}

	public void updateStars() {
		setStars(getCurrentGame().getPlayer().getStars(), 0);
		if (getCurrentGame().getNumberOfPlayers() > 1)
			setStars(getCurrentGame().getOpponents().get(0).getStars(), 1);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		containerId = container.getId();

		v = inflater.inflate(R.layout.maingamenew, container, false);

		((LoggedInMenu) getActivity()).disableAds();
		((LoggedInMenu) getActivity()).disableLowerChart();

		hideNextAnimation();

		//noinspection deprecation
		pbs[2] = getResources().getDrawable(R.drawable.progressbar_gameplay);
		//noinspection deprecation
		pbs[1] = getResources().getDrawable(R.drawable.progressbar_gameplay_yellow);
		//noinspection deprecation
		pbs[0] = getResources().getDrawable(R.drawable.progressbar_gameplay_red);

		/**
		 * Init the game object
		 */
		if (getCurrentGame() == null) {
			if (getActivity() == null)
				return v;

			getActivity().finish();
			return v;
		}

		questions = getCurrentGame().getQuestions(getActivity());
		if (questions == null || questions.length == 0) {
			DialogHelpers.showErrorDialog(getActivity(),
					R.string.ERROR_LOADING_SONG_TITLE,
					R.string.ERROR_LOADING_SONG_TEXT, R.string.OK,
					new OnOkButton() {
						@Override
						public void onOk() {
							if (getActivity() != null)
								getActivity().finish();
						}
					});
			return v;
		}

		maxQuestions = questions.length;

		LinearLayout ll = (LinearLayout) v.findViewById(R.id.horizontalLinearLayout1);

		for (int i = 0; i < questions.length; i++) {
			View vsd = inflater.inflate(R.layout.game_bla, ll, false);

			((TextView) vsd.findViewById(R.id.trackNumberTextView)).setText("" + (i + 1));

			questions[i].setImageView((ImageView) vsd.findViewById(R.id.indicatior));
			ll.addView(vsd, new LinearLayout.LayoutParams(vsd.getLayoutParams().width, vsd.getLayoutParams().height, 1));

			gamesBar.add(vsd);
		}

		int width = getResources().getDisplayMetrics().widthPixels;

		if (getResources().getBoolean(R.bool.tablet)) {
			width -= Helpers.dpToPx(getActivity(), 200);
		}

		RelativeLayout rl = (RelativeLayout) inflater.inflate(R.layout.game_bla, ll, false);
		rl.setLayoutParams(new RelativeLayout.LayoutParams(width / 2, LayoutParams.MATCH_PARENT));
		ll.addView(rl, 0);

		RelativeLayout rr = (RelativeLayout) inflater.inflate( R.layout.game_bla2, ll, false);
		rr.setLayoutParams(new RelativeLayout.LayoutParams(width / 2, LayoutParams.MATCH_PARENT));
		ll.addView(rr);

		v.findViewById(R.id.gameplay_question1_right_text).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (v.isEnabled())
							answer(0);
					}
				});

		v.findViewById(R.id.gameplay_question2_right_text).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (v.isEnabled())
							answer(1);
					}
				});

		v.findViewById(R.id.gameplay_question3_right_text).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (v.isEnabled())
							answer(2);
					}
				});

		v.findViewById(R.id.gameplay_next_button).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						startNextTrack();
					}
				});

		ivs[0] = (ImageView) v.findViewById(R.id.question1albumcover);
		ivs[1] = (ImageView) v.findViewById(R.id.question2albumcover);
		ivs[2] = (ImageView) v.findViewById(R.id.question3albumcover);
		questionTextViews[0] = (TextView) v.findViewById(R.id.gameplay_question1_right_text);
		questionTextViews[1] = (TextView) v.findViewById(R.id.gameplay_question2_right_text);
		questionTextViews[2] = (TextView) v.findViewById(R.id.gameplay_question3_right_text);
		basketImageViews[0] = (ImageView) v.findViewById(R.id.basket1);
		basketImageViews[1] = (ImageView) v.findViewById(R.id.basket2);
		basketImageViews[2] = (ImageView) v.findViewById(R.id.basket3);
		pauseSwirlImageView = v.findViewById(R.id.gameplay_pause_ring);
		nextSwirlImageView = v.findViewById(R.id.gameplay_next_ring);
		pauseImageView = (ImageView) v.findViewById(R.id.gameplay_pause_button);

		pb = (ProgressBar) v.findViewById(R.id.progressBar1);

		Helpers.setAllFonts(v, getActivity());

		Animation rotation = AnimationUtils.loadAnimation(getActivity(), R.anim.pulse);
		rotation.setRepeatCount(Animation.INFINITE);

		showPause();

		pauseSwirlImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (offlineMode)
					getActivity().finish();
				else {
					Helpers.refreshProfile(getActivity());
					((LoggedInMenu) getActivity()).stopMediaPlayers();
                    ((BTIApp)getActivity().getApplication()).stopSounds();
					GameFragment.this.getActivity().onBackPressed();
				}
			}
		});

		packId = currentGame.getPackId();
		String packName;

		maxQuestions = maxQuestions - 1;
		if (offlineMode) {
			packName = "Offline Game";
			question = 0;
			player0Score = 0;
		}
        else {
			player0Score = getCurrentGame().getPlayer().getScore();
			packName = Helpers.getPackNameFromPackId(Storage.getUserProfile(getActivity()), packId);
			question = maxQuestions - currentGame.getPlayer().getNumberOfUnansweredQuestions() + 1;
		}

		((TextView) v.findViewById(R.id.packTitle)).setText(packName);

		/**
		 * Close an unclosed game
		 */
		if (question > maxQuestions) {
			if (getCurrentGame() != null
					&& getCurrentGame() instanceof OnlineGame) {
				lastGameInfoReceived = true;
			}

			startNextTrack();
		}

		updatePlayerViews();

        if (getCurrentGame().getOpponents().size() > 0) {
            getCurrentGame().getOpponents().get(0).setupScoreAnswersFromStartJSON(maxScore, maxQuestions, maxTimeToAnswer);
        }

		v.findViewById(R.id.basket1).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						basketPressed(0);
					}
				});
		v.findViewById(R.id.basket2).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						basketPressed(1);
					}
				});
		v.findViewById(R.id.basket3).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						basketPressed(2);
					}
				});

		questionBarViews.add(v.findViewById(R.id.gameplay_questionbar1));
		questionBarViews.add(v.findViewById(R.id.basket1));
		questionBarViews.add(v.findViewById(R.id.gameplay_question1_right_text));
		questionBarViews.add(v.findViewById(R.id.question1albumcover));

		questionBarViews.add(v.findViewById(R.id.gameplay_questionbar2));
		questionBarViews.add(v.findViewById(R.id.basket2));
		questionBarViews.add(v.findViewById(R.id.gameplay_question2_right_text));
		questionBarViews.add(v.findViewById(R.id.question2albumcover));

		questionBarViews.add(v.findViewById(R.id.gameplay_questionbar3));
		questionBarViews.add(v.findViewById(R.id.basket3));
		questionBarViews.add(v.findViewById(R.id.gameplay_question3_right_text));
		questionBarViews.add(v.findViewById(R.id.question3albumcover));

		showAnswerBars(false);

		if (getCurrentGame().getPlayer().getFacebookId().length() > 0) {
			Helpers.displayImage(getActivity(), Helpers.getFacebookProfileImage(getCurrentGame().getPlayer().getFacebookId()), (ImageView) v.findViewById(R.id.imageView5));
		}

		if (question < questions.length) {
			try {
				if (offlineMode) {
					((LoggedInMenu) getActivity()).prepareSong(questions[0].getStreamURL(), null, true);
				}

				startNextTrack();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		v.getViewTreeObserver().addOnGlobalLayoutListener(mGlobalLayoutListener);
		/**
		 * Change Level indicator and bars to match played level
		 */
		ImageView indicator = (ImageView) v.findViewById(R.id.gameplayLevelIndicatorImage);
		ImageView indicatorLeftBar = (ImageView) v.findViewById(R.id.gameplayLevelIndicatorLeftBar);
		ImageView indicatorRightBar = (ImageView) v.findViewById(R.id.gameplayLevelIndicatorRightBar);
		levelNumber = getCurrentGame().getLevelNumber(getActivity());

		if (indicator != null) {
			if (levelNumber == 1) {
				indicator.setImageResource(R.drawable.level2);
				indicatorLeftBar.setImageResource(R.drawable.level2_left);
				indicatorRightBar.setImageResource(R.drawable.level2_right);
			}
			else if (levelNumber == 2) {
				indicator.setImageResource(R.drawable.level3);
				indicatorLeftBar.setImageResource(R.drawable.level3_left);
				indicatorRightBar.setImageResource(R.drawable.level3_right);
			}
		}

		updateStars();

		Pack p = getCurrentGame().getPack(getActivity());
		if (p != null) {
			for (int i = 0; i < 3; i++) {
				prevStars[i] = p.getStarsOnLevel(i);
			}
		}
		
		return v;
	}

	private OnErrorListener onMusicErrorListener = new OnErrorListener() {
		@Override
		public boolean onError(MediaPlayer mp, int what, int extra) {
			if (getActivity() != null) {
				DialogHelpers.showErrorDialog(getActivity(),
						R.string.ERROR_LOADING_SONG_TITLE,
						R.string.ERROR_LOADING_SONG_TEXT, R.string.OK,
						new OnOkButton() {
							@Override
							public void onOk() {
								if (getActivity() != null) {
									((LoggedInMenu) getActivity())
											.setShouldBlockBackButton(false);
									getActivity().onBackPressed();
								}
							}
						});
			}

			return false;
		}
	};

	private void basketPressed(int basketNumber) {
		basketImageViews[basketNumber].setEnabled(false);
		addTrackToBasket(questions[question - 1].getTrack(basketNumber));

		// Animation
		basketImageViews[basketNumber].setVisibility(View.INVISIBLE);
		ImageView iv = new ImageView(getActivity());
		iv.setImageResource(R.drawable.basket_icon);
        if (Build.VERSION.SDK_INT >= 17) {
            startTravelAnimation(basketImageViews[basketNumber], v.findViewById(R.id.player1basket), iv, 500, 0);
        }
	}

	@SuppressLint("NewApi")
	private void startTravelAnimation(View src, View target,
			final ImageView iv, int travelTime, int timeStartOffset) {
		if (android.os.Build.VERSION.SDK_INT >= 11) {
			TranslateAnimation ta = new TranslateAnimation(src.getX(),
					target.getX(), src.getY(), target.getY());
			ta.setDuration(travelTime);
			ta.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					iv.clearAnimation();
					iv.setImageDrawable(null);
					RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.mainGameView);
					if (rl != null)
						rl.removeView(iv);
				}
			});
			RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.mainGameView);
			rl.addView(iv);
			iv.setVisibility(View.INVISIBLE);
			ta.setFillEnabled(true);
			ta.setFillBefore(true);
			ta.setFillAfter(false);
			ta.setStartOffset(timeStartOffset);
			iv.startAnimation(ta);
			currentAnimations.add(ta);
		}
	}

	private void addTrackToBasket(TrackModel track) {
		itemsInBasket++;

		((LoggedInMenu) getActivity()).increaseBasketCount();

		TrackerHelpers.addTrackToBasket(getActivity(), track.getId());

		JSONObject o = JSONRequestHelpers.getLikeTrackJSON(getActivity(), Storage.getUserProfile(getActivity()), track.getId());

		Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o,
				new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						((TextView) v.findViewById(R.id.player1basketEntrys)).setText("" + itemsInBasket);
					}

					@Override
					public void badResult(String errorMessage) {
					}
				});
	}

	private void updatePlayerViews() {
		numberOfPlayers = getCurrentGame().getNumberOfPlayers();
		String player0Name = getCurrentGame().getPlayer().getName();
		((TextView) v.findViewById(R.id.player1Name)).setText(player0Name);

		// player0Score = getCurrentGame().getPlayer().getScore();
		((TextView) v.findViewById(R.id.player1ScoreTextView)).setText("" + player0Score);

		if (numberOfPlayers < 2) {
			hidePlayer2GUI();
		} else {
			updatePlayer2UI();
		}
	}

	private void updatePlayer2UI() {
		GamePlayer player2 = getCurrentGame().getOpponents().get(0);

		String player2Name = player2.getName();
		int score = getPlayer2ScoreUpTo(question);

		((TextView) v.findViewById(R.id.player2Name)).setText(player2Name);
		((TextView) v.findViewById(R.id.player2ScoreTextView)).setText("" + score);

		if (player2.getFacebookId().length() > 0) {
			Helpers.displayImage(getActivity(), Helpers.getFacebookProfileImage(player2.getFacebookId()), (ImageView) v.findViewById(R.id.imageView4));
		}
	}

	private void hidePlayer2GUI() {
		v.findViewById(R.id.player2ScoreTextView).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.player2ScoreImageView).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.player2Star1).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.player2Star2).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.player2Star3).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.player2basket).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.player2X).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.player2basketEntrys).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.imageView2).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.imageView4).setVisibility(View.INVISIBLE);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	private void setSongTexts() {
		for (int i = 0; i < 3; i++) {
			questionTextViews[i].setText(questions[question].getTrack(i).getText());

			if (offlineMode) {
				ivs[i].setImageResource(R.drawable.bti_default_pack);

				// MediaMetadataRetriever mmr = new MediaMetadataRetriever();
				// mmr.setDataSource(questions[question].getTrack(i).getStreamURL());
				// byte[] artBytes = mmr.getEmbeddedPicture();
				// if(artBytes != null) {
				// InputStream is = new
				// ByteArrayInputStream(mmr.getEmbeddedPicture());
				// Bitmap bm = BitmapFactory.decodeStream(is);
				// ivs[i].setImageBitmap(bm);
				// }
			}
            else
				Helpers.displayImage(getActivity(), questions[question].getTrack(i).getAlbumCoverURL(), ivs[i]);
		}
		correctAnswer = questions[question].getCorrectTrackNumber();
	}

	public void changeAnswerButtonTo(boolean b) {
		v.findViewById(R.id.gameplay_question1_right_text).setEnabled(b);
		v.findViewById(R.id.gameplay_question2_right_text).setEnabled(b);
		v.findViewById(R.id.gameplay_question3_right_text).setEnabled(b);

		for (ImageView iv : basketImageViews)
			iv.setVisibility(View.INVISIBLE);

		if (b)
			hideNextAnimation();
		else
			showNextAnimation();
	}

	private void showNextAnimation() {
		if (getActivity() == null)
			return;

		View nextButton = v.findViewById(R.id.gameplay_next_button);

		if (nextButton != null) {
			nextButton.setVisibility(View.VISIBLE);
		//	nextButton.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.nextarrowanim));
		}

		if (nextSwirlImageView != null) {
			nextSwirlImageView.setVisibility(View.VISIBLE);
			Animation rotation = AnimationUtils.loadAnimation(getActivity(), R.anim.pulse);
			rotation.setRepeatCount(Animation.INFINITE);
			nextSwirlImageView.setAnimation(rotation);
		}
	}

	private void hideNextAnimation() {
		if (getActivity() == null)
			return;

		View nextButton = v.findViewById(R.id.gameplay_next_button);
		if (nextButton != null) {
			if (nextButton.getAnimation() != null) {
				nextButton.clearAnimation();
			}
			nextButton.setVisibility(View.GONE);
		}

		if (v != null && nextSwirlImageView != null) {
			if (nextSwirlImageView.getAnimation() != null) {
				nextSwirlImageView.getAnimation().cancel();
				nextSwirlImageView.getAnimation().reset();
				nextSwirlImageView.clearAnimation();
			}
			nextSwirlImageView.setVisibility(View.INVISIBLE);
		}
	}

	private void hidePause() {
		if (getActivity() == null)
			return;

		if (v != null && pauseSwirlImageView != null && pauseImageView != null) {
			((LoggedInMenu) getActivity()).setShouldBlockBackButton(true);

			if (pauseSwirlImageView.getAnimation() != null) {
				pauseSwirlImageView.getAnimation().cancel();
				pauseSwirlImageView.getAnimation().reset();
				pauseSwirlImageView.clearAnimation();
			}

			pauseSwirlImageView.setVisibility(View.INVISIBLE);
			pauseImageView.setVisibility(View.INVISIBLE);
		}
	}

	private void showPause() {
		if (getActivity() == null)
			return;

		if (pauseSwirlImageView == null || pauseImageView == null)
			return;
		
		pauseSwirlImageView.setVisibility(View.VISIBLE);
		pauseImageView.setVisibility(View.VISIBLE);

		((LoggedInMenu) getActivity()).setShouldBlockBackButton(false);

		Animation rotation = AnimationUtils.loadAnimation(getActivity(),R.anim.pulse);
		rotation.setRepeatCount(Animation.INFINITE);
		
		pauseSwirlImageView.setAnimation(rotation);
	}

	public void stopPlayback() {
		((LoggedInMenu) getActivity()).stopMediaPlayers();
	}

	public void justBeforeStartPlayingSong() {
		if (getActivity() == null)
			return;

        ((BTIApp)getActivity().getApplication()).stopSounds();

		for (Animation a : currentAnimations)
			a.cancel();

		for (ValueAnimator a : valueAnimations)
			a.cancel();

		currentAnimations.clear();
		valueAnimations.clear();
		clearAnimationIfExists(v.findViewById(R.id.currentScoreImageView));

		updatePlayerViews();

		for (CountDownTimer cdt : animatePoints)
			cdt.cancel();
		animatePoints.clear();

		v.findViewById(R.id.progressBar2).setVisibility(View.GONE);

		resetButtons();
	}

	/**
	 * Called when the new song is done buffering and has started playing
     * Clears all ongoing animations etc.
	 */
	public void startedPlayingSong() {
		if (getActivity() == null)
			return;

		hidePause();

		showAnswerBars(true);

		setSongTexts();
		changeAnswerButtonTo(true);

		if (!offlineMode)
			timer = new SongBlaTimer(maxTimeToAnswer / (maxQuestions + 1), 10, pb, GameFragment.this, pbs, offlineMode);
		else
			timer = new SongBlaTimer(15000, 10, pb, GameFragment.this, pbs, offlineMode);
		timer.start();
	}

	private void clearAnimationIfExists(View v) {
		if (v != null)
			v.clearAnimation();
	}

	public void startPlayback(String mp3Path, final int seekTo) {
		if (((LoggedInMenu) getActivity()).playIfReady(mp3Path,
				new OnSongReadyListener() {
					@Override
					public void onReady(MediaPlayer mediaplayer) {
						justBeforeStartPlayingSong();
						if (seekTo > 0)
							mediaplayer.seekTo(seekTo);
						mediaplayer.start();
						startedPlayingSong();
					}
				}, seekTo)) {
			justBeforeStartPlayingSong();
			startedPlayingSong();
		}
	}

	/**
	 * Called when either answering a question or the time runs out. Prepares
	 * next song, sends answer to server, updates ui etc etc
	 * 
	 * @param i
	 *            The answer number
	 */
	public void answer(int i) {
		if (getActivity() == null)
			return;

		changeAnswerButtonTo(false);
		stopPlayback();

		addBlackOverlayToAlbumArt();

		if (question + 2 <= questions.length) {
			if (offlineMode) {
				((LoggedInMenu) getActivity()).prepareSong(questions[question + 1].getStreamURL(), onMusicErrorListener);
			} else {
				String mp3Path = Helpers.getIntroMp3URL(questions[question + 1].getCorrectTrackId());
				((LoggedInMenu) getActivity()).prepareSong(mp3Path, onMusicErrorListener);
			}
		}

		showPause();

		if (numberOfPlayers > 1)
			updatePlayer2Score();

		long timeToAnswer = maxTimeToAnswer / (maxQuestions + 1);
		if (timer != null)
			timeToAnswer = timer.getElapsedTime();

		if (i != correctAnswer) {
            ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.WRONG);

			final ImageView scoreImageView = (ImageView) v.findViewById(R.id.currentScoreImageView);
			final TextView scoreTextView = (TextView) v.findViewById(R.id.currentScore);
			scoreTextView.setVisibility(View.INVISIBLE);
			scoreImageView.setImageResource(R.drawable.gameplay_neon_incorrect);

			Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.scaleandfadeout);
			scoreImageView.setAnimation(anim);

			questions[question].setResult(false);

			((TextView) v.findViewById(R.id.currentScore)).setText("");
			v.findViewById(R.id.currentScoreDiscImage).setVisibility(View.INVISIBLE);
		}

		if (correctAnswer >= 0 && correctAnswer < 3) {
			if (0 <= i && i <= 3) {
				if (correctAnswer == i) {
                    ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.CORRECT);

					setCorrectButton(i);
					questions[question].setResult(true);
					int from = player0Score;
					player0Score = player0Score + getScoreFor(question, timeToAnswer);

					animatePointIncrease(from, player0Score, 0);

				}
                else
					setWrongButton(i);

			}
            else {
				setAllFalseButtons();
				questions[question].setResult(false);
			}
		}

		if (question + 1 > maxQuestions) {
			hideNextAnimation();
		}

		if (!offlineMode) {
			if (!Helpers.doesLikeTrackId(Storage.getUserProfile(getActivity()), questions[question].getTrack(0).getId())) {
				v.findViewById(R.id.basket1).setVisibility(View.VISIBLE);
				v.findViewById(R.id.basket1).setEnabled(true);
			}
			if (!Helpers.doesLikeTrackId(Storage.getUserProfile(getActivity()), questions[question].getTrack(1).getId())) {
				v.findViewById(R.id.basket2).setVisibility(View.VISIBLE);
				v.findViewById(R.id.basket2).setEnabled(true);
			}
			if (!Helpers.doesLikeTrackId(Storage.getUserProfile(getActivity()), questions[question].getTrack(2).getId())) {
				v.findViewById(R.id.basket3).setVisibility(View.VISIBLE);
				v.findViewById(R.id.basket3).setEnabled(true);
			}
		}

		String answerId = questions[question].getAnswerId();

		String answerTrackId;

		if (i == questions[question].getCorrectTrackNumber())
			answerTrackId = questions[question].getTrack(i).getId();
		else {
			int wrong = questions[question].getWrongTrackNumber();
			answerTrackId = questions[question].getTrack(wrong).getId();
		}

		if (offlineMode) {
			showNextAnimation();
			updateStars();
		}
        else {
			JSONObject jsonReq = JSONRequestHelpers.getAnswerJSON(Storage.getUserProfile(getActivity()), answerId, answerTrackId, (int) timeToAnswer, getCurrentGame().getId());
			Storage.addRequestToQueue(getActivity(), Storage.ANSWER_QUESTION_URL, jsonReq,
					new OnNetworkResult() {
						@Override
						public void okResult(JSONObject response) {
                            getCurrentGame().update(response);
							try {
								if (response.getJSONArray("Players").getJSONObject(0).getBoolean("Finished")) {
									lastGameInfoReceived = true;
									showNextAnimation();
									hidePause();
								}

								updateStars();
							} catch (JSONException e1) {
								e1.printStackTrace();
							}
						}

						@Override
						public void badResult(String errorMessage) {
							if (getActivity() != null) {
								String error = getString(R.string.ERROR_INTERNET_CONNECTION_TEXT);
								if (errorMessage != null && errorMessage.length() > 0)
									error = errorMessage;

								DialogHelpers.showErrorDialog(getActivity(),
										R.string.ERROR_TITLE, error,
										R.string.OK, new OnOkButton() {
											@Override
											public void onOk() {
												if (getActivity() != null) {
													((LoggedInMenu) getActivity()).setShouldBlockBackButton(false);
													getActivity().onBackPressed();
												}
											}
										});
							}
						}
					});
		}
		correctAnswer = -1;
		question++;

		if (timer != null)
			timer.cancel();
	}

	private void hideAlbumArt() {
		for (ImageView iv : ivs) {
			Helpers.cancelImageLoading(getActivity(), iv);
			iv.setImageDrawable(null);
			iv.setColorFilter(null);
		}
	}

	private void addBlackOverlayToAlbumArt() {
		for (ImageView iv : ivs) {
			iv.setColorFilter(color.blackfiftyaplha);
		}
	}

	private int getPlayer2ScoreUpTo(int question) {
		int score = 0;
		for (int i = 0; i < question; i++) {
            score += getCurrentGame().getOpponents().get(0).getScoreForQuestion(i);
		}
		return score;
	}

	private void updatePlayer2Score() {
		int player2TrackScore = player2TrackScore(question);
		if (player2TrackScore > 0) {
			int player2ScoreBeforeQuestion = getPlayer2ScoreUpTo(question);
			int player2ScoreAfterQuestion = player2ScoreBeforeQuestion + player2TrackScore;

			animatePointIncrease(player2ScoreBeforeQuestion, player2ScoreAfterQuestion, 1);
		}
	}

	/**
	 * Helper method to have the score calculation in one place
	 * 
	 * @param questionId
	 * @param timeToAnswer
	 * @return Returns the score for answering the questionId question after
	 *         timeToAnswer milliseconds
	 */
	private int getScoreFor(int questionId, long timeToAnswer) {
		if (offlineMode) {
			double maxScorePerQuestion = maxScore / (double) 10;
			double scorePerTimeUnit = maxScorePerQuestion / maxTimeToAnswer;

			return (int) (maxScorePerQuestion - timeToAnswer * scorePerTimeUnit);
		}
        else {
			double maxScorePerQuestion = maxScore / (double) (maxQuestions + 1);
			double scorePerTimeUnit = maxScorePerQuestion
					/ (maxTimeToAnswer / (maxQuestions + 1));

			return (int) (maxScorePerQuestion - timeToAnswer * scorePerTimeUnit);
		}
	}

	/**
	 * @param question
	 * @return Returns the milliseconds it took for player2 to guess the track
	 *         correct, if incorrect will return 0, if not answered returns -1
	 */
	private int player2TrackScore(int question) {
		try {
			GamePlayer player2 = getCurrentGame().getOpponents().get(0);
            return player2.getScoreForQuestion(question);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	private void setAllFalseButtons() {
		((ImageView) v.findViewById(R.id.gameplay_questionbar1)).setImageResource(R.drawable.gameplay_question_unselected_left);
		((ImageView) v.findViewById(R.id.gameplay_questionbar2)).setImageResource(R.drawable.gameplay_question_unselected_left);
		((ImageView) v.findViewById(R.id.gameplay_questionbar3)).setImageResource(R.drawable.gameplay_question_unselected_left);

		v.findViewById(R.id.gameplay_question1_right_text).setBackgroundResource(R.drawable.gameplay_question_unselected_right);
		v.findViewById(R.id.gameplay_question2_right_text).setBackgroundResource(R.drawable.gameplay_question_unselected_right);
		v.findViewById(R.id.gameplay_question3_right_text).setBackgroundResource(R.drawable.gameplay_question_unselected_right);
	}

	private void setCorrectButton(int i) {
		setAllFalseButtons();

		ImageView thumb = null;

		switch (i) {
		case 0:
			((ImageView) v.findViewById(R.id.gameplay_questionbar1)).setImageResource(R.drawable.gameplay_answer_correct_left);
			v.findViewById(R.id.gameplay_question1_right_text).setBackgroundResource(R.drawable.gameplay_answer_correct_right);
			thumb = (ImageView) v.findViewById(R.id.thumbQuestion1);
			break;

		case 1:
			((ImageView) v.findViewById(R.id.gameplay_questionbar2)).setImageResource(R.drawable.gameplay_answer_correct_left);
			v.findViewById(R.id.gameplay_question2_right_text).setBackgroundResource(R.drawable.gameplay_answer_correct_right);
			thumb = (ImageView) v.findViewById(R.id.thumbQuestion2);
			break;
		case 2:
			((ImageView) v.findViewById(R.id.gameplay_questionbar3)).setImageResource(R.drawable.gameplay_answer_correct_left);
			v.findViewById(R.id.gameplay_question3_right_text).setBackgroundResource(R.drawable.gameplay_answer_correct_right);
			thumb = (ImageView) v.findViewById(R.id.thumbQuestion3);
			break;
		}

		if (thumb != null) {
			thumb.setImageResource(R.drawable.gameplay_thumb_correct);
			thumb.setVisibility(View.VISIBLE);
		}
	}

	private void setWrongButton(int i) {
		setAllFalseButtons();

		ImageView thumb = null;

		switch (i) {
		case 0:
			((ImageView) v.findViewById(R.id.gameplay_questionbar1)).setImageResource(R.drawable.gameplay_answer_incorrect_left);
			v.findViewById(R.id.gameplay_question1_right_text).setBackgroundResource(R.drawable.gameplay_answer_incorrect_right);
			thumb = (ImageView) v.findViewById(R.id.thumbQuestion1);
			break;
		case 1:
			((ImageView) v.findViewById(R.id.gameplay_questionbar2)).setImageResource(R.drawable.gameplay_answer_incorrect_left);
			v.findViewById(R.id.gameplay_question2_right_text).setBackgroundResource(R.drawable.gameplay_answer_incorrect_right);
			thumb = (ImageView) v.findViewById(R.id.thumbQuestion2);
			break;
		case 2:
			((ImageView) v.findViewById(R.id.gameplay_questionbar3)).setImageResource(R.drawable.gameplay_answer_incorrect_left);
			v.findViewById(R.id.gameplay_question3_right_text).setBackgroundResource(R.drawable.gameplay_answer_incorrect_right);
			thumb = (ImageView) v.findViewById(R.id.thumbQuestion3);
			break;
		}
		if (thumb != null) {
			thumb.setImageResource(R.drawable.gameplay_thumb_incorrect);
			thumb.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * Resets all the question buttons to their original state
	 */
	private void resetButtons() {
		((ImageView) v.findViewById(R.id.gameplay_questionbar1)).setImageResource(R.drawable.gameplay_question_left);
		((ImageView) v.findViewById(R.id.gameplay_questionbar2)).setImageResource(R.drawable.gameplay_question_left);
		((ImageView) v.findViewById(R.id.gameplay_questionbar3)).setImageResource(R.drawable.gameplay_question_left);

		v.findViewById(R.id.gameplay_question1_right_text).setBackgroundResource(R.drawable.gameplay_question_right);
		v.findViewById(R.id.gameplay_question2_right_text).setBackgroundResource(R.drawable.gameplay_question_right);
		v.findViewById(R.id.gameplay_question3_right_text).setBackgroundResource(R.drawable.gameplay_question_right);

		v.findViewById(R.id.currentScore).setVisibility(View.VISIBLE);
		v.findViewById(R.id.currentScoreImageView).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.currentScoreDiscImage).setVisibility(View.VISIBLE);
	}

	public void setCurrPoints(int i) {
		((TextView) v.findViewById(R.id.currentScore)).setText("" + i);
	}

	private OnGlobalLayoutListener mGlobalLayoutListener = new OnGlobalLayoutListener() {
		@SuppressWarnings("deprecation")
		@Override
		@SuppressLint("NewApi")
		public void onGlobalLayout() {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
				v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
			} else {
				v.getViewTreeObserver().removeGlobalOnLayoutListener(this);
			}

			scrollthingie();
		}
	};

	/**
	 * Called when next button is pressed
	 */
	private void startNextTrack() {
		showAnswerBars(false);

		hideThumbs();
		hideNextAnimation();
		hideAlbumArt();

		Helpers.setVisibilityIfExists(v.findViewById(R.id.progressBar2), View.VISIBLE);

		/**
		 * If all questions are answered send finish game etc and return until we
		 * have an answer
		 */
		if (question > maxQuestions) {
			v.findViewById(R.id.progressBar2).setVisibility(View.GONE);

			hideMainProgressBar();

			showAnswerBars(false);
			hideTrackScrollView();

			sendCloseGameRequest();

			if (offlineMode) {
				getActivity().finish();
			}

			return;
		}

		// Scroll the lower question indicator view
		scrollthingie();

		for (View v : gamesBar)
			v.setBackgroundColor(getResources().getColor(android.R.color.transparent));

		gamesBar.get(question).setBackgroundResource(R.drawable.gameplay_resultbar_main);

		if (!offlineMode) {
			String mp3Path = Helpers.getIntroMp3URL(questions[question].getCorrectTrackId());
			startPlayback(mp3Path, 0);
		} else {
			startPlayback(questions[question].getStreamURL(), 30000);
		}
	}

	/**
	 * Centers the view if the view will fit in the window Otherwise centers the
	 * current question
	 */
	protected void scrollthingie() {
		HorizontalScrollView hsv = (HorizontalScrollView) v.findViewById(R.id.horizontalScrollView1);

		int middleWidth = (int) Helpers.dpToPx(getActivity(), maxQuestions * 24);
		int maxWidth = hsv.getWidth();

		if (middleWidth != 0) {
			if (middleWidth > maxWidth) {
				hsv.smoothScrollTo((int) Helpers.dpToPx(getActivity(), 24 * question + 12), 0);
			}
            else {
				int test = hsv.getChildAt(0).getWidth() / 2 - maxWidth / 2;
				hsv.smoothScrollTo(test, 0);
			}
		}
	}

	private void hideThumbs() {
		Helpers.setVisibilityIfExists(v.findViewById(R.id.thumbQuestion1), View.INVISIBLE);
		Helpers.setVisibilityIfExists(v.findViewById(R.id.thumbQuestion2), View.INVISIBLE);
		Helpers.setVisibilityIfExists(v.findViewById(R.id.thumbQuestion3), View.INVISIBLE);
	}

	private void sendCloseGameRequest() {
		if (offlineMode)
			return;
		JSONObject jsonReq = JSONRequestHelpers.getFinaliseGameGameJSON(Storage.getUserProfile(getActivity()), getCurrentGame().getId());
		Storage.addRequestToQueue(getActivity(), Storage.FINALISE_GAME_URL, jsonReq,
				new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						Storage.updateUserProfile(getActivity(), response, true);
						((LoggedInMenu) getActivity()).updateUI();

						if (lastGameInfoReceived)
							try {
								showWhoWonFragment(response.getJSONObject("GameInfo"));
							} catch (JSONException e) {
								e.printStackTrace();
								((LoggedInMenu) getActivity()).setShouldBlockBackButton(false);
							}
						else {
							((LoggedInMenu) getActivity()).setShouldBlockBackButton(false);
						}
					}

					@Override
					public void badResult(String errorMessage) {
						((LoggedInMenu) getActivity()).setShouldBlockBackButton(false);
					}
				});
	}

	/**
	 * Timer class that represents the main timer bar in a game
	 */
	public static class SongBlaTimer extends CountDownTimer {
		private long maxTime;
		private ProgressBar pb;
		private long curr;
		private GameFragment gameFragment;
		private Drawable[] pbs;
		private Drawable currPB;
		private boolean offlineMode;

		public SongBlaTimer(long millisInFuture, long countDownInterval, ProgressBar pb, GameFragment gameFragment, Drawable[] pbs, boolean offlineMode) {
			super(millisInFuture, countDownInterval);
			maxTime = millisInFuture;
			this.pb = pb;
			curr = maxTime;
			this.gameFragment = gameFragment;
			this.pbs = pbs;
			this.offlineMode = offlineMode;
			currPB = pbs[0];
		}

		public long getElapsedTime() {
			return maxTime - curr;
		}

		@Override
		public void onFinish() {
			gameFragment.setCurrPoints(0);
			gameFragment.answer(-1);
		}

		@Override
		public void onTick(long millisUntilFinished) {
			if (pb != null) {
				double p = (double) millisUntilFinished / (double) maxTime;

				int mepa = (int) (p * pbs.length);
				if (offlineMode)
					gameFragment.setCurrPoints((int) (p * gameFragment.maxScore / 10));
				else
					gameFragment.setCurrPoints((int) (p * gameFragment.maxScore / (gameFragment.maxQuestions + 1)));

				p *= pb.getMax();
				pb.setProgress((int) p);

				if (mepa >= 0 && mepa < pbs.length && pbs[mepa] != currPB) {
					pb.setProgressDrawable(pbs[mepa]);
					currPB = pbs[mepa];
				}
			}
			curr = millisUntilFinished;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			offlineMode = getArguments().getBoolean("offlineMode");

			if (!offlineMode)
				currentGame = new OnlineGame(new JSONObject(getArguments().getString("gameInfo")));
			else {
				currentGame = new OfflineGame();
			}

			try {
				maxTimeToAnswer = currentGame.getLevelType(getActivity()).getMaxTimeToAnswer();
				maxScore = currentGame.getLevelType(getActivity()).getMaxScoreReward();
			} catch (Exception e) {
				e.printStackTrace();
			}

			TrackerHelpers.packStarted(getActivity(), currentGame.getPackId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static GameFragment newInstance(String gameInfo, boolean offline) {
		GameFragment f = new GameFragment();
		Bundle b = new Bundle();
		b.putString("gameInfo", gameInfo);
		b.putBoolean("offlineMode", offline);
		f.setArguments(b);
		return f;
	}

	@Override
	public String getName() {
		return TAG;
	}
}
