package com.musicplode.beattheintro.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;

/**
 * Created by Daniel on 20/06/2014.
 */
@SuppressWarnings("DefaultFileTemplate")
public class ChoosePartyPlayOpponentsFragment extends BaseFragment {
    private View v;
    private int separatorStartY;
    private int separatorHeight;
    private int containerId;
    private String packId;
    private String levelId;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.choosepartyplay, container, false);

        Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));

        /**
         * Fix tablet divider position between bestmatch and choose opponent menu
         */
        if (v.findViewById(R.id.choosePartyPlaySeparator) != null) {
            ImageView iv = (ImageView) v.findViewById(R.id.choosePartyPlaySeparator);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) iv.getLayoutParams();
            lp.topMargin = separatorStartY;
            lp.height = separatorHeight;
            iv.setLayoutParams(lp);
        }

        setOnClickListenerIfViewExists(v.findViewById(R.id.choosePartyPlayTwoPlayers), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(containerId, PartyPlayLobbyFragment.newInstance(2, packId, levelId));
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        setOnClickListenerIfViewExists(v.findViewById(R.id.choosePartyPlayThreePlayers), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(containerId, PartyPlayLobbyFragment.newInstance(3, packId, levelId));
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        setOnClickListenerIfViewExists(v.findViewById(R.id.choosePartyPlayFourPlayers), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(containerId, PartyPlayLobbyFragment.newInstance(4, packId, levelId));
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        setOnClickListenerIfViewExists(v.findViewById(R.id.choosePartyPlayFivePlayers), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(containerId, PartyPlayLobbyFragment.newInstance(5, packId, levelId));
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        setOnClickListenerIfViewExists(v.findViewById(R.id.choosePartyPlaySixPlayers), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(containerId, PartyPlayLobbyFragment.newInstance(6, packId, levelId));
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        Helpers.setAllFonts(v, getActivity());

        return v;
    }

    private static void setOnClickListenerIfViewExists(View v, View.OnClickListener listener) {
        if (v != null) {
            System.out.println("exists");
            v.setOnClickListener(listener);
        }
    }

    @Override
    public String getName() {
        return "ChoosePartyPlayOpponents";
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        if (getArguments() != null) {
            separatorStartY = getArguments().getInt("separatorStartY");
            separatorHeight = getArguments().getInt("separatorHeight");
            containerId = getArguments().getInt("containerId");
            packId = getArguments().getString("packId");
            levelId = getArguments().getString("levelId");
        }
    }
    public static Fragment newInstance(String packId, String levelId, int separatorStartY, int separatorHeight, int containerId) {
        ChoosePartyPlayOpponentsFragment elf = new ChoosePartyPlayOpponentsFragment();
        Bundle b = new Bundle();
        b.putInt("separatorStartY", separatorStartY);
        b.putInt("separatorHeight", separatorHeight);
        b.putInt("containerId", containerId);
        b.putString("packId", packId);
        b.putString("levelId", levelId);
        elf.setArguments(b);

        return elf;
    }

    public static Fragment newInstance(String packId, String levelId) {
        return newInstance(packId, levelId, 0, 0, 0);
    }
}
