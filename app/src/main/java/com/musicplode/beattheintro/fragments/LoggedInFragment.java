package com.musicplode.beattheintro.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
// import android.widget.Button;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.storage.Storage;

public class LoggedInFragment extends BaseFragment {
	private View v;
	private String startGame;
	private static final int ANIMATION_DURATION = 400;
	private static final int ANIMATION_START = 50;
	private static final int ANIMATION_ADD = 200;
	
	
	@Override
	public String getName() {
		return "LoggedInMenu";
	}
	@Override
	public void onResume() {
		super.onResume();
		
		Helpers.refreshProfile(getActivity());

		((LoggedInMenu)getActivity()).disableLowerChart();
		((LoggedInMenu)getActivity()).enableAds();
		((LoggedInMenu)getActivity()).updateDiscAchievement();
		((LoggedInMenu)getActivity()).stopMediaPlayers();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.logged_in_screen, container, false);

		/**
		 * Add the slide in animations for the main menu buttons
		 */
		Animation slideIn = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left);
		slideIn.setDuration(ANIMATION_DURATION);
		slideIn.setStartOffset(ANIMATION_START);
		// slideIn.setAnimationListener(getOnDoneStartSwirl(v.findViewById(R.id.menuSwirl1)));
		v.findViewById(R.id.menuPlayButton).setAnimation(slideIn);
		
		Animation slideIn2 = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left);
		slideIn2.setDuration(ANIMATION_DURATION);
		slideIn2.setStartOffset(ANIMATION_START + ANIMATION_ADD);
		// slideIn2.setAnimationListener(getOnDoneStartSwirl(v.findViewById(R.id.menuSwirl2)));
		v.findViewById(R.id.menuMultiplayerlayButton).setAnimation(slideIn2);
		
		Animation slideIn3 = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left);
		slideIn3.setDuration(ANIMATION_DURATION);
		slideIn3.setStartOffset(ANIMATION_START + ANIMATION_ADD * 2);
		// slideIn3.setAnimationListener(getOnDoneStartSwirl(v.findViewById(R.id.menuSwirl3)));
		v.findViewById(R.id.menuMyGameslayButton).setAnimation(slideIn3);
		
		Animation slideIn4 = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_right);
		slideIn4.setDuration(ANIMATION_DURATION);
		slideIn4.setStartOffset(ANIMATION_START + ANIMATION_ADD * 3);
		// slideIn4.setAnimationListener(getOnDoneStartSwirl(v.findViewById(R.id.menuSwirl4)));
		v.findViewById(R.id.menuLeaderboardsButton).setAnimation(slideIn4);
		
		Animation slideIn5 = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_right);
		slideIn5.setDuration(ANIMATION_DURATION);
		slideIn5.setStartOffset(ANIMATION_START + ANIMATION_ADD * 4);
		slideIn5.setAnimationListener(getOnDoneStartSwirl());
		v.findViewById(R.id.menuMyBasketButton).setAnimation(slideIn5);

		v.findViewById(R.id.menuPlayButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startExitAnim(container.getId(), PackSelectFragment.newInstance(false), null);
			}
		});
		
		v.findViewById(R.id.menuMultiplayerlayButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startExitAnim(container.getId(), PackSelectFragment.newInstance(true), null);
			}
		});
		
		v.findViewById(R.id.menuMyGameslayButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startExitAnim(container.getId(), new MyGamesFragment(), null);
			}
		});
		
		v.findViewById(R.id.menuLeaderboardsButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startExitAnim(container.getId(), new LeaderboardFragment(), null);
			}
		});
		
		v.findViewById(R.id.menuMyBasketButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startExitAnim(container.getId(), new MusicBasketFragment(), LoggedInMenu.LOGGED_IN_MENU_STACK_KEY);
			}
		});

		Helpers.setAllFonts(v, getActivity());

		String startGame = Storage.getStoredString(getActivity(), Storage.START_GAME_KEY);
		if (startGame != null && Helpers.hasUnfinishedGameById(getActivity(), startGame)) {
			// Remove startgame key from pref
			Storage.remove(getActivity(), Storage.START_GAME_KEY);
			
			final ProgressDialog pd = new ProgressDialog(getActivity());
			pd.setMessage(getString(R.string.LOADING));
			pd.setCancelable(false);
			pd.show();
			
			Helpers.startGameById((LoggedInMenu) getActivity(), container.getId(), pd, startGame);
		}

		((LoggedInMenu)getActivity()).updateDiscAchievement(); // TEST: this seems to have fixed the progress chart error where the pick can erroneously show 100 (hard left)

		return v;
	}
	
	protected void startExitAnim(final int containerId, final Fragment startFragment, final String backStackKey) {
		v.findViewById(R.id.menuSwirl1).clearAnimation();
		v.findViewById(R.id.menuSwirl1).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.menuSwirl2).clearAnimation();
		v.findViewById(R.id.menuSwirl2).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.menuSwirl3).clearAnimation();
		v.findViewById(R.id.menuSwirl3).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.menuSwirl4).clearAnimation();
		v.findViewById(R.id.menuSwirl4).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.menuSwirl5).clearAnimation();
		v.findViewById(R.id.menuSwirl5).setVisibility(View.INVISIBLE);
		
		Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.out_left);
		anim.setDuration(200);
		anim.setAnimationListener(new AnimationListener() {
			public void onAnimationStart(Animation animation) { }
			public void onAnimationRepeat(Animation animation) { }

			@Override
			public void onAnimationEnd(Animation animation) {
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.setCustomAnimations(R.anim.in_left_offsetzero, 0);
				ft.replace(containerId, startFragment);
				ft.addToBackStack(backStackKey);
				ft.commit();
			}
		});
		
		v.findViewById(R.id.menuPlayButton).setAnimation(anim);
		v.findViewById(R.id.menuMultiplayerlayButton).setAnimation(anim);
		v.findViewById(R.id.menuMyGameslayButton).setAnimation(anim);
		
		
		Animation animRight = AnimationUtils.loadAnimation(getActivity(), R.anim.out_right);
		v.findViewById(R.id.menuLeaderboardsButton).setAnimation(animRight);
		v.findViewById(R.id.menuMyBasketButton).setAnimation(animRight);
		

		v.findViewById(R.id.menuPlayButton).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.menuMultiplayerlayButton).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.menuMyGameslayButton).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.menuLeaderboardsButton).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.menuMyBasketButton).setVisibility(View.INVISIBLE);
	}
	
	
	
	/** private AnimationListener getOnDoneStartSwirl(final View swirlView) {
		return new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				swirlView.setVisibility(View.INVISIBLE);
			}
			@Override
			public void onAnimationRepeat(Animation animation) {
				
			}
			@Override
			public void onAnimationEnd(Animation animation) {
				swirlView.setVisibility(View.VISIBLE);
				Animation pulse = AnimationUtils.loadAnimation(getActivity(), R.anim.pulse); // used to be clockwise_rotation
				swirlView.startAnimation(pulse);
				animation.setAnimationListener(null);
			}
			
		};
	} */

	private AnimationListener getOnDoneStartSwirl() {
		return new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				v.findViewById(R.id.menuSwirl1).setVisibility(View.INVISIBLE);
				v.findViewById(R.id.menuSwirl2).setVisibility(View.INVISIBLE);
				v.findViewById(R.id.menuSwirl3).setVisibility(View.INVISIBLE);
				v.findViewById(R.id.menuSwirl4).setVisibility(View.INVISIBLE);
				v.findViewById(R.id.menuSwirl5).setVisibility(View.INVISIBLE);
			}
			@Override
			public void onAnimationRepeat(Animation animation) {

			}
			@Override
			public void onAnimationEnd(Animation animation) {
				/**
				 * Add the smaller pulsing rings here, so that they are all in sync
				 */
				Helpers.addPulse(v.findViewById(R.id.menuSwirl1), 0);
				Helpers.addPulse(v.findViewById(R.id.menuSwirl2), 0);
				Helpers.addPulse(v.findViewById(R.id.menuSwirl3), 0);
				Helpers.addPulse(v.findViewById(R.id.menuSwirl4), 0);
				Helpers.addPulse(v.findViewById(R.id.menuSwirl5), 0);
				animation.setAnimationListener(null);
			}

		};
	}



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			startGame = getArguments().getString("startGame");
			
			if (startGame != null)
				getArguments().remove("startGame");
		}
	}
	public static Fragment newInstance() {
		return new LoggedInFragment();
	}
	
}
