package com.musicplode.beattheintro.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;

/**
 * How to Play Page Fragment
 */
public class HTPFragmentInstance extends Fragment {
	private int num;
	private int count;
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.htp, container, false);
		
		LinearLayout ll = (LinearLayout) v.findViewById(R.id.htpDotLayout);
		
		ll.removeAllViews();

        /**
         * Setup the bottom pager
         */
		for (int i = 0; i < count; i++) {
			ImageView iv = new ImageView(getActivity());
			if (i == num)
				iv.setImageResource(R.drawable.htp_dot_current);
			else
				iv.setImageResource(R.drawable.htp_dot);
			ll.addView(iv);
		}


        /**
         * Set the correct texts
         */
		String[] htpTexts = getResources().getStringArray(R.array.HTPTEXTS);
		String[] htpTitlesTexts = getResources().getStringArray(R.array.HTPTITLES);
		String text = htpTexts[num];
		String title = htpTitlesTexts[num];
		
		setText((TextView) v.findViewById(R.id.htpTitleTextView), title);
		setText((TextView) v.findViewById(R.id.htpTextView), text);
		
		ImageView icon = (ImageView) v.findViewById(R.id.htpIconImageView);
		ImageView image = (ImageView) v.findViewById(R.id.htpImageView);
		View leftarrow = v.findViewById(R.id.htpLeftArrow);
		View rightarrow = v.findViewById(R.id.htpRightArrow);

		/**
         * Set the correct Image & Icon & left/right arrows
         */
		switch (num) {
		case 0:
			icon.setImageResource(R.drawable.htp1_icon);
			image.setImageResource(R.drawable.htp1_image);
			leftarrow.setVisibility(View.INVISIBLE);
			break;
		case 1:
			icon.setImageResource(R.drawable.htp2_icon);
			image.setImageResource(R.drawable.htp2_image);
			leftarrow.setVisibility(View.VISIBLE);
			break;
		case 2:
			icon.setImageResource(R.drawable.htp3_icon);
			image.setImageResource(R.drawable.htp3_image);
			break;
		case 3:
			icon.setImageResource(R.drawable.htp4_icon);
			image.setImageResource(R.drawable.htp4_image);
			break;
		case 4:
			icon.setImageResource(R.drawable.htp5_icon);
			image.setImageResource(R.drawable.htp5_image);
			break;
		case 5:
			icon.setImageResource(R.drawable.htp6_icon);
			image.setImageResource(R.drawable.htp6_image);
			rightarrow.setVisibility(View.VISIBLE);
			break;
		case 6:
			icon.setImageResource(R.drawable.htp7_icon);
			image.setImageResource(R.drawable.htp7_image);
			rightarrow.setVisibility(View.INVISIBLE);
			break;
			
		default:
			break;
		}

		/**
		 * Don't call Helper routine, as we always need to have the Close X button - even on Amazon devices
		 */
		v.findViewById(R.id.backSwirl).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				getActivity().onBackPressed();
			}
		});

		Helpers.addPulse(v.findViewById(R.id.backSwirl), 1); // set up larger pulsing animation with alpha glow
		Helpers.addPulse(v.findViewById(R.id.backButtonRing), 0);  // set up smaller pulsing animation
		Helpers.setAllFonts(v, getActivity());
		
		return v;
	}

	private static void setText(TextView tv, String text) {
		if (tv == null)
			return;
		if (text == null)
			return;
		
		tv.setText(text);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		num = getArguments() != null ? getArguments().getInt("num") : 0;
		count = getArguments() != null ? getArguments().getInt("count") : 0;
	}
	public static Fragment newInstance(int num, int count) {
		HTPFragmentInstance htp = new HTPFragmentInstance();
		Bundle b = new Bundle();
		b.putInt("num", num);
		b.putInt("count", count);
		htp.setArguments(b);
		
		return htp;
	}
}
