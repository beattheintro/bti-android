package com.musicplode.beattheintro.fragments;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.LoggedInMenuLand;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.helpers.TwitterStuff;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

public class TwitterLoginFragment extends BaseFragment {
	@Override
	public String getName() {
		return "TwitterLogin";
	}
	private View v;
	private static final String CALLBACK_URL = "http://www.twitter.com";

    private WebView mWebView;
    private ProgressBar progressBar;
    private Token mRequestToken;
	private OAuthService mService;
	private boolean backOnDone = false;
	private boolean loginDone = false;
	private boolean isTablet;
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.twitterloginnew, container, false);
		
		isTablet = getResources().getBoolean(R.bool.tablet);
		
		mService = new ServiceBuilder()
            .provider(TwitterApi.class)
            .apiKey(TwitterStuff.TWITTER_CONSUMER_KEY)
            .apiSecret(TwitterStuff.TWITTER_CONSUMER_SECRET)
            .callback(TwitterLoginFragment.CALLBACK_URL)
            .build();

		mWebView = (WebView) v.findViewById(R.id.webView1);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		mWebView.clearCache(true);
		
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.getSettings().setDisplayZoomControls(false);
		mWebView.setWebViewClient(mWebViewClient);
		
		mWebView.setVisibility(View.INVISIBLE);
		progressBar.setVisibility(View.VISIBLE);
		startAuthorize();
		
		return v;
	}
	
	private void startAuthorize() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                try {
					mRequestToken = mService.getRequestToken();
					return mService.getAuthorizationUrl(mRequestToken);
				} catch (Exception e) {
					e.printStackTrace();
				}
                return null;
            }

            @Override
            protected void onPostExecute(String url) {
            	if (url == null) {
            		mWebView.setVisibility(View.INVISIBLE);
            		DialogHelpers.showErrorAndGoBackOnOkDialog(getActivity(), R.string.ERROR_INTERNET_CONNECTION_TITLE, R.string.ERROR_INTERNET_CONNECTION_TEXT, R.string.OK);
            	}
            	else
            		mWebView.loadUrl(url);
            }
        }.execute();
    }

    private WebViewClient mWebViewClient = new WebViewClient() {
    	@Override
		public void onPageFinished(WebView view, String url) {
    		if (!loginDone) {
	    		view.setVisibility(View.VISIBLE);
	    		progressBar.setVisibility(View.INVISIBLE);
    		}
    	}
    	
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (url != null && url.startsWith(CALLBACK_URL)) { // Override webview when user came back to CALLBACK_URL
            	mWebView.setVisibility(View.INVISIBLE); // Hide webview if necessary
                loginDone = true;
            	mWebView.stopLoading();
            	v.findViewById(R.id.progressBar1).setVisibility(View.VISIBLE);
            	
                Uri uri = Uri.parse(url);
                try {
					final Verifier verifier = new Verifier(uri.getQueryParameter("oauth_verifier"));
					new AsyncTask<Void, Void, Token>() {
					    @Override
					    protected Token doInBackground(Void... params) {
					        return mService.getAccessToken(mRequestToken, verifier);
					    }

					    @Override
					    protected void onPostExecute(final Token accessToken) {
					    	Storage.storeTwitterToken(getActivity(), accessToken);
					    	
					    	if (backOnDone) {
					    		String extraString = getActivity().getIntent() != null ? getActivity().getIntent().getStringExtra("extraString") : null;
					    		
					    		if (extraString != null) {
					    			Intent i = new Intent();
						    		i.putExtra("extraString", extraString);
						    		getActivity().setResult(Activity.RESULT_OK, i);
					    		}
					    		else
					    			getActivity().setResult(Activity.RESULT_OK);
					    		
					    		getActivity().finish();
					    	}
					    	else {
					        	new TwitterStuff.VerifyCredentials() {
									@Override
									public void onResult(String s) {
										loginViaTwitter(s);
									}
								}.execute(accessToken);
					    	}
					    }
					}.execute();
				} catch (Exception e) {
					e.printStackTrace();
					getActivity().onBackPressed();
				}
            } else {
                super.onPageStarted(view, url, favicon);
            }
        }
    };
    
    private void loginViaTwitter(String s) {
		try {
			JSONObject o = new JSONObject(s);

			String name = o.getString("name");
			String id = o.getString("id_str");
			String email = "nej@nej.nej";
			String token = Storage.getTwitterToken(getActivity()).getToken();	

			JSONObject json = JSONRequestHelpers.getLoginByTwitterJSON(name, id, email, token);
			Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, json, new OnNetworkResult() {
				@Override
				public void okResult(JSONObject response) {
					if (getActivity() != null) {
						Storage.store(getActivity(), Storage.USERPROFILE, response);
						
						Intent intent;
						if (isTablet)
							intent = new Intent(getActivity(), LoggedInMenuLand.class);
						else
							intent = new Intent(getActivity(), LoggedInMenu.class);
						
						startActivity(intent);
						getActivity().finish();
					}
				}
				
				@Override
				public void badResult(String errorMessage) {
				}
			});
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	
	}

    
	public void setBackOnDone() {
		backOnDone = true;
	}
}
