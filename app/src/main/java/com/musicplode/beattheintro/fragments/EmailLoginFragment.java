package com.musicplode.beattheintro.fragments;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.LoggedInMenuLand;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.DialogHelpers.OnRetryButton;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONException;
import org.json.JSONObject;

public class EmailLoginFragment extends BaseFragment {
	@Override
	public String getName() {
		return "EmailLogin";
	}
	private Button signInButton;
	private EditText emailTextView;
	private EditText passwordTextView;
	private EditText usernameEditText;
	private int separatorStartY;
	private int separatorHeight;
	private boolean isTablet;
	private View v;

	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.emaillogin, container, false);

		isTablet = getResources().getBoolean(R.bool.tablet);
		
		if (v.findViewById(R.id.emailLoginSeparator) != null) {
			ImageView iv = (ImageView) v.findViewById(R.id.emailLoginSeparator);
			
			LayoutParams lp = (LayoutParams) iv.getLayoutParams();
			lp.topMargin = separatorStartY;
			lp.height = separatorHeight;
			iv.setLayoutParams(lp);
		}
		
		signInButton = (Button) v.findViewById(R.id.emailSignInButton);

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
        Helpers.setAllFonts(v, getActivity());

		emailTextView = (EditText) v.findViewById(R.id.emailLoginForgotEmailEditText);
		usernameEditText = (EditText) v.findViewById(R.id.emailLoginUsernameEditText);
		passwordTextView = (EditText) v.findViewById(R.id.emailLoginPasswordEditText);
		
		final LinearLayout ll = (LinearLayout) v.findViewById(R.id.loginForgotPasswordLinearLayout);
		
		ll.getLayoutParams().height = 0;
        ll.requestLayout();
        final View closeForgotPasswordView = v.findViewById(R.id.loginCloseForgotPasswordView);
        closeForgotPasswordView.setVisibility(View.INVISIBLE);
        
		v.findViewById(R.id.emailLoginForgotPasswordButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                /**
                 * Show Reset button & reset-email field
                 */
				ValueAnimator va = ValueAnimator.ofInt(0, (int)Helpers.dpToPx(getActivity(), 98));
			    va.setDuration(300);
			    va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			        @Override
					public void onAnimationUpdate(ValueAnimator animation) {
			            Integer value = (Integer) animation.getAnimatedValue();
			            ll.getLayoutParams().height = value;
			            ll.requestLayout();
			        }
			    });
			    va.addListener(new AnimatorListener() {
					@Override
					public void onAnimationStart(Animator animation) { }
					@Override
					public void onAnimationRepeat(Animator animation) { }
					@Override
					public void onAnimationEnd(Animator animation) {
						closeForgotPasswordView.setVisibility(View.VISIBLE);
					}
					@Override
					public void onAnimationCancel(Animator animation) { }
				});
			    va.start();
			}
		});
		
		v.findViewById(R.id.loginCloseForgotPasswordView).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                /**
                 * Hide Reset button & reset-email field
                 */
				ValueAnimator va = ValueAnimator.ofInt(ll.getLayoutParams().height, 0);
			    va.setDuration(300);
			    va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			        @Override
					public void onAnimationUpdate(ValueAnimator animation) {
			            Integer value = (Integer) animation.getAnimatedValue();
			            ll.getLayoutParams().height = value;
			            ll.requestLayout();
			        }
			    });
			    va.addListener(new AnimatorListener() {
					@Override
					public void onAnimationStart(Animator animation) { }
					@Override
					public void onAnimationRepeat(Animator animation) { }
					@Override
					public void onAnimationEnd(Animator animation) {
						closeForgotPasswordView.setVisibility(View.INVISIBLE);
					}
					@Override
					public void onAnimationCancel(Animator animation) { }
				});
			    va.start();
			}
		});
		
		
		
		v.findViewById(R.id.emailCreateFreeAccountButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                /**
                 * Start the CreateNewEmailAccountFragment
                 */
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				Helpers.setFragmentAnimation(ft);
				ft.replace(container.getId(), CreateNewEmailAccountFragment.newInstance(separatorStartY, separatorHeight));
				ft.addToBackStack(null);
				ft.commit();
			}
		});

		
		v.findViewById(R.id.emailSendReminderButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v2) {
                /**
                 * Send password reset to server
                 */
				try {
					if (emailTextView.getText().toString().contains("@")) {
						JSONObject o = new JSONObject();
						o.put("Email", emailTextView.getText().toString());
						o.put("ForgotPassword", true);
						
						Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
							@Override
							public void okResult(JSONObject response) {
								try {
									if (response.getBoolean("Success")) {
										DialogHelpers.showOkDialog(getActivity(), R.string.EMAIL_REMINDER_SENT_TITLE, 0, R.string.OK);
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							
							@Override
							public void badResult(String errorMessage) {
								if (errorMessage == null) {
									DialogHelpers.showErrorDialog(getActivity(), R.string.ERROR_INTERNET_CONNECTION_TITLE, R.string.ERROR_INTERNET_CONNECTION_TEXT, R.string.ERROR_RETRY_BUTTON, R.string.CANCEL, new OnRetryButton() {
										@Override
										public void onRetry() {
											v.findViewById(R.id.emailSendReminderButton).performClick();
										}
									});
								}
								else {
									DialogHelpers.showHardCodedDialog(getActivity(), getString(R.string.ERROR_TITLE), errorMessage);
								}
							}
						});
					}
					else {
						Toast.makeText(getActivity(), "Enter email first", Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
		
		v.findViewById(R.id.emailSignInButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                /**
                 * Login with the entered values
                 */
				signInButton.setEnabled(false);
				
				JSONObject postJSON = JSONRequestHelpers.getLoginByEmailJSON(usernameEditText.getText().toString(), passwordTextView.getText().toString());
				
				Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, postJSON, new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						Storage.store(EmailLoginFragment.this.getActivity(), Storage.USERPROFILE, response);
						
						InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE); 
						try {
							inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						Intent intent;
						if (isTablet)
							intent = new Intent(getActivity(), LoggedInMenuLand.class);
						else
							intent = new Intent(getActivity(), LoggedInMenu.class);
						
						startActivity(intent);
						getActivity().finish();						
					}
					
					@Override
					public void badResult(String errorMessage) {
						if (errorMessage == null) {
							DialogHelpers.showErrorDialog(getActivity(), R.string.ERROR_INTERNET_CONNECTION_TITLE, R.string.ERROR_INTERNET_CONNECTION_TEXT, R.string.OK);
						}
						else {
							DialogHelpers.showHardCodedDialog(getActivity(), getString(R.string.ERROR_TITLE), errorMessage);
						}
						
						signInButton.setEnabled(true);
					}
				});
			}
		});
		
		return v;
	}
	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
		if (getArguments() != null) {
			separatorStartY = getArguments().getInt("separatorStartY");
			separatorHeight = getArguments().getInt("separatorHeight");
		}
	}
	public static Fragment newInstance(int separatorStartY, int separatorHeight) {
		EmailLoginFragment f = new EmailLoginFragment();
		Bundle b = new Bundle();
		b.putInt("separatorStartY", separatorStartY);
		b.putInt("separatorHeight", separatorHeight);
		f.setArguments(b);
		
		return f;
	}
}
