package com.musicplode.beattheintro.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.OpenRequest;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.musicplode.beattheintro.FbLoginActivity;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.TwitterLoginActivity;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.helpers.TwitterStuff;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.model.Token;

import java.util.List;

/**
 * Helper class that holds a bunch of useful methods for facebook & twitter interaction
 */
public abstract class SocialHelperFragment extends BaseFragment {
	private static final int LIKERESULTCODE = 1232;
	private static final int TWITTERFOLLOW = 1235;
	private static final int TWITTERTWEET = 1236;
	public static final int TWITTERLOGIN = 1237;
	private static final int FACEBOOKSHARERESULTCODE = 1238;
	private static final int FACEBOOKLOGINRESULTCODE = 1239;

	
	/***************************************************
	 * Override these for callbacks
	 ***************************************************/
	public void onFollowResult(boolean follows) { }
	public void onTwitterLogedIn() { }
	public void onTweeted() { }
	public void onTweetFail() { }
	public void onMoreFollowers(JSONObject followers) { }
	public void onLikedResult(boolean result) { }
	protected void onAlreadyLiked() { }
    public void onFacebookLogedIn() { }

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    uiHelper.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == LIKERESULTCODE) {
                likeBTI(false);
            } else if (requestCode == TWITTERFOLLOW) {
                onTwitterLogedIn();

                twitterFollowBTI(false);
            } else if (requestCode == TWITTERTWEET) {
                onTwitterLogedIn();

                String s = data.getStringExtra("extraString");
                if (s != null)
                    tweetScore(false, s);
            } else if (requestCode == TWITTERLOGIN) {
                onTwitterLogedIn();
            } else if (requestCode == FACEBOOKSHARERESULTCODE) {
                String facebookTitle = data.getStringExtra("facebookTitle");
                String facebookDesc = data.getStringExtra("facebookDesc");
                String facebookCaption = data.getStringExtra("facebookTitle");
                String facebookLink = data.getStringExtra("facebookLink");
                String facebookPicture = data.getStringExtra("facebookPicture");

                facebookShareWithoutApp(false, facebookTitle, facebookDesc, facebookCaption, facebookLink, facebookPicture);
            } else if (requestCode == FACEBOOKLOGINRESULTCODE) {
                onFacebookLogedIn();
            }
        }
	}

	
	
	/***************************************************
	 * Twitter
	 ***************************************************/
	public void tweetScore(boolean canLogin, String message) {
		Token t = Storage.getTwitterToken(getActivity());
		
		if (t != null) {
			new TwitterStuff.Tweet() {
				@Override
				public void onResult(String s) {
					if (s == null)
						onTweetFail();
					else {
						onTweeted();
					}
				}
			}.execute(message, Storage.getTwitterToken(getActivity()));
		}
		else if (canLogin)
			loginToTwitter(TWITTERTWEET, message);
	}
	
	public void twitterFollowBTI(boolean canLogin) {
		Token t = Storage.getTwitterToken(getActivity());
		
		if (t != null) {
			new TwitterStuff.FollowBti() {
				@Override
				public void onResult(String s) {
					JSONObject o;
					try {
						o = new JSONObject(s);
						onFollowResult(!o.optBoolean("following"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}.execute(Storage.getTwitterToken(getActivity()));
		}
		
		if (t == null) {
			loginToTwitter(TWITTERFOLLOW, null);
		}
	}
	
	public void twitterGetFollowers(boolean canLogin) {
		Token t = Storage.getTwitterToken(getActivity());
		
		if (t != null) {
			new TwitterStuff.GetFollowers() {
				@Override
				public void onResult(String s) {
					JSONObject o;
					try {
						o = new JSONObject(s);
						onMoreFollowers(o);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}.execute(Storage.getTwitterToken(getActivity()));
		}
		
		if (t == null) {
			loginToTwitter(TWITTERFOLLOW, null);
		}
	}
	
	public void loginToTwitter(int resultCode, String extraString) {
		Intent i = new Intent(getActivity(), TwitterLoginActivity.class);
		if (extraString != null)
			i.putExtra("extraString", extraString);
		startActivityForResult(i, resultCode);
	}
	
	/***************************************************
	 * Facebook
	 ***************************************************/
	public void facebookLogin() {
		if (Session.getActiveSession() == null || Session.getActiveSession().isClosed() || !Session.getActiveSession().isOpened()) {

			if (Session.getActiveSession() != null)
				Session.getActiveSession().closeAndClearTokenInformation();
			
			startActivityForResult(new Intent(getActivity(), FbLoginActivity.class), FACEBOOKLOGINRESULTCODE);
		}
	}
	protected static Session openActiveSession(Activity activity, boolean allowLoginUI, StatusCallback callback, List<String> permissions, boolean publish) {
	    OpenRequest openRequest = new OpenRequest(activity).setPermissions(permissions).setCallback(callback);
	    Session session = new Session.Builder(activity).build();
	    if (SessionState.CREATED_TOKEN_LOADED.equals(session.getState()) || allowLoginUI) {
	        Session.setActiveSession(session);
	        if (publish)
	        	session.openForPublish(openRequest);
	        else
	        	session.openForRead(openRequest);
	        return session;
	    }
	    return null;
	}


	protected boolean isLoggedInToFacebook() {
		if (Session.getActiveSession() == null || Session.getActiveSession().isClosed() || !Session.getActiveSession().isOpened()) {

			if (Session.getActiveSession() != null)
				Session.getActiveSession().closeAndClearTokenInformation();
			
			return false;
		}
		return true;
	}
	
	
	public void likeBTI(boolean allowLogin) {
		if (Session.getActiveSession() == null || Session.getActiveSession().isClosed() || !Session.getActiveSession().isOpened() || !Session.getActiveSession().getPermissions().contains("publish_actions")) {
			if (Session.getActiveSession() != null) {
				Session.getActiveSession().closeAndClearTokenInformation();
			}
			startActivityForResult(new Intent(getActivity(), FbLoginActivity.class), LIKERESULTCODE);
			return;
		}
		
		Bundle params = new Bundle();
		params.putString("object", "http://graph.facebook.com/304158029714006");
		new Request(
		    Session.getActiveSession(),
		    "/me/og.likes",
		    params,
		    HttpMethod.POST,
		    new Request.Callback() {
		        @Override
				public void onCompleted(Response response) {
		        	if (response == null)
		        		onLikedResult(false);

		        	if (response.getError() == null) {
		        		onLikedResult(true);
		        	}
		        	else {
		        		if (response.getError().getErrorCode() == 3501)
		        			onAlreadyLiked();

		        		onLikedResult(false);
		        	}
		        }
		    }
		).executeAsync();
	}

	public void facebookShareWithoutApp(boolean allowLogin, String facebookTitle, String facebookDesc, String facebookCaption, String facebookLink, String facebookPicture) {
		if (allowLogin && (Session.getActiveSession() == null || Session.getActiveSession().isClosed() || !Session.getActiveSession().isOpened())) {

			if (Session.getActiveSession() != null)
				Session.getActiveSession().closeAndClearTokenInformation();
			
			
			Intent i = new Intent(getActivity(), FbLoginActivity.class);
			i.putExtra("facebookTitle", facebookTitle);
			i.putExtra("facebookDesc", facebookDesc);
			i.putExtra("facebookCaption", facebookCaption);
			i.putExtra("facebookLink", facebookLink);
			i.putExtra("facebookPicture", facebookPicture);
			
			startActivityForResult(new Intent(getActivity(), FbLoginActivity.class), FACEBOOKSHARERESULTCODE);
			
			return;
		}
		
		if (facebookTitle == null)
			return;

		Bundle params = new Bundle();
		params.putString("name", facebookTitle);
		params.putString("caption", facebookCaption);
		params.putString("description", facebookDesc);
		params.putString("link", facebookLink);
		params.putString("picture", facebookPicture);
		
		WebDialog feedDialog = new WebDialog.FeedDialogBuilder(getActivity(),
		    Session.getActiveSession(),
		    params)
		    .setOnCompleteListener(new OnCompleteListener() {
		
		        @Override
		        public void onComplete(Bundle values,
		            FacebookException error) {
		            if (error == null) {
		                // When the story is posted, echo the success
					// and the post Id.
					            	
					final String postId = values.getString("post_id");
					if (postId != null) {
					    Toast.makeText(getActivity(),
					        "Posted story, id: "+postId,
					        Toast.LENGTH_SHORT).show();
					} else {
					    // User clicked the Cancel button
					Toast.makeText(getActivity().getApplicationContext(), 
					    "Publish cancelled", 
					            Toast.LENGTH_SHORT).show();
					    }
					} else if (error instanceof FacebookOperationCanceledException) {
					    // User clicked the "x" button
					Toast.makeText(getActivity().getApplicationContext(), 
					    "Publish cancelled", 
					        Toast.LENGTH_SHORT).show();
					} else {
					    // Generic, ex: network error
					Toast.makeText(getActivity().getApplicationContext(), 
					    "Error posting story", 
					                    Toast.LENGTH_SHORT).show();
					            }
					        }
					    })
		    .build();
		feedDialog.show();
	}
	
	private static final String TAG = "FacebookLoginFragment";
	private UiLifecycleHelper uiHelper;
	private GraphUser user;
	private boolean logingIn = false;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    uiHelper = new UiLifecycleHelper(getActivity(), callback);
	    uiHelper.onCreate(savedInstanceState);
	}

	
	
	@Override
	public void onResume() {
		super.onResume();

		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}
		uiHelper.onResume();
	}
	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}

	private void loginToBTI() {
		Session session = Session.getActiveSession();
		String accessToken = session.getAccessToken();
		
		if (accessToken != null && user != null && !logingIn ) {
        	logingIn = true;
        	
        	String email = "";
        	try {
				email = user.getInnerJSONObject().optString("email", "");
			} catch (Exception e) {
				e.printStackTrace();
			}
        	
        	JSONObject userProfile = Storage.getUserProfile(getActivity());
        	
        	JSONObject loginBla;
        	if (userProfile != null)
        		loginBla = JSONRequestHelpers.getCreateFacebookUserJSON(userProfile, user.getId(), user.getName(), email, accessToken);
        	else
        		loginBla = JSONRequestHelpers.getLoginByFacebookJSON(user.getId(), user.getName(), email, accessToken);
        	
			if (loginBla != null) {
				
				Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, loginBla, new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						if (getActivity() != null) {
							Storage.store(SocialHelperFragment.this.getActivity(), Storage.USERPROFILE, response);
							
							Intent intent = new Intent(getActivity(), LoggedInMenu.class);
							startActivity(intent);
							getActivity().finish();		
						}
					}
					
					@Override
					public void badResult(String errorMessage) { }
				});
			}
		}
	}
	
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened() && session != null && session.isOpened() && session.getAccessToken() != null) {
	        Log.i(TAG, "Logged in...");
	        loginToBTI();
	    } 
	    else if (state.isClosed()) {
	        Log.i(TAG, "Logged out...");
	    }
	}
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
	        onSessionStateChange(session, state, exception);
	    }
	};
}
