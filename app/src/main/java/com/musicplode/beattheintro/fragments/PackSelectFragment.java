package com.musicplode.beattheintro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.Sounds;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.modelhelpers.Pack;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.support.TimelineGallery;
import com.musicplode.beattheintro.support.TimelineGalleryAdapterView;
import com.musicplode.beattheintro.support.TimelineGalleryAdapterView.OnItemClickListener;
import com.musicplode.beattheintro.support.TimelineGalleryAdapterView.OnItemSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PackSelectFragment extends BaseFragment {
	@Override
	public String getName() {
		return "PackSelect";
	}
	private ViewPager pager;
	private int containerId;
	private View v;
	private int current = 0;
	private PackAdapter mna;
	private boolean multiplayer;
	private TimelineGallery ecoGallery;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        multiplayer  = getArguments() != null && getArguments().getBoolean("multiplayer");
        current = getArguments() != null ? getArguments().getInt("current") : 0;
    }
	public String getPackId(int position) {
		try {
			JSONArray packs = Storage.getUserProfile(getActivity()).getJSONObject("UserProfile").getJSONArray("Packs");
			return packs.getJSONObject(position).getString("Id");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	private String getPackTitle(int position) {
		try {
			JSONArray packs = Storage.getUserProfile(getActivity()).getJSONObject("UserProfile").getJSONArray("Packs");
			return packs.getJSONObject(position).getString("Name");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	private String getPackDescription(int position) {
		try {
			JSONArray packs = Storage.getUserProfile(getActivity()).getJSONObject("UserProfile").getJSONArray("Packs");
			return packs.getJSONObject(position).getString("Description");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private boolean isLevelPlayable(int position, int level) {
		try {
			JSONArray packs = Storage.getUserProfile(getActivity()).getJSONObject("UserProfile").getJSONArray("Packs");
			return new Pack(packs.getJSONObject(position)).isLevelUnlocked(level);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private int getStars(int position, int level) {
		try {
			JSONArray packs = Storage.getUserProfile(getActivity()).getJSONObject("UserProfile").getJSONArray("Packs");
			return new Pack(packs.getJSONObject(position)).getStarsOnLevel(level);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	private boolean isPlayablePack(int currentItem) {
		try {
			return Storage.getUserProfile(getActivity()).getJSONObject("UserProfile").getJSONArray("Packs").getJSONObject(currentItem).getBoolean("Playable");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public void onSaveInstanceState (Bundle outState) {
		super.onSaveInstanceState(outState);
		
		outState.putInt("currentPack", current);
	}
	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.trackselect, container, false);

		View jv = v.findViewById(R.id.backButtonRing); // only setup back button for views with a button. Portrait views don't have the burron specified, as yet!
		if (jv != null) {
			Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
		}

		containerId = container.getId();

        ecoGallery = (TimelineGallery) v.findViewById(R.id.ecoGallery);
        
        mna = new PackAdapter(getActivity(), 0);
        
    	for (JSONObject o : Helpers.getAllPackJSONS(getActivity()))
    		mna.add(new Pack(o));
        
        ecoGallery.setAdapter(mna);
        ecoGallery.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(TimelineGalleryAdapterView<?> parent,
					View view, int position, long id) {
				current = position;
				pageSelected(position);		
			}

			@Override
			public void onNothingSelected(TimelineGalleryAdapterView<?> parent) {
			}
		});


        /**
         * Set the current selected item to the requested one, via packId, place, or last played pack
         */
        if (current != 0) {
        	ecoGallery.setSelection(current);
        }
        else {
        	String lastPackId = Storage.getLastPlayedPackId(getActivity());
        	if (lastPackId != null) {
        		for (int i = 0; i < mna.getCount(); i++) {
        			if (mna.getItem(i).getId().contentEquals(lastPackId)) {
        				current = i;
        				break;
        			}
        		}
        	}
        	if (current != 0)
        		ecoGallery.setSelection(current);
        }

        /**
         * Tapping on a pack starts that packs first playable level or starts unlock pack fragment if it is locked
         */
        ecoGallery.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(TimelineGalleryAdapterView<?> parent, View view, int position, long id) {
				if (current == position) {
                    PackAdapter mna = (PackAdapter) parent.getAdapter();
					Pack o = mna.getItem(position);
					
					if (o.isPlayable()) {
						startLevel(getNextLevel(position));
					}
					else {
						FragmentTransaction ft = getFragmentManager().beginTransaction();
						Helpers.setFragmentAnimation(ft);
						ft.replace(R.id.frameLayout1, BuySinglePackFragment.newInstance(o.getJSON()));
						ft.addToBackStack(null);
						ft.commit();
					}
				}
			}
		});
        
        v.findViewById(R.id.trackSelectLeaderboardButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				Helpers.setFragmentAnimation(ft);
				ft.replace(container.getId(), LeaderboardFragment.newInstance(getSelectedPositon()));
				ft.addToBackStack(null);
				ft.commit();
			}
		});
        v.findViewById(R.id.level1Button).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startLevel(0);
			}
		});
        v.findViewById(R.id.level2Button).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startLevel(1);
			}
		});
        v.findViewById(R.id.level3Button).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startLevel(2);
			}
		});
        
        Helpers.setAllFonts(v, getActivity());
        
        View pulse = v.findViewById(R.id.packSelectSwirl);
        if (pulse != null) {
        	Helpers.addPulse(pulse,0); // Info button
        	pulse.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					DialogHelpers.showInfoDialog(getActivity(), R.string.UNLOCK_PACK_INFO_TITLE, R.string.UNLOCK_PACK_INFO_TEXT); 					
				}
			});
        }

		/**
		 * number of unlocks is now part of main header view
        TextView unlocks = (TextView) v.findViewById(R.id.packSelectUnlocksText);
        if (unlocks != null) {
        	String text = getString(R.string.PACKSELECT_UNLOCK_TEXT);
        	text = text.replace("[X]", "" + Helpers.getNumberOfUnlocks(Storage.getUserProfile(getActivity())));
        	unlocks.setText(text);
        }
		 */
        

		return v;
	}

	
	public class PackAdapter extends ArrayAdapter<Pack> {
		private boolean playable;

		public PackAdapter(Context context, int resource) {
			super(context, resource);
		}
		
		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater =
	                    (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.trackviewnew, null);
				Helpers.setAllFonts(view, getContext());
			}
			
			if (getActivity() == null)
				return view;
			
			ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			
			TextView padlockBannerTextView = (TextView) view.findViewById(R.id.padlockBannerTextView);
			ImageView padlockImage = (ImageView) view.findViewById(R.id.padlockImageView); 
			
			
			if (position == current) {
				view.findViewById(R.id.imageView2).setVisibility(View.VISIBLE);
			}
			else {
				view.findViewById(R.id.imageView2).setVisibility(View.INVISIBLE);
			}
			
			playable = getItem(position).isPlayable();
			
			if (playable) {
				padlockBannerTextView.setVisibility(View.INVISIBLE);
				padlockImage.setVisibility(View.INVISIBLE);
				view.findViewById(R.id.padlockImageView).setBackgroundColor(getResources().getColor(android.R.color.transparent));
			}
			else {
				padlockBannerTextView.setVisibility(View.VISIBLE);
				padlockImage.setVisibility(View.VISIBLE);
				view.findViewById(R.id.padlockImageView).setBackgroundColor(getResources().getColor(R.color.blackfiftyaplha));
			}
			
			int stars = getItem(position).getNumberOfStars();
			tv.setText("" + stars + "/9");
			
			iv.setImageResource(R.drawable.bti_default_pack);
			
        	Helpers.displayPackImage(getActivity(), getItem(position).getId(), iv);
			
			return view;
        }
	}
	private int getNextLevel(int position) {
		int highestPlayableLevel = 0;
		for (int i = 0; i < 3; i++) {
			if (isLevelPlayable(position, i))
				highestPlayableLevel = i;
		}
		return highestPlayableLevel;
	}
	
	private void startLevel(int i) {
		if (mna.getCount() < getSelectedPositon())
			return;

		Pack p = mna.getItem(getSelectedPositon());
		if (! p.isLevelUnlocked(i)) {
			DialogHelpers.showOkDialog(getActivity(), R.string.PACKSELECT_LEVEL_lOCKED_TITLE, R.string.PACKSELECT_LEVEL_lOCKED_TEXT, R.string.OK);
			return;
		}

		String levelId = p.getLevels().get(i);
		
		if (multiplayer) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			getFragmentManager().popBackStack();
			Helpers.setFragmentAnimation(ft);
			ft.replace(containerId, ChooseOpponentFragment.newInstance(getPackId(getSelectedPositon()), levelId));
			ft.addToBackStack(null);
			ft.commit();
		}
		else {
			Helpers.startGame((LoggedInMenu) getActivity(), containerId, "", getPackId(getSelectedPositon()), levelId);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();

        lastPosition = -1;
        if (indicators != null)
            indicators.clear();

		if (current != 0 && pager != null) {
			pageSelected(current);
			pager.setCurrentItem(current);
		}


		((LoggedInMenu)getActivity()).disableLowerChart();
		((LoggedInMenu)getActivity()).enableAds();
	}
	
	private int getSelectedPositon() {
		return current;
	}



    private long lastPosition = -1;
	protected void pageSelected(int position) {
		if (getActivity() == null)
			return;

        /**
         * Makes sure that the Open sound is only played once per pack
         */
        if (lastPosition != position) {
            lastPosition = position;
            ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.OPEN);
        }

		((TextView)v.findViewById(R.id.packTitle)).setText(getPackTitle(position));
		((TextView)v.findViewById(R.id.trackSelectPackDescription)).setText(getPackDescription(position));
				
		int starsLevel1 = getStars(position, 0);
		int starsLevel2 = getStars(position, 1);
		int starsLevel3 = getStars(position, 2);
		
		
		
		((ProgressBar)v.findViewById(R.id.progressBar1)).setProgress(starsLevel1);
		((ProgressBar)v.findViewById(R.id.progressBar2)).setProgress(starsLevel2);
		((ProgressBar)v.findViewById(R.id.progressBar3)).setProgress(starsLevel3);

		
		if (isLevelPlayable(position, 1)) {
			v.findViewById(R.id.packselectPadlockLevel2).setVisibility(View.INVISIBLE);
			((ImageView)v.findViewById(R.id.testImage2)).setColorFilter(null);
		}
		else {
			v.findViewById(R.id.packselectPadlockLevel2).setVisibility(View.VISIBLE);
			((ImageView)v.findViewById(R.id.testImage2)).setColorFilter(getResources().getColor(R.color.blackfiftyaplha));
		}
		
		if (isLevelPlayable(position, 2)) {
			v.findViewById(R.id.packselectPadlockLevel3).setVisibility(View.INVISIBLE);
			((ImageView)v.findViewById(R.id.testImage3)).setColorFilter(null);
		}
		else {
			v.findViewById(R.id.packselectPadlockLevel3).setVisibility(View.VISIBLE);
			((ImageView)v.findViewById(R.id.testImage3)).setColorFilter(getResources().getColor(R.color.blackfiftyaplha));
		}
		
		
		int visibility = isPlayablePack(position) ? View.VISIBLE : View.INVISIBLE;
		v.findViewById(R.id.meeep).setVisibility(visibility);
		
		fixIndicatorView();
	}
	
	private List<ImageView> indicators = new ArrayList<ImageView>();
	private int lastSelected = -1;
	
	private void fixIndicatorView() {
		if (getActivity() == null)
			return;
		
		if (mna.getCount() != indicators.size()) {
			LinearLayout ll = (LinearLayout) v.findViewById(R.id.indicatorLinearLayout);
			ll.removeAllViews();
			indicators.clear();
			
			for (int i = 0; i < mna.getCount(); i++) {
		    	ImageView iv = new ImageView(getActivity());
		    	if (i == current)
		    		iv.setImageResource(R.drawable.select_ball_selected);
		    	else
		    		iv.setImageResource(R.drawable.select_ball);
		    	ll.addView(iv);
		    	iv.setAdjustViewBounds(true);
		    	indicators.add(iv);
		    }
		}
		
		if (current != lastSelected) {
			if (current >= 0 && current < mna.getCount())
				indicators.get(current).setImageResource(R.drawable.select_ball_selected);
			
			if (lastSelected >= 0 && lastSelected < mna.getCount())
				indicators.get(lastSelected).setImageResource(R.drawable.select_ball);
		}
		lastSelected = current;
	}
	
	public static Fragment newInstance(boolean multiplayer) {
		return newInstance(multiplayer, 0);
	}
	public static Fragment newInstance(boolean multiplayer, int current) {
		PackSelectFragment f = new PackSelectFragment();
		Bundle b = new Bundle();
		b.putBoolean("multiplayer", multiplayer);
		b.putInt("current", current);
		f.setArguments(b);
		
		return f;
	}
}
