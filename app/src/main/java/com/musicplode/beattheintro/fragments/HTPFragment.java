package com.musicplode.beattheintro.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.musicplode.beattheintro.R;

/**
 * How to Play Holder
 */
public class HTPFragment extends Fragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.htpholder, container, false);
		
		ViewPager vp = (ViewPager) v.findViewById(R.id.htpHolderViewPager);
		vp.setAdapter(new HTPAdapter(getChildFragmentManager()));
		vp.getAdapter().notifyDataSetChanged();
		return v;
	}
	
	public class HTPAdapter extends FragmentPagerAdapter {
		public HTPAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int arg0) {
			return HTPFragmentInstance.newInstance(arg0, getCount());
		}

		@Override
		public int getCount() {
			return 7;
		}
		@Override
		public int getItemPosition(Object object) {
		    return POSITION_NONE;
		} 
	}

}
