package com.musicplode.beattheintro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
// import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;

public class ChooseEmailFragment extends BaseFragment {
	@Override
	public String getName() {
		return "ChooseEmailOpponent";
	}
	private View v;
	private int separatorStartY;
	private int separatorHeight;
	private String packId;
	private String levelId;
	private int containerId;
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.chooseemail, container, false);
		
		//Tablet version of this fragment does not cover entire content area
		if (containerId == 0)
			containerId = container.getId();
		
		/**
		 * Fix tablet divider position between bestmatch and choose opponent menu
		 */
		if (v.findViewById(R.id.chooseEmailSeparator) != null) {
			ImageView iv = (ImageView) v.findViewById(R.id.chooseEmailSeparator);
			
			LayoutParams lp = (LayoutParams) iv.getLayoutParams();
			lp.topMargin = separatorStartY;
			lp.height = separatorHeight;
			iv.setLayoutParams(lp);
		}
		
		v.findViewById(R.id.chooseEmailButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v2) {
                /**
                 * Start a game against the entered player
                 */
				String str = ((EditText) v.findViewById(R.id.chooseEmailEditText)).getText().toString();
				
				InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                View focused = getActivity().getCurrentFocus();
                if (focused != null)
				    inputManager.hideSoftInputFromWindow(focused.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

				startGame(str, containerId);
			}
		});

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
        Helpers.setAllFonts(v, getActivity());
		// Helpers.addSwirl(v.findViewById(R.id.chooseOpponentSwirl));
		
		return v;
	}
	
	public void startGame(String opponentName, final int containerId) {
		Helpers.startGame((LoggedInMenu) getActivity(), containerId, opponentName, packId, levelId);
	}
	
	
	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
		if (getArguments() != null) {
			separatorStartY = getArguments().getInt("separatorStartY");
			separatorHeight = getArguments().getInt("separatorHeight");
			packId = getArguments().getString("packId");
			levelId = getArguments().getString("levelId");
			containerId = getArguments().getInt("containerId");
		}
	}
	public static Fragment newInstance(String packId, String levelId, int separatorStartY, int separatorHeight, int containerId) {
		ChooseEmailFragment f = new ChooseEmailFragment();
		Bundle b = new Bundle();
		b.putInt("separatorStartY", separatorStartY);
		b.putInt("separatorHeight", separatorHeight);
		b.putInt("containerId", containerId);
		b.putString("packId", packId);
		b.putString("levelId", levelId);
		f.setArguments(b);
		
		return f;
	}
	public static Fragment newInstance(String packId, String levelId) {
        return newInstance(packId, levelId, 0, 0, 0);
	}

}
