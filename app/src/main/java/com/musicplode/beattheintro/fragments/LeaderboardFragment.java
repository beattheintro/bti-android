package com.musicplode.beattheintro.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.Sounds;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.modelhelpers.LeaderboardEntry;
import com.musicplode.beattheintro.modelhelpers.Pack;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;
import com.musicplode.beattheintro.support.TimelineGallery;
import com.musicplode.beattheintro.support.TimelineGalleryAdapterView;
import com.musicplode.beattheintro.support.TimelineGalleryAdapterView.OnItemSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * The mobile and tablet version of this fragment differs in the following ways:
 * Tablet has 3 leaderboards at once and no level selection
 * Tablet has a start game button
 */
public class LeaderboardFragment extends BaseFragment {
	@Override
	public String getName() {
		return "Leaderboard";
	}
	private ListView lvs[];
	private String packId;
	private boolean friendsOnly = false;
	private int currentBoardLevel;
	private View v;
	private int current;
	private int packCount;
	private boolean isTablet;
	private boolean meSelected;
	private String levelId;
	private String preSelectedPackId;
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.leaderboard, container, false);

		ImageView arrowDown;
		ImageView arrowUp;
		PackAdapter mna;

		if (meSelected) {
//			v.findViewById(R.id.leaderboardMeButton).setSelected(true);
		}

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));

		TimelineGallery ecoGallery = (TimelineGallery) v.findViewById(R.id.ecoGallery);
        
        mna = new PackAdapter(getActivity(), 0);
        for (JSONObject o : Helpers.getAllPackJSONS(getActivity())) {
			mna.add(new Pack(o));
    	}
        ecoGallery.setAdapter(mna);

        if (savedInstanceState != null) {
        	current = savedInstanceState.getInt("currentPack", 0);
        }
        
        arrowDown = (ImageView) v.findViewById(R.id.leaderboardArrowDownImageView);
        arrowUp = (ImageView) v.findViewById(R.id.leaderboardArrowUpImageView);

		packId = getCurrentPackId();
		packCount = mna.getCount();
		isTablet = getResources().getBoolean(R.bool.tablet);

        /**
         * Set up current board if a predefined game has been selected
         */
        if (packId != null) {
        	for (int i = 0; i < packCount; i++) {
        		if (mna.getItem(i).getId().equalsIgnoreCase(packId)) {
        			current = i;
        			
        			if (!isTablet && levelId != null) {
        				List<String> levels = mna.getItem(i).getLevels();        				
        				for (int j = 0; j < 3; j++) {
        					if (levelId.contentEquals(levels.get(j))) {
        						currentBoardLevel = j;
        					}
        				}
        			}
        			
        			break;
        		}
        	}
        }

        if (packId == null) {
        	if (current < 0 || current >= packCount)
        		current = 0;
        	
        	packId = getPackId(current);
        }
        
       	ecoGallery.setSelection(current);

		setLeftRightArrows();

        if (isTablet) {
        	lvs = new ListView[3];
        	lvs[0] = (ListView) v.findViewById(R.id.leaderboardLevel1ListView);
        	Helpers.addArrows(lvs[0], v.findViewById(R.id.leaderboardLevel1ArrowUpImageView), v.findViewById(R.id.leaderboardLevel1ArrowDownImageView));
        	
        	lvs[1] = (ListView) v.findViewById(R.id.leaderboardLevel2ListView);
        	Helpers.addArrows(lvs[1], v.findViewById(R.id.leaderboardLevel2ArrowUpImageView), v.findViewById(R.id.leaderboardLevel2ArrowDownImageView));
        	
        	lvs[2] = (ListView) v.findViewById(R.id.leaderboardLevel3ListView);
        	Helpers.addArrows(lvs[2], v.findViewById(R.id.leaderboardLevel3ArrowUpImageView), v.findViewById(R.id.leaderboardLevel3ArrowDownImageView));
        }
        else {
        	lvs = new ListView[1];
        	lvs[0] = (ListView) v.findViewById(R.id.leaderboardListView);
    		Helpers.addArrows(lvs[0], arrowUp, arrowDown);
    		
    		
    		v.findViewById(R.id.leaderboardLevel1ImageView).setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				selectLevelLeaderboard(0);
    			}
    		});
    		v.findViewById(R.id.leaderboardLevel2ImageView).setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				selectLevelLeaderboard(1);
    			}
    		});
    		v.findViewById(R.id.leaderboardLevel3ImageView).setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				selectLevelLeaderboard(2);
    			}
    		});
        }
        
		v.findViewById(R.id.leaderboardFriendsButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v2) {
				friendsOnly = !friendsOnly;
				
				if (friendsOnly)
					((Button)v.findViewById(R.id.leaderboardFriendsButton)).setText(R.string.ALL);
				else
					((Button)v.findViewById(R.id.leaderboardFriendsButton)).setText(R.string.FRIENDS);
				
				selectLevelLeaderboard(currentBoardLevel);
			}
		});
		
		v.findViewById(R.id.leaderboardMeButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v2) {
//				if (v2.isSelected()) {
//					v2.setSelected(false);
//				}
//				else {
//					v2.setSelected(true);
//					
					for (ListView lv : lvs) {
						int pos = getMyPosition(lv);
						if (pos >= 0) {
								int myPos = ((LeaderboardAdapter)lv.getAdapter()).getMyPostion();
								
								lv.smoothScrollToPosition(myPos);
								if (getMyPositionView(lv) != null)
									getMyPositionView(lv).setBackgroundColor(Color.CYAN);
						}
					}
//				}
			}
		});

		
		Button leaderboardStartNewGameButton = (Button) v.findViewById(R.id.leaderboardStartNewGameButton);
		// This button is only in the tablet version
		if (leaderboardStartNewGameButton != null) {
			leaderboardStartNewGameButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Pack p = null;
					List<JSONObject> packs = Helpers.getAllPackJSONS(getActivity());
					for (JSONObject o : packs) {
						p = new Pack(o);
						if (p.getId().equals(packId)) {
							if (p.isPlayable())
								Helpers.startGame((LoggedInMenu) getActivity(), container.getId(), "", packId, p.getLevels().get(0));
							return;
						}
					}
				}
			});
		}
		
		Helpers.setAllFonts(v, getActivity());
		
		// Helpers.addSwirl(v.findViewById(R.id.leaderboardSwirl));
		
		v.findViewById(R.id.leaderboardAmazonButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((LoggedInMenu)getActivity()).showAmazonLeaderboards();
				
			}
		});

		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		((LoggedInMenu)getActivity()).disableLowerChart();
		((LoggedInMenu)getActivity()).enableAds();
		
		TimelineGallery ecoGallery = (TimelineGallery) v.findViewById(R.id.ecoGallery);
		ecoGallery.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(TimelineGalleryAdapterView<?> parent,
					View view, int position, long id) {
				pageSelected(position);
			}

			@Override
			public void onNothingSelected(TimelineGalleryAdapterView<?> parent) {
			}
		});
	}
	
	private void pageSelected(int current) {
		if (getActivity() == null)
			return;
		
		this.current = current;

        ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.OPEN);
		
		packId = getPackId(current);
		
		selectLevelLeaderboard(currentBoardLevel);
	}

	private View getMyPositionView(ListView lv) {
		LeaderboardAdapter la = (LeaderboardAdapter) lv.getAdapter();
		return la.getMyPostionLayout();
	}
	
	private int getMyPosition(ListView lv) {
		LeaderboardAdapter la = (LeaderboardAdapter) lv.getAdapter();
		
		if (la != null)
			return la.getMyPostion();
		return -1;
	}
	private void setListVisible(boolean value) {
		if (value) {
			for (ListView lv : lvs)
				lv.setVisibility(View.VISIBLE);
			v.findViewById(R.id.progressBar1).setVisibility(View.INVISIBLE);
		}
		else {
			for (ListView lv : lvs)
				lv.setVisibility(View.INVISIBLE);
			v.findViewById(R.id.progressBar1).setVisibility(View.VISIBLE);
		}
	}

	/**
	 * Hide pertinent left/right arrows in gallery view "@+id/leaderboardArrowLeft" "@+id/leaderboardArrowRight"
	 */
	private void setLeftRightArrows() {
		View leftarrow = v.findViewById(R.id.leaderboardArrowLeft);
		View rightarrow = v.findViewById(R.id.leaderboardArrowRight);
		if (current == 0) {
			//hide left
			leftarrow.setVisibility(View.INVISIBLE);
		}
		else {
			leftarrow.setVisibility(View.VISIBLE);
			rightarrow.setVisibility(View.VISIBLE);
			if (current == (packCount-1)) {
				//hide right
				rightarrow.setVisibility(View.INVISIBLE);
			}
		}
	}

	private boolean shouldAutoScroll(String packId) {
		if (packId != null && preSelectedPackId != null)
			return preSelectedPackId.contentEquals(packId);
		return false;
	}
	
	/**
	 * Loads a new leaderboard
	 * @param i
	 */
	private void selectLevelLeaderboard(int i) {
		if (getActivity() == null)
			return;

		TextView levelTextView = (TextView) v.findViewById(R.id.leaderboardLevelTextView);
		
		if (levelTextView != null) {
			String s = getActivity().getString(R.string.LEADERBOARD_LEVEL_TEXT_MOBILE);
			s = s.replace("[X]", "" + (i+1));
			levelTextView.setText(s);
		}

		setLeftRightArrows(); // set the correct gallery arrows

		if (isTablet) {
            for (ListView lv : lvs) {
				lv.setAdapter(null);
			}

            /**
             * Load all three leaderboards for this pack at the same time
             */
			for (int j = 0; j < lvs.length; j++) {
				final LeaderboardAdapter la = new LeaderboardAdapter(getActivity(), 0);
				lvs[j].setAdapter(la);
				JSONObject o = JSONRequestHelpers.getLeaderBoard(getActivity(), friendsOnly, packId, getLevelId(current, j));
				setListVisible(false);
				
				
				final int bla = j;
				Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						setListVisible(true);
						if (getActivity() != null) {
							try {
								
								if (!response.getString("PackId").equals(packId))
									return;
								
								JSONArray likesArray = response.getJSONArray("LeaderboardInfo");
								
								for (int i = 0; i < likesArray.length(); i++) {
									la.add(new LeaderboardEntry(likesArray.getJSONObject(i)));
								}
								la.notifyDataSetChanged();
								
								if (shouldAutoScroll(packId)) {
									int pos = getMyPosition(lvs[bla]);
									if (pos >= 0) {
										int myPos = la.getMyPostion();
										
										lvs[bla].smoothScrollToPosition(myPos);
										if (getMyPositionView(lvs[bla]) != null)
											getMyPositionView(lvs[bla]).setBackgroundColor(Color.CYAN);
									}
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
					
					@Override
					public void badResult(String errorMessage) {
					}
				});
			}
		}
		else {
            /**
             * If the device is not a tablet set the correct level icon and load the selected levels leaderboard
             */
			if (lvs == null || lvs[0] == null)
				return;
			lvs[0].setAdapter(null);
			currentBoardLevel = i;

			((ImageView)v.findViewById(R.id.leaderboardLevel1ImageView)).setImageResource(R.drawable.level1); // try setting all to 'dark' versions, where current one will be highlighted immediately after
			((ImageView)v.findViewById(R.id.leaderboardLevel2ImageView)).setImageResource(R.drawable.level2);
			((ImageView)v.findViewById(R.id.leaderboardLevel3ImageView)).setImageResource(R.drawable.level3);
			
			switch (i) {
				case 0:
					((ImageView)v.findViewById(R.id.leaderboardLevel1ImageView)).setImageResource(R.drawable.level1_hl);
					break;
				case 1:
					((ImageView)v.findViewById(R.id.leaderboardLevel2ImageView)).setImageResource(R.drawable.level2_hl);
					break;
				case 2:
					((ImageView)v.findViewById(R.id.leaderboardLevel3ImageView)).setImageResource(R.drawable.level3_hl);
					break;
			}
			
			
			JSONObject o = JSONRequestHelpers.getLeaderBoard(getActivity(), friendsOnly, packId, getLevelId(current, i));
			Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
				@Override
				public void okResult(JSONObject response) {
					setListVisible(true);
					if (getActivity() != null) {
						try {
							
							//Add level check
							if (!response.getString("PackId").equals(packId))
								return;
							
							
						} catch (JSONException e1) {
							e1.printStackTrace();
						}
						
						
						LeaderboardAdapter la = new LeaderboardAdapter(getActivity(), 0);
						try {
							JSONArray likesArray = response.getJSONArray("LeaderboardInfo");
							
							for (int i = 0; i < likesArray.length(); i++) {
								la.add(new LeaderboardEntry(likesArray.getJSONObject(i)));
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						lvs[0].setAdapter(la);
						la.notifyDataSetChanged();
						
						if (shouldAutoScroll(packId)) {
							int pos = getMyPosition(lvs[0]);
							if (pos >= 0) {
								int myPos = la.getMyPostion();
								
								lvs[0].smoothScrollToPosition(myPos);
								if (getMyPositionView(lvs[0]) != null)
									getMyPositionView(lvs[0]).setBackgroundColor(Color.CYAN);
							}
						}
						
						
					}
				}
				
				@Override
				public void badResult(String errorMessage) {
				}
			}, true);
		}
	}
	
	public class PackAdapter extends ArrayAdapter<Pack> {
		public PackAdapter(Context context, int resource) {
			super(context, resource);
		}
		
		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater =
	                    (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.leaderboardtrackview, null);
				Helpers.setAllFonts(view, getContext());
			}
			
        	ImageView iv = (ImageView) view.findViewById(R.id.imageView1);

			View padLock = view.findViewById(R.id.leaderboardTrackPadlock);
			if (padLock != null) {
				if (getItem(position).isPlayable())
					padLock.setVisibility(View.GONE);
				else
					padLock.setVisibility(View.VISIBLE);
			}

        	if (position == current) {
        		view.findViewById(R.id.imageView2).setVisibility(View.VISIBLE);
        	}
        	else {
        		view.findViewById(R.id.imageView2).setVisibility(View.INVISIBLE);
        	}
        	
        	iv.setImageResource(R.drawable.bti_default_pack);
        	Helpers.displayPackImage(getActivity(), LeaderboardFragment.this.getPackId(position), iv);
        	
			return view;
        }
	}
	
	
	public class LeaderboardAdapter extends ArrayAdapter<LeaderboardEntry> {
		int myPostion = -1;
		LinearLayout myPostionLayout;
		
		
		public LeaderboardAdapter(Context context, int resource) {
			super(context, resource);
		}
		
		public int getMyPostion() {
			try {
				String myUserId = Storage.getUserProfile(getContext()).getJSONObject("UserProfile").getString("Id");

				for (int i = 0; i < getCount(); i++) {
					String userId = getItem(i).getUserId();
					if (myPostion < 0 && userId.contentEquals(myUserId)) {
						myPostion = i;
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			return myPostion;
		}
		public View getMyPostionLayout() {
			return myPostionLayout;
		}
		
		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.leaderboardcell, null);
				
				Helpers.setAllFonts(view, getContext());
			}
			
			try {
				((TextView)view.findViewById(R.id.leaderboardCellRankTextView)).setText("" + getItem(position).getRank());
				((TextView)view.findViewById(R.id.leaderboardCellNameTextView)).setText("" + getItem(position).getName());
				((TextView)view.findViewById(R.id.leaderboardCellScoreTextView)).setText("" + Storage.numberFormat.format(getItem(position).getScore()));
				
				((ImageView)view.findViewById(R.id.leaderboardCellImageView)).setImageResource(R.drawable.profile_unknown);
				
				if (getItem(position).getFBId().length() != 0) {
					Helpers.displayImage(getActivity(), Helpers.getFacebookProfileImage(getItem(position).getFBId()), (ImageView)view.findViewById(R.id.leaderboardCellImageView));
				}
				
				String userId = getItem(position).getUserId();
				String myUserId = Storage.getUserProfile(getContext()).getJSONObject("UserProfile").getString("Id");
				
				if (userId.contentEquals(myUserId)) {
					myPostion = position;
					view.setBackgroundResource(R.drawable.button_selected);
				}
				else {
					if (position % 2 == 0)
						view.setBackgroundColor(getResources().getColor(R.color.blackthirtyaplha));
					else
						view.setBackgroundColor(getResources().getColor(android.R.color.transparent));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			return view;
		}
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        packId = getArguments() != null ? getArguments().getString("packId") : null;
        current = getArguments() != null ? getArguments().getInt("currentPack") : 0;
        meSelected = getArguments() != null && getArguments().getBoolean("meSelected");
        levelId = getArguments() != null ? getArguments().getString("levelId") : null;
        if (meSelected)
        	preSelectedPackId = levelId;
    }
	
	public String getCurrentPackId() {
		String lastPackId = Storage.getLastPlayedPackId(getActivity());
		if (lastPackId != null)
			return lastPackId;
		else
			return getPackId(0); // gets the first pack
	}
	
	public String getPackId(int position) {
		try {
			JSONArray packs = Storage.getUserProfile(getActivity()).getJSONObject("UserProfile").getJSONArray("Packs");
			return packs.getJSONObject(position).getString("Id");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String getLevelId(int position, int level) {
		try {
			JSONArray packs = Storage.getUserProfile(getActivity()).getJSONObject("UserProfile").getJSONArray("Packs");
			
			Pack p = new Pack(packs.getJSONObject(position));
			return p.getLevelTypeId(level);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Fragment newInstance(int packNo) {
        LeaderboardFragment f = new LeaderboardFragment();

        Bundle args = new Bundle();
        args.putInt("currentPack", packNo);
        f.setArguments(args);
        
		return f;
	}
	
	public static Fragment newInstance(String packId) {
		return newInstance(packId, false, null);
	}
	public static Fragment newInstance(String packId, boolean meSelected, String levelId) {
        LeaderboardFragment f = new LeaderboardFragment();

        Bundle args = new Bundle();
        args.putString("packId", packId);
        args.putBoolean("meSelected", meSelected);
        if (levelId != null)
        	args.putString("levelId", levelId);
        f.setArguments(args);

		return f;
	}
}
