package com.musicplode.beattheintro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;

/**
 * The lobby that lanches a PartyPlay game,
 * Each users text button requests focus to their EditTexts, or releases focus if they had focus
 */
public class PartyPlayLobbyFragment extends BaseFragment {
    private View v;
    private int numberOfPlayers;
    private String packId;
    private String levelId;

    private int currentFocus = -1;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.partyplay_lobby, container, false);

        Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));

        Helpers.addPulse(v.findViewById(R.id.partyPlayLobbyPlayer1EditButton),0);
        Helpers.addPulse(v.findViewById(R.id.partyPlayLobbyPlayer2EditButton),0);

        /**
         * Show/hide player UI depending on how many players the user selected
         */
        if (numberOfPlayers < 6)
            v.findViewById(R.id.partyPlayLobbyPlayer6Layout).setVisibility(View.INVISIBLE);
        else
            Helpers.addPulse(v.findViewById(R.id.partyPlayLobbyPlayer6EditButton),0);

        if (numberOfPlayers < 5)
            v.findViewById(R.id.partyPlayLobbyPlayer5Layout).setVisibility(View.INVISIBLE);
        else
            Helpers.addPulse(v.findViewById(R.id.partyPlayLobbyPlayer5EditButton),0);

        if (numberOfPlayers < 4)
            v.findViewById(R.id.partyPlayLobbyPlayer4Layout).setVisibility(View.INVISIBLE);
        else
            Helpers.addPulse(v.findViewById(R.id.partyPlayLobbyPlayer4EditButton),0);

        if (numberOfPlayers < 3)
            v.findViewById(R.id.partyPlayLobbyPlayer3Layout).setVisibility(View.INVISIBLE);
        else
            Helpers.addPulse(v.findViewById(R.id.partyPlayLobbyPlayer3EditButton),0);


        v.findViewById(R.id.partyPlayLobbyPlayer1EditButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText yourEditText= (EditText) v.findViewById(R.id.partyPlayLobbyPlayer1Name);

                if (currentFocus != 0) {
                    yourEditText.requestFocus();

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(yourEditText, InputMethodManager.SHOW_IMPLICIT);

                    currentFocus = 0;
                }
                else {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(yourEditText.getWindowToken(), 0);
                    currentFocus = -1;
                }
            }
        });
        v.findViewById(R.id.partyPlayLobbyPlayer2EditButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText yourEditText= (EditText) v.findViewById(R.id.partyPlayLobbyPlayer2Name);

                if (currentFocus != 1) {
                    yourEditText.requestFocus();

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(yourEditText, InputMethodManager.SHOW_IMPLICIT);

                    currentFocus = 1;
                }
                else {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(yourEditText.getWindowToken(), 0);
                    currentFocus = -1;
                }
            }
        });
        v.findViewById(R.id.partyPlayLobbyPlayer3EditButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText yourEditText= (EditText) v.findViewById(R.id.partyPlayLobbyPlayer3Name);

                if (currentFocus != 2) {
                    yourEditText.requestFocus();

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(yourEditText, InputMethodManager.SHOW_IMPLICIT);

                    currentFocus = 2;
                }
                else {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(yourEditText.getWindowToken(), 0);
                    currentFocus = -1;
                }
            }
        });
        v.findViewById(R.id.partyPlayLobbyPlayer4EditButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText yourEditText= (EditText) v.findViewById(R.id.partyPlayLobbyPlayer4Name);

                if (currentFocus != 3) {
                    yourEditText.requestFocus();

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(yourEditText, InputMethodManager.SHOW_IMPLICIT);

                    currentFocus = 3;
                }
                else {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(yourEditText.getWindowToken(), 0);
                    currentFocus = -1;
                }
            }
        });
        v.findViewById(R.id.partyPlayLobbyPlayer5EditButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText yourEditText= (EditText) v.findViewById(R.id.partyPlayLobbyPlayer5Name);

                if (currentFocus != 4) {
                    yourEditText.requestFocus();

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(yourEditText, InputMethodManager.SHOW_IMPLICIT);

                    currentFocus = 4;
                }
                else {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(yourEditText.getWindowToken(), 0);
                    currentFocus = -1;
                }
            }
        });
        v.findViewById(R.id.partyPlayLobbyPlayer6EditButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText yourEditText= (EditText) v.findViewById(R.id.partyPlayLobbyPlayer6Name);

                if (currentFocus != 5) {
                    yourEditText.requestFocus();

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(yourEditText, InputMethodManager.SHOW_IMPLICIT);

                    currentFocus = 5;
                }
                else {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(yourEditText.getWindowToken(), 0);
                    currentFocus = -1;
                }
            }
        });


        v.findViewById(R.id.partyPlayLobbyStartGameButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Helpers.startPartyPlayGame((LoggedInMenu) getActivity(), container.getId(), packId, levelId, getPlayerNames());
            }
        });

        Helpers.setAllFonts(v, getActivity());

        return v;
    }

    private String[] getPlayerNames(){
        String playerNames[] = new String[numberOfPlayers];
        playerNames[0] = ((EditText)v.findViewById(R.id.partyPlayLobbyPlayer1Name)).getText().toString();
        playerNames[1] = ((EditText)v.findViewById(R.id.partyPlayLobbyPlayer2Name)).getText().toString();

        if (numberOfPlayers > 2)
            playerNames[2] = ((EditText)v.findViewById(R.id.partyPlayLobbyPlayer3Name)).getText().toString();
        if (numberOfPlayers > 3)
            playerNames[3] = ((EditText)v.findViewById(R.id.partyPlayLobbyPlayer4Name)).getText().toString();
        if (numberOfPlayers > 4)
            playerNames[4] = ((EditText)v.findViewById(R.id.partyPlayLobbyPlayer5Name)).getText().toString();
        if (numberOfPlayers > 5)
            playerNames[5] = ((EditText)v.findViewById(R.id.partyPlayLobbyPlayer6Name)).getText().toString();

        return playerNames;
    }

    @Override
    public String getName() {
        return "PartyPlayLobby";
    }
    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        if (getArguments() != null) {
            numberOfPlayers = getArguments().getInt("numberOfPlayers");
            packId = getArguments().getString("packId");
            levelId = getArguments().getString("levelId");
        }
    }
    public static Fragment newInstance(int numberOfPlayers, String packId, String levelId) {
        PartyPlayLobbyFragment lobby = new PartyPlayLobbyFragment();
        Bundle b = new Bundle();
        b.putInt("numberOfPlayers", numberOfPlayers);
        b.putString("packId", packId);
        b.putString("levelId", levelId);
        lobby.setArguments(b);

        return lobby;
    }
}
