package com.musicplode.beattheintro.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.LoggedInMenuLand;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.DialogHelpers.OnRetryButton;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONObject;

public class CreateNewEmailAccountFragment extends BaseFragment {
	@Override
	public String getName() {
		return "CreateNewEmailAccount";
	}
	private Button b;
	private View v;
	private boolean isTablet;
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.createaccount, container, false);

		isTablet = getResources().getBoolean(R.bool.tablet);
		
		if (v.findViewById(R.id.emailCreateSeparator) != null) {
			ImageView iv = (ImageView) v.findViewById(R.id.emailCreateSeparator);
			
			LayoutParams lp = (LayoutParams) iv.getLayoutParams();
			lp.topMargin = separatorStartY;
			lp.height = separatorHeight;
			iv.setLayoutParams(lp);
		}
		
		b = (Button) v.findViewById(R.id.createEmailAccountConfirmButton);
		b.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				final String password1 = ((EditText)v.findViewById(R.id.editText3)).getText().toString();
				final String email = ((EditText)v.findViewById(R.id.editText1)).getText().toString();
				String username = ((EditText)v.findViewById(R.id.editText2)).getText().toString();
				String password2 = ((EditText)v.findViewById(R.id.editText4)).getText().toString();
				
				/**
				 * Make sure the 2 passwords enter are the same password
				 */
				if (!password1.contentEquals(password2)) {
					DialogHelpers.showErrorDialog(getActivity(), R.string.ERROR_TITLE, R.string.PASSWORDS_ARE_NOT_THE_SAME, R.string.OK);

                    // Clear the password fields
					((EditText)v.findViewById(R.id.editText4)).setText("");
					((EditText)v.findViewById(R.id.editText3)).setText("");

					return ;
				}
				else if (password1.length() == 0 || email.length() == 0 || username.length() == 0) {
					return;
				}
				
				final ProgressDialog pd = new ProgressDialog(getActivity());
				pd.setMessage(getString(R.string.CREATING_ACCOUNT));
				pd.setCancelable(false);
				pd.show();

				// Disables possibility to register twice
				b.setEnabled(false);

				
				JSONObject userProfile = Storage.getUserProfile(getActivity());

				/**
				 * Register the new user on the server
				 */
				JSONObject o = JSONRequestHelpers.getCreateAccountJSON(userProfile, email, username, password1);
				Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						if (getActivity() == null)
							return;
						
						// Login using the new account
						JSONObject postJSON = JSONRequestHelpers.getLoginByEmailJSON(email, password1);
						Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, postJSON, new OnNetworkResult() {
							@Override
							public void okResult(JSONObject response) {
								Storage.store(CreateNewEmailAccountFragment.this.getActivity(), Storage.USERPROFILE, response);
								
								Intent intent;
								if (isTablet)
									intent = new Intent(getActivity(), LoggedInMenuLand.class);
								else
									intent = new Intent(getActivity(), LoggedInMenu.class);

								pd.cancel();
								
								startActivity(intent);
								getActivity().finish();
								
							}

							@Override
							public void badResult(String errorMessage) {
								pd.cancel();
							}
							
						});
					}
					
					@Override
					public void badResult(String errorMessage) {
						pd.cancel();
						if (b != null)
							b.setEnabled(true);
						
						if (errorMessage != null) {
							DialogHelpers.showHardCodedDialog(getActivity(), getString(R.string.ERROR_TITLE), errorMessage);
						}
						else {
							DialogHelpers.showErrorDialog(getActivity(), R.string.ERROR_INTERNET_CONNECTION_TITLE, R.string.ERROR_INTERNET_CONNECTION_TEXT, R.string.ERROR_RETRY_BUTTON, R.string.CANCEL, new OnRetryButton() {
								@Override
								public void onRetry() {
									b.performClick();
								}
							});
						}
					}
				});
			}
		});

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
        Helpers.setAllFonts(v, getActivity());
		return v;
	}
	

	int separatorStartY;
	int separatorHeight;
	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
		if (getArguments() != null) {
			separatorStartY = getArguments().getInt("separatorStartY");
			separatorHeight = getArguments().getInt("separatorHeight");
		}
	}
	public static Fragment newInstance(int separatorStartY, int separatorHeight) {
		CreateNewEmailAccountFragment elf = new CreateNewEmailAccountFragment();
		Bundle b = new Bundle();
		b.putInt("separatorStartY", separatorStartY);
		b.putInt("separatorHeight", separatorHeight);
		elf.setArguments(b);
		
		return elf;
	}
}
