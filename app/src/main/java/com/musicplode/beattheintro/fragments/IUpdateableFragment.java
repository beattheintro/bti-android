package com.musicplode.beattheintro.fragments;

/**
 * Interface for fragments that can be updated on the fly
 */
public interface IUpdateableFragment {
	/**
	 * Method will be called when a update has been requested, 
	 * currently only on pushnotifications
	 */
	public void onUpdate();
}
