package com.musicplode.beattheintro.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
// import android.widget.Button;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.UserInfoChangedCallback;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.LoggedInMenuLand;
import com.musicplode.beattheintro.MainActivity;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.DialogHelpers.OnOkButton;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONObject;

public class LoginFragment extends BaseFragment {
	@Override
	public String getName() {
		return "LoginMenu";
	}
	private int startY;
	private int height;
	private View v;
	private boolean isTablet;
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.login, container, false);
		
		isTablet = getResources().getBoolean(R.bool.tablet);
		
		//TODO: Add again when twitter login is needed
//		((Button) v.findViewById(R.id.twitterLoginButton)).setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				FragmentTransaction ft = getFragmentManager().beginTransaction();
//				Helpers.setFragmentAnimation(ft);
//				ft.replace(container.getId(), new TwitterLoginFragment());
//				ft.addToBackStack(null);
//				ft.commit();
//				
//			}
//		});
		
		v.findViewById(R.id.loginEmailButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v2) {
                /**
                 * Start the EmailLoginFragment
                 */
				startY = (int) v.findViewById(R.id.loginEmailButton).getY();
				height = v.findViewById(R.id.loginEmailButton).getHeight();
				final String APA = "APAPAPAPA";
				boolean isTablet = getResources().getBoolean(R.bool.tablet);
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				if (isTablet) {
					getFragmentManager().popBackStackImmediate(APA, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					Helpers.setFragmentAnimation(ft);
					ft.replace(R.id.loginEmailFrame, EmailLoginFragment.newInstance(startY, height));
					ft.addToBackStack(APA);
				}
				else {
					Helpers.setFragmentAnimation(ft);
					ft.replace(container.getId(), new EmailLoginFragment());
					ft.addToBackStack(null);
				}
				ft.commit();

			}
		});
		
		v.findViewById(R.id.loginLaterButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v2) {
                /**
                 * Login as an anonymous user
                 */
				final ProgressDialog pd = new ProgressDialog(getActivity());
				pd.setMessage(getActivity().getString(R.string.LOADING));
				pd.setCancelable(false);
				pd.show();
				
				if (isSignup) {
					loginViaSessionId(pd);
					return;
				}
				
				JSONObject loginBla = JSONRequestHelpers.getLoginLaterJSON();
				
				if (loginBla != null) {
					Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, loginBla, new OnNetworkResult() {
						@Override
						public void okResult(JSONObject response) {
							if (getActivity() != null) {
								Storage.store(LoginFragment.this.getActivity(), Storage.USERPROFILE, response);
								
								Intent intent;
								if (isTablet)
									intent = new Intent(getActivity(), LoggedInMenuLand.class);
								else
									intent = new Intent(getActivity(), LoggedInMenu.class);

								startActivity(intent);
								getActivity().finish();
							}
							pd.cancel();
						}
						
						@Override
						public void badResult(String errorMessage) {
							pd.cancel();

							if (getActivity() != null) {
								if (errorMessage != null && errorMessage.length() > 0) {
									DialogHelpers.showErrorDialog(getActivity(), errorMessage);
								}
								else {
									DialogHelpers.showErrorDialog(getActivity(), R.string.ERROR_INTERNET_CONNECTION_TITLE, R.string.ERROR_INTERNET_CONNECTION_TEXT, R.string.ERROR_RETRY_BUTTON,  new OnOkButton() {
										@Override
										public void onOk() {
											v.findViewById(R.id.loginLaterButton).performClick();
										}
									});									
								}
							}
						}
					});
				}
			}
		});
		
		/**
		 * Fix and style facebook authButton to match the rest of the buttons
		 */
		LoginButton authButton = (LoginButton) v.findViewById(R.id.authButton);
		authButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		authButton.setBackgroundResource(R.drawable.fbbutton);
		authButton.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		authButton.setTextAppearance(getActivity(), android.R.style.TextAppearance_Large);
		authButton.setTextColor(Color.WHITE);
		authButton.setReadPermissions("read_friendlists", "email");
		authButton.setUserInfoChangedCallback(new UserInfoChangedCallback() {
			@Override
			public void onUserInfoFetched(GraphUser user) {
				LoginFragment.this.user = user;
				loginToBTI();
			}
		});
	    authButton.setFragment(this);

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
        Helpers.setAllFonts(v, getActivity());
	    // Helpers.addSwirl(v.findViewById(R.id.loginSwirl));


        /**
         * Show how to play if it has not been shown
         */
        Boolean hasShownHTP = Storage.getStoredBoolean(getActivity(), Storage.HOWTOPLAYKEY);
	    if (hasShownHTP == null || !hasShownHTP) {
		    Storage.store(getActivity(), Storage.HOWTOPLAYKEY, true);
		    
		    FragmentTransaction ft = getFragmentManager().beginTransaction();
		    ft.replace(container.getId(), new HTPFragment());
		    ft.addToBackStack(null);
		    ft.commit();
	    }
	    
		return v;
	}
	
	private void loginViaSessionId(final ProgressDialog pd) {
		JSONObject storedUser = Storage.getUserProfile(getActivity());
		if (storedUser != null) {
			JSONObject o = JSONRequestHelpers.getLoginRequestJSON(storedUser);
			Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
				@Override
				public void okResult(JSONObject response) {
					Storage.store(getActivity(), Storage.USERPROFILE, response);
				
					Intent intent;
					if (isTablet)
						intent = new Intent(getActivity(), LoggedInMenuLand.class);
					else
						intent = new Intent(getActivity(), LoggedInMenu.class);
					
					startActivity(intent);
					getActivity().finish();
					pd.cancel();
				}
				
				@Override
				public void badResult(String errorMessage) {
					pd.cancel();
				}
			});
		}
		
	}
	
	
	
	
	/**
	 * Facebook stuffs
	 */
	private UiLifecycleHelper uiHelper;
	private GraphUser user;
	private boolean logingIn = false;
	private boolean isSignup;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    uiHelper = new UiLifecycleHelper(getActivity(), callback);
	    uiHelper.onCreate(savedInstanceState);
	    
		isSignup = getArguments() != null && getArguments().getBoolean("isSignup", false);
	}

	@Override
	public void onResume() {
		super.onResume();

		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);

			if (session.isOpened())
				v.setVisibility(View.INVISIBLE);
		}
		uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}

	private void loginToBTI() {
		Session session = Session.getActiveSession();
		String accessToken = session.getAccessToken();
		
		if (accessToken != null && user != null && !logingIn ) {
        	logingIn = true;
        	
        	String email = "";
        	try {
				email = user.getInnerJSONObject().optString("email", "");
			} catch (Exception e) {
				e.printStackTrace();
			}
        	
        	final JSONObject userProfile = Storage.getUserProfile(getActivity());
        	
        	JSONObject loginBla;
        	if (userProfile != null)
        		loginBla = JSONRequestHelpers.getCreateFacebookUserJSON(userProfile, user.getId(), user.getName(), email, accessToken);
        	else
        		loginBla = JSONRequestHelpers.getLoginByFacebookJSON(user.getId(), user.getName(), email, accessToken);
        	
			if (loginBla != null) {
				
				Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, loginBla, new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						if (getActivity() != null) {
							if (!response.has("UserProfile")) {
								Storage.removeUserProfile(getActivity());
								logingIn = false;
								loginToBTI();
								return;
							}
							
							
							Storage.store(LoginFragment.this.getActivity(), Storage.USERPROFILE, response);
							
							Intent intent;
							if (isTablet)
								intent = new Intent(getActivity(), LoggedInMenuLand.class);
							else
								intent = new Intent(getActivity(), LoggedInMenu.class);
							
							startActivity(intent);
							getActivity().finish();	
						}
					}
					
					@Override
					public void badResult(String errorMessage) {
						if (getActivity() == null)
							return;
						Storage.clearUserData(getActivity());
						user = null;
						getActivity().startActivity(new Intent(getActivity(), MainActivity.class));
						getActivity().finish();
					}
				});
			}
		}
	}
	
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened() && session != null && session.isOpened() && session.getAccessToken() != null) {
	        loginToBTI();
	    }
	}
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
	        onSessionStateChange(session, state, exception);
	    }
	};
	
	public static Fragment newInstance(boolean isSignup) {
		LoginFragment lf = new LoginFragment();
		Bundle b = new Bundle();
		b.putBoolean("isSignup", isSignup);
		lf.setArguments(b);
		
		return lf;
	}
}
