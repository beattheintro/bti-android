package com.musicplode.beattheintro.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.Session;
import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.MainActivity;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.Sounds;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.modelhelpers.Pack;
import com.musicplode.beattheintro.modelhelpers.UserProfile;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.support.StickyGridHeadersGridView;
import com.musicplode.beattheintro.support.StickyGridHeadersSimpleArrayAdapter;

import org.json.JSONObject;

public class ProfileFragment extends BaseFragment {
	@Override
	public String getName() {
		return "Profile";
	}
//	private static final int SELECT_PHOTO = 239;
	private StickyGridHeadersGridView lv;

	private View v;
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.profile, container, false);

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));

		v.findViewById(R.id.profileLogoutButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                /**
                 * Start the logout section
                 */
				final Dialog d = DialogHelpers.getDialog(getActivity(), R.string.LOGOUT_TITLE, R.string.LOGOUT_SWITCH_USER, R.string.KEEP_PLAYING);
				
				d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
                        /**
                         * Clear all userData and log out
                         */
						Session session = Session.getActiveSession();
						if (session != null) {
							session.closeAndClearTokenInformation();
						}
						
						
						Storage.clearUserData(getActivity());
						Intent i = new Intent(getActivity(), MainActivity.class);
						startActivity(i);
						getActivity().finish();
						d.hide();
						d.dismiss();
					}
				});
				
				d.findViewById(R.id.dialogButton2).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
                        /**
                         * Cancel logout
                         */
						d.hide();
						d.dismiss();
					}
				});
				
				d.show();
			}
		});
		
		if (v.findViewById(R.id.profileMyCoinsButton) != null) {
			v.findViewById(R.id.profileMyCoinsButton).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
                    /**
                     * Start the MyDiscsFragment
                     */
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					getFragmentManager().popBackStack(LoggedInMenu.LOGGED_IN_MENU_STACK_KEY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					Fragment f = new MyDiscsFragment();
					ft.replace(container.getId(), f);
					ft.addToBackStack(null);
					ft.commit();
				}
			});
		}
		
		v.findViewById(R.id.profileMyChartButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                /**
                 * Start the MyChartFragment
                 */
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				Helpers.setFragmentAnimation(ft);
				getFragmentManager().popBackStack(LoggedInMenu.LOGGED_IN_MENU_STACK_KEY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				Fragment f = new MyChartFragment();
				ft.replace(container.getId(), f);
				ft.addToBackStack(null);
				ft.commit();
			}
		});
		
		
		lv = (StickyGridHeadersGridView) v.findViewById(R.id.profileListView);
		lv.setAreHeadersSticky(false);

		if (lv != null) {
			PackAdapter pa = new PackAdapter(getActivity(), 0);

            /**
             * Add all packs to the GridView's adapter
             */
			for (JSONObject o : Helpers.getAllPackJSONS(getActivity())) {
				pa.add(new Pack(o));
			}
			
			lv.setAdapter(pa);
			pa.notifyDataSetChanged();

			lv.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					getFragmentManager().popBackStack(LoggedInMenu.LOGGED_IN_MENU_STACK_KEY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					Fragment f = PackSelectFragment.newInstance(false, arg2);
					ft.replace(container.getId(), f);
					ft.addToBackStack(null);
					ft.commit();
				}
			});
		}
		
		Helpers.setAllFonts(v, getActivity());
			
		// Helpers.addSwirl(v.findViewById(R.id.profileSwirl));
		
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		((LoggedInMenu)getActivity()).disableLowerChart();
		((LoggedInMenu)getActivity()).enableAds();

        ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.OPEN);
	}
	
	public class PackAdapter extends StickyGridHeadersSimpleArrayAdapter<Pack> {
		public PackAdapter(Context context, int resource) {
			super(context, resource);
		}
		
		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater =
	                    (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.profilepackcell, null);
				Helpers.setAllFonts(view, getContext());
			}
			
			TextView title = (TextView) view.findViewById(R.id.profilePackCellTitleTextView);
			TextView stars = (TextView) view.findViewById(R.id.profilePackCellStarsTextView);
			ImageView iv = (ImageView) view.findViewById(R.id.profilePackCellPackImageView);
//			ImageView padlock = (ImageView) view.findViewById(R.id.profilePackCellPadlockImageView);
			
			title.setText(getItem(position).getName());
			stars.setText("" + getItem(position).getNumberOfStars() + "/9");
			
			
			Helpers.cancelImageLoading(getContext(), iv);
			iv.setImageResource(R.drawable.bti_default_pack);
			Helpers.displayPackImage(getContext(), getItem(position).getId(), iv);
			
			
			TextView highestScoreTextView = (TextView) view.findViewById(R.id.profilePackCellHighestScore);
			
			int highestScoredLevel = getItem(position).getHighestScoredLevel();
			int highestScore = getItem(position).getHighestScoreOnLevel(highestScoredLevel);
			
			String s = getContext().getString(R.string.PROFILE_PACK_HIGHSCORETEXT);
			s = s.replace("[X]", "" + highestScore);
			s = s.replace("[Y]", "" + (highestScoredLevel+1));
			
			highestScoreTextView.setText(s);
			
			return view;
		}

		@Override
		public long getHeaderId(int position) {
			return -12123;
		}

		@Override
		public View getHeaderView(int position, View v, ViewGroup parent) {
			if (v == null) {
				LayoutInflater inflater =
	                    (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = inflater.inflate(R.layout.profileheader, null);
				Helpers.setAllFonts(v, getContext());
			}

			try {
				((TextView)v.findViewById(R.id.profileNameTextView)).setText(Storage.getUserProfile(getContext()).getJSONObject("UserProfile").optString("Name", ""));
//				((TextView)v.findViewById(R.id.profileCityTextView)).setText("" + "missing server info");
			} catch (Exception e) {
				e.printStackTrace();
			}

			//TODO: Continue with this when the server accepts user submitted pictures
//			ImageView profileImage = (ImageView) v.findViewById(R.id.profileImageView);
//			profileImage.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//					Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//					photoPickerIntent.setType("image/*");
//					startActivityForResult(photoPickerIntent, SELECT_PHOTO);  
//				}
//			});
			

			int highestScore = 1000;
			int totalGames = 55;
			int gamesWon = 3;
			int gamesLost = 13;
			int chartPosition = 13;
			int maxAVGTime = 100;
			int avgTime = 50;

			String avgAnswerTime = "1.234s";
			int maxScore = 15000;
			int rank = 0;
			
			try {
				UserProfile up = new UserProfile(Storage.getUserProfile(getContext()).getJSONObject("UserProfile"));
				chartPosition = Helpers.getChartPositionFromDiscs(up.getNumberOfDiscs());
				rank = Helpers.getPercentageCompletedFromDiscs(up.getNumberOfDiscs());
				
				highestScore = up.getHighestScore();
				totalGames = up.getTotalPlayedGames();
				gamesWon = up.getGamesWon();
				gamesLost = up.getGamesLost();

				avgAnswerTime = up.getAVGTime();
				
				if (avgAnswerTime == null)
					avgAnswerTime = "1.23s";
				else
					avgAnswerTime = avgAnswerTime + "s";

				avgTime = (int) up.getAVGTimeDouble();
				maxAVGTime = (int) up.getMaxAVGTimeDouble();
				
				maxScore = Helpers.getMaxLevelScore(getContext());
			} catch (Exception e) {
				e.printStackTrace();
			}

			((ProgressBar) v.findViewById(R.id.progressBarRoundA)).setProgress(rank);
			((TextView) v.findViewById(R.id.progressBarRoundTextA)).setText("" + rank + "%");
			
			((ProgressBar) v.findViewById(R.id.progressBarRoundB)).setMax(maxScore);
			((ProgressBar) v.findViewById(R.id.progressBarRoundB)).setProgress(highestScore);
			((TextView) v.findViewById(R.id.progressBarRoundTextB)).setText("" + highestScore);

			int gamesWonPercent = totalGames == 0 ? 0 : (int) (100 * gamesWon/(double)Math.max(totalGames,1));
			((ProgressBar) v.findViewById(R.id.progressBarRoundC)).setProgress(gamesWonPercent);
			((TextView) v.findViewById(R.id.progressBarRoundTextC)).setText("" + gamesWonPercent + "%\n(" + gamesWon +")");
			
			int gamesLostPercent = totalGames == 0 ? 0 : (int) (100 * gamesLost/(double)Math.max(totalGames,1));
			((ProgressBar) v.findViewById(R.id.progressBarRoundD)).setProgress(gamesLostPercent);
			((TextView) v.findViewById(R.id.progressBarRoundTextD)).setText("" + gamesLostPercent + "%\n(" + gamesLost +")");

			((ProgressBar) v.findViewById(R.id.progressBarRoundE)).setProgress(100-chartPosition);
			((TextView) v.findViewById(R.id.progressBarRoundTextE)).setText("" + chartPosition + "\nSilver");

			((ProgressBar) v.findViewById(R.id.progressBarRoundF)).setMax(maxAVGTime);
			((ProgressBar) v.findViewById(R.id.progressBarRoundF)).setProgress(avgTime);
			((TextView) v.findViewById(R.id.progressBarRoundTextF)).setText(avgAnswerTime);
			
			return v;
		}
	
	}
	
	//TODO: Readd & continue when server can accept user submitted pictures 
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
//	    super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 
//
//	    switch(requestCode) { 
//	    case SELECT_PHOTO:
//	        if(resultCode == Activity.RESULT_OK){  
//	            try {
//					Uri selectedImage = imageReturnedIntent.getData();
//					InputStream imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
//					Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
//					
////					profileImage.setImageBitmap(yourSelectedImage);
//					
//				} catch (FileNotFoundException e) {
//					e.printStackTrace();
//				}
//	        }
//	    }
//	}
}
