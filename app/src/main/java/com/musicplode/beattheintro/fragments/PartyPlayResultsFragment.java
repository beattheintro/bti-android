package com.musicplode.beattheintro.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Daniel on 23/06/2014.
 */
@SuppressWarnings("DefaultFileTemplate")
public class PartyPlayResultsFragment extends BaseFragment {
    private View v;
    private int numberOfPlayers;
    private int numberOfQuestions;
    private String[] playerNames;
    private int[][] answerTimeArray;
    private boolean[][] answeredCorrectArray;
    private int maxTimeToAnswer;
    private int maxScore;
    private String packId;
    private String levelId;
    private int levelNumber;

    @Override
    public String getName() {
        return "PartyPlayResults";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.partyplay_results, container, false);


        /**
         * Get the player names for the 5 different PartyPlayAchievements
         */
        String moogle = getHighestScoredPlayerName();
        String fastest = getFastestPlayer();
        String itchy = getMostWrongAnswerPlayerName();
        String slowest = getSlowestPlayer();
        String lamb = getLowestScoredPlayerName();

        ((TextView)v.findViewById(R.id.partyPlayResultsBadgeMogelText)).setText(moogle);
        ((TextView)v.findViewById(R.id.partyPlayResultsBadgeFastestText)).setText(fastest);
        ((TextView)v.findViewById(R.id.partyPlayResultsBadgeItchyText)).setText(itchy);
        ((TextView)v.findViewById(R.id.partyPlayResultsBadgeSlowestText)).setText(slowest);
        ((TextView)v.findViewById(R.id.partyPlayResultsBadgeLambText)).setText(lamb);


        /**
         * Sort the players according to their score
         */
        @SuppressWarnings("Convert2Diamond") List<Integer> playerPositions = new ArrayList<Integer>();
        for (int i = 0; i < numberOfPlayers; i++) {
            playerPositions.add(i);
        }
        Collections.sort(playerPositions, new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer integer2) {
                return calculatePlayerScore(integer2) - calculatePlayerScore(integer);
            }
        });


        TextView playerNameTextViews[] = new TextView[6];
        playerNameTextViews[0] = (TextView) v.findViewById(R.id.partyPlayResultsFirstPlaceName);
        playerNameTextViews[1] = (TextView) v.findViewById(R.id.partyPlayResultsSecondPlaceName);
        playerNameTextViews[2] = (TextView) v.findViewById(R.id.partyPlayResultsThirdPlaceName);
        playerNameTextViews[3] = (TextView) v.findViewById(R.id.partyPlayResultsFourthPlaceName);
        playerNameTextViews[4] = (TextView) v.findViewById(R.id.partyPlayResultsFifthPlaceName);
        playerNameTextViews[5] = (TextView) v.findViewById(R.id.partyPlayResultsSixthPlaceName);

        TextView playerScoreTextViews[] = new TextView[6];
        playerScoreTextViews[0] = (TextView) v.findViewById(R.id.partyPlayResultsFirstPlaceScore);
        playerScoreTextViews[1] = (TextView) v.findViewById(R.id.partyPlayResultsSecondPlaceScore);
        playerScoreTextViews[2] = (TextView) v.findViewById(R.id.partyPlayResultsThirdPlaceScore);
        playerScoreTextViews[3] = (TextView) v.findViewById(R.id.partyPlayResultsFourthPlaceScore);
        playerScoreTextViews[4] = (TextView) v.findViewById(R.id.partyPlayResultsFifthPlaceScore);
        playerScoreTextViews[5] = (TextView) v.findViewById(R.id.partyPlayResultsSixthPlaceScore);

        for (int i = 0; i < numberOfPlayers; i++) {
            playerNameTextViews[i].setText(playerNames[playerPositions.get(i)]);
            playerScoreTextViews[i].setText("" + calculatePlayerScore(playerPositions.get(i)));
        }

        if (numberOfPlayers < 3)
            Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayResultsThirdPlaceLayout), View.INVISIBLE);
        if (numberOfPlayers < 4)
            Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayResultsFourthPlaceLayout), View.INVISIBLE);
        if (numberOfPlayers < 5)
            Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayResultsFifthPlaceLayout), View.INVISIBLE);
        if (numberOfPlayers < 6)
            Helpers.setVisibilityIfExists(v.findViewById(R.id.partyPlayResultsSixthPlaceLayout), View.INVISIBLE);

        v.findViewById(R.id.partyPlayResultsPlayAgainButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((LoggedInMenu)getActivity()).startGame("", packId, levelId, playerNames, true);
            }
        });

        v.findViewById(R.id.partyPlayResultsMainMenuButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((LoggedInMenu)getActivity()).setClearStackBeforeNext();
                ((LoggedInMenu)getActivity()).clearBackStackIfNeeded();
            }
        });

        v.findViewById(R.id.partyPlayResultsNextLevelButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nextLevelId = null;

                List<String> packIds = Helpers.getLevelIdsForPack(getActivity(), packId);
                for (int i = 0; i < packIds.size(); i++) {
                    if (packIds.get(i).equals(levelId) && i < 2)
                        nextLevelId = packIds.get(i+1);
                }

                if (nextLevelId != null) {
                    ((LoggedInMenu)getActivity()).startGame("", packId, nextLevelId, playerNames, true);
                }
            }
        });

        Helpers.setAllFonts(v, getActivity());

        return v;
    }

    private String getMostWrongAnswerPlayerName() {
        String name;
        int mostwrongAnswers = 0;

        @SuppressWarnings("Convert2Diamond") List<Integer> playerIdsWithMostWrongQuestions = new ArrayList<Integer>();

        for (int i = 0; i < numberOfPlayers; i++) {
            if (getNumberOfIncorrectQuestions(i) > mostwrongAnswers) {
                mostwrongAnswers = getNumberOfIncorrectQuestions(i);
                name = playerNames[i];
                playerIdsWithMostWrongQuestions.add(i);
            }
        }

        Collections.sort(playerIdsWithMostWrongQuestions, new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer integer2) {
                return calculatePlayerScore(integer) - calculatePlayerScore(integer2);
            }
        });

        return playerNames[playerIdsWithMostWrongQuestions.get(0)];
    }

    private int getNumberOfIncorrectQuestions(int player) {
        int wrongAnswers = 0;
        for (int i = 0; i < numberOfQuestions; i++) {
            if (!answeredCorrectArray[player][i])
                wrongAnswers++;
        }
        return wrongAnswers;
    }

    private int getTimeForPlayer(int player) {
        int time = 0;

        for (int j = 0; j < numberOfQuestions; j++) {
            time += answerTimeArray[player][j];
        }

        return time;
    }

    private String getSlowestPlayer() {
        int highestTotalTime = Integer.MIN_VALUE;
        String name = "";


        for (int i = 0; i < numberOfPlayers; i++) {
            int time = getTimeForPlayer(i);

            if (time > highestTotalTime) {
                highestTotalTime = time;
                name = playerNames[i];
            }
        }

        return name;
    }

    private String getFastestPlayer() {
        int lowestTotalTime = Integer.MAX_VALUE;
        String name = "";


        for (int i = 0; i < numberOfPlayers; i++) {
            int time = getTimeForPlayer(i);

            if (time < lowestTotalTime) {
                lowestTotalTime = time;
                name = playerNames[i];
            }
        }

        return name;
    }

    private String getHighestScoredPlayerName() {
        int maxScore = -1;
        String name = "";
        for (int i = 0; i < numberOfPlayers; i++) {
            if (calculatePlayerScore(i) >= maxScore) {
                maxScore = calculatePlayerScore(i);
                name = playerNames[i];
            }
        }

        return name;
    }

    private String getLowestScoredPlayerName() {
        int minScore = Integer.MAX_VALUE;
        String name = "";
        for (int i = 0; i < numberOfPlayers; i++) {
            if (calculatePlayerScore(i) <= minScore) {
                minScore = calculatePlayerScore(i);
                name = playerNames[i];
            }
        }

        return name;
    }


    private int calculatePlayerScore(int player) {
        int score = 0;
        for (int i = 0; i < numberOfQuestions; i++) {
            if (answeredCorrectArray[player][i])
                score += calculateScore(answerTimeArray[player][i]);
        }

        return score;
    }
    private int calculateScore(int timeToAnswer) {
        double maxScorePerQuestion = maxScore / (double) (numberOfQuestions);
        double scorePerTimeUnit = maxScorePerQuestion
                / (maxTimeToAnswer / (numberOfQuestions));

        return (int) (maxScorePerQuestion - timeToAnswer * scorePerTimeUnit);
    }

    int answerArray[][];

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Bundle b = getArguments(); // Never used??
        numberOfPlayers = getArguments().getInt("numberOfPlayers");
        numberOfQuestions = getArguments().getInt("numberOfQuestions");
        playerNames = getArguments().getStringArray("playerNames");

        maxScore = getArguments().getInt("maxScore");
        maxTimeToAnswer = getArguments().getInt("maxTimeToAnswer");

        packId = getArguments().getString("packId");
        levelId = getArguments().getString("levelId");
        levelNumber = getArguments().getInt("levelNumber");

        answerTimeArray = new int[numberOfPlayers][];
        answeredCorrectArray = new boolean[numberOfPlayers][];

        for (int i = 0; i < numberOfPlayers; i++) {
            answerTimeArray[i] = getArguments().getIntArray("playerTime" + i);
            answeredCorrectArray[i] = getArguments().getBooleanArray("playerGuessedCorrect" + i);
        }
    }

    public static Fragment newInstance(int[][] answerArray, String[] playerNames) {
        return new PartyPlayResultsFragment();
    }

    public static Fragment newInstance(Bundle bundle) {
        Fragment f =  new PartyPlayResultsFragment();
        f.setArguments(bundle);
        return f;
    }
}
