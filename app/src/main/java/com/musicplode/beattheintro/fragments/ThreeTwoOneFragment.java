package com.musicplode.beattheintro.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.PartyPlayActivity;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.Sounds.Sound;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.modelhelpers.IGame;
import com.musicplode.beattheintro.modelhelpers.OnlineGame;
import com.musicplode.beattheintro.modelhelpers.Question;
import com.musicplode.beattheintro.storage.Storage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Simple fragment that controls the count down timer before a game starts
 */
public class ThreeTwoOneFragment extends BaseFragment {
    private boolean partyPlay;
    private String[] playerNames;

    @Override
	public String getName() {
		return "ThreeTwoOne";
	}
	private Integer ivs[];
	private Integer threetwoonestartImageView;
	private ViewGroup container;
	private Integer[] pbs;
	
	private ImageView number;
	private JSONObject gameInfo;
	private boolean offlineMode;
	
	
	public static final String BACKSTACK_KEY = "threeTwoOne";

    protected int getLayout() {
        return R.layout.three_two_one;
    }

	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		this.container = container;

        View v;
        if (partyPlay)
            v = inflater.inflate(R.layout.partyplaythreetwoone, container, false);
        else {
            ((LoggedInMenu)getActivity()).setShouldBlockBackButton(true);
            ((LoggedInMenu)getActivity()).disableAds();
            ((LoggedInMenu)getActivity()).disableLowerChart();

            v = inflater.inflate(R.layout.three_two_one, container, false);
        }
		threetwoonestartImageView = R.drawable.counter_start;
		number = (ImageView) v.findViewById(R.id.imageView1);
		
		ivs = new Integer[3];
		
		ivs[2] = R.drawable.counter_number_3;
		ivs[1] = R.drawable.counter_number_2;
		ivs[0] = R.drawable.counter_number_1;
		
		pbs = new Integer[3];
		
		MyTimer mt = new MyTimer(4);
		mt.start();

        ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sound.COUNTDOWN);
		
		return v;
	}
	class MyTimer extends CountDownTimer {
		private int onSec;
		private long maxTime;
		
		public MyTimer(int sec) {
			super(sec*1000, 50);
			maxTime = sec*1000;
			onSec = sec;
		}
		@Override
		public void onFinish() {
			if (getActivity() != null && getFragmentManager() != null) {
				try {
					FragmentTransaction ft = getFragmentManager().beginTransaction();

                    if (partyPlay)
                        ft.replace(container.getId(), PartyPlayGameFragment.newInstance(gameInfo.toString(), playerNames));
                    else {
                        ft.replace(container.getId(), GameFragment.newInstance(gameInfo.toString(), offlineMode), GameFragment.TAG);
                        ft.addToBackStack(null);
                    }

					ft.commit();
				} catch (Exception e) {
					e.printStackTrace();
					
					if (getActivity() != null)
						getActivity().finish();
				}
			}
		}
		@SuppressLint("NewApi")
		@Override
		public void onTick(long millisUntilFinished) {
			int currSec = (int) (millisUntilFinished/1000);
			double progress = (maxTime - millisUntilFinished)%1000;
			
			progress = progress/1000;
			
			int progC = (int)(progress*34);
			
			if (currSec != onSec) {
				onSec = currSec;
				
				int i = currSec - 1;
				
				if (0 <= i && i < pbs.length) {
					number.setImageResource(ivs[currSec-1]);
					
					if (android.os.Build.VERSION.SDK_INT>=11) {
						number.setScaleX(currSec-1);
						number.setScaleY(currSec-1);
					}
				}
			}
			if (currSec-1 >= 0 && currSec <= 3) {
				if (android.os.Build.VERSION.SDK_INT>=11) {
					number.setScaleX(1+progC/(float)34);
					number.setScaleY(1+progC/(float)34);
				}
			}
			else {
				if (android.os.Build.VERSION.SDK_INT>=11) { 
					number.setScaleX(1);
					number.setScaleY(1);
				}
				number.setImageResource(threetwoonestartImageView);
			}
		}
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
            if (getArguments().containsKey("gameInfo"))
    			gameInfo = new JSONObject(getArguments().getString("gameInfo"));
			offlineMode = getArguments().getBoolean("offlineMode");
            partyPlay = getArguments().getBoolean("partyPlay");
            if (getArguments().containsKey("playerNames"))
                playerNames = getArguments().getStringArray("playerNames");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		if (getActivity() instanceof LoggedInMenu)
    		((LoggedInMenu)getActivity()).stopMediaPlayers();


        if (getActivity() instanceof PartyPlayActivity) {
            try {
                String mp3Path = Helpers.getIntroMp3URL(gameInfo.getJSONArray("Questions").getJSONObject(0).getJSONObject("TrackCorrect").getString("Id"));
                ((PartyPlayActivity)getActivity()).prepareSong(mp3Path, null, true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
		else if (!offlineMode && gameInfo != null) {
            IGame game = new OnlineGame(gameInfo);
			Storage.storeLastPlayedPack(getActivity(), game.getPackId());
			for (Question q : game.getPlayer().getQuestions()) {
				if (!q.hasAnswered()) {

                    if (getActivity() instanceof LoggedInMenu)
                        ((LoggedInMenu)getActivity()).prepareSong(q.getCorrectTrack().getStreamURL(), null, true);
                    else if (getActivity() instanceof PartyPlayActivity)
                        ((PartyPlayActivity)getActivity()).prepareSong(q.getCorrectTrack().getStreamURL(), null, true);

					break;
				}
			}
		}
	}
	public static Fragment newInstance(JSONObject gameInfo, boolean offlineMode, boolean partyPlay, String playerNames[]) {
		ThreeTwoOneFragment f = new ThreeTwoOneFragment();
		Bundle b = new Bundle();

        if (gameInfo != null)
    		b.putString("gameInfo", gameInfo.toString());
		b.putBoolean("offlineMode", offlineMode);
        b.putBoolean("partyPlay", partyPlay);
        if (playerNames != null)
            b.putStringArray("playerNames", playerNames);
		f.setArguments(b);
		return f;
	}
}
