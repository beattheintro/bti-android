package com.musicplode.beattheintro.fragments;

import android.support.v4.app.Fragment;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.musicplode.beattheintro.BTIApp;


/**
 * Every fragment should extend this abstract fragment and implement getName() to enable automagic tracking
 */
public abstract class BaseFragment extends Fragment {
    public abstract String getName();

    /**
	 * Reports the name (from getName()) as the screen name for this fragment
	 */
    @Override
    public void onResume() {
        super.onResume();
        
        Tracker t = ((BTIApp)getActivity().getApplication()).getTracker();
        if (t != null) {
        	t.setScreenName(getName());
        	t.send(new HitBuilders.AppViewBuilder().build());
        }
    }
}
