package com.musicplode.beattheintro.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.modelhelpers.UserProfile;
import com.musicplode.beattheintro.storage.Storage;

/**
 * The menu that holds the different choices on what type of opponent to invite, such as Facebook, Email etc
 */
public class ChooseOpponentFragment extends SocialHelperFragment {
	@Override
	public String getName() {
		return "ChooseOpponentList";
	}
	private View v;
	private boolean isTablet;
	public static final String BACKSTACK_ENTRY = "ChooseOpponentFragment";
	
	private int startY;
	private int height;
	private String packId;
	private String levelId;
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.chooseopponentmenu, container, false);

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));

		isTablet = getResources().getBoolean(R.bool.tablet);
		
		v.findViewById(R.id.chooseOpponentFacebookButton).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
                /**
                 * Start the ChooseFacebookFragment
                 */
				startY = (int) v.findViewById(R.id.chooseOpponentFacebookButton).getY();
				height = v.findViewById(R.id.chooseOpponentFacebookButton).getHeight();
				
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				if (isTablet) {
					getFragmentManager().popBackStackImmediate(BACKSTACK_ENTRY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    Helpers.setFragmentAnimation(ft);
                    //ft.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left);
					ft.replace(R.id.chooseOpponentFrameLayout, ChooseFacebookFragment.newInstance(packId, levelId, startY, height, container.getId()));
					ft.addToBackStack(BACKSTACK_ENTRY);
				}
				else {
					ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
					ft.replace(container.getId(), ChooseFacebookFragment.newInstance(packId, levelId));
					ft.addToBackStack(null);
				}
				ft.commit();
			}
		});
		v.findViewById(R.id.chooseOpponentEmailButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                /**
                 * Start the ChooseEmailFragment
                 */
				startY = (int) v.findViewById(R.id.chooseOpponentEmailButton).getY();
				height = v.findViewById(R.id.chooseOpponentEmailButton).getHeight();
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				if (isTablet) {
					getFragmentManager().popBackStackImmediate(BACKSTACK_ENTRY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					Helpers.setFragmentAnimation(ft);
					ft.replace(R.id.chooseOpponentFrameLayout, ChooseEmailFragment.newInstance(packId, levelId, startY, height, container.getId()));
					ft.addToBackStack(BACKSTACK_ENTRY);
				}
				else {
					Helpers.setFragmentAnimation(ft);
					ft.replace(container.getId(), ChooseEmailFragment.newInstance(packId, levelId));
					ft.addToBackStack(null);
				}
				ft.commit();
			}
		});
		v.findViewById(R.id.chooseOpponentBestMatchButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                /**
                 * Start the ChooseBestMatchFragment
                 */
				startY = (int) v.findViewById(R.id.chooseOpponentBestMatchButton).getY();
				height = v.findViewById(R.id.chooseOpponentBestMatchButton).getHeight();
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				if (isTablet) {
					getFragmentManager().popBackStackImmediate(BACKSTACK_ENTRY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					Helpers.setFragmentAnimation(ft);
					ft.replace(R.id.chooseOpponentFrameLayout, ChooseBestMatchFragment.newInstance(packId, levelId, startY, height, container.getId()));
					ft.addToBackStack(BACKSTACK_ENTRY);
				}
				else {
					Helpers.setFragmentAnimation(ft);
					ft.replace(container.getId(), ChooseBestMatchFragment.newInstance(packId, levelId));
					ft.addToBackStack(null);
				}
				ft.commit();
			}
		});


        /**
         * PartyPlay mode is only enabled if the button "chooseOpponentPartyPlayButton" exists
         */
        View partyPlayButton = v.findViewById(R.id.chooseOpponentPartyPlayButton);
        if (partyPlayButton != null) {
            partyPlayButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    /**
                     * Start the ChoosePartyPlayOpponentsFragment
                     */
                    startY = (int) v.findViewById(R.id.chooseOpponentPartyPlayButton).getY();
                    height = v.findViewById(R.id.chooseOpponentPartyPlayButton).getHeight();
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    if (isTablet) {
                        getFragmentManager().popBackStackImmediate(BACKSTACK_ENTRY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        Helpers.setFragmentAnimation(ft);
                        ft.replace(R.id.chooseOpponentFrameLayout, ChoosePartyPlayOpponentsFragment.newInstance(packId, levelId, startY, height, container.getId()));
                        ft.addToBackStack(BACKSTACK_ENTRY);
                    }
                    else {
                        Helpers.setFragmentAnimation(ft);
                        ft.replace(container.getId(), ChoosePartyPlayOpponentsFragment.newInstance(packId, levelId));
                        ft.addToBackStack(null);
                    }
                    ft.commit();
                }
            });
        }

		//TODO: Readd when twitter is readded to server
//		v.findViewById(R.id.chooseOpponentTwitterButton).setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				startY = (int) v.findViewById(R.id.chooseOpponentTwitterButton).getY();
//				height = v.findViewById(R.id.chooseOpponentTwitterButton).getHeight();
//				
//				FragmentTransaction ft = getFragmentManager().beginTransaction();
//				if (isTablet) {
//					Helpers.setFragmentAnimation(ft);
//					getFragmentManager().popBackStackImmediate(BACKSTACK_ENTRY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//					ft.replace(R.id.chooseOpponentFrameLayout, ChooseTwitterFragment.newInstance(packId, levelId, startY, height));
//					ft.addToBackStack(BACKSTACK_ENTRY);
//				}
//				else {
//					Helpers.setFragmentAnimation(ft);
//					ft.replace(container.getId(), ChooseTwitterFragment.newInstance(packId, levelId));
//					ft.addToBackStack(null);
//				}
//				ft.commit();
//			}
//		});
		
		Helpers.setAllFonts(v, getActivity());
		// Helpers.addSwirl(v.findViewById(R.id.chooseOpponentSwirl));

        /**
         * If the user is logged in via facebook, enable the facebook option
         */
		UserProfile up = new UserProfile(Storage.getUserProfile(getActivity()));
		if (up.getFacebookId().length() <= 0) {
			v.findViewById(R.id.chooseOpponentFacebookButton).setEnabled(false);
		}
		
		return v;
	}
	
	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
		if (getArguments() != null) {
			packId = getArguments().getString("packId");
			levelId = getArguments().getString("levelId");
		}
	}
	public static Fragment newInstance(String packId, String levelId) {
		Fragment f = new ChooseOpponentFragment();
		Bundle b = new Bundle();
		b.putString("packId", packId);
		b.putString("levelId", levelId);
		f.setArguments(b);
		
		return f;
	}
}
