package com.musicplode.beattheintro.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.modelhelpers.IGame;
import com.musicplode.beattheintro.modelhelpers.OnlineGame;

import org.json.JSONObject;

public class WhoWonContentFragment extends BaseFragment {
	@Override
	public String getName() {
		return "WhoWon";
	}
	
	private IGame game;
	private String packId;
	private int containerId;
	
	public void updateUI() {
		if (getActivity() != null)
			((LoggedInMenu)getActivity()).updateUI();
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        updateUI();
        ((LoggedInMenu)getActivity()).setShouldBlockBackButton(false);
        
        try {
			game = new OnlineGame(new JSONObject(getArguments().getString("json")));
			
			containerId = getArguments().getInt("containerId");
			
			packId = game.getPackId();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	@Override
	public void onResume() {
		super.onResume();

        if (isVisible()) {
            ((LoggedInMenu) getActivity()).enableLowerChart();
            ((LoggedInMenu) getActivity()).disableAds();
        }
	}
	
	private int getMyScore() {
		return game.getPlayer().getScore();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.whowon, container, false);

		((TextView) v.findViewById(R.id.resultScoreTextView)).setText("" + getMyScore());
		
		
		String s = getString(R.string.WHO_WON_AVG_ANSWER_TIME);
		String avgTime = game.getPlayer().getAVGTime();
		
		if (avgTime == null)
			avgTime = "";

		s = s.replace("[X]", avgTime);
		((TextView) v.findViewById(R.id.whoWonAvgTimeTextView)).setText(s);
		
		
		View newChartPos = v.findViewById(R.id.whoWonNewChartPositionTextView);
		View newHighScore = v.findViewById(R.id.whoWonNewHighScoreTextView);
		
		
		if (!game.isNewHighScore()) {
			if (newHighScore != null)
				newHighScore.setVisibility(View.INVISIBLE);
			newHighScore = null;
		}
		
		if (!game.isNewChartPos()) {
			if (newChartPos != null)
				newChartPos.setVisibility(View.INVISIBLE);
			newChartPos = null;
		}

		if (newChartPos != null) {
            /**
             * Spin in the New Chart Position badge
             */
			Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.spin);
			newChartPos.setAnimation(a);
			
			newChartPos.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					clearBackStack(getActivity());
					
					FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					ft.replace(containerId, new MyChartFragment());
					ft.addToBackStack(null);
					ft.commit();
				}
			});
		}
		
		if (newHighScore != null) {
            /**
             * Spin in the New Highscore Position badge
             */
			Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.spin);
			newHighScore.setAnimation(a);
			
			newHighScore.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					clearBackStack(getActivity());
					
					FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					ft.replace(containerId, LeaderboardFragment.newInstance(packId, true, game.getLevelTypeId()));
					ft.addToBackStack(null);
					ft.commit();
				}
			});
		}
		
		
		Helpers.setAllFonts(v, getActivity());


        /**
         * Animate in the number of stars the Player achieved
         */
		ImageView stars[] = new ImageView[3];
		stars[0] = (ImageView) v.findViewById(R.id.star1);
		stars[1] = (ImageView) v.findViewById(R.id.star2);
		stars[2] = (ImageView) v.findViewById(R.id.star3);
		
		int acheivedStars = game.getPlayer().getStars();
		int maxOffset = 0;
		for (int i = 0; i < stars.length; i++) {
			if (i < acheivedStars) {
				stars[i].setImageResource(R.drawable.gameplay_star);
				
				Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.starscale);
				maxOffset = i*500;
				a.setStartOffset(maxOffset);
				stars[i].setAnimation(a);
			}
			else {
				stars[i].setImageResource(R.drawable.gameplay_star_empty);
				
				Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.starscale);
				a.setStartOffset(maxOffset+1000);
				a.setDuration(1);
				stars[i].setAnimation(a);
			}
		}
		
		
		
		/**
		 * Who won image view
		 */
		if (game.getNumberOfPlayers() > 1) {
			boolean didPlayer1Win = game.getPlayer().didWin();
			boolean didPlayer2Win = game.getOpponents().get(0).didWin();

			if (didPlayer1Win) {
				ImageView whoWonYouWonImageView = (ImageView) v.findViewById(R.id.whoWonYouWonImageView);
				whoWonYouWonImageView.setImageResource(R.drawable.you_won);
				whoWonYouWonImageView.setVisibility(View.VISIBLE);
			}
			else if (didPlayer2Win) {
				ImageView whoWonYouWonImageView = (ImageView) v.findViewById(R.id.whoWonYouWonImageView);
				whoWonYouWonImageView.setImageResource(R.drawable.you_lost);
				whoWonYouWonImageView.setVisibility(View.VISIBLE);
			}
		}
		
		return v;
	}

    /**
     * Go back to the very first fragment
     */
	private static void clearBackStack(FragmentActivity a ) {
		((LoggedInMenu) a).stopMediaPlayers();
		
		a.onBackPressed();
		
		int curr = a.getSupportFragmentManager().getBackStackEntryCount();
		int prev = curr + 1;
		while (curr != 0 && curr != prev) {
			a.onBackPressed();
			prev = curr;
			curr = a.getSupportFragmentManager().getBackStackEntryCount();
		}
	}
	public static Fragment newInstance(String json, JSONObject trackToArtist, int containerId) {
		WhoWonContentFragment f = new WhoWonContentFragment();
		Bundle args = new Bundle();
        args.putString("json", json);
        args.putString("trackToArtist", trackToArtist.toString());
        args.putInt("containerId", containerId);
        f.setArguments(args);
		
		return f;
	}
}
