package com.musicplode.beattheintro.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.Sounds;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.modelhelpers.IGame;
import com.musicplode.beattheintro.modelhelpers.OnlineGame;
import com.musicplode.beattheintro.modelhelpers.Pack;

import org.json.JSONObject;

public class WhoWonFragment extends BaseFragment {
	@Override
	public String getName() {
		return "WhoWon";
	}
	
	private IGame game;
	private String packId;
	private JSONObject trackToArtist;
	private String levelTypeId;
	private int containerId;
	private String opponentName = "";
	private View v;
	private ResultsAfterGameFragment resultsAfterGameFragment;
	private int[] prevStars;
	private int[] currStars = new int[3];
	private int bonusReward;
	
	public void updateUI() {
		if (getActivity() != null)
			((LoggedInMenu)getActivity()).updateUI();
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        
        ((LoggedInMenu)getActivity()).setClearStackBeforeNext();
        
        updateUI();
        ((LoggedInMenu)getActivity()).setShouldBlockBackButton(false);
        
        try {
			game = new OnlineGame(new JSONObject(getArguments().getString("json")));
			
			bonusReward = game.getPlayer().getBonus();
			
			if (game.getNumberOfPlayers() > 1)
				opponentName = game.getOpponents().get(0).getName();
			

	        prevStars = getArguments().getIntArray("prevStars");
	        
			trackToArtist = new JSONObject(getArguments().getString("trackToArtist"));
			containerId = getArguments().getInt("containerId");
			
			packId = game.getPackId();
			levelTypeId = game.getLevelTypeId();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.whowonframe, container, false);
		
		v.findViewById(R.id.seeResultsButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v2) {
                /**
                 * Show the Results fragment
                 */
				View fireworks = v.findViewById(R.id.fireworkView1);
				if (fireworks != null)
					fireworks.setVisibility(View.GONE);
				
				
				if (resultsAfterGameFragment != null) {
					resultsAfterGameFragment.onResultButtonSwitch();
				}
				else {
					FragmentTransaction ft = getChildFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					ft.replace(R.id.whowonFrame, ResultsAfterGameFragment.newInstance(game.getJSON().toString(), trackToArtist, container.getId(), opponentName, levelTypeId));
					ft.commit();
				}
			}
		});
		
		v.findViewById(R.id.mainMenuButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                /**
                 * Exit to main menu
                 */
				if (getActivity() != null) {
					FragmentActivity apa = getActivity();
					clearBackStack(apa);
					((LoggedInMenu)apa).showInterstitial();
				}
			}
		});
		
		
		v.findViewById(R.id.nextLevelButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                /**
                 * Start the next level (or the current level if the next level is not accessible)
                 */
				if (game.getNumberOfPlayers() <= 1) {
					FragmentActivity apa = getActivity();
					clearBackStack(apa);
					String levelId = game.getPack(apa).getNextPlayableLevel(levelTypeId);
					
					if (levelId != null) {
						((LoggedInMenu)apa).startGame("", packId, levelId, null, true);
					}
					
					return;
				}
				
				final Dialog d = DialogHelpers.getDialog(getActivity(), R.string.WANT_TO_PLAY_AGAIN, R.string.PLAY_OPPONENT_AGAIN, R.string.CHOOOSE_A_NEW_OPPONENT, R.string.SINGLE_PLAYER);
				d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						/**
						 * Play opponenet again
						 */
						FragmentActivity apa = getActivity();
						clearBackStack(apa);
					
						String levelId = game.getPack(apa).getHighestPlayableLevelId();
						if (levelId != null) {
							((LoggedInMenu)apa).startGame(opponentName, packId, levelId, null, true);
						}
						
						d.hide();
						d.cancel();
					}
				});
				
				if (game.getNumberOfPlayers() <= 1)
					d.findViewById(R.id.dialogButton1).setVisibility(View.GONE);
				
				d.findViewById(R.id.dialogButton2).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						FragmentActivity apa = getActivity();
						clearBackStack(apa);
						
						FragmentTransaction ft = apa.getSupportFragmentManager().beginTransaction();
						Helpers.setFragmentAnimation(ft);
						ft.replace(containerId, PackSelectFragment.newInstance(true));
						ft.addToBackStack(null);
						ft.commit();
						
						d.hide();
						d.cancel();					
					}
				});
				d.findViewById(R.id.dialogButton3).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						FragmentActivity apa = getActivity();
						clearBackStack(apa);
						
						String levelId = game.getPack(apa).getHighestPlayableLevelId();
						
						if (levelId != null)
							Helpers.startGame((LoggedInMenu) apa, R.id.frameLayout1, "", packId, levelId);
						
						d.hide();
						d.cancel();
					}
				});
				
				d.show();
			}
		});
		
		FragmentTransaction ft = getChildFragmentManager().beginTransaction();
		ft.replace(R.id.whowonFrame, WhoWonContentFragment.newInstance(game.getJSON().toString(), trackToArtist, containerId));
		ft.commit();
		
		Helpers.setAllFonts(v, getActivity());
		
		((LoggedInMenu)getActivity()).enableAds();
		
		String levelId = game.getPack(getActivity()).getHighestPlayableLevelId();
		if (game.getLevelTypeId().equals(levelId) && v.findViewById(R.id.nextLevelButton) != null) {
			((TextView)v.findViewById(R.id.nextLevelButton)).setText(R.string.PLAY_AGAIN);
		}
		
		
		/**
		 * Bonus & unlock  & mastering dialog
		 */
		boolean didMasterPack = false;
		int unlockedLevel = 0;
		
		Pack p = game.getPack(getActivity());
		
		for (int i = 0; i < 3; i++) {
			currStars[i] = p.getStarsOnLevel(i);
		}
		
		if (prevStars != null) {
			int sumPrevStars = 0, sumCurrStars = 0;
			for (int i : prevStars)
				sumPrevStars += i;
			for (int i : currStars)
				sumCurrStars += i;
			
			if (sumPrevStars != sumCurrStars && sumCurrStars == 9)
				didMasterPack = true;
			
			for (int i = 0; i < 2; i++) {
				if (prevStars[i] < 2 && currStars[i] >= 2)
					unlockedLevel = i+2;
			}
		}
		String s = null;
		
		if (unlockedLevel > 0)
			s = getString(R.string.UNLOCKED_LEVEL_TEXT).replace("[levelNumber]", "" + unlockedLevel);
		else if (didMasterPack)
			s = getString(R.string.MASTERED_PACK_TEXT).replace("[packName]", game.getPack(getActivity()).getName());
		
		if (bonusReward > 0) {
			if (s == null)
				s = "";
			else
				s += "\n\n";
			
			s += getString(R.string.BONUS_REWARD_TEXT).replace("[bonusReward]", "" + bonusReward); 
		}
		
		if (s != null) {
			DialogHelpers.showHardCodedDialog(getActivity(), s, null);
		}
		
		
		((LoggedInMenu)getActivity()).setPackAchievement(packId, game.getPack(getActivity()).getNumberOfStars());

        ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.SMALLAPPLAUSE);

		return v;
	}
	
	public static void clearBackStack(FragmentActivity a ) {
		((LoggedInMenu) a).stopMediaPlayers();
		a.onBackPressed();
		
		int curr = a.getSupportFragmentManager().getBackStackEntryCount();
		int prev = curr + 1;
		while (curr != 0 && curr != prev) {
			a.onBackPressed();
			prev = curr;
			curr = a.getSupportFragmentManager().getBackStackEntryCount();
		}
	}
	public static Fragment newInstance(String json, JSONObject trackToArtist, int containerId, int prevStars[]) {
		WhoWonFragment f = new WhoWonFragment();
		Bundle args = new Bundle();
        args.putString("json", json);
        args.putString("trackToArtist", trackToArtist.toString());
        args.putInt("containerId", containerId);
        args.putIntArray("prevStars", prevStars);
        f.setArguments(args);
		
		return f;
	}

	public void setResultButton(ResultsAfterGameFragment resultsAfterGameFragment) {
		this.resultsAfterGameFragment = resultsAfterGameFragment;
	}

	public void setResultText(int textResource) {
		((TextView)v.findViewById(R.id.seeResultsButton)).setText(textResource);
	}

	public void hideResultButton() {
		if (getResources().getBoolean(R.bool.tablet))
			v.findViewById(R.id.seeResultsButton).setVisibility(View.INVISIBLE);
		else
			v.findViewById(R.id.seeResultsButton).setVisibility(View.GONE);
	}
}
