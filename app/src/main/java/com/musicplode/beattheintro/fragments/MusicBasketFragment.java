package com.musicplode.beattheintro.fragments;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.LoggedInMenu.OnSongCompletion;
import com.musicplode.beattheintro.LoggedInMenu.OnSongReadyListener;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.Sounds;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.helpers.TrackerHelpers;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;
import com.musicplode.beattheintro.support.SwipeDismissListViewTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MusicBasketFragment extends BaseFragment implements IUpdateableFragment {
	private ListView lv;
	private View v;

    @Override
    public void onStop() {
        super.onStop();

        if (getActivity() != null && getActivity() instanceof LoggedInMenu) {
            ((LoggedInMenu)getActivity()).stopMediaPlayers();
        }
    }

    @Override
	public String getName() {
		return "MusicBasketList";
	}
	public void updateUI() {
		if (getActivity() != null)
			((LoggedInMenu)getActivity()).updateUI();
	}
	
	public void updateMusicBasketList() {
		if (v == null || getActivity() == null)
			return;
		
		int firstVisiblePos = 0; 
		
		if (lv != null) 
			firstVisiblePos = lv.getFirstVisiblePosition();
		
		lv = (ListView) v.findViewById(R.id.listView1);
		final LikedTracksAdapter lta = new LikedTracksAdapter(getActivity(), 0);

		try {
			JSONObject userProfile = Storage.getUserProfile(getActivity());
			JSONArray likesArray = userProfile.getJSONObject("UserProfile").getJSONArray("Likes");
			
			for (int i = 0; i < likesArray.length(); i++) {
				lta.add(new LikedTrack(likesArray.getJSONObject(i)));
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		lv.setAdapter(lta);
		lv.setEmptyView(v.findViewById(android.R.id.empty));
		
		lv.setSelection(firstVisiblePos);
		
		// Add swipe to remove functionality
		SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        lv,
                        new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                            	return true;
                            }

                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                	if (lta.getItem(position) == null)
                                		continue;
                                	
                                    String trackId = "";
                					try {
                						trackId = lta.getItem(position).getJSON().getString("Id");
                					} catch (JSONException e) {
                						e.printStackTrace();
                					}
                					
                					TrackerHelpers.removeTrackFromBasket(getActivity(), trackId);
                					
                					JSONObject o = JSONRequestHelpers.getRemoveLikedTrack(getActivity(), trackId);
                					Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
                						@Override
                						public void okResult(JSONObject response) {
                							Storage.store(getActivity(), Storage.USERPROFILE, response);
                							updateUI();
                						}
                						
                						@Override
                						public void badResult(String errorMessage) {
                						}
                					});
                					
                					lta.remove(lta.getItem(position));
                                }
                                
                                lta.notifyDataSetChanged();
                            }
                        });
        lv.setOnTouchListener(touchListener);
        
        lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (arg0 != null && arg0.getAdapter() != null)
					((LikedTracksAdapter)arg0.getAdapter()).playSong(arg2);
			}
		});
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.musicbasket, container, false);

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));

		updateMusicBasketList();
        
		Helpers.setAllFonts(v, getActivity());
		
		// Helpers.addSwirl(v.findViewById(R.id.musicbasketSwirl));
		
		Helpers.refreshProfile(getActivity());
		
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		((LoggedInMenu)getActivity()).disableLowerChart();
		((LoggedInMenu)getActivity()).enableAds();

        ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.OPEN);
	}
	
	public class LikedTracksAdapter extends ArrayAdapter<LikedTrack> {
		private String currentlyPlayingTrack;
		private boolean playing;
		private String lastTrackId;

		public LikedTracksAdapter(Context context, int resource) {
			super(context, resource);
		}
		
		public void setCurrentTrack(String currentTrack, boolean playing) {
			currentlyPlayingTrack = currentTrack;
			this.playing = playing;
		
			notifyDataSetChanged();
		}
		
		/**
		 * Start playback of song at position. If song at position is currently
		 * playing it will be paused
		 * 
		 * @param position
		 */
		public void playSong(int position) {
			if (getActivity() == null)
				return;
			String trackId = "";
			
			try {
				if (getItem(position) != null && getItem(position).getJSON() != null) {
					trackId = getItem(position).getJSON().getString("Id");
					
					if (playing) {
						((LoggedInMenu)getActivity()).stopMediaPlayers();
						playing = false;
						notifyDataSetChanged();
						
						if (lastTrackId != null && trackId.equals(lastTrackId)) 
							return;
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			TrackerHelpers.playTrackInBasket(getActivity(), trackId);
			lastTrackId = trackId;
			
			final String mp3Path = Helpers.getIntroMp3URL(trackId);
			
			LoggedInMenu mainActivity = (LoggedInMenu)getActivity();
			if (mainActivity != null) {
				mainActivity.stopPreparingSongs();
				mainActivity.prepareSong(mp3Path, new OnErrorListener() {
					@Override
					public boolean onError(MediaPlayer mp, int what, int extra) {
						
						return false;
					}
				});
				mainActivity.playIfReady(mp3Path, new OnSongReadyListener() {
					@Override
					public void onReady(MediaPlayer mediaplayer) {
						mediaplayer.start();
						setCurrentTrack(mp3Path, true);
						mediaplayer.setOnCompletionListener(new OnCompletionListener() {
							@Override
							public void onCompletion(MediaPlayer mp) {
								setCurrentTrack(mp3Path, false);
								notifyDataSetChanged();
							}
						});
					}
				}, new OnSongCompletion() {
					@Override
					public void onSongComplete(MediaPlayer mediaplayer, String songPath) {
						setCurrentTrack(mp3Path, false);
						notifyDataSetChanged();
					}
				}, 0);
			}
		}
		
		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater =
	                    (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.musicbasketcell, null);
				Helpers.setAllFonts(view, getContext());
			}
			
			TextView musicBasketTrackName = (TextView)view.findViewById(R.id.musicBasketTrackName);
			TextView musicBasketArtistName = (TextView)view.findViewById(R.id.musicBasketArtistName);
			
			if (playing) {
				String mp3Path = null;
				try {
					mp3Path = Helpers.getIntroMp3URL(getItem(position).getJSON().getString("Id"));
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (mp3Path != null && mp3Path.equals(currentlyPlayingTrack))
					((ImageView)view.findViewById(R.id.musicbasketListenImageView)).setImageResource(R.drawable.gameplay_pause);
				else
					((ImageView)view.findViewById(R.id.musicbasketListenImageView)).setImageResource(R.drawable.basket_listen);
			}
			else
				((ImageView)view.findViewById(R.id.musicbasketListenImageView)).setImageResource(R.drawable.basket_listen);
			
			view.findViewById(R.id.musicbasketListenImageView).setOnClickListener(null);
			view.findViewById(R.id.musicbasketAmazonButton).setOnClickListener(null);
			
			Helpers.addPulse(view.findViewById(R.id.musicbasketListenSwirlImageView), 0); // Add small Pulse to button
			
			/**
			 * If the current item is null, display EMPTYBASKET text
			 */
			if (getItem(position) == null) {
				musicBasketArtistName.setText(R.string.EMPTYBASKET);
				musicBasketTrackName.setVisibility(View.GONE);
				return view;
			}
			
			try {
				musicBasketTrackName.setVisibility(View.VISIBLE);
				musicBasketTrackName.setText(getItem(position).getJSON().getString("Title"));
				musicBasketArtistName.setText(getItem(position).getJSON().getString("Artist"));
				
				ImageView iv = (ImageView)view.findViewById(R.id.albumCover);
				Helpers.displayImage(getContext(), Helpers.getAlbumArtURL(getItem(position).getJSON().getString("Id")), iv);
				
				
				if (getItem(position).getJSON().has("URLAmazon") && getItem(position).getJSON().optString("URLAmazon").length() > 0) {
					view.findViewById(R.id.musicbasketAmazonButton).setVisibility(View.VISIBLE);
					view.findViewById(R.id.musicbasketAmazonButton).setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							try {
								String url = getItem(position).getJSON().getString("URLAmazon");
								Intent i = new Intent(Intent.ACTION_VIEW);
								i.setData(Uri.parse(url));
								getContext().startActivity(i);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					});
				}
				else {
					view.findViewById(R.id.musicbasketAmazonButton).setVisibility(View.INVISIBLE);
				}
				view.findViewById(R.id.musicbasketListenImageView).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						playSong(position);
					}
				});
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return view;
		}
	}
	
	public class LikedTrack {
		private JSONObject json;
		
		public LikedTrack(JSONObject json) {
			this.json = json;
		}
		
		public JSONObject getJSON() {
			return json;
		}
	}

	@Override
	public void onUpdate() {
		updateMusicBasketList();
	}
}
