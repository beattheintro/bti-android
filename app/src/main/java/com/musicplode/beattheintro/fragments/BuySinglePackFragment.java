package com.musicplode.beattheintro.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Shows a single Pack that the user can choose to unlock
 */
public class BuySinglePackFragment extends BaseFragment {
	@Override
	public String getName() {
		return "BuySinglePack";
	}
	private JSONObject json;
	private String packId;
	private boolean isTablet;
    private View v = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.buysinglepack, container, false);
		
		TextView title = (TextView) v.findViewById(R.id.buySinglePackTitle);
		TextView text = (TextView) v.findViewById(R.id.buySinglePackTextView);
		ImageView iv = (ImageView) v.findViewById(R.id.buySinglePackImageView);
		Button buyWithCoins = (Button) v.findViewById(R.id.buySinglePackBuyWithCoins);
		Button buySinglePackBuyMoreDiscs = (Button) v.findViewById(R.id.buySinglePackBuyMoreDiscs);
		
		
		isTablet = getResources().getBoolean(R.bool.tablet);
		
		if (buySinglePackBuyMoreDiscs != null) {
			buySinglePackBuyMoreDiscs.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
                    /**
                     * Start the BuyDiscsFragment
                     */
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					
					if (isTablet)
						ft.replace(container.getId(), BuyDiscsFragment.newInstance(-1, -1, 0));
					else
						ft.replace(container.getId(), new BuyDiscsFragment());
					
					ft.addToBackStack(null);
					ft.commit();
				}
			});
		}
		
		try {
			title.setText(json.getString("Name"));
			text.setText(json.getString("Description"));
			
			packId = json.getString("Id");
			
			Helpers.displayImage(getActivity(), Helpers.getPackArtURL(packId), iv);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		buyWithCoins.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                /**
                 * Unlock the selected pack
                 */
				final ProgressDialog pd = new ProgressDialog(getActivity());
				pd.setMessage(getString(R.string.LOADING));
				pd.setCancelable(false);
				pd.show();
				
				JSONObject jsonReq = JSONRequestHelpers.getUnlockPackJSON(Storage.getUserProfile(getActivity()), packId);
				Storage.addRequestToQueue(getActivity(), Storage.UNLOCKPACK_URL, jsonReq, new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						if (getActivity() != null) {
							Storage.storeLastPlayedPack(getActivity(), packId);
							
							Storage.updateUserProfile(getActivity(), response, true);
							//DialogHelpers.showOkDialog(getActivity(), R.string.AFTER_BUY_PACK_TITLE, R.string.AFTER_BUY_TEXT, R.string.OK);
							pd.cancel();
							
							getActivity().onBackPressed();
						}
					}
					
					@Override
					public void badResult(String errorMessage) {
						if (getActivity() != null) {
                            if (errorMessage != null) {
                                DialogHelpers.showHardCodedDialog(getActivity(), getString(R.string.ERROR_TITLE), errorMessage);
                            }
                            else {
                                DialogHelpers.showErrorDialog(getActivity(), R.string.ERROR_INTERNET_CONNECTION_TITLE, R.string.ERROR_INTERNET_CONNECTION_TEXT, R.string.OK);
                            }
                            pd.cancel();
						}
					}
				});
			}
		});
		
		Helpers.setAllFonts(v, getActivity());
		
		return v;
	}

	public static Fragment newInstance(JSONObject packJSON) {
		BuySinglePackFragment fragment = new BuySinglePackFragment();
		Bundle b = new Bundle();
		b.putString("json", packJSON.toString());
		fragment.setArguments(b);
		
		return fragment;
	}
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
			json = new JSONObject(getArguments() != null ? getArguments().getString("json") : "{}");
		} catch (JSONException e) {
			e.printStackTrace();
		}
    }
}
