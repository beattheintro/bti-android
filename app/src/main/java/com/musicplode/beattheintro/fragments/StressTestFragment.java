package com.musicplode.beattheintro.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;

import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.modelhelpers.Pack;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class StressTestFragment extends Fragment {
	private JSONObject userSession;
    private View v;
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.stresstest, container, false);

		userSession = Storage.getUserProfile(getActivity());
		
		v.findViewById(R.id.stressButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startStressTest();
			}
		});
		
		v.findViewById(R.id.stressHideAll).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				JSONObject o = JSONRequestHelpers.getRefreshUserProfileJSON(getActivity());
				Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						if (getActivity() == null)
							return;
						Storage.updateUserProfile(getActivity(), response, true);
						hideAllGames(System.currentTimeMillis(), -1);
					}
					
					@Override
					public void badResult(String errorMessage) {
						
					}
				});
				
			}
		});
		
		
		v.findViewById(R.id.stressMultiUser).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				for (int i = 0; i < 10; i++) {
					JSONObject jsonReq = JSONRequestHelpers.getLoginLaterJSON();
					final long startTime = System.currentTimeMillis();
					
					Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, jsonReq, new OnNetworkResult() {
						@Override
						public void okResult(JSONObject response) {
							startNewGame(response, System.currentTimeMillis()-startTime);
						}
						@Override
						public void badResult(String errorMessage) {
						}
					});
				}
			}
		});
		
		return v;
	}

	protected void hideAllGames(final long startTime, int games) {
		JSONArray arr;
		try {
			arr = userSession.getJSONObject("UserProfile").getJSONArray("Games");
		
			if (arr.length() > 0) {
				if (games == -1)
					games = arr.length();
				
				final int apa = games;
				JSONObject o = JSONRequestHelpers.getCloseGameJSON(userSession, arr.getJSONObject(0).getString("Id"));
				if (getActivity() == null)
					return;
				Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						userSession = response;
						hideAllGames(startTime, apa);
					}
					@Override
					public void badResult(String errorMessage) {
						
					}
				});
			}
			else {
				if (getActivity() == null)
					return;
				addString(games + " hidden in " + (System.currentTimeMillis() - startTime) + "ms\n");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	protected void startStressTest() {
		if (userSession == null) {
			if (getActivity() == null)
				return;
			userSession = Storage.getUserProfile(getActivity());
		}
		
		for (int i = 0; i < 10; i++) {
			startNewGame(userSession, -1);
		}
	}

	private void startNewGame(final JSONObject userSession, final long userProfileCreateTime) {
		if (getActivity() == null)
			return;
		List<JSONObject> packs = Helpers.getAllPackJSONS(getActivity());
		Pack p = null;
		for (JSONObject o : packs) {
			p = new Pack(o);
			if (p.isPlayable())
				break;
		}
		
		if (p != null) {
			JSONObject o = JSONRequestHelpers.getStartPackJSON(userSession, p.getId(), p.getLevelTypeId(0), "");
			final long startStartPackTime = System.currentTimeMillis();
			Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
				@Override
				public void okResult(JSONObject response) {
					final long startStartPackTotalTime = System.currentTimeMillis() - startStartPackTime;
					String gameId = "";
					try {
						gameId = response.getString("GameId");
					} catch (JSONException e) {
						e.printStackTrace();
					}
					if (gameId.length() > 0) {
						JSONObject o2 = JSONRequestHelpers.getResumeGameJSON(userSession, gameId);
						if (getActivity() == null)
							return;
						final long startTime = System.currentTimeMillis();
						Storage.addRequestToQueue(getActivity(), Storage.GAMERESUME_URL, o2, new OnNetworkResult() {
							@Override
							public void okResult(JSONObject response) {
								startAnsweringQuestions(userSession, response, userProfileCreateTime, startStartPackTotalTime, System.currentTimeMillis() - startTime, 0, 0);
							}
							
							@Override
							public void badResult(String errorMessage) {
								
							}
						});
					}
				}
				
				@Override
				public void badResult(String errorMessage) {
					
				}
			});
		}
	}
	private void startAnsweringQuestions(final JSONObject userSession, JSONObject response, final long userProfileCreateTime, final long createGameTime, final long resumeGameTime, final long answerQuestionTime, final int answerdQuestions) {
		try {
			JSONArray arr = response.getJSONArray("Players").getJSONObject(0).getJSONArray("Questions");
			
			for (int i = 0; i < arr.length(); i++) {
				JSONObject o = arr.getJSONObject(i);
				if (o.getString("AnswerTrackId").equals("00000000-0000-0000-0000-000000000000")) {
					String correctTrack;
					
					if (o.get("TrackCorrect") instanceof JSONObject)
						correctTrack =  o.getJSONObject("TrackCorrect").getString("Id");
					else
						correctTrack = o.getString("TrackCorrect");
					
					JSONObject req = JSONRequestHelpers.getAnswerJSON(userSession, o.getString("AnswerId"), correctTrack, 1, null);
					if (getActivity() == null)
						return;
					
					final long answerStartTime = System.currentTimeMillis();
					Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, req, new OnNetworkResult() {
						@Override
						public void okResult(JSONObject response) {
							
							final long answerTotalTimes = answerQuestionTime + System.currentTimeMillis() - answerStartTime;
							final int answerss = answerdQuestions + 1;
							
							try {
								
								if (response.getJSONObject("GameInfo").getJSONArray("Players").getJSONObject(0).getBoolean("Finished")) {
									
									JSONObject o = JSONRequestHelpers.getFinaliseGameGameJSON(userSession, response.getJSONObject("GameInfo").getString("Id"));
									if (getActivity() == null)
										return;
									final long finiliseGameStart = System.currentTimeMillis();
									Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, o, new OnNetworkResult() {
										@Override
										public void okResult(JSONObject response) {
											if (getActivity() == null)
												return;
											
											long finilistTime = System.currentTimeMillis() - finiliseGameStart;
											String sasdasd = "";
											if (userProfileCreateTime > 0)
												sasdasd += "Create Account time: " + userProfileCreateTime + "\n";
											
											sasdasd += "Create Game time: " + createGameTime + "\n" +
													"Resume Game time: " + resumeGameTime + "\n" +
													"Answer " + answerss + " questions took: " + answerTotalTimes + " (" + answerTotalTimes/(double)answerss +")\n" +
													"FinalizeGame took: " + finilistTime + "\n\n"; 
										
											addString(sasdasd);
										}
										
										@Override
										public void badResult(String errorMessage) {
											
										}
									});
								}
								else {
									startAnsweringQuestions(userSession, response.getJSONObject("GameInfo"), userProfileCreateTime, createGameTime, resumeGameTime, answerTotalTimes, answerss);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
						@Override
						public void badResult(String errorMessage) {
							
						}
					});
					break;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	private synchronized void addString(String s) {
		EditText et = (EditText) v.findViewById(R.id.editText1);
		et.setText(et.getText().toString() + s);
	}
}
