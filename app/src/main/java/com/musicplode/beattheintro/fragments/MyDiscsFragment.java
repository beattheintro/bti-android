package com.musicplode.beattheintro.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.Sounds;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.Helpers;

public class MyDiscsFragment extends BaseFragment {
	@Override
	public String getName() {
		return "MyCoinsList";
	}

	public static final String BACKSTACK_ENTRY = "MyCoinsFragment";
	private int startY;
	private int height;
	private int containerId;
	private int rootContainerId;
	private boolean isTablet;
	private Button selectedButton = null;
	private View v;
	private int preselected = -1;
	
	private void enableAllButtons() {
		v.findViewById(R.id.myCoinsBuyButton).setEnabled(true);
		v.findViewById(R.id.myCoinsEarnButton).setEnabled(true);
		v.findViewById(R.id.myCoinsUnlockPackButton).setEnabled(true);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		/**
		 * Store the current selected button to be used for the tablet version
		 */
		if (preselected >= 0) {
			if (preselected == 0)
				selectedButton = (Button) v.findViewById(R.id.myCoinsBuyButton);
		}
		
		updateButtons();

        if (isVisible()) {
            ((LoggedInMenu) getActivity()).enableLowerChart();
            ((LoggedInMenu) getActivity()).disableAds();
        }
	}
	
	protected void updateButtons() {
		enableAllButtons();
		
		if (selectedButton == null)
			return;
		
		selectedButton.setEnabled(false);
		v.findViewById(selectedButton.getId()).setEnabled(false);
		
		startY = (int) selectedButton.getY();
		height = selectedButton.getHeight();
		
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		Helpers.setFragmentAnimation(ft);
		
		if (isTablet) {
			getFragmentManager().popBackStackImmediate(BACKSTACK_ENTRY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
		
		switch (selectedButton.getId()) {
		case R.id.myCoinsBuyButton:
			ft.replace(containerId, BuyDiscsFragment.newInstance(startY, height, rootContainerId));
			break;
		}
		
		
		if (!isTablet) {
			ft.addToBackStack(BACKSTACK_ENTRY);
			selectedButton = null;
		}
		
		ft.commit();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.mydiscs, container, false);

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));

		isTablet = getResources().getBoolean(R.bool.tablet);
		
		rootContainerId = container.getId();
		containerId = container.getId();
		
		if (isTablet)
			containerId = R.id.mycoinsFrameLayout;
		
		v.findViewById(R.id.myCoinsBuyButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectedButton = (Button) v;
				updateButtons();
			}
		});
		
		v.findViewById(R.id.myCoinsEarnButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				Helpers.setFragmentAnimation(ft);
				ft.replace(container.getId(), EarnDiscsFragment.newInstance(0, 0, containerId));
				ft.addToBackStack(null);
				ft.commit();
			}
		});
		
		v.findViewById(R.id.myCoinsUnlockPackButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				Helpers.setFragmentAnimation(ft);
				ft.replace(container.getId(), new BuyMusicPacksFragment());
				ft.addToBackStack(null);
				ft.commit();
			}
		});

		
	/** Not needed?
	 * if (v.findViewById(R.id.infoButtonImageView) != null) {
			v.findViewById(R.id.infoButtonImageView).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					DialogHelpers.showInfoDialog(getActivity(), R.string.MY_DISCS_INFO_TITLE, R.string.MY_DISCS_INFO_TEXT, R.drawable.info_discs);
				}
			}); 
		} */

		/**
		* The info button is 'hidden' behind the menu bar left-hand icon.
		* Put another way, simply press the icon to get some help
		*/
				if (v.findViewById(R.id.mycoinsInfoSwirl) != null) {
			v.findViewById(R.id.mycoinsInfoSwirl).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					DialogHelpers.showInfoDialog(getActivity(), R.string.MY_DISCS_INFO_TITLE, R.string.MY_DISCS_INFO_TEXT, R.drawable.info_discs);
				}
			}); 
		}
		
		Helpers.setAllFonts(v, getActivity());

		// Helpers.addSwirl(v.findViewById(R.id.mycoinsSwirl));
		// Helpers.addPulse(v.findViewById(R.id.mycoinsInfoSwirl),0); // No need to pulse an invisible button!
		
		
		return v;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (getArguments() != null)
			preselected = getArguments().getInt("preselected", -1);

        ((BTIApp)getActivity().getApplication()).playSound(getActivity(), Sounds.Sound.OPEN);
	}
	public static Fragment newInstance(int i) {
		Fragment f = new MyDiscsFragment();
		Bundle b = new Bundle();
		b.putInt("preselected", i);
		f.setArguments(b);
		
		return f;
	}
}
