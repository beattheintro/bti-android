package com.musicplode.beattheintro.fragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.Session;
import com.facebook.SessionState;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.modelhelpers.EarnDiscTypes;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;
import com.trialpay.android.base.TrialpayManager;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class EarnDiscsFragment extends SocialHelperFragment {
	@Override
	public String getName() {
		return "EarnCoinsList";
	}
	
	@Override
	public void onFollowResult(boolean follows) {
		/**
		 * Callback after the user has pressed follow BTI
		 */
		if (follows) {
			EarnDiscTypes earn = Storage.getEarnDiscTypes(getActivity());
			String id = earn.getNameToIdMap().get("TwitterFollow");
			
			JSONObject jsonReq = JSONRequestHelpers.getFacebookShareJSON(getActivity(), id);
			Storage.addRequestToQueue(getActivity(), Storage.EARNDISC_URL, jsonReq, new OnNetworkResult() {
				@Override
				public void okResult(JSONObject response) {
					if (getActivity() != null) {
						Helpers.refreshProfile(getActivity());
					}
				}
				@Override
				public void badResult(String errorMessage) {
				}
			});
		}
		else
			DialogHelpers.showErrorDialog(getActivity(), R.string.TWITTER_ERROR_ALREADY_FOLLOWING_TITLE, R.string.TWITTER_ERROR_ALREADY_FOLLOWING_TEXT, R.string.OK);
	}
	
	@Override
	protected void onAlreadyLiked() {
		DialogHelpers.showErrorDialog(getActivity(), R.string.FACEBOOK_ERROR_ALREADY_LIKED_TITLE, R.string.FACEBOOK_ERROR_ALREADY_LIKED_TEXT, R.string.OK);
	}
	
	@Override
	public void onLikedResult(boolean result) {
		if (result) {
			DialogHelpers.showHardCodedDialog(getActivity(), "Facebook like", "Liked the app bti");
			
			EarnDiscTypes earn = Storage.getEarnDiscTypes(getActivity());
			String id = earn.getNameToIdMap().get("FbLike");
			
			JSONObject jsonReq = JSONRequestHelpers.getFacebookShareJSON(getActivity(), id);
			Storage.addRequestToQueue(getActivity(), Storage.EARNDISC_URL, jsonReq, new OnNetworkResult() {
				@Override
				public void okResult(JSONObject response) {
				}
				@Override
				public void badResult(String errorMessage) {
				}
			});
		}
		else {
			DialogHelpers.showErrorDialog(getActivity(), R.string.FACEBOOK_ERROR_LIKED_ERROR_TITLE, R.string.FACEBOOK_ERROR_LIKED_ERROR_TEXT, R.string.OK);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.earndiscs, container, false);

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
		Helpers.setAllFonts(v, getActivity());
		
		ListView lv = (ListView) v.findViewById(R.id.earnCoinsListView);
		EarnCoinAdapter eca = new EarnCoinAdapter(getActivity(), 0);
		
		eca.add(new EarnCoinItem(EarnCoinType.BUTTON, R.drawable.earn_offerwall, getString(R.string.OFFERWALL), "+Loads", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TrialpayManager trialpayManager = TrialpayManager.getInstance(getActivity());
				trialpayManager.open("TrialPay Android");
			}
			
		}));
		
		//TODO: Readd when server accepts new login credentials
//		eca.add(new EarnCoinItem(EarnCoinType.BUTTON, R.drawable.earn_login, getString(R.string.EARN_LOGIN), "+5,000", new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				TrialpayManager trialpayManager = TrialpayManager.getInstance(getActivity());
//				trialpayManager.open("TrialPay Android");
//			}
//		}));
		
		eca.add(new EarnCoinItem(EarnCoinType.BUTTON, R.drawable.earn_like, getString(R.string.LIKE_BTI), "+2,000", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (Session.getActiveSession() != null) {
					// Make sure publish_actions permission is in the permission list, otherwise request it
					if (Session.getActiveSession().getPermissions().contains("publish_actions")) {
						likeBTI(true);
					}
					else {
						List<String> publishPermissions = Arrays.asList("publish_actions");
						openActiveSession(getActivity(), true, new Session.StatusCallback() {
							@Override
							public void call(Session session,
									SessionState state, Exception exception) {
								likeBTI(false);
							}
						}, publishPermissions, true);
						
					}
				}
			}
		}));
		
		//TODO: Readd when twitter is readded to server
		eca.add(new EarnCoinItem(EarnCoinType.BUTTON, R.drawable.earn_follow, getString(R.string.FOLLOW_BTI), "+2,000", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				twitterFollowBTI(true);
			}
		}));
		eca.add(new EarnCoinItem(EarnCoinType.BUTTON, R.drawable.earn_video, getString(R.string.WATCH_VIDEO), "+Variable", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((LoggedInMenu)getActivity()).showAdColony();
			}
		}));
		
		eca.add(new EarnCoinItem(EarnCoinType.DIVIDER, 0, getString(R.string.YOULL_BE_REWARDER_FOR), "+100", null));
		
		String[] earnTexts = getResources().getStringArray(R.array.earn_titles);
		String[] earnTextDiscs = getResources().getStringArray(R.array.earn_titles_disc_texts);
		
		TypedArray ar = getResources().obtainTypedArray(R.array.earn_titles_icons);
		int len = ar.length();
		int[] earnTextIcons = new int[len];
		for (int i = 0; i < len; i++)
			earnTextIcons[i] = ar.getResourceId(i, 0);
		ar.recycle();
		
		
		for (int i = 0; i < Math.min(earnTexts.length, earnTextDiscs.length); i++) {
			eca.add(new EarnCoinItem(EarnCoinType.GRAY, earnTextIcons[i], earnTexts[i], earnTextDiscs[i], null));
		}
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				EarnCoinAdapter eca = (EarnCoinAdapter) arg0.getAdapter();
				if (eca.getItem(arg2).getType() == EarnCoinType.BUTTON) {
					eca.getItem(arg2).getOnClickListener().onClick(arg1);
				}
			}
		});
		
		lv.setAdapter(eca);
		
		Helpers.addArrows(lv, v.findViewById(R.id.earnCoinsArrowUp), v.findViewById(R.id.earnCoinsArrowDown));
		// Helpers.addSwirl(v.findViewById(R.id.earnCoinsSwirl));
		
		return v;
	}
	
	public static class EarnCoinAdapter extends ArrayAdapter<EarnCoinItem> {
		public EarnCoinAdapter(Context context, int resource) {
			super(context, resource);
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}
		
		@Override
		public int getItemViewType(int position) {
		    return getItem(position).getType().ordinal();
		}

		@Override
		public int getViewTypeCount() {
		    return EarnCoinType.values().length;
		}
		
		@Override
		public boolean isEnabled(int position) {
			return getItem(position).getType()==EarnCoinType.BUTTON;
		}
		
		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			switch (getItem(position).getType()) {
			case BUTTON:
				if (view == null || !view.getTag().equals(getItem(position).getType().ordinal())) {
					LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					view = inflater.inflate(R.layout.mydiscsstandardcell, null);
					Helpers.setAllFonts(view, getContext());
				}
				((TextView) view.findViewById(R.id.buyPackTitle)).setText(getItem(position).getDescription());
				((TextView) view.findViewById(R.id.buyPackPrice)).setText("" + getItem(position).getAmountChange());
				((ImageView) view.findViewById(R.id.buyPackImageView)).setImageResource(getItem(position).getArtAsset());
				break;

			case GRAY:
				if (view == null || !view.getTag().equals(getItem(position).getType().ordinal())) {
					LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					view = inflater.inflate(R.layout.mydiscsstandardcellgray, null);
					Helpers.setAllFonts(view, getContext());
				}
				((TextView) view.findViewById(R.id.buyPackTitle)).setText(getItem(position).getDescription());
				((TextView) view.findViewById(R.id.buyPackPrice)).setText("" + getItem(position).getAmountChange());
				((ImageView) view.findViewById(R.id.buyPackImageView)).setImageResource(getItem(position).getArtAsset());
				break;
				
			case DIVIDER:
				if (view == null || !view.getTag().equals(getItem(position).getType().ordinal())) {
					LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					view = inflater.inflate(R.layout.dividercell, null);
					Helpers.setAllFonts(view, getContext());
				}
				((TextView) view.findViewById(R.id.dividerCellTextView)).setText("" + getItem(position).getDescription());
				break;
//			case WEBVIEW: 
//				WebView wv = new WebView(getContext());
//				wv.getSettings().setJavaScriptEnabled(true);
//				wv.loadData("<iframe src=\"http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fbeattheintro&amp;width&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=126006197606086\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden; height:62px;\" allowTransparency=\"true\"></iframe>", "text/html", null);
//				view = wv;
//				wv.setBackgroundResource(android.R.color.transparent);
//				break;
				
			default:
				LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				Helpers.setAllFonts(view, getContext());
				view = inflater.inflate(R.layout.mydiscsstandardcell, null);
				return view;
			}
			
			view.setTag(getItem(position).getType().ordinal());
			
			return view;
		}
			
	}
	
	public enum EarnCoinType {
		BUTTON,
		//BUTTONACTIVATED,
		DIVIDER,
		GRAY, WEBVIEW
	}
	
	public class EarnCoinItem {
		private EarnCoinType type;
		private int artAsset;
		private String description;
		private OnClickListener onClickListener;
		private String amountChange;

		public EarnCoinItem(EarnCoinType type, int artAsset, String description, String amountChange, OnClickListener onClickListener) {
			this.type = type;
			this.artAsset = artAsset;
			this.description = description;
			this.amountChange = amountChange;
			this.onClickListener = onClickListener;
		}
		public Spannable getDescription() {
			return Helpers.createSpannable(getActivity(), description);
		}
		public EarnCoinType getType() {
			return type;
		}

		public int getArtAsset() {
			return artAsset;
		}

		public OnClickListener getOnClickListener() {
			return onClickListener;
		}

		public String getAmountChange() {
			return amountChange;
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
	}
	public static Fragment newInstance(int separatorStartY, int separatorHeight, int containerId) {
		Fragment elf = new EarnDiscsFragment();
		Bundle b = new Bundle();
		b.putInt("separatorStartY", separatorStartY);
		b.putInt("separatorHeight", separatorHeight);
		b.putInt("containerId", containerId);
		elf.setArguments(b);
		
		return elf;
	}
}
