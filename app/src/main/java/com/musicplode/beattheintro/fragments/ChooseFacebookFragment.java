package com.musicplode.beattheintro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChooseFacebookFragment extends SocialHelperFragment {
	@Override
	public String getName() {
		return "ChooseFacebookOpponent";
	}
	private View v;
	private List<FacebookFriend> friendsWhoPlay = new ArrayList<FacebookFriend>();
	private int separatorStartY;
	private int separatorHeight;
	private String packId;
	private String levelId;
	private boolean onlyInstalledFriends = true;
	private int containerId;	
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.choosefacebook, container, false);
		
		//Tablet version of this fragment does not cover entire content area
		if (containerId == 0)
			containerId = container.getId();
		
		/**
		 * Fix tablet divider position between bestmatch and choose opponent menu
		 */
		if (v.findViewById(R.id.chooseFacebookSeparator) != null) {
			ImageView iv = (ImageView) v.findViewById(R.id.chooseFacebookSeparator);
			
			LayoutParams lp = (LayoutParams) iv.getLayoutParams();
			lp.topMargin = separatorStartY;
			lp.height = separatorHeight;
			iv.setLayoutParams(lp);
		}

		Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
        Helpers.setAllFonts(v, getActivity());
		
		if (v.findViewById(R.id.chooseFacebookSwitchInstalledNotinstalledButton) != null) {
			v.findViewById(R.id.chooseFacebookSwitchInstalledNotinstalledButton).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v2) {
					onlyInstalledFriends = !onlyInstalledFriends;
					updateFriendsList();
					
					if (!onlyInstalledFriends) {
						((Button)v.findViewById(R.id.chooseFacebookSwitchInstalledNotinstalledButton)).setText(R.string.FRIENDS_WHO_PLAY_BTI);
						((TextView)v.findViewById(R.id.chooseFacebookTitleTextView)).setText(R.string.FRIENDS_WHO_HAVENT_PLAYED_BTI_TITLE);
					}
					else {
						((Button)v.findViewById(R.id.chooseFacebookSwitchInstalledNotinstalledButton)).setText(R.string.FRIENDS_WHO_HAVENT_PLAYED_BTI_BUTTON);
						((TextView)v.findViewById(R.id.chooseFacebookTitleTextView)).setText(R.string.FRIENDS_WHO_PLAY_BTI_TITLE);
					}
				}
		});
		}
		ListView lv = (ListView) v.findViewById(R.id.chooseFacebookListView);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				FriendsAdapter fa = (FriendsAdapter) arg0.getAdapter(); 
				if (fa != null) {
					startGame(fa.getItem(position).getName(), containerId);
				}
			}
		});
		
		/**
		 * Open the facebook login dialog if the user is not logged in
		 */
		if (!isLoggedInToFacebook()) {
			facebookLogin();
		}
		else
			downloadFriendsList();

		// Helpers.addSwirl(v.findViewById(R.id.chooseOpponentSwirl));
		
		return v;
	}

    /**
     * Callback that will be called when the user has logged in to Facebook
     */
	@Override
	public void onFacebookLogedIn() {
		downloadFriendsList();
	}
	
	public void startGame(String opponentName, final int containerId) {
		Helpers.startGame((LoggedInMenu) getActivity(), containerId, opponentName, packId, levelId);
	}

	/**
	 * Download the friend list from the BTI server
	 */
	public void downloadFriendsList() {
		if (v.findViewById(R.id.chooseFacebookProgressBar) != null) {
			v.findViewById(R.id.chooseFacebookProgressBar).setVisibility(View.VISIBLE);
		}
		
		JSONObject jsonReq = JSONRequestHelpers.getFacebookFriendsWhoPlayJSON(Storage.getUserProfile(getActivity()));
		Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, jsonReq, new OnNetworkResult() {
			@Override
			public void okResult(JSONObject response) {
				if (v.findViewById(R.id.chooseFacebookProgressBar) != null) {
					v.findViewById(R.id.chooseFacebookProgressBar).setVisibility(View.INVISIBLE);
				}
				friendsWhoPlay.clear();
				try {
					for (int i = 0; i < response.getJSONArray("FriendsWhoPlay").length(); i++) {
						JSONObject user = response.getJSONArray("FriendsWhoPlay").getJSONObject(i);
							String fbId = response.getJSONArray("FriendsWhoPlay").getJSONObject(i).getString("id");
							String name = user.getString("name");
							boolean installed = user.getBoolean("installed");
							friendsWhoPlay.add(new FacebookFriend(fbId, name, installed));
					}
					
					Collections.sort(friendsWhoPlay);
					updateFriendsList();
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			}
			
			@Override
			public void badResult(String errorMessage) {
				if (v.findViewById(R.id.chooseFacebookProgressBar) != null) {
					v.findViewById(R.id.chooseFacebookProgressBar).setVisibility(View.INVISIBLE);
				}
			}
		});
	}

    /**
     * Update the Friendlist UI
     */
	public void updateFriendsList() {
		if (getActivity() != null) {
			ListView lv = (ListView) v.findViewById(R.id.chooseFacebookListView);
			FriendsAdapter fa;
			if (lv.getAdapter() == null) {
				fa = new FriendsAdapter(getActivity(), 0);
				lv.setAdapter(fa);
			}
			else {
				fa = (FriendsAdapter)lv.getAdapter();
				fa.clear();
			}
			
			for (FacebookFriend ff : friendsWhoPlay)  {
				if (ff.hasInstalled() == onlyInstalledFriends)
					fa.add(ff);
			}
			
			lv.setAdapter(fa);
			fa.notifyDataSetChanged();
		}
	}

	public static class FriendsAdapter extends ArrayAdapter<FacebookFriend> {
		public FriendsAdapter(Context context, int resource) {
			super(context, resource);
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.firendcell, null);
				Helpers.setAllFonts(view, getContext());
			}

			TextView tv = (TextView) view.findViewById(R.id.friendCellNameTextView);
			ImageView iv = (ImageView) view.findViewById(R.id.friendCellImageView);
			iv.setImageResource(R.drawable.profile_unknown);
			
			Helpers.displayImage(getContext(), Helpers.getFacebookProfileImage(getItem(position).getId()), iv);
			tv.setText("" + getItem(position).getName());
			
			return view;
		}
	}
	
	/**
	 * Container class for a facebook friend
	 */
	public class FacebookFriend implements Comparable<FacebookFriend> {
		private String id;
		private String name;
		private boolean installed;
		
		public FacebookFriend(String id, String name, boolean installed) {
			this.id = id;
			this.name = name;
			this.installed = installed;
		}
		public boolean hasInstalled() {
			return installed;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getId() {
			return id;
		}
		@Override
		public int compareTo(FacebookFriend another) {
			return name.compareTo(another.getName());
		}
	}

	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
		if (getArguments() != null) {
			separatorStartY = getArguments().getInt("separatorStartY");
			separatorHeight = getArguments().getInt("separatorHeight");
			containerId = getArguments().getInt("containerId");
			packId = getArguments().getString("packId");
			levelId = getArguments().getString("levelId");
		}
	}
	public static Fragment newInstance(String packId, String levelId, int separatorStartY, int separatorHeight, int containerId) {
		ChooseFacebookFragment elf = new ChooseFacebookFragment();
		Bundle b = new Bundle();
		b.putInt("separatorStartY", separatorStartY);
		b.putInt("separatorHeight", separatorHeight);
		b.putInt("containerId", containerId);
		b.putString("packId", packId);
		b.putString("levelId", levelId);
		elf.setArguments(b);
		
		return elf;
	}
	
	public static Fragment newInstance(String packId, String levelId) {
		return newInstance(packId, levelId, 0, 0, 0);
	}

}
