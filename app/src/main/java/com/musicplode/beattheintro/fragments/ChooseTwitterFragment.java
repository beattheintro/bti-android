package com.musicplode.beattheintro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Twitter is currently disabled
 */
public class ChooseTwitterFragment extends SocialHelperFragment {
	@Override
	public String getName() {
		return "ChooseTwitterOpponent";
	}

	private List<TwitterFriend> friendsWhoPlay = new ArrayList<TwitterFriend>();
	private View v;
	private int separatorStartY;
	private int separatorHeight;
//	private String packId;
//	private String levelId;
//	private FriendsAdapter fa;
	private boolean onlyInstalledFriends = true;
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.choosetwitter, container, false);
		
		if (v.findViewById(R.id.chooseTwitterSeparator) != null) {
			ImageView iv = (ImageView) v.findViewById(R.id.chooseTwitterSeparator);
			
			LayoutParams lp = (LayoutParams) iv.getLayoutParams();
			lp.topMargin = separatorStartY;
			lp.height = separatorHeight;
			iv.setLayoutParams(lp);
		}
		
		if (v.findViewById(R.id.chooseTwitterSwitchInstalledNotinstalledButton) != null) {
			v.findViewById(R.id.chooseTwitterSwitchInstalledNotinstalledButton).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v2) {
					onlyInstalledFriends = !onlyInstalledFriends;
					updateFriendsList();
					
					if (!onlyInstalledFriends) {
						((Button)v.findViewById(R.id.chooseTwitterSwitchInstalledNotinstalledButton)).setText(R.string.FOLLOWERS_WHO_PLAY_BTI);
						((TextView)v.findViewById(R.id.chooseTwitterTitleTextView)).setText(R.string.FOLLOWERS_WHO_HAVENT_PLAYED_BTI_TITLE);
					}
					else {
						((Button)v.findViewById(R.id.chooseTwitterSwitchInstalledNotinstalledButton)).setText(R.string.FOLLOWERS_WHO_HAVENT_PLAYED_BTI_BUTTON);
						((TextView)v.findViewById(R.id.chooseTwitterTitleTextView)).setText(R.string.FOLLOWERS_WHO_PLAY_BTI_TITLE);
					}
				}
			});
		}
		
		Helpers.setAllFonts(v, getActivity());
		
		if (Storage.getTwitterToken(getActivity()) == null) {
			loginToTwitter(TWITTERLOGIN, null);
		}
		
		// Helpers.addSwirl(v.findViewById(R.id.chooseOpponentSwirl));
		
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (Storage.getTwitterToken(getActivity()) != null)
			updateFollowerList();
	}
	
	@Override
	public void onTwitterLogedIn() { 
		updateFollowerList();
	}
	
	public void updateFollowerList() {
		if (v.findViewById(R.id.chooseTwitterProgressBar) != null) {
			v.findViewById(R.id.chooseTwitterProgressBar).setVisibility(View.VISIBLE);
		}
		
		JSONObject jsonReq = JSONRequestHelpers.getTwitterFollowesWhoPlayJSON(Storage.getUserProfile(getActivity()));
		Storage.addRequestToQueue(getActivity(), Storage.MAIN_URL, jsonReq, new OnNetworkResult() {
			@Override
			public void okResult(JSONObject response) {
				if (v.findViewById(R.id.chooseTwitterProgressBar) != null) {
					v.findViewById(R.id.chooseTwitterProgressBar).setVisibility(View.INVISIBLE);
				}
				friendsWhoPlay.clear();
				try {
					for (int i = 0; i < response.getJSONArray("FriendsWhoPlay").length(); i++) {
						JSONObject user = response.getJSONArray("FriendsWhoPlay").getJSONObject(i);
							String fbId = response.getJSONArray("FriendsWhoPlay").getJSONObject(i).getString("id");
							String name = user.getString("name");
							String profileImageUrl = user.getString("profile_image");
							boolean installed = user.getBoolean("installed");
							friendsWhoPlay.add(new TwitterFriend(fbId, name, profileImageUrl, installed));
					}
					
					Collections.sort(friendsWhoPlay);
					updateFriendsList();
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			}
			
			@Override
			public void badResult(String errorMessage) {
				if (v.findViewById(R.id.chooseTwitterProgressBar) != null) {
					v.findViewById(R.id.chooseTwitterProgressBar).setVisibility(View.INVISIBLE);
				}
			}
		});
	}
	
	public void updateFriendsList() {
		if (getActivity() != null) {
			ListView lv = (ListView) v.findViewById(R.id.chooseTwitterListView);
			FriendsAdapter fa;
			if (lv.getAdapter() == null) {
				fa = new FriendsAdapter(getActivity(), 0);
				lv.setAdapter(fa);
			}
			else {
				fa = (FriendsAdapter)lv.getAdapter();
				fa.clear();
			}
			
			for (TwitterFriend ff : friendsWhoPlay)  {
				if (ff.hasInstalled() == onlyInstalledFriends)
					fa.add(ff);
			}
			
			lv.setAdapter(fa);
			fa.notifyDataSetChanged();
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
		if (getArguments() != null) {
			separatorStartY = getArguments().getInt("separatorStartY");
			separatorHeight = getArguments().getInt("separatorHeight");
//			packId = getArguments().getString("packId");
//			levelId = getArguments().getString("levelId");
		}
	}
	public static Fragment newInstance(String packId, String levelId, int separatorStartY, int separatorHeight) {
		ChooseTwitterFragment elf = new ChooseTwitterFragment();
		Bundle b = new Bundle();
		b.putInt("separatorStartY", separatorStartY);
		b.putInt("separatorHeight", separatorHeight);
		b.putString("packId", packId);
		b.putString("levelId", levelId);
		elf.setArguments(b);
		
		return elf;
	}
	public static Fragment newInstance(String packId, String levelId) {
		return newInstance(packId, levelId, 0, 0);
	}

	public static class FriendsAdapter extends ArrayAdapter<TwitterFriend> {
		public FriendsAdapter(Context context, int resource) {
			super(context, resource);
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.firendcell, null);
				Helpers.setAllFonts(view, getContext());
			}

			TextView tv = (TextView) view.findViewById(R.id.friendCellNameTextView);
			ImageView iv = (ImageView) view.findViewById(R.id.friendCellImageView);
			iv.setImageResource(R.drawable.profile_unknown);
			
			Helpers.displayImage(getContext(), getItem(position).getImageUrl(), iv);
			tv.setText("" + getItem(position).getName());
			
			return view;
		}
	}
	
	public class TwitterFriend implements Comparable<TwitterFriend> {
		private String id;
		private String name;
		private boolean installed;
		private String imageUrl;
		
		public TwitterFriend(String id, String name, String imageUrl, boolean installed) {
			this.id = id;
			this.name = name;
			this.imageUrl = imageUrl;
			this.installed = installed;
		}
		public String getImageUrl() {
			return imageUrl;
		}
		public boolean hasInstalled() {
			return installed;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getId() {
			return id;
		}
		@Override
		public int compareTo(TwitterFriend another) {
			return name.compareTo(another.getName());
		}
	}
}
