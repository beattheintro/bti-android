package com.musicplode.beattheintro.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
// import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.amazon.inapp.purchasing.Item;
import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.modelhelpers.InAppProducts;
import com.musicplode.beattheintro.storage.Storage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class BuyDiscsFragment extends BaseFragment {
    private int containerId = 0;
    @Override
    public String getName() {
        return "BuyCoinsList";
    }
    @Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.buydiscs, container, false);

        //Tablet version of this fragment does not cover entire content area
        if (containerId == 0)
			containerId = container.getId();

        /**
         * Fix tablet divider position between My Discs and Buy Discs sub-menu
         */
		if (v.findViewById(R.id.buyCoinsSeparator) != null) {
			ImageView iv = (ImageView) v.findViewById(R.id.buyCoinsSeparator);

			if (separatorStartY >= 0) {
				LayoutParams lp = (LayoutParams) iv.getLayoutParams();
				lp.topMargin = separatorStartY+9;
				lp.height = separatorHeight;
				iv.setLayoutParams(lp);
			}
			else {
				iv.setVisibility(View.INVISIBLE);
			}
		}

		ListView lv = (ListView) v.findViewById(R.id.buyCoinsListView);

        /**
         * If the device is an amazon the amazon IAP will be launched when tapping a cell, otherwise the Google IAP will be launched
         */

        if (((LoggedInMenu)getActivity()).isAmazon()) {
            /**
             * Amazon IAP
             */
            // AmazonDiscAdapter ba = new AmazonDiscAdapter(getActivity(), 0);
            // JJ Hmmm... Other Fragments call the modified AmazonDiscAdaptor NOT the base ArrayAdaptor... But it still isn't working!
            ArrayAdapter<Item> ba = new AmazonDiscAdapter(getActivity(), 0);
            Map<String, Item> apa = ((LoggedInMenu)getActivity()).getAllItems();

            List<Item> items = new ArrayList<>();

            for (Item i : apa.values()) {
                items.add(i);
            }

            /**
             * Sort the list of items by sku textual reference
             */
            final InAppProducts iap = new InAppProducts(Storage.getInAppProducts(getActivity()));
            Collections.sort(items, new Comparator<Item>() {
                @Override
                public int compare(Item lhs, Item rhs) {
                    return iap.compare(lhs.getSku(), rhs.getSku());
                }
            });
            for (Item i : items) {
                ba.add(i);
            }

            lv.setAdapter(ba);
            ba.notifyDataSetChanged();
        }
        else {
            /**
             * Google IAP
             */
            GoogleDiscAdapter ba = new GoogleDiscAdapter(getActivity(), 0);
            // ArrayAdapter<JSONObject> adapter = new GoogleDiscAdapter(getActivity(), 0);
            Map<String, JSONObject> products = ((LoggedInMenu)getActivity()).getAllGoogleItems();

            List<JSONObject> items = new ArrayList<>();

            for (JSONObject o : products.values()) {
                items.add(o);
            }

            /**
             * Sort the list of items by sku textual reference
             */
            final InAppProducts iap = new InAppProducts(Storage.getInAppProducts(getActivity()));
            Collections.sort(items, new Comparator<JSONObject>() {
                @Override
                public int compare(JSONObject lhs, JSONObject rhs) {
                    try {
                        return iap.compare(lhs.getString("productId"), rhs.getString("productId"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return 0;
                }
            });
            for (JSONObject o : items) {
                ba.add(o);
            }

            lv.setAdapter(ba);
            ba.notifyDataSetChanged();

        }

        Helpers.setupBackPulseButton(v.findViewById(R.id.backButton), v.findViewById(R.id.backButtonRing), v.findViewById(R.id.backSwirl));
        Helpers.setAllFonts(v, getActivity());

		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                String s;
                String title;

                if (((LoggedInMenu)getActivity()).isAmazon()) {
                    AmazonDiscAdapter ca = (AmazonDiscAdapter) arg0.getAdapter();
                    final Item i = ca.getItem(position);
                    s = i.getSku();
                    title = i.getTitle();
                }
                else {
                    GoogleDiscAdapter gda = (GoogleDiscAdapter) arg0.getAdapter();
                    final JSONObject i = gda.getItem(position);
                    try {
                        s = i.getString("productId");
                        title = i.getString("title");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }
                }

                final String sku = s;
				
				final Dialog d = DialogHelpers.getDialog(getActivity(), R.string.PURCHASE_DISC_TITLE, R.string.BUY_NOW, R.string.CANCEL);
				String text = ((TextView)d.findViewById(R.id.dialogTitleTextView)).getText().toString();
				((TextView)d.findViewById(R.id.dialogTitleTextView)).setText(text.replace("[X]", ""+title));

				d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						((LoggedInMenu)getActivity()).buy(sku);
						d.hide();
						d.cancel();
					}
				});
				
				d.findViewById(R.id.dialogButton2).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						d.hide();
						d.cancel();
					}
				});
				
				d.show();
			}
		});

        // Helpers.addSwirl(v.findViewById(R.id.mycoinsSwirl));

		return v;
	}

	public class AmazonDiscAdapter extends ArrayAdapter<Item> {
		public AmazonDiscAdapter(Context context, int resource) {
			super(context, resource);
		}
		// TODO JJ Hmmm we never seem to call this code, below. Hence, we don't draw the list of In App Purchases
        // In other cases where ArrayAdapter is overridden, the defined <types> are free-standing java code.
        // But here, we seem to be using the base <types>. This is the only occurrence!
        // NOTE: It is only the Amazon version which is NOT working. The Google (plain Android) version is fine.
        // Maybe this is solely down to In App Purchase iap needing updating to version 2 ????
		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.buydisccell, null);
				Helpers.setAllFonts(view, getContext());
			}

            TextView tvCoins = (TextView) view.findViewById(R.id.buyCoinCellCoinsTextView);
            TextView tvPrice = (TextView) view.findViewById(R.id.buyCoinCellCostTextView);

            ImageView iv = (ImageView) view.findViewById(R.id.buyCoinIcon);
            Helpers.cancelImageLoading(getContext(), iv);
            iv.setImageResource(0);

            String imageURLa = getItem(position).getSmallIconUrl();
            if (imageURLa != null) {
                if (imageURLa.length() > 10) {
                    Helpers.displayImage(getContext(), imageURLa, iv);
                }
            }

            tvCoins.setText("" + getItem(position).getTitle());
            tvPrice.setText("" + getItem(position).getPrice());

			return view;
		}
	}

    public class GoogleDiscAdapter extends ArrayAdapter<JSONObject> {
        public GoogleDiscAdapter(Context context, int resource) {
            super(context, resource);
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.buydisccell, null);
                Helpers.setAllFonts(view, getContext());
            }

            TextView tvCoins = (TextView) view.findViewById(R.id.buyCoinCellCoinsTextView);
            TextView tvPrice = (TextView) view.findViewById(R.id.buyCoinCellCostTextView);

            ImageView iv = (ImageView) view.findViewById(R.id.buyCoinIcon);
            iv.setImageResource(R.drawable.iap_discs);

            try {
                tvCoins.setText("" + getItem(position).getString("title"));
                tvPrice.setText("" + getItem(position).getString("price"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return view;
        }
    }

	private int separatorStartY = 0;
    private int separatorHeight = 0;
    @Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
		if (getArguments() != null) {
			
			separatorStartY = getArguments().getInt("separatorStartY");
			separatorHeight = getArguments().getInt("separatorHeight");
            containerId = getArguments().getInt("containerId"); // JJ added to match other submenu instances
		}
	}
	public static Fragment newInstance(int separatorStartY, int separatorHeight, int containerId) {
		Fragment f = new BuyDiscsFragment();
		Bundle b = new Bundle();
		b.putInt("separatorStartY", separatorStartY);
		b.putInt("separatorHeight", separatorHeight);
		b.putInt("containerId", containerId);
		f.setArguments(b);
		
		return f;
	}

}
