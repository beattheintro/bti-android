package com.musicplode.beattheintro.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.preference.PreferenceManager;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.Session;
import com.musicplode.beattheintro.LoggedInMenu;
// import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.modelhelpers.EarnDiscTypes;
import com.musicplode.beattheintro.modelhelpers.InAppProducts;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.model.Token;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Contains all the storage methods that the app will use.
 */
public class Storage {
	/**
	 * Server config constants
	 */
//	public static final String SERVER = "http://bticloudservicetoloadtest-large.cloudapp.net";
//  public static final String SERVER = "https://www.beattheintro.com";  // no longer HTTPS
    public static final String SERVER = "http://azure.bti-app.net"; // was www.beattheintro.com
    public static final String MAIN_URL = SERVER + "/api/syscommand";

    public static final String CREATE_GAME_URL = SERVER + "/api/gameinfo/create";
    public static final String CREATE_PARTYPLAY_GAME_URL = SERVER + "/api/GameInfo/CreatePartyPlay";

    public static final String GAMERESUME_URL = SERVER + "/api/gameinfo/resume";
    public static final String FINALISE_GAME_URL = SERVER + "/api/gameinfo/finalise";
    public static final String ANSWER_QUESTION_URL = SERVER + "/api/gameinfo/answer";

    public static final String BESTMATCH_URL = SERVER + "/api/gameinfo/bestmatch";
	public static final String METADATA_URL = SERVER + "/api/meta/get";
	public static final String INAPPPURCHASE_URL = SERVER + "/api/InAppPurchase/Spend";
	public static final String UNLOCKPACK_URL = SERVER + "/api/profile/unlocksinglepack";
	public static final String PUSH_URL = SERVER + "/api/profile/SetPushNotificationToken";
	public static final String NUDGE_URL = SERVER + "/api/Profile/Nudge";
	public static final String EARNDISC_URL = SERVER + "/api/InAppPurchase/Earn";
	
	
	/**
	 * Keys for sharedPreferences
	 */
	public static final String USERPROFILE = "userProfile";
	public static final String TWITTER_TOKEN = "TWITTER_TOKEN";
	public static final String TWITTER_TOKEN_SECRET = "TWITTER_TOKEN_SECRET";
	public static final String TWITTER_TOKEN_RAW = "TWITTER_TOKEN_RAW";
	public static final String GCMREGID = "GCMREGID";
	public static final String ADMREGID = "ADMREGID";
	public static final String METADATA = "METADATA";
	private static final String LAST_PLAYED_PACK_KEY = "LastPlayedPack";
	private static final String INAPPPRODUCTS_KEY = "InAppProducts";
	
	/**
	 * Push message json keys
	 */
	public static final String PUSH_MESSAGE_KEY = "m";
	public static final String PUSH_MESSAGE_START_GAME_KEY = "startGame";
	
	public static final String START_GAME_KEY = "startGame";
	public static final String DONT_SHOW_RATE = "DontShowRate";
	public static final String HOWTOPLAYKEY = "HowToPlay";
    public static final String GOOGLE_IN_APP_PRODUCTS = "GoogleIAPs";


    private static RequestQueue requestQueue;
	private static Typeface font;
	private static JSONObject userProfile;
	public static NumberFormat numberFormat = new DecimalFormat("###,###,###");
	
	public static DisplayImageOptions displayImageOption = new DisplayImageOptions.Builder()
        .cacheInMemory(true)
        .cacheOnDisc(true)
        .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
        .build();
	
	/**
	 * Same random object should be used through out the game
	 */
	public static Random random = new Random(System.currentTimeMillis());
	
	/**
	 * Kill switch 
	 */
	public static int CLIENT_SERVER_COMMUNICATION_VERSION = 1;
	
	
	public interface OnNetworkResult {
		/**
		 * Will be called if there was a response and either success = true or success = null
		 * @param response
		 */
		void okResult(JSONObject response);
		
		/**
		 * Called on failed response or if success == false
		 * @param errorMessage
		 */
		void badResult(String errorMessage);
	}
	
	public static void addRequestToQueue(Context context, String url, JSONObject postData, final OnNetworkResult onNetworkResult) {
		addRequestToQueue(context, url, postData, onNetworkResult, false);
	}
	
	/**
	 * Adds a network request to the network queue
	 * @param context
	 * @param url
	 * @param postData
	 * @param onNetworkResult Callbacks for good and bad results
	 * @param shouldCache Sets if this request should cache or not
	 */
	public static void addRequestToQueue(Context context, String url, JSONObject postData, final OnNetworkResult onNetworkResult, boolean shouldCache) {
		if (requestQueue == null) 
			requestQueue = Volley.newRequestQueue(context);
		
		JsonObjectRequest jor = new JsonObjectRequest(url, postData, new Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				if (onNetworkResult == null)
					return;
				
				if (response == null)
					onNetworkResult.badResult(null);
				
				if (response.optBoolean("Success", true)) {
					onNetworkResult.okResult(response);
				}
				else {
					String info = response.optString("Info", null);
					onNetworkResult.badResult(info);
				}
			}
		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				if (onNetworkResult != null)
					onNetworkResult.badResult(error.getLocalizedMessage());
			}
		}){     
	        @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	return getHeader(super.getHeaders());
	        }
	    };
		
	    jor.shouldCache();
		requestQueue.add(jor);
	}

    /**
     * Holds the default headers that should be sent with all requests
     * @param headers
     * @return
     */
    public static Map<String, String> getHeader(Map<String, String> headers) {
        @SuppressWarnings("Convert2Diamond") Map<String, String> params = new HashMap<String, String>();
        params.put("Accept", "application/vnd.bti.api.v1+json");
        return params;
    }

	public static Typeface getFont(Context context) {
		if (Storage.font == null)
			font = Typeface.createFromAsset(context.getAssets(), "fonts/font.ttf");
			
		return font;
	}
	
	/**
	 * Remove some stored value from the sharedPreferences
	 * @param context
	 * @param key
	 */
	public static void remove(Context context, String key) {
		if (context == null)
			return;
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPref.edit();
		editor.remove(key);
		editor.apply();
		if (key.contentEquals(USERPROFILE))
			userProfile = null;
	}
	
	/**
	 * Clears all shared data
	 * @param context
	 */
	private static void removeAllSharedData(Context context) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPref.edit();
		editor.clear();
		editor.apply();
	}

    /**
     * Stores a given value using the SharedPreferences
     */
	public static void store(Context context, String key, String value) {
		if (context != null) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
			Editor editor = sharedPref.edit();
			editor.putString(key, value);
			editor.apply();
		}
	}

    /**
     * Stores a given value using the SharedPreferences
     */
	public static void store(Context context, String key, boolean value) {
		if (context != null) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
			Editor editor = sharedPref.edit();
			editor.putBoolean(key, value);
			editor.apply();
		}
	}
	public static Boolean getStoredBoolean(Context context, String key) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		if (sharedPref.contains(key))
			return sharedPref.getBoolean(key, false);
		else
			return null;
	}

    /**
     * Stores a given value using the SharedPreferences
     */
	public static void store(Context context, String key, JSONObject value) {
		Storage.store(context, key, value.toString());
		
		if (key.contentEquals(USERPROFILE))
			userProfile = value;
	}
	public static String getStoredString(Context context, String key) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		return sharedPref.getString(key, null);
	}
	
	/**
	 * Get a stored json object, if invalid json or non existant it will return null
	 * @param context
	 * @param key
	 * @return returns null if invalid or nonexistant
	 */
	public static JSONObject getStoredJSON(Context context, String key) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		if (sharedPref.contains(key)) {
			String s = sharedPref.getString(key, null);
			if (s != null) {
				try {
					return new JSONObject(s);
				} catch (JSONException e) {
					return null;
				}
			}
		}
		return null;
	}
	
	/**
	 * Returns the stored userprofile
	 * @param context
	 * @return
	 */
	public static JSONObject getUserProfile(Context context) {
		if (userProfile == null)
			userProfile = getStoredJSON(context, USERPROFILE);
		return userProfile;
	}

	/**
	 * Updates the stored userprofile
	 * @param context
	 * @param response
	 */
	public static void updateUserProfile(Context context, JSONObject response, boolean triggerUpdate) {
		if (context != null && response != null) {
			store(context, USERPROFILE, response);
			userProfile = response;
			
			if (triggerUpdate && context instanceof LoggedInMenu) {
				((LoggedInMenu)context).updateUI();
			}
		}
	}

	public static Token getTwitterToken(Context context) {
		try {
			return new Token(Storage.getStoredString(context, Storage.TWITTER_TOKEN),
					Storage.getStoredString(context, Storage.TWITTER_TOKEN_SECRET), 
					Storage.getStoredString(context, Storage.TWITTER_TOKEN_RAW));
		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}
	public static void storeTwitterToken(Context context, Token t) {
		store(context, TWITTER_TOKEN, t.getToken());
		store(context, TWITTER_TOKEN_SECRET, t.getSecret());
		store(context, TWITTER_TOKEN_RAW, t.getRawResponse());
	}

	public static void removeUserProfile(Context context) {
		Storage.remove(context, Storage.USERPROFILE);		
	}

	/**
	 * Removes the current facebook session if it exists
	 * Removes all the shared data
	 * @param context
	 */
	public static void clearUserData(Context context) {
		if (Session.getActiveSession() != null) {
			Session.getActiveSession().closeAndClearTokenInformation();
		}
		
		Storage.remove(context, Storage.USERPROFILE);		
		removeAllSharedData(context);
		
		Storage.store(context, HOWTOPLAYKEY, true);
	}

	public static void storeLastPlayedPack(Context context, String packId) {
		store(context, LAST_PLAYED_PACK_KEY, packId);
	}
	
	public static String getLastPlayedPackId(Context context) {
		return getStoredString(context, LAST_PLAYED_PACK_KEY);
	}

	public static void storeInAppProducts(Context context, JSONObject response) {
		store(context, INAPPPRODUCTS_KEY, response);
	}

	public static JSONObject getInAppProducts(Context context) {
		JSONObject o = Storage.getStoredJSON(context, Storage.METADATA);
		try {
			if (o != null)
				return o.getJSONObject("SpendDiscs");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static JSONObject getMetaJSON(Context context) {
		return Storage.getStoredJSON(context, Storage.METADATA);
	}
	
	public static EarnDiscTypes getEarnDiscTypes(Context context) {
		return new EarnDiscTypes(getMetaJSON(context));
	}
	
	public static JSONArray getPushProviderTypes(Context context) {
		JSONObject o = Storage.getStoredJSON(context, Storage.METADATA);
		try {
			if (o != null)
				return o.getJSONArray("PushProviderTypes");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Set<String> getAllSKUS(Context context) {
		JSONObject o = Storage.getInAppProducts(context);
		InAppProducts iap = new InAppProducts(o);
		return iap.getAllSKUS();
	}

	public static boolean isAnonuser(Context context) {
        try {
            return getUserProfile(context).getJSONObject("UserProfile").getBoolean("IsAnonymous");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}
