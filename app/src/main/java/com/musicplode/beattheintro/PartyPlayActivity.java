package com.musicplode.beattheintro;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.musicplode.beattheintro.fragments.ThreeTwoOneFragment;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.Helpers;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Test activity that handles the partyPlay mode
 */
public class PartyPlayActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.emptyframe);
        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Helpers.setFragmentAnimation(ft);

            if (getIntent() != null && getIntent().getExtras() != null) {
                String playerNames[] = getIntent().getExtras().getStringArray("playerNames");

                JSONObject json = null;
                try {
                    json = new JSONObject(getIntent().getExtras().getString("gameInfo"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Fragment f = ThreeTwoOneFragment.newInstance(json, false, true, playerNames);
                ft.add(R.id.frameLayout2, f);
                ft.commit();
            }
        }
    }

    /*******************************************************************************************
     * MediaPlayer stuffs start
     ******************************************************************************************/
    private MediaPlayer mediaPlayers[] = new MediaPlayer[1];
    private int currMediaPlayer = 0;
    private Map<String, Integer> readyToPlaySongs = new HashMap<String, Integer>();
    private Map<String, OnSongReadyListener> reportToWhenDone = new HashMap<String, OnSongReadyListener>();
    public void stopPreparingSongs() {
        if (mediaPlayers == null)
            return;

        for (MediaPlayer m : mediaPlayers) {
            if (m != null)
                m.reset();
        }
    }
    public boolean prepareSong(final String songPath, MediaPlayer.OnErrorListener errorListener, boolean firstTrackInAGame) {
        if (firstTrackInAGame) {
            return prepareSong(songPath, new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    System.out.println("mp error " + what + " extra " + extra);
                    if (PartyPlayActivity.this != null) {
                        DialogHelpers.showErrorDialog(PartyPlayActivity.this, R.string.ERROR_LOADING_SONG_TITLE, R.string.ERROR_LOADING_SONG_TEXT, R.string.OK, new DialogHelpers.OnOkButton() {
                            @Override
                            public void onOk() {
                                if (PartyPlayActivity.this != null) {
                                    PartyPlayActivity.this.setShouldBlockBackButton(false);
                                    PartyPlayActivity.this.onBackPressed();
                                }
                            }
                        });
                    }

                    return false;
                }
            });
        }
        else
            return prepareSong(songPath, errorListener);
    }

    private void setShouldBlockBackButton(boolean value) {

    }

    public boolean prepareSong(final String songPath, MediaPlayer.OnErrorListener errorListener) {
        currMediaPlayer = (currMediaPlayer+1)%mediaPlayers.length;

        if (mediaPlayers[currMediaPlayer] == null)
            mediaPlayers[currMediaPlayer] = new MediaPlayer();

        MediaPlayer mp = mediaPlayers[currMediaPlayer];

        mediaPlayers[currMediaPlayer].setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer arg0) {
                lock.lock();
                if (reportToWhenDone.containsKey(songPath)) {
                    reportToWhenDone.get(songPath).onReady(arg0);
                    reportToWhenDone.remove(songPath);
                }
                else
                    readyToPlaySongs.put(songPath, currMediaPlayer);
                lock.unlock();
            }
        });

        mp.reset();
        try {
            mp.setDataSource(songPath);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mp.setLooping(false);

        mp.setOnErrorListener(errorListener);

        mp.prepareAsync();

        return true;
    }

    public interface OnSongReadyListener {
        public void onReady(MediaPlayer mediaplayer);
    }

    public interface OnSongCompletion {
        public void onSongComplete(MediaPlayer mediaplayer, String songPath);
    }
    private ReentrantLock lock = new ReentrantLock();
    public boolean playIfReady(final String songPath, OnSongReadyListener listener, int seekTo) {
        lock.lock();
        if (readyToPlaySongs.containsKey(songPath)) {
            if (seekTo > 0)
                mediaPlayers[readyToPlaySongs.get(songPath)].seekTo(seekTo);
            mediaPlayers[readyToPlaySongs.get(songPath)].start();
            readyToPlaySongs.remove(songPath);

            lock.unlock();
            return true;
        }
        else {
            reportToWhenDone.put(songPath, listener);

            lock.unlock();
            return false;
        }
    }
    public boolean playIfReady(final String songPath, OnSongReadyListener listener, final OnSongCompletion onSongComplete, int seekTo) {
        lock.lock();

        if (readyToPlaySongs.containsKey(songPath)) {
            if (seekTo > 0)
                mediaPlayers[readyToPlaySongs.get(songPath)].seekTo(seekTo);

            mediaPlayers[readyToPlaySongs.get(songPath)].start();
            mediaPlayers[readyToPlaySongs.get(songPath)].setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.setOnCompletionListener(null);
                    onSongComplete.onSongComplete(mp, songPath);
                }
            });
            readyToPlaySongs.remove(songPath);

            lock.unlock();

            return true;
        }
        else {
            reportToWhenDone.put(songPath, listener);

            lock.unlock();
            return false;
        }
    }

    public void setListenerOnLoaded(String songPath, OnSongReadyListener onSongReadyListener) {
        if (readyToPlaySongs.containsKey(songPath))
            onSongReadyListener.onReady(mediaPlayers[readyToPlaySongs.get(songPath)]);
        else
            reportToWhenDone.put(songPath, onSongReadyListener);
    }

    public void stopCurrentSong() {
        for (MediaPlayer mp : mediaPlayers) {
            if (mp != null && mp.isPlaying()) {
                mp.stop();
                mp.reset();
            }
            else if (mp != null) {
                try {
                    mp.reset();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private AudioManager audio;

}
