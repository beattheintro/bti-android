package com.musicplode.beattheintro;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.amazon.device.messaging.ADMMessageHandlerBase;
import com.amazon.device.messaging.ADMMessageReceiver;
import com.musicplode.beattheintro.storage.Storage;

public class ADMMessageHandler extends ADMMessageHandlerBase 
{
    private static final int NOTIFICATION_ID = 1;
    /** Tag for logs. */
    private final static String TAG = "ADMSampleMessenger";

    /**
     * The MessageAlertReceiver class listens for messages from ADM and forwards them to the 
     * SampleADMMessageHandler class.
     */
    public static class MessageAlertReceiver extends ADMMessageReceiver
    {
        /** {@inheritDoc} */
        public MessageAlertReceiver()
        {
            super(ADMMessageHandler.class);
        }
    }

    /**
     * Class constructor.
     */
    public ADMMessageHandler()
    {
        super(ADMMessageHandler.class.getName());
    }
	
    /**
     * Class constructor, including the className argument.
     * 
     * @param className The name of the class.
     */
    public ADMMessageHandler(final String className) 
    {
        super(className);
    }

    /** {@inheritDoc} */
    @Override
    protected void onMessage(final Intent intent) {
        /* String to access message field from data JSON. */
        final String msgKey = getString(R.string.json_data_msg_key);

        /* String to access timeStamp field from data JSON. */
        final String timeKey = getString(R.string.json_data_time_key);
        
        /* Intent action that will be triggered in onMessage() callback. */
        final String intentAction = getString(R.string.intent_msg_action);

        /* Extras that were included in the intent. */
        final Bundle extras = intent.getExtras();
        
        /* Extract message from the extras in the intent. */
        final String msg = extras.getString(Storage.PUSH_MESSAGE_KEY);
    	
        final String time = extras.getString(timeKey);
        String gameId = null;
        if (intent.getExtras().containsKey("nt")) {
        	String s = intent.getExtras().getString("nt");
        	if (s != null && s.equals("ShowGame") && intent.getExtras().containsKey("id")) {
        		gameId = intent.getExtras().getString("id");
        		
//        		if (gameId != null && gameId.length() > 0)
//                	Storage.store(getApplicationContext(), Storage.START_GAME_KEY, gameId);
        	}
        }

        /* Create a notification with message data. */
        /* This is required to test cases where the app or devicemay be off. */
        postNotification(msgKey, timeKey, intentAction, msg, time, gameId);

        /* Intent category that will be triggered in onMessage() callback. */
        final String msgCategory = getString(R.string.intent_msg_category);
        
        /* Broadcast an intent to update the app UI with the message. */
        /* The broadcast receiver will only catch this intent if the app is within the onResume state of its lifecycle. */
        /* User will see a notification otherwise. */
        final Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(intentAction);
        broadcastIntent.addCategory(msgCategory);
        broadcastIntent.putExtra(msgKey, msg);
        broadcastIntent.putExtra(timeKey, time);
        this.sendBroadcast(broadcastIntent);

        
        Intent i = new Intent("com.musicplode.beattheintro.DISPLAY_PUSH");
        i.putExtra("msg", msg);
        getApplicationContext().sendBroadcast(i);
        
    }

    /**
     * This method posts a notification to notification manager.
     * 
     * @param msgKey String to access message field from data JSON.
     * @param timeKey String to access timeStamp field from data JSON.
     * @param intentAction Intent action that will be triggered in onMessage() callback.
     * @param msg Message that is included in the ADM message.
     * @param time Timestamp of the ADM message.
     */
    private void postNotification(final String msgKey, final String timeKey,
            final String intentAction, final String msg, final String time, String gameId) {
        final Context context = getApplicationContext();
        final NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        /* Clicking the notification should bring up the MainActivity. */
        /* Intent FLAGS prevent opening multiple instances of MainActivity. */
        final Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra(msgKey, msg);
        notificationIntent.putExtra(timeKey, time);
        notificationIntent.putExtra("startedFromPush", true);

        if (gameId != null) {
        	notificationIntent.putExtra("startGameById", gameId);
        }
        
        /* Android reuses intents that have the same action. Adding a time stamp to the action ensures that */
        /* the notification intent received in onResume() isn't one that was recycled and that may hold old extras. */
        notificationIntent.setAction(intentAction + time);

        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL);
        
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.ic_notification)
        .setContentTitle(getString(R.string.app_name))
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(msg))
        .setAutoCancel(true)
        .setContentText(msg);

        mBuilder.setContentIntent(pendingIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
    
	/**
	 * 02-28 15:17:05.287: W/BroadcastQueue(1235): Permission Denial: receiving
	 * Intent { act=com.amazon.device.messaging.intent.REGISTRATION
	 * cat=[com.musicplode.beattheintro] flg=0x10
	 * pkg=com.musicplode.beattheintro
	 * cmp=com.musicplode.beattheintro/.ADMMessageHandler$MessageAlertReceiver
	 * (has extras) } to
	 * com.musicplode.beattheintro/.ADMMessageHandler$MessageAlertReceiver
	 * requires com.musicplode.beattheintro.permission.RECEIVE_ADM_MESSAGE due
	 * to sender com.amazon.device.messaging (uid 32011)
	 */

    /** {@inheritDoc} */
    @Override
    protected void onRegistrationError(final String string) {
        Log.e(TAG, "ADMMessageHandler onRegistrationError " + string);
        Storage.remove(this, Storage.ADMREGID);
    }

    /** {@inheritDoc} */
    @Override
    protected void onRegistered(final String registrationId) {
        Log.i(TAG, "ADMMessageHandler onRegistered");
        Log.i(TAG, registrationId);

		Storage.store(this, Storage.ADMREGID, registrationId);
    }

    /** {@inheritDoc} */
    @Override
    protected void onUnregistered(final String registrationId) {
        Log.i(TAG, "ADMMessageHandler onUnregistered");
        Storage.remove(this, Storage.ADMREGID);
    }
}