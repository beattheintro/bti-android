package com.musicplode.beattheintro;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
// import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amazon.device.messaging.ADM;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.DialogHelpers.OnOkButton;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.helpers.OfflineHelpers;
import com.musicplode.beattheintro.helpers.OfflineHelpers.Track;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
// import java.security.Signature;
// import java.security.SignatureException;
import java.util.List;
import java.util.Random;

public class MainActivity extends FragmentActivity {
	private TextView[] textViews = new TextView[4];
	private int showTextView;
	private boolean isTablet;
    private boolean userProfileDone = false;
	private boolean userProfileReloginSuccess = false;
	private boolean splashScreenDone = false;
	private boolean metaDataDownloaded = false;
	private boolean exitRequested;
	private GoogleCloudMessaging gcm;
	private static final int SPLASH_MS = 2000; // JJ changed to show splash longer, it was 1000mS
	private static final int NOTIFICATION_TIME_SHOWN = 4000;
	private static final int LOADING_MAX_TIME = 10000;

    private static final String GCM_SENDER_ID = "139701229856";


    private String regid;
	private boolean isSignup;

    /**
     * Helper that Animates the flares on the loading screen
     * @param counterClockWise
     * @param roataton
     * @param v
     * @param duration
     */
	@SuppressLint("NewApi")
	private void setAnim(boolean counterClockWise, float roataton, View v, int duration) {
		Animation rot;
		if (counterClockWise)
			rot = AnimationUtils.loadAnimation(this, R.anim.clockwise_rotation);
		else
			rot = AnimationUtils.loadAnimation(this, R.anim.counter_clockwise_rotation);
		
		rot.setDuration(duration);

		if (v == null)
			return;
		
		if (android.os.Build.VERSION.SDK_INT>=11) {
			v.setRotation(roataton);
		}
		v.setAnimation(rot);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		exitRequested = true;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getIntent() != null && getIntent().getExtras() != null) {
			boolean startedFromPush = getIntent().getExtras().getBoolean("startedFromPush", false);

			if (startedFromPush) {
				getIntent().removeExtra("startedFromPush");
				
				if (getIntent().hasExtra("startGameById")) {
					String gameId = getIntent().getExtras().getString("startGameById");
					if (gameId != null && gameId.length() > 0)
						Storage.store(getApplicationContext(), Storage.START_GAME_KEY, gameId);
				}
			}
		}

//		/**
//		 * Echo the facebook keydigest
//		 */
		try {
		    PackageInfo info = getPackageManager().getPackageInfo("com.musicplode.beattheintro", PackageManager.GET_SIGNATURES);
		    for (android.content.pm.Signature signature : info.signatures) {
		        MessageDigest md = MessageDigest.getInstance("SHA");
		        md.update(signature.toByteArray());
		        Log.e("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
		    }
		} catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}


		/**
         * Sets up tablet vs phone settings
         */
		isTablet = getResources().getBoolean(R.bool.tablet);

		if(isTablet){
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	    }
		else
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		setContentView(R.layout.loadingscreen);


		
		
		
		/**
		 * Start splash screen for SPLASH_MS milliseconds
		 */
		SplashCounter sc = new SplashCounter(SPLASH_MS, SPLASH_MS);
		sc.start();
		
		
		
		
		/*****************************************************
		 * PushMessages
		 ****************************************************/
		
		/**
		 * GCM
		 */
		if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
			String lastRegId = Storage.getStoredString(this, Storage.GCMREGID);

			//noinspection StatementWithEmptyBody
			if (lastRegId != null && lastRegId.length() > 0) {
//				System.out.println("GCM RegId: " + lastRegId);
			}
			else {
				registerGCMInBackground();
			}
        }
		
		/**
		 * ADM (Amazon)
		 */
		boolean admAvailable = false ;
		try {
		    Class.forName( "com.amazon.device.messaging.ADM" );
		    admAvailable = true ;
		} catch (ClassNotFoundException e) {
		    // Handle the exception.
		}
		if (admAvailable) {
			final ADM adm = new ADM(this);
			if (adm.isSupported()) {
				if (adm.getRegistrationId() == null) {
				   adm.startRegister();
				}
				else {
					Storage.store(MainActivity.this, Storage.ADMREGID, adm.getRegistrationId());
				}
			}
		}
		
		isSignup = getIntent() != null && getIntent().getBooleanExtra("signup", false);
		
		/**
		 * Start background tasks if the user has previously logged in, i.e relogin
		 */
		JSONObject storedUser = Storage.getUserProfile(this);
		if (storedUser != null && !isSignup) {
			// prev logged in
			
			JSONObject o = JSONRequestHelpers.getLoginRequestJSON(storedUser);
			Storage.addRequestToQueue(this, Storage.MAIN_URL, o, new OnNetworkResult() {
				@Override
				public void okResult(JSONObject response) {
					userProfileDone = true;
					
					Storage.store(MainActivity.this, Storage.USERPROFILE, response);
					userProfileReloginSuccess = true;
					startNext();
				}
				
				@Override
				public void badResult(String errorMessage) {
					Storage.removeUserProfile(MainActivity.this);
					userProfileDone = true;
					userProfileReloginSuccess = false;
					
					startNext();
				}
			});
		}
		else {
			// Not previously logged in
			userProfileDone = true;
			userProfileReloginSuccess = false;
		}
		
		

		/**
		 * Update the metadata
		 */
		Storage.addRequestToQueue(this, Storage.METADATA_URL, null,  metaOnNetworkResult);

		/**
		 * Sets the correct font for this view's children
		 */
		Helpers.setAllFonts(findViewById(R.id.content), this);
	}
	public OnNetworkResult metaOnNetworkResult = new OnNetworkResult() {
		@Override
		public void okResult(JSONObject response) {
			Storage.store(MainActivity.this, Storage.METADATA, response);

            metaDataDownloaded = isValidServerVersion(response);
			
			startNext();
		}
		/**
		 * NOTE: Play in offlinemode needs some local MP3 data to show as an option. offlinemode seems to be rather limited and circular!
		 */
		@Override
		public void badResult(String errorMessage) {
			final Dialog d = DialogHelpers.getDialog(MainActivity.this, R.string.ERROR_INTERNET_CONNECTION_TITLE, R.string.ERROR_RETRY_BUTTON, R.string.OFFLINE_MODE, R.string.CANCELQUIT, R.string.ERROR_INTERNET_CONNECTION_TEXT);
			if (d != null) {
				d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						d.cancel();
					}
				});
				
				d.findViewById(R.id.dialogButton2).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent i = new Intent(MainActivity.this, LoggedInMenu.class);
						i.putExtra("offlineMode", true);
						startActivity(i);
						d.cancel();
					}
				});
				
				List<Track> apa = OfflineHelpers.getRandomTracks(MainActivity.this);
				if (apa == null || apa.size() < 19)
					d.findViewById(R.id.dialogButton2).setVisibility(View.GONE);

				d.findViewById(R.id.dialogButton3).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						d.setOnCancelListener(null);
						d.cancel();
						finish();
					}
				});
				
				d.show();
				d.setOnCancelListener(new OnCancelListener() {
					@Override
					public void onCancel(DialogInterface arg0) {
						Storage.addRequestToQueue(MainActivity.this, Storage.METADATA_URL, null,  metaOnNetworkResult);
					}
				});
			}
		}
	};

    /**
     * The kill switch to make sure that the client version isent to old.
     * @param response
     * @return
     */
	private boolean isValidServerVersion(JSONObject response) {
		int serverMinVersion = 1;
		
		if (response != null && response.has("v")) {
			try {
				serverMinVersion = response.getInt("v");
			
				if (serverMinVersion > Storage.CLIENT_SERVER_COMMUNICATION_VERSION) {
					DialogHelpers.showErrorDialog(this, R.string.ERROR_WRONG_SERVER_VERSION_TITLE, R.string.ERROR_WRONG_SERVER_VERSION_TEXT, R.string.OK, new OnOkButton() {
						@Override
						public void onOk() {
							finish();
						}
					});
					return false;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
		return true;
	}

	private void registerGCMInBackground() {
        new AsyncTask<Void, Void, String>() {
			@Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(MainActivity.this);
                    }
                    regid = gcm.register(GCM_SENDER_ID);
                    echoRegId(regid);
                } catch (IOException ex) {

                }
                return msg;
            }
        }.execute(null, null, null);
    }
	
	public void echoRegId(String regId) {
    	Storage.store(this, Storage.GCMREGID, regId);
    }
	
	
	/**
	 * Starts the next nofification window.
	 * Fades out the currently active and fades in the next
	 */
	private void animateToNextTextView() {
		if (textViews[showTextView] == null)
			return;
		
		TextView outTextView = textViews[(showTextView+textViews.length-1)%textViews.length];
		if (outTextView.getVisibility() == View.VISIBLE) {
			final Animation out = new AlphaAnimation(1.0f, 0.0f);
			out.setDuration(500);
			outTextView.setVisibility(View.INVISIBLE);
			outTextView.setAnimation(out);
		}
		
		final Animation in = new AlphaAnimation(0.0f, 1.0f);
		in.setDuration(500);
		textViews[showTextView].setVisibility(View.VISIBLE);
		textViews[showTextView].setAnimation(in);
		
		showTextView = (showTextView+1) % textViews.length;
		
		CountDownTimer cdt = new CountDownTimer(NOTIFICATION_TIME_SHOWN, NOTIFICATION_TIME_SHOWN) {
			@Override
			public void onTick(long millisUntilFinished) {
			}
			@Override
			public void onFinish() {
				animateToNextTextView();
			}
		};
		cdt.start();
	}
	
	/**
	 * Starts the next activity.
	 */
	public synchronized void startNext() {
		if (splashScreenDone && userProfileDone && metaDataDownloaded) {
			if (!exitRequested) {
				Intent intent = null;
				
				if (userProfileReloginSuccess) {
					if (isTablet)
						intent = new Intent(getApplicationContext(), LoggedInMenuLand.class);
					else
						intent = new Intent(getApplicationContext(), LoggedInMenu.class);
				}
				else {
					if (isTablet)
						intent = new Intent(getApplicationContext(), MainMenuLand.class);
					else
						intent = new Intent(getApplicationContext(), MainMenu.class);
				}
				
				intent.putExtra("signup", isSignup);
				
				if (!isFinishing())
					startActivity(intent);
			}
			finish();
		}
	}
	
	/**
	 * Starts the notification/hints if it is not done loading the userProfile
	 */
	public void afterSplashDone() {
		splashScreenDone = true;
		if (userProfileDone && metaDataDownloaded) {
			startNext();
			return;
		}
		
		/**
		 * Remove splash views
		 */
		findViewById(R.id.splashBroughtImageView).setVisibility(View.GONE);
		findViewById(R.id.splashCopyrightTextView).setVisibility(View.GONE);
		findViewById(R.id.splashLogoImageView).setVisibility(View.GONE);
		findViewById(R.id.content).setBackgroundColor(getResources().getColor(R.color.black));
		findViewById(R.id.loadingTextView).setVisibility(View.VISIBLE);
		findViewById(R.id.loadingBlueline).setVisibility(View.VISIBLE);
		
		
		/**
		 */
		textViews[0] = (TextView) findViewById(R.id.loadingText1);
		textViews[1] = (TextView) findViewById(R.id.loadingText2);
		textViews[2] = (TextView) findViewById(R.id.loadingText3);
		textViews[3] = (TextView) findViewById(R.id.loadingText4);

		showTextView = new Random(System.currentTimeMillis()).nextInt(4);
		
		findViewById(R.id.linearLayout1).setVisibility(View.VISIBLE);

        for (TextView textView : textViews) {
            textView.setVisibility(View.INVISIBLE);
        }
		
		/**
		 * Start notification views
		 */
		animateToNextTextView();

		
		/**
		 * Start animation on the flares
		 */
		Random rand = new Random(System.currentTimeMillis());
        setAnim(false, rand.nextFloat()*360, findViewById(R.id.imageView1), 1500 + rand.nextInt(2000));
        setAnim(true, rand.nextFloat()*360, findViewById(R.id.imageView2), 1500 + rand.nextInt(2000));
        setAnim(true, rand.nextFloat()*360, findViewById(R.id.imageView3), 1500 + rand.nextInt(2000));
        setAnim(false, rand.nextFloat()*360, findViewById(R.id.imageView4), 1500 + rand.nextInt(2000));
        setAnim(true, rand.nextFloat()*360, findViewById(R.id.imageView5), 1500 + rand.nextInt(2000));
		
		
        /**
         * Start the scale animtaion on the middle speaker view
         */
        Animation scaleAnim = AnimationUtils.loadAnimation(this, R.anim.scale);
		scaleAnim.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar1);
				if (pb.getProgress() < 100) {
					animation.reset();
					animation.start();
				}
				else {
					splashScreenDone = true;
					userProfileDone = true;
					userProfileReloginSuccess = false;
					startNext();
				}
			}
			@Override
			public void onAnimationStart(Animation animation) { }
			@Override
			public void onAnimationRepeat(Animation animation) { }
		});
		findViewById(R.id.imageView6).startAnimation(scaleAnim);
		
		/**
		 * DEBUG stuff: on click start next directly
		 */
		findViewById(R.id.content).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startNext();
			}
		});
		
		/**
		 * Start the progressbar counter
		 */
		DebugCounter dc = new DebugCounter(LOADING_MAX_TIME, 10, (ProgressBar) findViewById(R.id.progressBar1));
		dc.start();
	}
	
	public class SplashCounter extends CountDownTimer {
		public SplashCounter(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}
		@Override
		public void onFinish() {
			afterSplashDone();
		}
		@Override
		public void onTick(long millisUntilFinished) { }
	}
	
	public static class DebugCounter extends CountDownTimer {
		private long startTime;
		private ProgressBar pb;
		public DebugCounter(long millisInFuture, long countDownInterval, ProgressBar pb) {
			super(millisInFuture, countDownInterval);
			startTime = millisInFuture;
			this.pb = pb;
		}

		@Override
		public void onFinish() {
			pb.setProgress(100);
		}

		@Override
		public void onTick(long millisUntilFinished) {
			if (pb != null) {
				double p = (double)(startTime - millisUntilFinished)/(double)startTime;
				p *= 100;
				pb.setProgress((int) p );
			}
		}
	}
}
