package com.musicplode.beattheintro.helpers;

//import android.animation.AnimatorInflater;
//import android.animation.AnimatorSet;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.musicplode.beattheintro.LoggedInMenu;
import com.musicplode.beattheintro.MainActivity;
import com.musicplode.beattheintro.PartyPlayActivity;
import com.musicplode.beattheintro.R;
import com.musicplode.beattheintro.fragments.ThreeTwoOneFragment;
import com.musicplode.beattheintro.modelhelpers.IGame;
import com.musicplode.beattheintro.modelhelpers.LevelType;
import com.musicplode.beattheintro.modelhelpers.OnlineGame;
import com.musicplode.beattheintro.modelhelpers.Pack;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
// import java.util.Map;

public class Helpers {
    public static final int PARTY_PLAY_REQUEST_CODE = 323;

    /**
	 * Sets all textviews font in the given view to the default font
	 * 
	 * @param v
	 * @param context
	 */
	public static void setAllFonts(View v, Context context) {
		if (v == null || !(v instanceof ViewGroup))
			return;

		for (int i = 0; i < ((ViewGroup) v).getChildCount(); ++i) {
			View nextChild = ((ViewGroup) v).getChildAt(i);

			if (nextChild == null)
				continue;
			
			if (nextChild instanceof ViewGroup)
				setAllFonts(nextChild, context);

			if (nextChild instanceof TextView) {
				setFont((TextView) nextChild, context);
			}
		}
	}

	private static int fontShadowColor = -1;
	public static void setFont(TextView tv, Context context) {
		if (tv == null)
			return;

		if (fontShadowColor == -1)
			fontShadowColor = context.getResources().getColor(R.color.blackthirtyaplha);

		tv.setTypeface(Storage.getFont(context));
		tv.setShadowLayer(1, 2, 2, fontShadowColor);
	}

	/**
	 * Prints a large textblob to System.out used to overcome eclipse buffer limit
	 * 
	 * @param string
	 */
	public static void printLargeText(String string) {
		if (string == null)
			return;
		int max = 4000;

		for (int i = 0; i < string.length(); i += max) {
			if (string.length() - i < max)
				System.out.println(string.substring(i, i + string.length() - i));
			else
				System.out.println(string.substring(i, i + max));
		}
	}

	public static String getPackNameFromPackId(JSONObject storedJSON, String packId) {
		try {
			JSONArray packs = storedJSON.getJSONObject("UserProfile").getJSONArray("Packs");
			for (int i = 0; i < packs.length(); i++) {
				String s = packs.getJSONObject(i).getString("Id");
				if (s.contentEquals(packId))
					return packs.getJSONObject(i).getString("Name");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Performs a check if the given id is in the likes array of the userprofile
	 * @param userProfile
	 * @param id
	 * @return
	 */
	public static boolean doesLikeTrackId(JSONObject userProfile, String id) {
		try {
			JSONArray likesArray = userProfile.getJSONObject("UserProfile").getJSONArray("Likes");

			for (int i = 0; i < likesArray.length(); i++) {
				if (likesArray.getJSONObject(i).getString("Id").contentEquals(id))
					return true;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Refreshes the stored profile
	 * 
	 * @param context
	 */
	public static void refreshProfile(final Context context) {
		if (context == null)
			return;
		
		JSONObject jsonReq = JSONRequestHelpers.getRefreshUserProfileJSON(context);
		if (jsonReq == null)
			return;

		Storage.addRequestToQueue(context, Storage.MAIN_URL, jsonReq,
				new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						Storage.updateUserProfile(context, response, true);
					}
					@Override
					public void badResult(String errorMessage) { }
				});
	}

	/**
	 * Converts denstityPoints to pixels
	 * 
	 * @param context
	 * @param dp
	 * @return
	 */
	public static float dpToPx(Context context, int dp) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		return dp * (metrics.densityDpi / 160f);
	}

	/**
	 * Returns the amount of discs that the player has
	 * @param userProfile
	 * @return
	 */
	public static int getPlayerDiscs(JSONObject userProfile) {
		if (userProfile == null)
			return 0;
		try {
			return userProfile.getJSONObject("UserProfile").getInt("Discs");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static int getPlayerPosition(JSONObject userProfile) {
		if (userProfile == null)
			return 0;
		try {
			float maxDiscs = 2000000;
		    float percent = userProfile.getJSONObject("UserProfile").getInt("Discs")/maxDiscs;
		    return (int) (100.0*percent);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	
	public static String getAlbumArtURL(String trackId) {
		return "http://btistorage.blob.core.windows.net/tracks/" + trackId + "_50.jpg";
	}
	
	public static String getSmallSquarePackArtURL(String packId) {
		return "https://btistorage.blob.core.windows.net/packimages/leaderboards_icons/" + packId + ".png";
	}

	public static String getBigSquarePackArtURL(String packId) {
		return "https://btistorage.blob.core.windows.net/packimages/leaderboards/" + packId + ".png";
	}

	public static String getPackArtURL(String packId) {
		return "https://btistorage.blob.core.windows.net/packimages/mobile/" + packId + ".png";
	}

	public static String getIntroMp3URL(String trackId) {
		return "http://btistorage.blob.core.windows.net/tracks/" + trackId + ".mp3";
	}
	
	public static void displayPackImage(Context context, String packId,
			ImageView packImageView) {
		Helpers.displayImage(context, getPackArtURL(packId), packImageView);
	}

	private static HashMap<String, Integer> stringsToImages = new HashMap<String, Integer>();

	/**
	 * Creates a spannable from a string, patterns stored in stringsToImages
	 * @param context
	 * @param text
	 * @return
	 */
	public static Spannable createSpannable(Context context, String text) {
		if (stringsToImages.isEmpty()) {
			stringsToImages.put("[*]", R.drawable.spannable_star);
			stringsToImages.put("[()]", R.drawable.spannable_disc);
		}

		SpannableStringBuilder builder = new SpannableStringBuilder(text);
		if (TextUtils.isEmpty(text) || !text.contains("["))
			return builder;

        for (int i = 0; i < builder.length(); i++) {
			for (String s : stringsToImages.keySet()) {
				if (builder.length() - i - s.length() >= 0) {
					if (s.contentEquals(builder.subSequence(i, i + s.length()))) {
						builder.setSpan(new ImageSpan(context, stringsToImages.get(s)), i, i + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					}
				}
			}
		}
		return builder;
	}

	/**
	 * Takes a given textview and converts its text to spannable
	 * @param context
	 * @param textView
	 */
	public static void changeStringToSpannable(Context context, TextView textView) {
		if (textView == null || context == null)
			return;
		String s = textView.getText().toString();
		textView.setText(createSpannable(context, s));
	}

	public static ImageLoader getImageLoader(Context context) {
		ImageLoader il = ImageLoader.getInstance();
		if (!il.isInited()) {
			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
			il.init(config);
		}
		return il;
	}

	public static void displayImage(Context context, String url,
			ImageView imageView) {
		try {
			ImageLoader il = ImageLoader.getInstance();
			if (!il.isInited()) {
				ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
				il.init(config);
			}
			getImageLoader(context).displayImage(url, imageView, Storage.displayImageOption);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void cancelImageLoading(Context context, ImageView imageView) {
		if (context == null || imageView == null)
			return;
		
		getImageLoader(context).cancelDisplayTask(imageView);
	}

	/**
	 * Returns the path to the given facebookids user photo
	 * @param fbID
	 * @return
	 */
	public static String getFacebookProfileImage(String fbID) {
		return "https://graph.facebook.com/" + fbID + "/picture?type=small";
	}

	public static OnScrollListener getArrowScroll(final ListView lv,
			final View arrowUp, final View arrowDown) {
		return new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				if (lv != null && lv.getAdapter() != null) {
					if (arrowDown != null) {
						if (lv.getLastVisiblePosition() == lv.getAdapter()
								.getCount() - 1) {
							arrowDown.setVisibility(View.INVISIBLE);
						}
                        else {
							arrowDown.setVisibility(View.VISIBLE);
						}
					}
					if (arrowUp != null) {
						if (lv.getFirstVisiblePosition() == 0) {
							arrowUp.setVisibility(View.INVISIBLE);
						}
                        else {
							arrowUp.setVisibility(View.VISIBLE);
						}
					}
				}
			}
		};
	}

	public static List<JSONObject> getAllPackJSONS(Context context) {
		@SuppressWarnings("Convert2Diamond") List<JSONObject> packs = new ArrayList<JSONObject>();

		try {
			for (int i = 0; i < Storage.getUserProfile(context).getJSONObject("UserProfile").getJSONArray("Packs").length(); i++) {
				JSONObject o = Storage.getUserProfile(context).getJSONObject("UserProfile").getJSONArray("Packs").getJSONObject(i);
				packs.add(o);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return packs;
	}

	public static void addArrows(final ListView lv, View arrowUp, View arrowDown) {
		if (lv == null)
			return;

		lv.setOnScrollListener(getArrowScroll(lv, arrowUp, arrowDown));

		if (arrowUp != null) {
			arrowUp.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					lv.smoothScrollByOffset(-5);
					return true;
				}
			});
		}
		if (arrowDown != null) {
			arrowDown.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View arg0, MotionEvent arg1) {
					lv.smoothScrollByOffset(5);
					return true;
				}
			});
		}
	}

	/**
	 * Resumes a game from a given gameId
	 * @param activity
	 * @param containerId
	 * @param pd
	 * @param gameId
	 */
	public static void startGameById(final LoggedInMenu activity,
			final int containerId, final ProgressDialog pd, String gameId) {
		JSONObject jsonReq = JSONRequestHelpers.getResumeGameJSON(Storage.getUserProfile(activity), gameId);

		Storage.addRequestToQueue(activity, Storage.GAMERESUME_URL, jsonReq,
				new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						try {
							activity.setOnBackExitTo(ThreeTwoOneFragment.BACKSTACK_KEY);
							String levelTypeId = response.getString("LevelTypeId");
							LevelType lt = new LevelType(activity, levelTypeId);
	
							if (lt == null || !lt.isValid()) {
								// TODO: Display fatal error message
								pd.cancel();
								pd.dismiss();
								activity.startActivity(new Intent(activity, MainActivity.class));
								activity.finish();
								return;
							}
	
							FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
							Helpers.setFragmentAnimation(ft);
							ft.replace(containerId, ThreeTwoOneFragment.newInstance(response, false, false, null));
							ft.addToBackStack(ThreeTwoOneFragment.BACKSTACK_KEY);
							ft.commitAllowingStateLoss();
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						pd.cancel();
						pd.dismiss();
					}

					@Override
					public void badResult(String errorMessage) {
						pd.cancel();
						pd.dismiss();
						
						if (activity != null && errorMessage != null && errorMessage.length() > 0)
							DialogHelpers.showErrorDialog(activity, errorMessage);
					}
				});
	}

	/**
	 * Starts a new game
	 * 
	 * @param activity
	 * @param containerId
	 * @param opponentName
	 * @param packId
	 * @param levelType
	 */
	public static void startGame(final LoggedInMenu activity,
			final int containerId, String opponentName, String packId,
			String levelType) {
		final ProgressDialog pd = new ProgressDialog(activity);
		pd.setMessage(activity.getString(R.string.LOADING));
		pd.setCancelable(false);
		
		try {
			pd.show();
		} catch (Exception e1) {
			e1.printStackTrace();
			return;
		}

		JSONObject jsonReq = JSONRequestHelpers.getStartPackJSON(Storage.getStoredJSON(activity, Storage.USERPROFILE), packId, levelType, opponentName);

		Storage.addRequestToQueue(activity, Storage.CREATE_GAME_URL, jsonReq,
				new OnNetworkResult() {
					@Override
					public void okResult(JSONObject response) {
						if (activity != null) {
							try {
								startGameById(activity, containerId, pd, response.getString("GameInstanceId"));
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}

					@Override
					public void badResult(String errorMessage) {
						pd.cancel();
						pd.dismiss();
						
						if (activity != null && errorMessage != null && errorMessage.length() > 0)
							DialogHelpers.showErrorDialog(activity, errorMessage);
					}
				});
	}



    /**
     * Starts a new game
     *  @param activity
     * @param containerId
     * @param packId
     * @param levelType
     * @param playerNames
     */
    public static void startPartyPlayGame(final LoggedInMenu activity, final int containerId, String packId, String levelType, final String[] playerNames) {
        final ProgressDialog pd = new ProgressDialog(activity);
        pd.setMessage(activity.getString(R.string.LOADING));
        pd.setCancelable(false);

        try {
            pd.show();
        } catch (Exception e1) {
            e1.printStackTrace();
            return;
        }

        JSONObject jsonReq = JSONRequestHelpers.getStartPackJSON(
                Storage.getStoredJSON(activity, Storage.USERPROFILE), packId,
                levelType, "");

        Storage.addRequestToQueue(activity, Storage.CREATE_PARTYPLAY_GAME_URL, jsonReq,
                new OnNetworkResult() {
                    @Override
                    public void okResult(JSONObject response) {
                        try {
                            activity.setOnBackExitTo(ThreeTwoOneFragment.BACKSTACK_KEY);
                            String levelTypeId = response.getString("LevelTypeId");
                            LevelType lt = new LevelType(activity, levelTypeId);

                            if (lt == null || !lt.isValid()) {
                                // TODO: Display fatal error message
                                pd.cancel();
                                pd.dismiss();
                                activity.startActivity(new Intent(activity, MainActivity.class));
                                activity.finish();
                                return;
                            }


                            Bundle b = new Bundle();
                            b.putStringArray("playerNames", playerNames);
                            b.putString("gameInfo", response.toString());

                            activity.setClearStackBeforeNext();
                            activity.clearBackStackIfNeeded();

                            Intent intent = new Intent(activity, PartyPlayActivity.class);
                            intent.setAction("" + System.currentTimeMillis());
                            intent.putExtras(b);
                            activity.startActivityForResult(intent, Helpers.PARTY_PLAY_REQUEST_CODE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        pd.cancel();
                        pd.dismiss();
                    }

                    @Override
                    public void badResult(String errorMessage) {
                        pd.cancel();
                        pd.dismiss();

                        if (activity != null && errorMessage != null && errorMessage.length() > 0)
                            DialogHelpers.showErrorDialog(activity, errorMessage);
                    }
                });
    }


    public static boolean shouldShowBackButton() {
		return Build.MANUFACTURER.equals("Amazon");
	}

	/**
	 * Adds back functionality to a given button and adds animations to a view
	 * @param backButton
	 * @param pulse  which is a coloured circular disc
	 * @param glow  which is an emtpy outer gold ring
	 * */
	public static void setupBackPulseButton(View backButton, View pulse, View glow) {
		if (backButton != null && shouldShowBackButton()) {
			backButton.setVisibility(View.VISIBLE);
			backButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (v.getContext() != null && v.getContext() instanceof Activity) {
						((Activity) v.getContext()).onBackPressed();
					}
				}
			});

			addPulse(pulse, 0); // set up smaller pulsing animation
			addPulse(glow, 1);  // set up larger pulsing animation with alpha glow

		} else if (!shouldShowBackButton()) {
			if (backButton != null)
				backButton.setVisibility(View.INVISIBLE);
			if (pulse != null)
				pulse.setVisibility(View.INVISIBLE);
			if (glow != null)
				glow.setVisibility(View.INVISIBLE);
		}
	}

	/**
	 * Adds back functionality to a given button and adds swirl animation to a view
	 * @param backButton
	 * @param swirl

	public static void setupBackButton(View backButton, View swirl) {
		if (backButton != null && shouldShowBackButton()) {
			backButton.setVisibility(View.VISIBLE);
			backButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (v.getContext() != null && v.getContext() instanceof Activity) {
						((Activity) v.getContext()).onBackPressed();
					}
				}
			});
			addSwirl(swirl);

		} else if (!shouldShowBackButton()) {
			if (backButton != null)
				backButton.setVisibility(View.INVISIBLE);
			if (swirl != null)
				swirl.setVisibility(View.INVISIBLE);
		}
	}
	*/
	/**
	 * Gives a given view swirl animation
	 * @param swirl

	public static void addSwirl(View swirl) {
		if (swirl == null) {
			return;
		}
		Animation rotation = AnimationUtils.loadAnimation(swirl.getContext(), R.anim.clockwise_rotation_wait_three_forever);
		swirl.setAnimation(rotation);
	}
	*/
	/**
	 * Gives a given view pulsing effect using the scale animation
	 * @param pulse
	 * @param integer 0 for small pulse or 1 for big pulse with alpha change effect
	 */
	public static void addPulse(View pulse, int size) {
		if (pulse == null) {
			return;
		}
		pulse.setVisibility(View.VISIBLE);
		Animation scale;
		if (size == 0) scale = AnimationUtils.loadAnimation(pulse.getContext(), R.anim.pulse); // normal animation
		else scale = AnimationUtils.loadAnimation(pulse.getContext(), R.anim.pulse_big); //larger animation with alpha change
		// pulse.setAnimation(scale);
		pulse.startAnimation(scale);
	}

	/**
	 * Returns the users id
	 * @param userProfile
	 * @return
	 */
	public static String getUserId(JSONObject userProfile) {
		if (userProfile == null)
			return "";

		try {
			return userProfile.getJSONObject("UserProfile").getString("Id");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return "";
	}
	
	public static int getNumberOfUnlocks(JSONObject userProfile) {
		if (userProfile == null)
			return 0;

		try {
			return userProfile.getJSONObject("UserProfile").getInt("UnlocksAvailable");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return 0;
	}

	
	public static int getPercentageCompletedFromDiscs(int discs) {
	    float percent = discs/2000000f;
		return Math.min(100, (int)(100*percent));
	}
	public static int getChartPositionFromDiscs(int discs) {
		int rank=(int)(101.0-getPercentageCompletedFromDiscs(discs));
		return Math.min(100, Math.max(rank, 1));
	}
	
	public static String getLevelStringFromDiscs(Context context, int discs) {
		if (getPercentageCompletedFromDiscs(discs) == 100)
			return context.getString(R.string.DIAMOND_DISC);
		else if (getPercentageCompletedFromDiscs(discs) >= 75)
			return context.getString(R.string.PLATINUM_DISC);
		else if (getPercentageCompletedFromDiscs(discs) >= 50)
			return context.getString(R.string.GOLD_DISC);
		else if (getPercentageCompletedFromDiscs(discs) >= 25)
			return context.getString(R.string.SILVER_DISC);
		
		return null;
	}
	
	public static int getImageForChartPos(int chartPosition) {
		if (chartPosition <= 1)
			return R.drawable.diamond_disc_smaller;
		else if (chartPosition <= 25)
			return R.drawable.platinum_disc_smaller;
		else if (chartPosition <= 50)
			return R.drawable.gold_disc_smaller;
		else if (chartPosition <= 75)
			return R.drawable.silver_disc_smaller;
		
		return android.R.color.transparent;
	}

	public static String getDiscString(int discs) {
		int m = discs / 1000000;
		int k = discs % 1000000 / 1000;
		int h = discs % 1000 / 100;

		if (m > 100)
			return m + "M";
		else if (m > 0)
			return m + "." + k / 100 + "M";
		else if (k > 100)
			return k + "k";
		else if (k > 0)
			return k + "." + h + "k";
		else
			return discs + "";
	}

	public static int getMaxLevelScore(Context context) {
		int highest = 0;
		for (LevelType lt : LevelType.getAll(context)) {
			highest = Math.max(highest, lt.getMaxScoreReward());
		}
		return highest;
	}
	public static void setFragmentAnimation(FragmentTransaction ft) {
		ft.setCustomAnimations(R.anim.in_left, R.anim.out_left);
	}

	public static List<String> getLevelIdsForPack(Context context, String packId) {
		for (JSONObject o : getAllPackJSONS(context)) {
			Pack p = new Pack(o);
			if (p.getId().equals(packId))
				return p.getLevels();
		}
		
		return null;
	}
	
	
	private static String getPushTypeId(Context context, String name) {
		JSONArray arr = Storage.getPushProviderTypes(context);
		try {
			for (int i = 0; i < arr.length(); i++) {
				JSONObject o = arr.getJSONObject(i);
				if (o.getString("Name").contentEquals(name)) {
					return o.getString("PushProviderId");
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getADMPushTypeId(Context context) {
		return getPushTypeId(context, "PushAmazonProvider");
	}
	public static String getGCMPushTypeId(Context context) {
		return getPushTypeId(context, "PushGoogleProvider");
	}

	public static void setVisibilityIfExists(View view, int visibility) {
		if (view != null)
			view.setVisibility(visibility);
	}

	public static void setColorFilterMode(final ImageView iv) {
		if (iv == null)
			return;
		iv.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
					iv.setColorFilter(iv.getContext().getResources().getColor(R.color.blackfiftyaplha));
				}
				else if (event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_UP) {
					iv.clearColorFilter();
				}
				else
					iv.clearColorFilter();
				return false;
			}
		});
	}

	public static boolean hasUnfinishedGameById(Context context, String startGame) {
		try {
			JSONObject userProfile = Storage.getUserProfile(context);
			JSONArray gamesArray = userProfile.getJSONObject("UserProfile").getJSONArray("Games");
			
			for (int i = 0; i < gamesArray.length(); i++) {
				IGame g = new OnlineGame(gamesArray.getJSONObject(i));
				if (g.getId().equals(startGame) && !g.getPlayer().isFinished())
					return true;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return false;
	}


    public static void setClickListenerToViews(OnClickListener listener, View... views) {
        if (views == null || views.length == 0)
            return;

        for (View v : views) {
            if (v != null)
                v.setOnClickListener(listener);
        }
    }
	
}
