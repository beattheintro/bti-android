package com.musicplode.beattheintro.helpers;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.AudioColumns;
import android.provider.MediaStore.MediaColumns;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;


public class OfflineHelpers {
	public static class Track {
		String artist;
		String track;
		String path;
		
		public Track(String artist, String track, String path) {
			this.artist = artist;
			this.track = track;
			this.path = path;
		}

		public String getArtist() {
			return artist;
		}

		public String getTrack() {
			return track;
		}

		public String getPath() {
			return path;
		}
	}
	
	/**
	 * Generates a list of tracks from the memory of the device
	 * @param context
	 * @return Returns a list of max 150 tracks, or null if error occurs
	 */
	public static List<Track> getRandomTracks(Context context) {
		long startTime = System.currentTimeMillis();
		
		String selection = AudioColumns.IS_MUSIC + " != 0 AND " + AudioColumns.DURATION + " < 800000 AND " + AudioColumns.DURATION + " > 33000";
		String[] projection = {
			BaseColumns._ID,
			AudioColumns.ARTIST,
			MediaColumns.TITLE,
			MediaColumns.DATA,
			MediaColumns.DISPLAY_NAME,
			AudioColumns.DURATION
		};
		
		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = 
		      resolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, 
		            projection, 
		            selection, 
		            null, 
		            null);
		
		@SuppressWarnings("Convert2Diamond") List<Pair<String, List<Pair<String, String>>>> artistList = new ArrayList<Pair<String,List<Pair<String,String>>>>();
		
		while(cursor.moveToNext()){
			String artist = cursor.getString(1);
			String title = cursor.getString(2);
			String data = cursor.getString(3);
			
			Pair<String, List<Pair<String, String>>> artistPair = null;
			
			for (Pair<String, List<Pair<String, String>>> e : artistList) {
				if (e.first.equals(artist)) {
					artistPair = e;
					break;
				}
			}
			
			if (artistPair == null) {
				artistPair = new Pair<String, List<Pair<String,String>>>(artist, new ArrayList<Pair<String,String>>());
				artistList.add(artistPair);
			}

			//noinspection Convert2Diamond
			artistPair.second.add(new Pair<String, String>(title, data));
		}
		cursor.close();
		
		Collections.sort(artistList, new Comparator<Pair<String, List<Pair<String, String>>>>() {
			@Override
			public int compare(Pair<String, List<Pair<String, String>>> arg0, Pair<String, List<Pair<String, String>>> arg1) {
				return arg1.second.size() - arg0.second.size();
			}
		});
		
		
		@SuppressWarnings("Convert2Diamond") List<Track> smallBla = new ArrayList<Track>();
		
		for (int i = 0; i < artistList.size(); i++) {
			if (artistList.get(i).second.size() < 3) {
				for (int j = 0; j < artistList.get(i).second.size(); j++) {
					smallBla.add(new Track(artistList.get(i).first, artistList.get(i).second.get(j).first, artistList.get(i).second.get(j).second));
				}
				artistList.remove(i);
				i--;
			}
		}
		
		for (Pair<String, List<Pair<String, String>>> e : artistList) {
			Collections.shuffle(e.second);
		}
		
		Random rand = new Random(System.currentTimeMillis());
		
		@SuppressWarnings("Convert2Diamond") List<Track> out = new ArrayList<Track>();
		
		Collections.shuffle(smallBla);
		
		while (!artistList.isEmpty()) {
			if (smallBla.size() > 3 && rand.nextBoolean()) {
				for (int j = 0; j < 3; j++) {
					out.add(smallBla.get(0));
					smallBla.remove(0);
				}
			}
			else {
				int artistIndex = rand.nextInt(artistList.size());
				
				for (int j = 0; j < 3; j++) {
					out.add(new Track(artistList.get(artistIndex).first, artistList.get(artistIndex).second.get(j).first, artistList.get(artistIndex).second.get(j).second));
				}
				
				if (artistList.get(artistIndex).second.size() < 6)
					artistList.remove(artistIndex);
				else {
					for (int j = 0; j < 3; j++)
						artistList.get(artistIndex).second.remove(0);
				}
			}
			if (out.size()/3 >= 50)
				artistList.clear();
			
		}
		
		return out;
	}

}
