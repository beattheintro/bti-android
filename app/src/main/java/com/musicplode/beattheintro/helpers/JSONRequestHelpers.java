package com.musicplode.beattheintro.helpers;

import android.content.Context;

import com.musicplode.beattheintro.storage.Storage;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONRequestHelpers {
	public static JSONObject getFacebookFriendsWhoPlayJSON(JSONObject userProfile) {
		JSONObject jsonReq = new JSONObject();
		try {
			jsonReq.put("SessionId", userProfile.getJSONObject("UserProfile").getString("SessionId"));
            jsonReq.put("UserId", userProfile.getJSONObject("UserProfile").getString("Id"));
			jsonReq.put("GetFriendsWhoPlay", true);
			return jsonReq;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static JSONObject getTwitterFollowesWhoPlayJSON(JSONObject userProfile) {
		JSONObject jsonReq = new JSONObject();
		try {
			jsonReq.put("SessionId", userProfile.getJSONObject("UserProfile").getString("SessionId"));
            jsonReq.put("UserId", userProfile.getJSONObject("UserProfile").getString("Id"));
			jsonReq.put("GetFollowersWhoPlay", true);
			return jsonReq;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static JSONObject getEnterCompetitionJSON(JSONObject userProfile, String competitionId) {
		JSONObject jsonReq = new JSONObject();
		try {
			jsonReq.put("SessionId", userProfile.getJSONObject("UserProfile").getString("SessionId"));
            jsonReq.put("UserId", userProfile.getJSONObject("UserProfile").getString("Id"));
			jsonReq.put("EnterCompetition", true);
			jsonReq.put("CompetitionId", competitionId);
			
			return jsonReq;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static JSONObject getRemoveLikedTrack(Context context, String trackId) {
		JSONObject jsonReq = new JSONObject();
		try {
			jsonReq.put("SessionId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("SessionId"));
            jsonReq.put("UserId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("Id"));
			jsonReq.put("UnlikeTrack", true);
			jsonReq.put("AnswerTrackId", trackId);
			return jsonReq;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	public static JSONObject getUnlockPackJSON(JSONObject userProfile, String packId) {
		JSONObject jsonReq = new JSONObject();
		try {
			jsonReq.put("SessionId", userProfile.getJSONObject("UserProfile").getString("SessionId"));
            jsonReq.put("UserId", userProfile.getJSONObject("UserProfile").getString("Id"));
//			jsonReq.put("PurchasePack", true);
//			jsonReq.put("SpendTypeId", "c07c4db8-7163-4e3d-9bf8-1ca5473f0135");
			jsonReq.put("PackId", packId);
			
			return jsonReq;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	public static JSONObject getLoginRequestJSON(JSONObject storedUser) {
		JSONObject o = new JSONObject();
		try {
			o.put("Login", true);
			o.put("SessionId", storedUser.getJSONObject("UserProfile").getString("SessionId"));
            o.put("UserId", storedUser.getJSONObject("UserProfile").getString("Id"));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return o;
	}

	public static JSONObject getLoginByEmailJSON(String email, String password) {
		JSONObject o = new JSONObject();
		try {
			o.put("Login", true);
			o.put("Email", email);
			o.put("Password", password);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return o;
	}

	public static JSONObject getLoginByFacebookJSON(String id, String name, String email, String accessToken) {
		try {
			JSONObject o = new JSONObject();
			o.put("LoginByFacebook", true);
			o.put("FacebookId", id);
			o.put("AccessToken", accessToken);
			o.put("Name", name);
			o.put("Email", email);
			
			return o;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static JSONObject getCreateFacebookUserJSON(JSONObject storedJSON, String id, String name, String email, String accessToken) {
		try {
			JSONObject o = new JSONObject();
			o.put("CreateAccount", true);
			o.put("SessionId", storedJSON.getJSONObject("UserProfile").getString("SessionId"));
            o.put("UserId", storedJSON.getJSONObject("UserProfile").getString("Id"));
			o.put("Name", name);
			o.put("Email", email);
			o.put("FacebookId", id);
			o.put("AccessToken", accessToken);
			
			return o;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	

	public static JSONObject getStartPackJSON(JSONObject storedJSON, String packId, String levelType, String otherPlayerName) {
		JSONObject o = new JSONObject();
		try {
			o.put("SessionId", storedJSON.getJSONObject("UserProfile").getString("SessionId"));
            o.put("UserId", storedJSON.getJSONObject("UserProfile").getString("Id"));
			o.put("PackId", packId);
			o.put("LevelType", levelType);
			if (otherPlayerName == null)
				o.put("OtherPlayer", "");
			else
				o.put("OtherPlayer", otherPlayerName);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return o;
	}

	public static JSONObject getTrackInfoJSON(JSONObject storedJSON, String trackId) {
		JSONObject o = new JSONObject();

		try {
			o.put("TrackId", trackId);
			o.put("SessionId", storedJSON.getJSONObject("UserProfile").getString("SessionId"));
            o.put("UserId", storedJSON.getJSONObject("UserProfile").getString("Id"));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return o;
	}
	public static JSONObject getAnswerJSON(JSONObject storedJSON, String answerId, String answerTrackId, int answerSpeedMS, String gameId) {
		JSONObject o = new JSONObject();
		try {
			o.put("SessionId", storedJSON.getJSONObject("UserProfile").getString("SessionId"));
            o.put("UserId", storedJSON.getJSONObject("UserProfile").getString("Id"));
			o.put("AnswerId", answerId);
			o.put("AnswerTrackId", answerTrackId);
			o.put("AnswerSpeedMS", answerSpeedMS);
            if (gameId != null)
                o.put("GameId", gameId);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return o;
	}

	public static JSONObject getCloseGameJSON(JSONObject storedJSON, String gameId) {
		JSONObject o = new JSONObject();
		try {
			o.put("HideGame", true);
			o.put("SessionId", storedJSON.getJSONObject("UserProfile").getString("SessionId"));
            o.put("UserId", storedJSON.getJSONObject("UserProfile").getString("Id"));
			o.put("GameId", gameId);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return o;
	}

	public static JSONObject getCreateAccountJSON(JSONObject userProfile, String email, String username, String password) {
		JSONObject o = new JSONObject();
		
		try {
			o.put("CreateAccount", true);
			o.put("Email", email);
			o.put("Password", password);
			o.put("Name", username);
			
			if (userProfile != null) {
                o.put("SessionId", userProfile.getJSONObject("UserProfile").getString("SessionId"));
                o.put("UserId", userProfile.getJSONObject("UserProfile").getString("Id"));
            }
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return o;
	}

	public static JSONObject getLikeTrackJSON(Context context, JSONObject userProfile, String id) {
		JSONObject jsonReq = new JSONObject();
		try {
			jsonReq.put("LikeTrack", true);
			jsonReq.put("SessionId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("SessionId"));
            jsonReq.put("UserId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("Id"));
			jsonReq.put("AnswerTrackId", id);
			return jsonReq;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static JSONObject getLeaderBoard(Context context, boolean friendsOnly, String packId, String levelType) {
		JSONObject jsonReq = new JSONObject();
		try {
			jsonReq.put("SessionId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("SessionId"));
            jsonReq.put("UserId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("Id"));
			jsonReq.put("GetLeaderboard", true);
			jsonReq.put("GetLeaderboardFriendsOnly", friendsOnly);
			jsonReq.put("PackId", packId);
			jsonReq.put("LevelType", levelType);
			return jsonReq;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static JSONObject getResumeGameJSON(JSONObject userProfile, String gameId) {
		try {
			JSONObject o = new JSONObject();
			o.put("SessionId", userProfile.getJSONObject("UserProfile").getString("SessionId"));
            o.put("UserId", userProfile.getJSONObject("UserProfile").getString("Id"));
			o.put("GameId", gameId);
			return o;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static JSONObject getLoginLaterJSON() {
		try {
			JSONObject o = new JSONObject();
			o.put("Login", true);
			return o;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static JSONObject getLoginByTwitterJSON(String name, String id, String email, String token) {
		//{"Name":"Twitter Login Test","TwId":"1234567890","Email":"example0000@musicplode.com","TwToken":"_DATA TEST_","LoginByTwitter":true}

		try {
			JSONObject o = new JSONObject();
			o.put("Name", name);
			o.put("TwId", id);
			o.put("TwToken", token);
			o.put("LoginByTwitter", true);
			
			return o;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static JSONObject getBestMatchJSON(JSONObject userProfile, String packId, String levelType, int howMany) {
		try {
			JSONObject o = new JSONObject();
			o.put("SessionId", userProfile.getJSONObject("UserProfile").getString("SessionId"));
            o.put("UserId", userProfile.getJSONObject("UserProfile").getString("Id"));
			o.put("PackId", packId);
			o.put("LevelType", levelType);
			o.put("HowMany", howMany);
			return o;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static JSONObject getListAmazonProductsJSON() {
		try {
			JSONObject o = new JSONObject();
			o.put("Amazon", true);
			return o;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static JSONObject getFinaliseGameGameJSON(JSONObject userProfile,
			String gameId) {
		try {
			JSONObject o = new JSONObject();
			o.put("SessionId", userProfile.getJSONObject("UserProfile").getString("SessionId"));
            o.put("UserId", userProfile.getJSONObject("UserProfile").getString("Id"));
			o.put("GameId", gameId);
			return o;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		return null;
	}

	public static JSONObject getSpendRequest(JSONObject userProfile, String spendTypeId, String purchaseToken) {
		try {
			JSONObject o = new JSONObject();
			o.put("SessionId", userProfile.getJSONObject("UserProfile").getString("SessionId"));
            o.put("UserId", userProfile.getJSONObject("UserProfile").getString("Id"));
			o.put("Receipt", purchaseToken);
			o.put("SpendTypeId", spendTypeId);
			return o;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static JSONObject getPushJSON(JSONObject userProfile, String pushToken, String pushTypeId) {
		try {
			JSONObject o = new JSONObject();
			o.put("SessionId", userProfile.getJSONObject("UserProfile").getString("SessionId"));
            o.put("UserId", userProfile.getJSONObject("UserProfile").getString("Id"));
			o.put("DeviceToken", pushToken);
			o.put("PushProviderId", pushTypeId);
			return o;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static JSONObject getRefreshUserProfileJSON(Context context) {
		try {
			JSONObject jsonReq = new JSONObject();
			jsonReq.put("RefreshProfile", true);
			jsonReq.put("SessionId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("SessionId"));
            jsonReq.put("UserId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("Id"));
			return jsonReq;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static JSONObject getNudgeJSON(Context context, String userId) {
		try {
			JSONObject jsonReq = new JSONObject();
			jsonReq.put("SessionId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("SessionId"));
            jsonReq.put("UserId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("Id"));
			jsonReq.put("ToUserId", userId);
			return jsonReq;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static JSONObject getFacebookShareJSON(Context context, String earnTypeId) {
		try {
			JSONObject jsonReq = new JSONObject();
			jsonReq.put("SessionId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("SessionId"));
            jsonReq.put("UserId", Storage.getUserProfile(context).getJSONObject("UserProfile").getString("Id"));
			jsonReq.put("EarnTypeId", earnTypeId);
			return jsonReq;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
