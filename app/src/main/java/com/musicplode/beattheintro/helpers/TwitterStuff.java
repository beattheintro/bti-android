package com.musicplode.beattheintro.helpers;

import android.os.AsyncTask;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class TwitterStuff {
	public static String TWITTER_CONSUMER_KEY = "p5cD5WBybCLkSRJGz2tIg";
	public static String TWITTER_CONSUMER_SECRET = "PVkdyvcpM6brcBk7VYNRUAiiadDdgiUYQG5O3q03GE0";
	
	public static abstract class ScribeTwitterStuff extends AsyncTask<String, Integer, String> {
		public abstract void onResult(String s); 

		public void execute(Token accessToken) {
			execute(accessToken.getToken(), accessToken.getSecret(), accessToken.getRawResponse());
		}
		
		@Override
		public void onPostExecute(String s) {
			onResult(s);
		}
	}
	public static abstract class VerifyCredentials extends ScribeTwitterStuff {
		@Override
		protected String doInBackground(String... params) {
			Token t = new Token(params[0], params[1], params[2]);
        	
        	OAuthService service2 = new ServiceBuilder()
                .provider(TwitterApi.Authenticate.class)
                .apiKey(TwitterStuff.TWITTER_CONSUMER_KEY)
                .apiSecret(TwitterStuff.TWITTER_CONSUMER_SECRET)
                .build();
        	
			String url = "https://api.twitter.com/1.1/account/verify_credentials.json";

			OAuthRequest request2 = new OAuthRequest(Verb.GET, url);
		    service2.signRequest(t, request2);
		    Response response2 = request2.send();

		    if (response2.getCode() == 200)
		    	return response2.getBody();
	    	return null;
		}
	}
	
	public static abstract class Tweet extends ScribeTwitterStuff {
		public void execute(String tweetMsg, Token accessToken) {
			execute(tweetMsg, accessToken.getToken(), accessToken.getSecret(), accessToken.getRawResponse());
		}
		
		@Override
		protected String doInBackground(String... params) {
			Token t = new Token(params[1], params[2], params[3]);
        	
        	OAuthService service2 = new ServiceBuilder()
                .provider(TwitterApi.Authenticate.class)
                .apiKey(TwitterStuff.TWITTER_CONSUMER_KEY)
                .apiSecret(TwitterStuff.TWITTER_CONSUMER_SECRET)
                .build();
        	
			try {
				String tweet = URLEncoder.encode(params[0], "UTF-8");

				String urlTweet = "https://api.twitter.com/1.1/statuses/update.json?status=" + tweet;
                
				OAuthRequest request2 = new OAuthRequest(Verb.POST, urlTweet);
			    service2.signRequest(t, request2);
			    Response response2 = request2.send();

			    if (response2.getCode() >= 400)
			    	return null;
			    
				return response2.getBody();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

	
	public static abstract class GetFollowers extends ScribeTwitterStuff {
		@Override
		protected String doInBackground(String... params) {
			Token t = new Token(params[0], params[1], params[2]);
        	String cursor = "-1";
			if (params.length >= 4) {
				cursor = params[3];
			}
        	OAuthService service2 = new ServiceBuilder()
                .provider(TwitterApi.Authenticate.class)
                .apiKey(TwitterStuff.TWITTER_CONSUMER_KEY)
                .apiSecret(TwitterStuff.TWITTER_CONSUMER_SECRET)
                .build();
        	
			String url = "https://api.twitter.com/1.1/followers/list.json?user_id=" + params[0].split("-")[0] + "&cursor=" + cursor;
            
			OAuthRequest request2 = new OAuthRequest(Verb.GET, url);
		    service2.signRequest(t, request2);
		    Response response2 = request2.send();

		    return response2.getBody();
		}
	}
	
	
	public static abstract class FollowBti extends ScribeTwitterStuff {
		@Override
		protected String doInBackground(String... params) {
			Token t = new Token(params[0], params[1], params[2]);
        	
        	OAuthService service2 = new ServiceBuilder()
                .provider(TwitterApi.Authenticate.class)
                .apiKey(TwitterStuff.TWITTER_CONSUMER_KEY)
                .apiSecret(TwitterStuff.TWITTER_CONSUMER_SECRET)
                .build();
        	
			String url = "https://api.twitter.com/1.1/friendships/create.json?screen_name=beattheintro&follow=true";
            
			OAuthRequest request2 = new OAuthRequest(Verb.POST, url);
		    service2.signRequest(t, request2);
		    Response response2 = request2.send();

		    return response2.getBody();
		}
	}
}
