package com.musicplode.beattheintro.helpers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.musicplode.beattheintro.MainActivity;
import com.musicplode.beattheintro.R;

public class DialogHelpers {
	public interface OnRetryButton{
		void onRetry();
	}
	public interface OnCancelButton{
		void onCancel();
	}
	public interface OnOkButton{
		void onOk();
	}
	
	public static Dialog getDialog(Activity activity, int titleTextId, int button1TextId, int button2TextId, int button3TextId, int text) {
		if (activity == null)
			return null;
		
		Dialog d;
		try {
			d = new Dialog(activity, R.style.CustomDialogTheme);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		d.setContentView(R.layout.dialoglayout);
		d.setCancelable(true);
		d.setCanceledOnTouchOutside(true);
		Helpers.setAllFonts(d.findViewById(R.id.dialogRootLayout), activity);
		
		if (text != 0) {
			((TextView)d.findViewById(R.id.dialogTextView)).setText(text);
		}
		else
			d.findViewById(R.id.dialogTextView).setVisibility(View.GONE);
		
		if (titleTextId != 0)
			((TextView)d.findViewById(R.id.dialogTitleTextView)).setText(titleTextId);
		
		if (button1TextId != 0)
			((Button)d.findViewById(R.id.dialogButton1)).setText(button1TextId);
		else
			d.findViewById(R.id.dialogButton1).setVisibility(View.GONE);
		
		if (button2TextId != 0)
			((Button)d.findViewById(R.id.dialogButton2)).setText(button2TextId);
		else
			d.findViewById(R.id.dialogButton2).setVisibility(View.GONE);
		
		if (button3TextId != 0)
			((Button)d.findViewById(R.id.dialogButton3)).setText(button3TextId);
		else
			d.findViewById(R.id.dialogButton3).setVisibility(View.GONE);
		
		
		return d;
	}
	public static Dialog getDialog(Activity activity, int titleTextId, int button1TextId, int button2TextId, int button3TextId) {
		return getDialog(activity, titleTextId, button1TextId, button2TextId, button3TextId, 0);
	}
	public static Dialog getDialog(Activity activity, int titleTextId, int button1TextId, int button2TextId) {
		return getDialog(activity, titleTextId, button1TextId, button2TextId, 0, 0);
	}
	public static Dialog getDialog(Activity activity, int titleTextId, int button1TextId) {
		return getDialog(activity, titleTextId, button1TextId, 0, 0, 0);
	}
	
	public static void showDeleteDialog(FragmentActivity activity, int title, int button1, int buttonCancel, final OnOkButton onDeleteButton) {
		final Dialog d = getDialog(activity, title, button1, buttonCancel);
		d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.cancel();
				onDeleteButton.onOk();
			}
		});
		d.findViewById(R.id.dialogButton2).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.cancel();
			}
		});
		d.show();
	}
	
	
	
	
	public static void showErrorDialog(FragmentActivity activity, int errorTitle, int errorText, int errorRetryButtonText, int errorCancelButtonText, final OnRetryButton onRetry) {
		showErrorDialog(activity, errorTitle, errorText, errorRetryButtonText, errorCancelButtonText, onRetry, null);
	}
	public static void showErrorDialog(FragmentActivity activity, int errorTitle, int errorText, int errorRetryButtonText, int errorCancelButtonText, final OnRetryButton onRetry, final OnCancelButton onCancel) {
		if (activity == null)
			return;
		final Dialog d = getDialog(activity, errorTitle, errorRetryButtonText, errorCancelButtonText, 0, errorText);
		d.setCancelable(false);
		d.setCanceledOnTouchOutside(false);
		d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.hide();
				onRetry.onRetry();
				d.cancel();
			}
		});
		d.findViewById(R.id.dialogButton2).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.hide();
				if (onCancel != null)
					onCancel.onCancel();
				d.cancel();
			}
		});
		d.show();
	}
	
	public static Dialog showErrorDialog(FragmentActivity activity, int errorTitle, String errorText, int errorOkButtonText, final OnOkButton onOkButton) {
		if (activity == null)
			return null;
		
		final Dialog d = showErrorDialog(activity, errorTitle, errorTitle, errorOkButtonText, onOkButton);
		
		TextView errorTextView = (TextView) d.findViewById(R.id.dialogTextView);
		
		if (errorTextView != null && errorText != null)
			errorTextView.setText(errorText);
		
		return d;
	}
	
	
	public static Dialog showErrorDialog(FragmentActivity activity, int errorTitle, int errorText, int errorOkButtonText, final OnOkButton onOkButton) {
		if (activity == null)
			return null;
		final Dialog d = getDialog(activity, errorTitle, errorOkButtonText, 0, 0, errorText);
		
		d.setCancelable(false);
		
		d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.hide();
				d.cancel();
				onOkButton.onOk();
			}
		});
		d.show();
		return d;
	}
	
	public static Dialog showErrorDialog(FragmentActivity activity, int errorTitle, int errorText, int errorOkButtonText) {
		final Dialog d = getDialog(activity, errorTitle, errorOkButtonText, 0, 0, errorText);

		d.setCancelable(false);
		
		d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.hide();
				d.cancel();
			}
		});
		d.show();

		
		return d;
	}
	
	public static Dialog showErrorAndGoBackOnOkDialog(final FragmentActivity activity, int errorTitle, int errorText, int errorOkButtonText) {
		if (activity == null)
			return null;
		
		final Dialog d = getDialog(activity, errorTitle, errorOkButtonText, 0, 0, errorText);
		d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.cancel();
				activity.onBackPressed();
			}
		});
		d.show();
		return d;
	}
	
	public static Dialog showOkDialog(FragmentActivity activity, int okTitle, int okText, int okButtonText) {
		if (activity == null)
			return null;
		
		final Dialog d = getDialog(activity, okTitle, okButtonText, 0, 0, okText);
		d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.hide();
				d.cancel();
			}
		});
		d.show();
		return d;
	}
	
	
	public static void showRegisterDialog(final Activity activity) {
		if (activity == null)
			return;
		
		final Dialog d = getDialog(activity, R.string.DONT_FORGET_TO_REGISTER_TITLE, R.string.DONT_FORGET_TO_REGISTER_BUTTON_REGISTER, R.string.DONT_FORGET_TO_REGISTER_BUTTON_LATER);
		d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(activity, MainActivity.class);
				i.putExtra("signup", true);
				activity.startActivity(i);
				d.hide();
				d.cancel();
				activity.finish();
			}
		});
		
		d.findViewById(R.id.dialogButton2).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.hide();
				d.cancel();
			}
		});
		d.setCancelable(true);
		d.show();
	}
	public static void showErrorDialog(FragmentActivity activity, String message) {
		if (activity == null)
			return;
		
		final Dialog d = new Dialog(activity, R.style.CustomDialogTheme);
		d.setContentView(R.layout.dialoglayout);
		d.setCancelable(true);
		d.setCanceledOnTouchOutside(true);
		Helpers.setAllFonts(d.findViewById(R.id.dialogRootLayout), activity);
		
		((TextView)d.findViewById(R.id.dialogTextView)).setText(message);
		((TextView)d.findViewById(R.id.dialogTitleTextView)).setText(activity.getString(R.string.ERROR_TITLE));
		
		((Button)d.findViewById(R.id.dialogButton1)).setText(activity.getString(R.string.OK));
		
		d.findViewById(R.id.dialogButton2).setVisibility(View.GONE);
		d.findViewById(R.id.dialogButton3).setVisibility(View.GONE);
		
		d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.hide();
				d.cancel();
			}
		});
		d.show();
	}
	public static Dialog showHardCodedDialog(FragmentActivity activity, String title, String message) {
		if (activity == null)
			return null;
		
		final Dialog d = new Dialog(activity, R.style.CustomDialogTheme);
		d.setContentView(R.layout.dialoglayout);
		d.setCancelable(true);
		d.setCanceledOnTouchOutside(true);
		Helpers.setAllFonts(d.findViewById(R.id.dialogRootLayout), activity);
		
		if (message == null)
			d.findViewById(R.id.dialogTextView).setVisibility(View.GONE);
		else
			((TextView)d.findViewById(R.id.dialogTextView)).setText(message);
		
		if (title == null)
			d.findViewById(R.id.dialogTitleTextView).setVisibility(View.GONE);
		else
			((TextView)d.findViewById(R.id.dialogTitleTextView)).setText(title);
		
		((Button)d.findViewById(R.id.dialogButton1)).setText(activity.getString(R.string.OK));
		
		d.findViewById(R.id.dialogButton2).setVisibility(View.GONE);
		d.findViewById(R.id.dialogButton3).setVisibility(View.GONE);
		
		d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				d.hide();
				d.cancel();
			}
		});
		d.show();
		return d;
	}
	
	public static void showInfoDialog(Context context, int title, int text) {
		showInfoDialog(context, title, text, 0);
	}
	
	public static void showInfoDialog(Context context, int title, int text, int image) {
		if (context == null)
			return;
		
		final Dialog d = new Dialog(context, R.style.CustomInfoDialogTheme);
		d.setContentView(R.layout.infodialog);
		d.setCancelable(true);
		d.setCanceledOnTouchOutside(true);
		
		Helpers.setAllFonts(d.findViewById(R.id.dialogRootLayout), context);
		
		if (image != 0) {
			((ImageView)d.findViewById(R.id.dialogImageView)).setImageResource(image);
		}
		
		((TextView)d.findViewById(R.id.dialogTextView)).setText(context.getText(text));
		((TextView)d.findViewById(R.id.dialogTitleTextView)).setText(context.getText(title));
		
		((TextView)d.findViewById(R.id.dialogTextView)).setGravity(Gravity.CENTER);
		Helpers.changeStringToSpannable(context, (TextView)d.findViewById(R.id.dialogTextView));
		Helpers.changeStringToSpannable(context, (TextView)d.findViewById(R.id.dialogTitleTextView));
		
		d.show();
	}
	
	public static void showStandardBadResult(FragmentActivity activity, String errorMessage) {
		if (activity == null)
			return;
		
		String title = activity.getString(R.string.ERROR_TITLE);
		String message;
		
		if (errorMessage == null)
			message = activity.getString(R.string.ERROR_TITLE);
		else
			message = errorMessage;
		
		showHardCodedDialog(activity, title, message);
	}
}
