package com.musicplode.beattheintro.helpers;

import android.support.v4.app.FragmentActivity;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.musicplode.beattheintro.BTIApp;
import com.musicplode.beattheintro.storage.Storage;

public class TrackerHelpers {
	public static void successfulPackPurchase(FragmentActivity activity,
			String packId, long price) {
		if (activity == null)
			return;

		Tracker t = ((BTIApp) activity.getApplication()).getTracker();
		t.send(new HitBuilders.EventBuilder().setCategory("packs")
				.setAction("purchaseSuccess").setLabel(packId).setValue(price)
				.build());
	}

	public static void unsuccessfulPackPurchase(FragmentActivity activity,
			String packId, long price) {
		if (activity == null)
			return;
		Tracker t = ((BTIApp) activity.getApplication()).getTracker();
		t.send(new HitBuilders.EventBuilder().setCategory("packs")
				.setAction("purchaseUnsuccessful").setLabel(packId)
				.setValue(price).build());
	}

	public static void packStarted(FragmentActivity activity, String packId) {
		if (activity == null)
			return;

		Tracker t = ((BTIApp) activity.getApplication()).getTracker();
		t.send(new HitBuilders.EventBuilder().setCategory("game")
				.setAction("gameStarted").setLabel(packId).build());
	}

	public static void addTrackToBasket(FragmentActivity activity,
			String trackId) {
		if (activity == null)
			return;

		Tracker t = ((BTIApp) activity.getApplication()).getTracker();
		t.send(new HitBuilders.EventBuilder().setCategory("basket")
				.setAction("add").setLabel(trackId).build());
	}

	public static void playTrackInBasket(FragmentActivity activity,
			String trackId) {
		if (activity == null)
			return;

		Tracker t = ((BTIApp) activity.getApplication()).getTracker();
		t.send(new HitBuilders.EventBuilder()
				.setCategory("basket")
				.setAction("play")
				.setLabel(trackId).build());
	}

	public static void removeTrackFromBasket(FragmentActivity activity,
			String trackId) {
		if (activity == null)
			return;

		Tracker t = ((BTIApp) activity.getApplication()).getTracker();
		t.send(new HitBuilders.EventBuilder()
				.setCategory("basket")
				.setAction("remove")
				.setLabel(trackId).build());
	}

	public static void bannerLoaded(FragmentActivity activity) {
		if (activity == null)
			return;

		Tracker t = ((BTIApp) activity.getApplication()).getTracker();
		t.send(new HitBuilders.EventBuilder()
				.setCategory("ads")
				.setAction("loaded")
				.setLabel(Helpers.getUserId(Storage.getUserProfile(activity)))
				.build());
	}

	public static void bannerClicked(FragmentActivity activity) {
		if (activity == null)
			return;

		Tracker t = ((BTIApp) activity.getApplication()).getTracker();
		t.send(new HitBuilders.EventBuilder()
				.setCategory("ads")
				.setAction("clicked")
				.setLabel(Helpers.getUserId(Storage.getUserProfile(activity)))
				.build());
	}
}
