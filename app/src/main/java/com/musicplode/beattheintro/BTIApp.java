package com.musicplode.beattheintro;

import android.app.Application;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
// import android.media.SoundPool.Builder;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 *  Application object for BTI, handles GoogleAnalytics tracker object
 */
public class BTIApp extends Application {
    private SoundPool soundPool;
    private HashMap<Sounds.Sound, Integer> sounds = new HashMap<Sounds.Sound, Integer>();

    public BTIApp() {
        super();
    }
    private Tracker mTracker;

    public synchronized Tracker getTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }

		return mTracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        /**
         * Init the sound module
         */
        initSounds(this);
    }



    /**
     * Init the soundPool if it isn't initialised otherwise just returns
     * @param context
     */
    private synchronized void initSounds(Context context) {
        if (soundPool == null) {
            soundPool = new SoundPool(2, AudioManager.STREAM_MUSIC, 100); //TODO needs swapping out with SoundPool.builder

            if (soundPool == null)
                return;

            sounds.put(Sounds.Sound.COUNTDOWN, soundPool.load(context, R.raw.countdown, 1) );
			sounds.put(Sounds.Sound.CARDTURN, soundPool.load(context, R.raw.cardturn, 1) );
			sounds.put(Sounds.Sound.CLOSE, soundPool.load(context, R.raw.close, 1) );
            sounds.put(Sounds.Sound.CORRECT, soundPool.load(context, R.raw.correct, 1) );
			sounds.put(Sounds.Sound.CREDITSCORE, soundPool.load(context, R.raw.creditscore, 1) );
			sounds.put(Sounds.Sound.LARGECOINS, soundPool.load(context, R.raw.largecoins, 1) );
			sounds.put(Sounds.Sound.LOSE, soundPool.load(context, R.raw.lose, 1) );
			sounds.put(Sounds.Sound.OFF, soundPool.load(context, R.raw.off, 1) );
            sounds.put(Sounds.Sound.OPEN, soundPool.load(context, R.raw.open, 1) );
			sounds.put(Sounds.Sound.RIGHTANSWER, soundPool.load(context, R.raw.rightanswer, 1) );
			sounds.put(Sounds.Sound.SMALLAPPLAUSE, soundPool.load(context, R.raw.smallapplause, 1) );
			sounds.put(Sounds.Sound.SMALLCOINS, soundPool.load(context, R.raw.smallcoins, 1) );
			sounds.put(Sounds.Sound.STAR, soundPool.load(context, R.raw.star, 1) );
			sounds.put(Sounds.Sound.STOP, soundPool.load(context, R.raw.stop, 1) );
			sounds.put(Sounds.Sound.WAHOO, soundPool.load(context, R.raw.wahoo, 1) );
            sounds.put(Sounds.Sound.WRONG, soundPool.load(context, R.raw.wronganswer, 1) );
        }
    }

    private Set<Integer> soundPoolStreams = new HashSet<Integer>();

    /**
     * Plays a soundeffect
     * @param context
     * @param s The enum representing the effect that should be played
     */
    public void playSound(Context context, Sounds.Sound s) {
        initSounds(context);

        if (soundPool == null)
            return;

        float volume = 1f;
        soundPoolStreams.add(soundPool.play(sounds.get(s), volume, volume, 1, 0, 1f));
    }

    /**
     * Stops all sounds in the soundPoolStream if any
     */
    public void stopSounds() {
        for (int i : soundPoolStreams) {
            soundPool.stop(i);
        }
        soundPoolStreams.clear();
    }
    /*******************************************************************************************
     * MediaPlayer stuffs end
     ******************************************************************************************/

}
