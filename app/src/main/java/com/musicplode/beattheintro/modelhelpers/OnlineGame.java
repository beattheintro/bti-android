package com.musicplode.beattheintro.modelhelpers;

import android.content.Context;

import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.model.QuestionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OnlineGame implements IGame {
	protected JSONObject json;
	private List<GamePlayer> opponents = null;
	private GamePlayer player;
	private LevelType levelType;

	public OnlineGame(JSONObject json) {
		this.json = json;
	}
	
	@Override
	public LevelType getLevelType(Context context) {
		if (levelType != null)
			return levelType;
		
		levelType = new LevelType(context, getLevelTypeId());
		
		return levelType;
	}
	
	@Override
	public String getLevelTypeId() {
		try {
			return json.getString("LevelTypeId");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	/**
	 * Updates with a new json as the main source
	 * @param json
	 */
	@Override
	public synchronized void update(JSONObject json) {
		try {
			//this.json = json.getJSONObject("GameInfo");
            getPlayer().updateInfo(json.getJSONArray("Players").getJSONObject(0));

            for (int i = 1; i < json.getJSONArray("Players").length(); i++) {
                getOpponents().get(i-1).updateInfo(json.getJSONArray("Players").getJSONObject(i));
            }
		} catch (JSONException e) {
			e.printStackTrace();
		}
//		player = null;
//		opponents = null;
//		player = getPlayer();
//		opponents = getOpponents();
	}
	
	@Override
	public String toString() {
		String s = "id: " + getId() + " packId: " + getPackId() ;
		
		for (GamePlayer o : getOpponents()) {
			s += "|" + o.toString(); 
		}
		return s;
	}
	
	@Override
	public int getNumberOfPlayers() {
		try {
			return json.getJSONArray("Players").length();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	@Override
	public List<GamePlayer> getOpponents() {
		if (opponents != null)
			return opponents;

		//noinspection Convert2Diamond
		opponents = new ArrayList<GamePlayer>();
		
		for (int i = 1; i < getNumberOfPlayers(); i++) {
			try {
				opponents.add(new GamePlayer(json.getJSONArray("Players").getJSONObject(i)));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return opponents;
	}
	
	@Override
	public int getNumberOfQuestions() {
		try {
			return json.getInt("Questions");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	@Override
	public String getPackId() {
		try {
			return json.getString("PackId");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	@Override
	public String getId() {
		try {
			return json.getString("Id");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public GamePlayer getPlayer() {
		if (player != null)
			return player;
		
		try {
			player = new GamePlayer(json.getJSONArray("Players").getJSONObject(0));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return player;
	}

	@Override
	public boolean isStarted() {
		if (getPlayer().getNumberOfUnansweredQuestions() != getNumberOfQuestions()) {
			return true;
		}
		
		for (GamePlayer o : getOpponents()) {
			if (o.getNumberOfUnansweredQuestions() != getNumberOfQuestions())
				return true;
		}

		return false;
	}

	@Override
	public JSONObject getJSON() {
		return json;
	}

	@Override
	public String getName() {
		try {
			return json.getString("Name");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String getDescription() {
		try {
			return json.getString("Description");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String getProfileURL() {
		if (getOpponents().size() > 0)
			return getOpponents().get(0).getProfileImageURL();
		else
			return getPlayer().getProfileImageURL();
	}

	@Override
	public boolean isFinished() {
		try {
			return json.getBoolean("Finished");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}
	@Override
	public boolean didWin() {
		for (GamePlayer opponent : getOpponents()) {
			if (opponent.getScore() > getPlayer().getScore())
				return false;
		}
		
		return true;
	}
	
	@Override
	public int getLevelNumber(Context context) {
		List<String> levelIds = Helpers.getLevelIdsForPack(context, getPackId());
		
		if (levelIds == null)
			return 0;
		
		for (int i = 0; i < levelIds.size(); i++) {
			if (levelIds.get(i).contentEquals(getLevelTypeId()))
				return i;
		}
		return 0;
	}

	@Override
	public boolean isNewHighScore() {
		return getPlayer().isNewHighScore();
	}

	@Override
	public boolean isNewChartPos() {
		return getPlayer().isNewChartPos();
	}

	@Override
	public Pack getPack(Context context) {
		List<JSONObject> packs = Helpers.getAllPackJSONS(context);
		for (JSONObject o : packs) {
			Pack p = new Pack(o);
			if (p.getId().equals(getPackId())) {
				return p;
			}
		}
		
		return null;
	}

	@Override
	public QuestionModel[] getQuestions(Context context) {
		try {
			JSONArray arr = getJSON().getJSONArray("Players").getJSONObject(0).getJSONArray("Questions");
			QuestionModel qm[] = new QuestionModel[arr.length()];
			
			for (int i = 0; i < arr.length(); i++) {
				qm[i] = new QuestionModel(arr.getJSONObject(i));
			}
			
			return qm;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
}
