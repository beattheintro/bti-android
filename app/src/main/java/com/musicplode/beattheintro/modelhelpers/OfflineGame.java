package com.musicplode.beattheintro.modelhelpers;

import android.content.Context;

import com.musicplode.beattheintro.helpers.OfflineHelpers;
import com.musicplode.beattheintro.helpers.OfflineHelpers.Track;
import com.musicplode.beattheintro.model.QuestionModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OfflineGame implements IGame {
	public OfflineGame() {
	
	}
	
	@Override
	public LevelType getLevelType(Context context) {
		return new OfflineLevelType();
	}

	@Override
	public String getLevelTypeId() {
		return null;
	}

	@Override
	public void update(JSONObject json) {
		
	}

	@Override
	public int getNumberOfPlayers() {
		return 0;
	}

	@Override
	public List<GamePlayer> getOpponents() {
		//noinspection Convert2Diamond
		return new ArrayList<GamePlayer>();
	}

	@Override
	public int getNumberOfQuestions() {
		return 10;
	}

	@Override
	public String getPackId() {
		return null;
	}

	@Override
	public String getId() {
		return null;
	}

    private OfflinePlayer op = new OfflinePlayer();
	
	@Override
	public GamePlayer getPlayer() {
		return op;
	}

	@Override
	public boolean isStarted() {
		return false;
	}

	@Override
	public JSONObject getJSON() {
		return null;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public String getProfileURL() {
		return null;
	}

	@Override
	public boolean isFinished() {
		return false;
	}

	@Override
	public boolean didWin() {
		return false;
	}

	@Override
	public int getLevelNumber(Context context) {
		return 0;
	}

	@Override
	public boolean isNewHighScore() {
		return false;
	}

	@Override
	public boolean isNewChartPos() {
		return false;
	}

	@Override
	public Pack getPack(Context context) {
		return null;
	}

    private QuestionModel qm[];
	@Override
	public QuestionModel[] getQuestions(Context context) {
		if (qm == null) {
			List<Track> tracks =  OfflineHelpers.getRandomTracks(context);
			if (tracks == null || tracks.size() == 0)
				return null;
			
			qm = new QuestionModel[tracks.size()/3];
			
			
			for (int i = 0; i < tracks.size(); i+=3) {
				qm[i/3] = new QuestionModel(tracks.get(i), tracks.get(i+1), tracks.get(i+2));
			}
		}
		return qm;
	}
}