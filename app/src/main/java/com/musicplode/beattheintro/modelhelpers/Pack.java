package com.musicplode.beattheintro.modelhelpers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Pack {

	private JSONObject json;
	private List<String> levels;

	public Pack(JSONObject json) {
		this.json = json;
	}

	public boolean isPlayable() {
		try {
			return json.getBoolean("Playable");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	public JSONObject getJSON() {
		return json;
	}

	public int getPrice() {
		try {
			return json.getInt("Coins");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}

//	public int getTotalAmountOfStars() {
//		 try {
//			return json.getInt("Stars10") + json.getInt("Stars20") + json.getInt("Stars30");
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//		
//		return 0;
//	}

	public String getId() {
		try {
			return json.getString("Id");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public List<String> getLevels() {
		if (levels != null)
			return levels;
		//noinspection Convert2Diamond
		levels = new ArrayList<String>();

		try {
//			for (int i = 0; i < json.getJSONArray("LevelTypes").length(); i++) {
//				levels.add(json.getJSONArray("LevelTypes").getString(i));
//			}
			JSONArray arr = json.getJSONArray("LevelTypes");
			for (int i = 0; i < arr.length(); i++) {
				levels.add(arr.getJSONObject(i).getString("LevelTypeId"));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return levels;
	}

	public String getName() {
		try {
			return json.getString("Name");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	public int getNumberOfStars() {
		try {
			int stars = 0;
			JSONArray arr = json.getJSONArray("LevelTypes");
			for (int i = 0; i < arr.length(); i++) {
				stars += arr.getJSONObject(i).getInt("Stars");
			}
			return stars;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getStarsOnLevel(int level) {
		try {
			JSONArray arr = json.getJSONArray("LevelTypes");
			return arr.getJSONObject(level).getInt("Stars");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public String getLevelTypeId(int level) {
		try {
			return json.getJSONArray("LevelTypes").getJSONObject(level).getString("LevelTypeId");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public int getHighestScoreOnLevel(int level) {
		try {
			JSONArray arr = json.getJSONArray("LevelTypes");
			return arr.getJSONObject(level).getInt("HighScore");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int getHighestScoredLevel() {
		int maxScore = 0;
		int maxScoredLevel = 0;
		for (int i = 0; i < 3; i++) {
			if (getHighestScoreOnLevel(i) > maxScore) {
				maxScore = getHighestScoreOnLevel(i);
				maxScoredLevel = i;
			}
		}
		
		return maxScoredLevel;
	}
	
	public boolean isLevelUnlocked(int level) {
		return level == 0 || (getStarsOnLevel(level - 1) >= 2);
	}

	public String getHighestPlayableLevelId() {
		int highestPlayableLevel = -1;
		for (int i = 0; i < 3; i++){
			if (isLevelUnlocked(i))
				highestPlayableLevel = i;
		}
		if (highestPlayableLevel == -1)
			return null;
		
		try {
			return getLevels().get(highestPlayableLevel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public String getNextPlayableLevel(String levelId) {
		for (int i = 0; i < 2; i++){
			if (getLevelTypeId(i).equals(levelId) && isLevelUnlocked(i+1)) {
				return getLevels().get(i+1);
			}
		}
		
		return levelId;
	}
}
