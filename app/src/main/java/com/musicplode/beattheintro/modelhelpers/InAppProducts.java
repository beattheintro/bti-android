package com.musicplode.beattheintro.modelhelpers;

import android.util.Pair;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InAppProducts {
	private JSONObject json;

	public InAppProducts(JSONObject o) {
		json = o;
	}
	
	public Set<String> getAllDiscSKUS() {
		@SuppressWarnings("Convert2Diamond") Set<String> skus = new HashSet<String>();
		
		if (json == null)
			return skus;
		
		try {
			for (int i = 0 ; i < json.getJSONObject("Amazon").getJSONArray("discs").length(); i++) {
				skus.add(json.getJSONObject("Amazon").getJSONArray("discs").getJSONObject(i).getString("SKU"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return skus;
	}
	
	public Set<String> getAllUnlockSKUS() {
		@SuppressWarnings("Convert2Diamond") Set<String> skus = new HashSet<String>();
		
		if (json == null)
			return skus;
		
		try {

			for (int i = 0 ; i < json.getJSONObject("Amazon").getJSONArray("unlockAllPacks").length(); i++) {
				skus.add(json.getJSONObject("Amazon").getJSONArray("unlockAllPacks").getJSONObject(i).getString("SKU"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return skus;
	}
	
	public Set<String> getAllSKUS() {
		@SuppressWarnings("Convert2Diamond") Set<String> skus = new HashSet<String>();
		
		skus.addAll(getAllDiscSKUS());
		skus.addAll(getAllUnlockSKUS());
		
		return skus;
	}

	public int compare(String sku, String sku2) {
		if (sku == null || sku2 == null)
			return 0;
		int starta = json.toString().indexOf(sku);
		int startb = json.toString().indexOf(sku2);
		
		return starta - startb;
	}

	public List<Pair<String, String>> getSkuToSpendTypeIdList() {
		@SuppressWarnings("Convert2Diamond") List<Pair<String, String>> list = new ArrayList<Pair<String, String>>();
		
		if (json == null)
			return list;
		
		try {
			for (int i = 0 ; i < json.getJSONObject("Amazon").getJSONArray("unlockAllPacks").length(); i++) {
				String sku = json.getJSONObject("Amazon").getJSONArray("unlockAllPacks").getJSONObject(i).getString("SKU");
				String spendTypeId = json.getJSONObject("Amazon").getJSONArray("unlockAllPacks").getJSONObject(i).getString("SpendTypeId");

				//noinspection Convert2Diamond
				list.add(new Pair<String, String>(sku, spendTypeId));
			}
			
			for (int i = 0 ; i < json.getJSONObject("Amazon").getJSONArray("discs").length(); i++) {
				String sku = json.getJSONObject("Amazon").getJSONArray("discs").getJSONObject(i).getString("SKU");
				String spendTypeId = json.getJSONObject("Amazon").getJSONArray("discs").getJSONObject(i).getString("SpendTypeId");

				//noinspection Convert2Diamond
				list.add(new Pair<String, String>(sku, spendTypeId));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	public String getSpendTypeId(String sku) {
		List<Pair<String, String>> list = getSkuToSpendTypeIdList();
		for (Pair<String, String> e : list) {
			if (e.first.contentEquals(sku))
				return e.second;
		}
		
		return null;
	}
}
