package com.musicplode.beattheintro.modelhelpers;

import android.content.Context;

import com.musicplode.beattheintro.storage.Storage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LevelType {
	private JSONObject json;
	private boolean validType = false;
	
	public static List<LevelType> getAll(Context context) {
		@SuppressWarnings("Convert2Diamond") List<LevelType> levelTypes = new ArrayList<LevelType>();
		
		JSONObject o = Storage.getStoredJSON(context, Storage.METADATA);
		try {
			for (int i = 0; i < o.getJSONArray("LevelType").length(); i++) {
				JSONObject level = o.getJSONArray("LevelType").getJSONObject(i);
				levelTypes.add(new LevelType(level));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return levelTypes;
	}
	public LevelType() { }
	public LevelType(Context context, String levelTypeId) {
		JSONObject o = Storage.getStoredJSON(context, Storage.METADATA);
		try {
			for (int i = 0; i < o.getJSONArray("LevelType").length(); i++) {
				JSONObject level = o.getJSONArray("LevelType").getJSONObject(i);
				if (level.getString("Id").equals(levelTypeId)) {
					json = level;
					validType = true;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public LevelType(JSONObject json) {
		this.json = json;
		validType = true;
	}

	public boolean isValid() {
		return validType;
	}
	
	public int getMaxTimeToAnswer() {
		try {
			return json.getInt("MaxTimeToAnswer");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int getMaxScoreReward() {
		try {
			return json.getInt("MaxScoreReward");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}


}
