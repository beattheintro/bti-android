package com.musicplode.beattheintro.modelhelpers;

import android.content.Context;

import com.musicplode.beattheintro.model.QuestionModel;

import org.json.JSONObject;

import java.util.List;

public interface IGame {
	public LevelType getLevelType(Context context);
	public String getLevelTypeId();
	public void update(JSONObject json);
	public int getNumberOfPlayers();
	
	public List<GamePlayer> getOpponents();
	public int getNumberOfQuestions();
	public String getPackId();
	public String getId();
	public GamePlayer getPlayer();
	public boolean isStarted();

	public JSONObject getJSON();

	public String getName();

	public String getDescription();

	public String getProfileURL();

	public boolean isFinished();
	public boolean didWin();
	
	public int getLevelNumber(Context context);

	public boolean isNewHighScore();

	public boolean isNewChartPos();

	public Pack getPack(Context context);
	
	public QuestionModel[] getQuestions(Context context);
}
