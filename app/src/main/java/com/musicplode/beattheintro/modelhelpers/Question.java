package com.musicplode.beattheintro.modelhelpers;

import org.json.JSONException;
import org.json.JSONObject;

public class Question {
	private TrackModel correctTrack;
	
	private JSONObject json;

	public Question(JSONObject json) {
		this.json = json;
	}
	
	public String getId() {
		try {
			return json.getString("QuestionId");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public TrackModel getCorrectTrack() {
		if (correctTrack != null)
			return correctTrack;
	
		try {
			correctTrack = new TrackModel(json.getJSONObject("TrackCorrect"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return correctTrack;
	}
	
	public boolean hasAnswered() {
		try {
			return json.getInt("AnswerSpeedMS") != 0;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}
	
}
