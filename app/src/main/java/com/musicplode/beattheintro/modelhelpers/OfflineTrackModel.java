package com.musicplode.beattheintro.modelhelpers;

import com.musicplode.beattheintro.helpers.OfflineHelpers.Track;

public class OfflineTrackModel extends TrackModel {
    private Track track;
	public OfflineTrackModel(Track track) {
		this.track = track;
	}
	@Override
	public String getAlbumCoverURL() {
		return "";
	}
	
	@Override
	public String getArtist() {
		return track.getArtist().trim();
	}
	
	@Override
	public String getTitle() {
		return track.getTrack().trim();
	}
	
	@Override
	public String getStreamURL() {
		return track.getPath();
	}
	@Override
	public String getId() {
		return "";
	}
}
