package com.musicplode.beattheintro.modelhelpers;

import com.musicplode.beattheintro.helpers.Helpers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class GamePlayer {

	private JSONObject json;
	private List<Question> questions;
    private int discs, stars;
    private boolean finished;
    private int[] scoreAnswers;

    public GamePlayer() { }
	public GamePlayer(JSONObject json) {
		this.json = json;

        try {
            if (json.has("Stars"))
                stars = json.getInt("Stars");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
	@Override
	public String toString() {
		return getName() + " score:" + getScore() + " unanswered:" + getNumberOfUnansweredQuestions();
	}
	public int getScore() {
		try {
			return json.getInt("Score");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int getNumberOfUnansweredQuestions() {
		try {
			return json.getInt("UnansweredCount");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0; 
	}
	
	public String getName() {
		try {
			return json.getString("Name");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	public String getFacebookId() {
		try {
			return json.getString("FacebookId");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	public JSONObject getJSON() {
		return json;
	}
	public String getProfileImageURL() {
		String fbID = getFacebookId();
		if (fbID.length() > 0)
			return Helpers.getFacebookProfileImage(fbID);
		return null;
	}
	//public int getQuestionNumber() {
	//	return 0;
	//}
	
	public List<Question> getQuestions() {
		if (questions != null)
			return questions;

		//noinspection Convert2Diamond
		questions = new ArrayList<Question>();

		try {
			for (int i = 0; i < json.getJSONArray("Questions").length(); i++) {
				questions.add(new Question(json.getJSONArray("Questions").getJSONObject(i)));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return questions;
	}
	public int getRewardDiscs() {
		try {
			return json.getInt("Score");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	public String getAVGTime() {
		double avg = 10000;
		try {
			avg = json.getDouble("AvgAnswerTime");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		avg /= 1000;
		DecimalFormat df = new DecimalFormat("#0.000");
		return df.format(avg);
	}
	
	public boolean isNewHighScore() {
		try {
			return json.getBoolean("NewHighScore");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	public boolean isNewChartPos() {
		try {
			return json.getBoolean("NewChartPosition");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	public int getStars() {
		return stars;
	}
	public int getChartPosition() {
		try {
			return json.getInt("ChartPosition");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 100;
	}
	public boolean isFinished() {
		try {
			return json.getBoolean("Finished");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}
	public String getUserId() {
		try {
			return json.getString("UserId");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	public int getBonus() {
		try {
			return json.getInt("Bonus");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	public boolean didWin() {
		try {
			return json.getBoolean("Win");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

    public void updateInfo(JSONObject playerJSON) {
        try {
            this.discs = playerJSON.getInt("Discs");
            this.stars = playerJSON.getInt("Stars");
            this.finished = playerJSON.getBoolean("Finished");

            if (playerJSON.has("ScoreAnswers")) {
                this.scoreAnswers = new int[playerJSON.getJSONArray("ScoreAnswers").length()];

                for (int i = 0; i < scoreAnswers.length; i++) {
                    scoreAnswers[i] = playerJSON.getJSONArray("ScoreAnswers").getInt(i);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private int calculateScore(long timeToAnswer, int maxScore, int maxQuestions, int maxTimeToAnswer) {
            double maxScorePerQuestion = maxScore / (double) (maxQuestions + 1);
            double scorePerTimeUnit = maxScorePerQuestion / (maxTimeToAnswer / (maxQuestions + 1));
            return (int) (maxScorePerQuestion - timeToAnswer * scorePerTimeUnit);
    }

    public void setupScoreAnswersFromStartJSON(int maxScore, int maxQuestions, int maxTimeToAnswer) {
        JSONArray questions;
        try {
            questions = json.getJSONArray("Questions");
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        scoreAnswers = new int[questions.length()];

        for (int i = 0; i < questions.length(); i++) {
            try {
                if (questions.getJSONObject(i).getString("AnswerTrackId").equals(questions.getJSONObject(i).getJSONObject("TrackCorrect").getString("Id"))) {
                    System.out.println("Correct " + i);
                    scoreAnswers[i] = calculateScore(questions.getJSONObject(i).getInt("AnswerSpeedMS"), maxScore, maxQuestions, maxTimeToAnswer);
                }
                else
                    System.out.println("Incorrect " + i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public int getScoreForQuestion(int question) {
        if (scoreAnswers == null || scoreAnswers.length < question)
            return 0;
        return scoreAnswers[question];
    }
}
