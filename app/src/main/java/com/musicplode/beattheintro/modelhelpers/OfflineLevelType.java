package com.musicplode.beattheintro.modelhelpers;

public class OfflineLevelType extends LevelType {
	@Override
	public int getMaxScoreReward() {
		return 1500;
	}
	
	@Override
	public int getMaxTimeToAnswer() {
		return 150000;
	}
	
}
