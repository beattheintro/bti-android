package com.musicplode.beattheintro.modelhelpers;

import org.json.JSONException;
import org.json.JSONObject;

public class LeaderboardEntry {

	private JSONObject json;

	public LeaderboardEntry (JSONObject json) {
		this.json = json;
	}
	
	public String getUserId() {
		try {
			return json.getString("UserId");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	public int getRank() {
		try {
			return json.getInt("Rank");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public String getName() {
		try {
			return json.getString("Name");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public int getScore() {
		try {
			return json.getInt("Score");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public String getFBId() {
		return json.optString("FacebookId", "");
	}
	
}
