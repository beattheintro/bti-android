package com.musicplode.beattheintro.modelhelpers;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class UserProfile {

	private JSONObject json;
	
	public UserProfile(JSONObject o) {
		json = o;
		if (o.has("UserProfile")) {
			try {
				json = o.getJSONObject("UserProfile");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	public int getHighestScore() {
		try {
			return json.getInt("Highscores");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int getGamesWon() {
		try {
			return json.getInt("Won");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	public int getGamesLost() {
		try {
			return json.getInt("Lost");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	public int getNumberOfDiscs() {
		try {
			return json.getInt("Discs");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int getTotalPlayedGames() {
		try {
			return json.getInt("TotalPlayed");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	public int getChartPosition() {
		try {
			return json.getInt("ChartPosition");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public double getAVGTimeDouble() {
		try {
			return json.getDouble("AvgAnswerTime");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 1;
	}
	public double getMaxAVGTimeDouble() {
		try {
			return json.getDouble("MaxAvgAnswerTime");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 1;
	}
	public String getAVGTime() {
		double avg = 1234;
		try {
			avg = json.getDouble("AvgAnswerTime");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		avg /= 1000;
		DecimalFormat df = new DecimalFormat("#.00");
		return df.format(avg);
	}

	public String getFacebookId() {
		try {
			return json.getString("FacebookId");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
}
