package com.musicplode.beattheintro.modelhelpers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EarnDiscTypes {

	private JSONObject json;
	
	public EarnDiscTypes(JSONObject json) {
		this.json = json;
	}
	
	public List<EarnDiscType> getAll() {
		@SuppressWarnings("Convert2Diamond") List<EarnDiscType> list = new ArrayList<EarnDiscTypes.EarnDiscType>();
		
		try {
			JSONArray arr = json.getJSONArray("EarnDiscsTypes");
			for (int i = 0; i < arr.length(); i++) {
				String id = arr.getJSONObject(i).getString("Id");
				int discs = arr.getJSONObject(i).getInt("Discs");
				String name = arr.getJSONObject(i).getString("Name");
				list.add(new EarnDiscType(id, name, discs));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public Map<String, String> getNameToIdMap() {
		@SuppressWarnings("Convert2Diamond") Map<String, String> map = new HashMap<String, String>();
		
		try {
			JSONArray arr = json.getJSONArray("EarnDiscsTypes");
			for (int i = 0; i < arr.length(); i++) {
				String id = arr.getJSONObject(i).getString("Id");
				String name = arr.getJSONObject(i).getString("Name");

				map.put(name, id);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	
	public class EarnDiscType {
		private String id;
		private String name;

		public EarnDiscType(String id, String name, int discs) {
			this.id = id;
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
		public String getId() {
			return id;
		}
	}
}
