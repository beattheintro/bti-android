package com.musicplode.beattheintro.modelhelpers;

import android.content.Context;

import com.musicplode.beattheintro.model.QuestionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OnlinePartyPlayGame extends OnlineGame {
    private ArrayList<Question> questions;

    public OnlinePartyPlayGame(JSONObject json) {
        super(json);
    }

    @Override
    public QuestionModel[] getQuestions(Context context) {
        try {
            JSONArray arr = getJSON().getJSONArray("Questions");
            QuestionModel qm[] = new QuestionModel[arr.length()];

            for (int i = 0; i < arr.length(); i++) {
                qm[i] = new QuestionModel(arr.getJSONObject(i));
            }

            return qm;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Question> getQuestions() {
        if (questions != null)
            return questions;

        //noinspection Convert2Diamond
        questions = new ArrayList<Question>();

        try {
            for (int i = 0; i < json.getJSONArray("Questions").length(); i++) {
                questions.add(new Question(json.getJSONArray("Questions").getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return questions;
    }

}
