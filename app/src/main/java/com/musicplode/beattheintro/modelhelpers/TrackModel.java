package com.musicplode.beattheintro.modelhelpers;

import com.musicplode.beattheintro.helpers.Helpers;

import org.json.JSONException;
import org.json.JSONObject;

public class TrackModel {
	private JSONObject json;

	public TrackModel() { }
	
	public TrackModel(JSONObject json) {
		this.json = json;
	}

	public String getId() {
		try {
			return json.getString("Id");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public String getStreamURL() {
		return Helpers.getIntroMp3URL(getId());
	}

	
	public String getText() {
		try {
			return getArtist() + " - " + getTitle();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String toString() {
		return getText();
	}

	public String getAlbumCoverURL() {
		try {
			return Helpers.getAlbumArtURL(getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public String getArtist() {
		try {
			return json.getString("Artist");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	public String getTitle() {
		try {
			return json.getString("Title");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	
}
