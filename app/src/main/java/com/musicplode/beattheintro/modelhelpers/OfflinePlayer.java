package com.musicplode.beattheintro.modelhelpers;

import org.json.JSONObject;

public class OfflinePlayer extends GamePlayer {

	public OfflinePlayer(JSONObject json) {
		super(json);
	}

	public OfflinePlayer() {}
	@Override
	public String getName() {
		return "offline player";
	}
	@Override
	public int getScore() {
		return 0;
	}
	@Override
	public int getStars() {
		return 0;
	}
	@Override
	public String getFacebookId() {
		return "";
	}
}
