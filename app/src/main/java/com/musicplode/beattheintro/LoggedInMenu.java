package com.musicplode.beattheintro;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazon.ags.api.AGResponseCallback;
import com.amazon.ags.api.AGResponseHandle;
import com.amazon.ags.api.AmazonGamesCallback;
import com.amazon.ags.api.AmazonGamesClient;
import com.amazon.ags.api.AmazonGamesFeature;
import com.amazon.ags.api.AmazonGamesStatus;
import com.amazon.ags.api.achievements.AchievementsClient;
import com.amazon.ags.api.achievements.UpdateProgressResponse;
import com.amazon.ags.api.leaderboards.LeaderboardsClient;
import com.amazon.device.ads.Ad;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdListener;
import com.amazon.device.ads.AdProperties;
import com.amazon.device.ads.AdRegistration;
import com.amazon.device.ads.AdTargetingOptions;
import com.amazon.device.ads.InterstitialAd;
import com.amazon.inapp.purchasing.Item;
import com.amazon.inapp.purchasing.PurchasingManager;
import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyAd;
import com.jirbo.adcolony.AdColonyAdListener;
import com.jirbo.adcolony.AdColonyV4VCAd;
import com.jirbo.adcolony.AdColonyV4VCListener;
import com.jirbo.adcolony.AdColonyV4VCReward;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;
import com.mopub.mobileads.MoPubInterstitial.InterstitialAdListener;
import com.mopub.mobileads.MoPubView;
import com.mopub.mobileads.MoPubView.BannerAdListener;
import com.musicplode.beattheintro.AppPurchasingObserver.PurchaseData;
import com.musicplode.beattheintro.AppPurchasingObserver.PurchaseDataStorage;
import com.musicplode.beattheintro.AppPurchasingObserver.SKUData;
import com.musicplode.beattheintro.fragments.BuyMusicPacksFragment;
import com.musicplode.beattheintro.fragments.HTPFragment;
import com.musicplode.beattheintro.fragments.IUpdateableFragment;
import com.musicplode.beattheintro.fragments.LeaderboardFragment;
import com.musicplode.beattheintro.fragments.LoggedInFragment;
import com.musicplode.beattheintro.fragments.MusicBasketFragment;
import com.musicplode.beattheintro.fragments.MyChartFragment;
import com.musicplode.beattheintro.fragments.MyDiscsFragment;
import com.musicplode.beattheintro.fragments.MyGamesFragment;
import com.musicplode.beattheintro.fragments.PackSelectFragment;
import com.musicplode.beattheintro.fragments.PartyPlayResultsFragment;
import com.musicplode.beattheintro.fragments.ProfileFragment;
import com.musicplode.beattheintro.fragments.ThreeTwoOneFragment;
import com.musicplode.beattheintro.helpers.DialogHelpers;
import com.musicplode.beattheintro.helpers.DialogHelpers.OnOkButton;
import com.musicplode.beattheintro.helpers.Helpers;
import com.musicplode.beattheintro.helpers.JSONRequestHelpers;
import com.musicplode.beattheintro.helpers.TrackerHelpers;
import com.musicplode.beattheintro.modelhelpers.EarnDiscTypes;
import com.musicplode.beattheintro.modelhelpers.InAppProducts;
import com.musicplode.beattheintro.modelhelpers.UserProfile;
import com.musicplode.beattheintro.storage.Storage;
import com.musicplode.beattheintro.storage.Storage.OnNetworkResult;
import com.musicplode.beattheintro.support.Rotate3dAnimation;
import com.musicplode.beattheintro.util.IabHelper;
import com.musicplode.beattheintro.util.IabResult;
import com.musicplode.beattheintro.util.Inventory;
import com.musicplode.beattheintro.util.Purchase;
import com.musicplode.beattheintro.util.SkuDetails;
import com.trialpay.android.base.TrialpayManager;

// import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Activity that handles the LoggedInState of the app.
 */
public class LoggedInMenu extends FragmentActivity implements AppPurchasingObserverListener, AdColonyAdListener, AdColonyV4VCListener, InterstitialAdListener  {
	private MoPubView moPubView;
	public static final String LOGGED_IN_MENU_STACK_KEY = "LOGED_IN_MENU_STACK_KEY";
	private PurchaseDataStorage purchaseDataStorage;
	private TrialpayManager trialpayManager;
	private Map<String, Item> itemData;
    private Map<String, JSONObject> googleItemData;
	private boolean shouldBlockBack;
	private String backstackKey;
	private boolean isTablet;
	private boolean skip = false;
	private final static String AMAZON_APP_KEY = "d490be1c13ce438f9412064c4f202a21";

    private int pbWidth = 0;
    private int pickWidth = 0;

    private String startGameOpponent, startGamePackId, startGameLevelId;
    private String startGamePlayerNames[];
    private boolean clearStackBeforeNext;
    private boolean isLoggedIn = true;

    private EnumSet<AmazonGamesFeature> myGameFeatures = EnumSet.of(AmazonGamesFeature.Leaderboards, AmazonGamesFeature.Achievements);


    /*******************************************************************************************
	 * MediaPlayer stuffs start
	 ******************************************************************************************/
	private MediaPlayer mediaPlayers[] = new MediaPlayer[1];
	private int currMediaPlayer = 0;
	private Map<String, Integer> readyToPlaySongs = new HashMap<String, Integer>();
	private Map<String, OnSongReadyListener> reportToWhenDone = new HashMap<String, OnSongReadyListener>();
    private IabHelper iabHelper;
    private AudioManager audio;

    public void stopPreparingSongs() {
		if (mediaPlayers == null)
			return;

		for (MediaPlayer m : mediaPlayers) {
			if (m != null)
				m.reset();
		}
	}

    /**
     * Prepares a given mp3 from some songPath
     * @param songPath
     * @param errorListener
     * @param firstTrackInAGame
     * @return
     */
	public boolean prepareSong(final String songPath, OnErrorListener errorListener, boolean firstTrackInAGame) {
		if (firstTrackInAGame) {
			return prepareSong(songPath, new OnErrorListener() {
				@Override
				public boolean onError(MediaPlayer mp, int what, int extra) {
					System.out.println("mp error " + what + " extra " + extra);
					if (LoggedInMenu.this != null) {
						DialogHelpers.showErrorDialog(LoggedInMenu.this, R.string.ERROR_LOADING_SONG_TITLE, R.string.ERROR_LOADING_SONG_TEXT, R.string.OK, new OnOkButton() {
							@Override
							public void onOk() {
								if (LoggedInMenu.this != null) {
									LoggedInMenu.this.setShouldBlockBackButton(false);
									LoggedInMenu.this.onBackPressed();
								}
							}
						});
					}

					return false;
				}
			});
		}
		else
			return prepareSong(songPath, errorListener);
	}

	public boolean prepareSong(final String songPath, OnErrorListener errorListener) {
		currMediaPlayer = (currMediaPlayer+1)%mediaPlayers.length;

		if (mediaPlayers[currMediaPlayer] == null)
			mediaPlayers[currMediaPlayer] = new MediaPlayer();

		MediaPlayer mp = mediaPlayers[currMediaPlayer];

		mediaPlayers[currMediaPlayer].setOnPreparedListener(new OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer arg0) {
				lock.lock();
				if (reportToWhenDone.containsKey(songPath)) {
					reportToWhenDone.get(songPath).onReady(arg0);
					reportToWhenDone.remove(songPath);
				}
				else
					readyToPlaySongs.put(songPath, currMediaPlayer);
				lock.unlock();
			}
		});

		mp.reset();
		try {
			mp.setDataSource(songPath);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mp.setLooping(false);

		mp.setOnErrorListener(errorListener);

		mp.prepareAsync();

		return true;
	}

	public interface OnSongReadyListener {
        /**
         * Will be called when a song is ready to be played
         * @param mediaplayer
         */
		void onReady(MediaPlayer mediaplayer);
	}

	public interface OnSongCompletion {
		void onSongComplete(MediaPlayer mediaplayer, String songPath);
	}
    private ReentrantLock lock = new ReentrantLock();
	public boolean playIfReady(final String songPath, OnSongReadyListener listener, int seekTo) {
		lock.lock();
		if (readyToPlaySongs.containsKey(songPath)) {
			if (seekTo > 0)
				mediaPlayers[readyToPlaySongs.get(songPath)].seekTo(seekTo);
			mediaPlayers[readyToPlaySongs.get(songPath)].start();
			readyToPlaySongs.remove(songPath);

			lock.unlock();
			return true;
		}
		else {
			reportToWhenDone.put(songPath, listener);

			lock.unlock();
			return false;
		}
	}
	public boolean playIfReady(final String songPath, OnSongReadyListener listener, final OnSongCompletion onSongComplete, int seekTo) {
		lock.lock();

		if (readyToPlaySongs.containsKey(songPath)) {
			if (seekTo > 0)
				mediaPlayers[readyToPlaySongs.get(songPath)].seekTo(seekTo);

			mediaPlayers[readyToPlaySongs.get(songPath)].start();
			mediaPlayers[readyToPlaySongs.get(songPath)].setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.setOnCompletionListener(null);
					onSongComplete.onSongComplete(mp, songPath);
				}
			});
			readyToPlaySongs.remove(songPath);

			lock.unlock();

			return true;
		}
		else {
			reportToWhenDone.put(songPath, listener);

			lock.unlock();
			return false;
		}
	}

    /**
     * Stops all the mediaplayers
     */
	public void stopMediaPlayers() {
		for (MediaPlayer mp : mediaPlayers) {
			if (mp != null && mp.isPlaying()) {
				mp.stop();
				mp.reset();
			}
			else if (mp != null) {
				try {
					mp.reset();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

    /*******************************************************************************************
     * MediaPlayer stuffs stop
     ******************************************************************************************/




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//noinspection StatementWithEmptyBody
		if (iabHelper != null && iabHelper.handleActivityResult(requestCode, resultCode, data)) {
            // Handled by google InApp billing
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == Helpers.PARTY_PLAY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                setClearStackBeforeNext();
                clearBackStackIfNeeded();

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout1, PartyPlayResultsFragment.newInstance(data.getExtras()));
                ft.addToBackStack(null);
                ft.commit();
            }
        }
    }

    private Dialog currentPushDialog;
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (offlineMode)
			return;
		Helpers.refreshProfile(LoggedInMenu.this);

		Set<String> skus = Storage.getAllSKUS(this);
		if (!skus.isEmpty()) {
			PurchasingManager.initiateGetUserIdRequest();
			PurchasingManager.initiateItemDataRequest(Storage.getAllSKUS(this));

            if (!isAmazon())
                googleIAPSetup();
		}

		if (Storage.getUserProfile(this) == null || Storage.getUserProfile(this).optJSONObject("UserProfile") == null) {
			System.out.println("UserProfile == null -> reset && restart");
			Storage.clearUserData(this);
			
			Intent i = new Intent(this, MainActivity.class);
			startActivity(i);
			finish();
			return;
		}
		
		isLoggedIn = !Storage.isAnonuser(this);
		
		AdColony.resume(this);
		
//		AmazonGamesClient.initialize(this, callback, myGameFeatures);
	}
	
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {
            	String message = "" + b.getString("msg");
            	
            	if (currentPushDialog != null && currentPushDialog.isShowing())  {
            		currentPushDialog.hide();
            		currentPushDialog.cancel();
            	}
            	
            	currentPushDialog = DialogHelpers.showHardCodedDialog(LoggedInMenu.this, message, "");
            }
            Helpers.refreshProfile(LoggedInMenu.this);
        }
    };
	private ListView mDrawerList;
	private ProgressBar pb;
	private ProgressBar pbRound;
	private RelativeLayout pickLayout;
//	private TextView tv2;
	private TextView tv;
	private AdColonyV4VCAd v4vc_ad;
	private MoPubInterstitial interstitial;
	private InterstitialAd interstitialAmazon;
	
	private AdLayout amazonAdView;

	private boolean offlineMode = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (getIntent() != null && getIntent().getExtras() != null) {
			offlineMode = getIntent().getExtras().getBoolean("offlineMode", false);
		}
		
		// Setup listener that listens to messages from pushmessage services
		registerReceiver(mHandleMessageReceiver, new IntentFilter("com.musicplode.beattheintro.DISPLAY_PUSH"));
		
		isTablet = getResources().getBoolean(R.bool.tablet);

		setContentView(R.layout.headerandfooter);

	    if (offlineMode) {
	    	DrawerLayout dl = (DrawerLayout) findViewById(R.id.drawerLayout);
	    	if (dl != null)
	    		dl.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
	    }

		/**
		 * If app is reinstantiated dont readd the fragments
		 */
		if (savedInstanceState == null) {
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			Helpers.setFragmentAnimation(ft);
			Fragment f;
			if (!offlineMode) {
				 f = LoggedInFragment.newInstance();
			}
			else {
				try {
					// Give soundpool a chance to load its sounds
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				f = ThreeTwoOneFragment.newInstance(new JSONObject(), true, false, null);
			}
			ft.add(R.id.frameLayout1, f);
			ft.commit();
		}

		Helpers.setAllFonts(findViewById(R.id.headerAndFooterRelativeLayout), this);

		moPubView = (MoPubView) findViewById(R.id.adMopubview);

    	/**
    	 * Start the spin animations on topbar buttons

		Animation rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_rotation_wait_three_forever);
		findViewById(R.id.headerLocksButton).setAnimation(rotation);
		findViewById(R.id.headerDiscsButtonRing).setAnimation(rotation);
		findViewById(R.id.headerHowToButton).setAnimation(rotation);
		findViewById(R.id.headerBasketButtonRing).setAnimation(rotation);
		 */

		/**
		 * Start the small pulse animations on topbar buttons
		 */
		Animation scale = AnimationUtils.loadAnimation(this, R.anim.pulse);
		findViewById(R.id.headerLocksButton).setAnimation(scale);
		findViewById(R.id.headerDiscsButtonRing).setAnimation(scale);
		findViewById(R.id.headerHowToButton).setAnimation(scale);
		findViewById(R.id.headerBasketButtonRing).setAnimation(scale);

		/**
         * Add 3d spin effect on the disc in to top right corner
         */
		findViewById(R.id.imageView13).addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
			@Override
			public void onLayoutChange(View v, int left, int top, int right,
					int bottom, int oldLeft, int oldTop, int oldRight,
					int oldBottom) {
				if (v.getAnimation() == null) {
					v.removeOnLayoutChangeListener(this);
					
					final int width = right - left;
					
					final int height = bottom - top;
					
					final Rotate3dAnimation rotation = new Rotate3dAnimation(0,
							360, width / 2, height / 2, 0f, false);
					rotation.setDuration(2500);
					rotation.setInterpolator(new LinearInterpolator());
					rotation.setRepeatCount(Animation.INFINITE);
					v.setAnimation(rotation);
				}
			}
		});


		/**
		 * Dont set up buttons etc if it is an offline game
		 */
	    if (offlineMode)
	    	return;
	    
	    
		/**
		 * Basket button -> start MusicBasketFragmenet
		 */
		findViewById(R.id.headerBasketButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				clearBackStackIfNeeded();
				closeDrawerIfOpen();
				
				if (isLoggedIn) {
					FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					getSupportFragmentManager().popBackStack(LOGGED_IN_MENU_STACK_KEY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					Fragment f = new MusicBasketFragment();
					ft.replace(R.id.frameLayout1, f);
					ft.addToBackStack(LOGGED_IN_MENU_STACK_KEY);
					ft.commit();
				}
				else
					DialogHelpers.showRegisterDialog(LoggedInMenu.this);
			}
		});

		
		/**
		 * Howto button
		 */
		findViewById(R.id.headerHowToButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				clearBackStackIfNeeded();
				
				DrawerLayout dl = (DrawerLayout) findViewById(R.id.drawerLayout);
				if (dl != null) {
					if (dl.isDrawerOpen(Gravity.LEFT))
						dl.closeDrawer(Gravity.LEFT);
					else
						dl.openDrawer(Gravity.LEFT);
				}
			}
		});
		
		/**
		 * Locks button
		 */
		findViewById(R.id.headerLocksButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				clearBackStackIfNeeded();
				closeDrawerIfOpen();
				if (isLoggedIn) {
					FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					getSupportFragmentManager().popBackStack(LOGGED_IN_MENU_STACK_KEY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					Fragment f = new BuyMusicPacksFragment();
					ft.replace(R.id.frameLayout1, f);
					ft.addToBackStack(LOGGED_IN_MENU_STACK_KEY);
					ft.commit();
				}
				else
					DialogHelpers.showRegisterDialog(LoggedInMenu.this);
			}
		});
		
		/**
		 * Disc button
		 */
		findViewById(R.id.headerCoinBackground).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				clearBackStackIfNeeded();
				closeDrawerIfOpen();
				if (isLoggedIn) {
					FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					getSupportFragmentManager().popBackStack(LOGGED_IN_MENU_STACK_KEY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					ft.replace(R.id.frameLayout1, new MyDiscsFragment());
					ft.addToBackStack(LOGGED_IN_MENU_STACK_KEY);
					ft.commit();
				}
				else
					DialogHelpers.showRegisterDialog(LoggedInMenu.this);
			}
		});

		
		/**
		 * LowerChart
		 */
		findViewById(R.id.progressLayout).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				clearBackStackIfNeeded();
				closeDrawerIfOpen();
				if (!shouldBlockBack)  {
					FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
					Helpers.setFragmentAnimation(ft);
					getSupportFragmentManager().popBackStack(LOGGED_IN_MENU_STACK_KEY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					Fragment f = new MyChartFragment();
					ft.replace(R.id.frameLayout1, f);
					ft.addToBackStack(LOGGED_IN_MENU_STACK_KEY);
					ft.commit();
				}
			}
		});
		
		
	    /**
	     * Amazon inapp setup
	     */
	    purchaseDataStorage = new PurchaseDataStorage(this);
		AppPurchasingObserver purchasingObserver = new AppPurchasingObserver(this, purchaseDataStorage);
    	purchasingObserver.setListener(this);
    	PurchasingManager.registerObserver(purchasingObserver);
		
    	
		/**
		 * Mopub interstitial setup
		 */
		interstitial = new MoPubInterstitial(this, getString(R.string.mopubInterstitialAdUnitId));
		interstitial.setInterstitialAdListener(this);
		interstitial.load();
		
		/**
		 * Trialpay setup
		 */
		trialpayManager = TrialpayManager.getInstance(this);
		try {
			JSONObject userProfile = Storage.getUserProfile(this);
			
			if (userProfile != null && userProfile.optJSONObject("UserProfile") != null) {
				trialpayManager.setSid(userProfile.getJSONObject("UserProfile").getString("Id"));
				
				// Register the Offer Wall touchpoint
				trialpayManager.registerVic(getString(R.string.trialPayTouchpointName), getString(R.string.trialPayVIC));
				
				// Initiate Balance API checks. Not needed if you accept callbacks
				// to your server
				trialpayManager.initiateBalanceChecks();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		// Get responses from TrialPay's balance check
		trialpayManager.addEventListener(new TrialpayManager.EventListener() {
			@Override
			public void onBalanceUpdate(String touchpointName) {
			}
			@Override
			public void onClose(String touchpointName) {
			}
		});
		
		
		/**
		 * Logobutton - Clears the backstack if possible
		 */
		findViewById(R.id.imageView2).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				clearBackStackIfNeeded();
				closeDrawerIfOpen();
				updateUI();

				int curr = getSupportFragmentManager().getBackStackEntryCount();
				int prev = curr + 1;
				while (curr != 0 && curr != prev) {
					onBackPressed();
					prev = curr;
					curr = getSupportFragmentManager().getBackStackEntryCount();
				}
			}
		});
		

		// Drawer
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        addItemsToDrawer(mDrawerList);

		final DrawerLayout dl = (DrawerLayout) findViewById(R.id.drawerLayout);
		dl.setDrawerListener(new DrawerListener() {
			@Override
			public void onDrawerStateChanged(int arg0) {
				/**
				 * Creates a selected effect on drawer topbar item
				 */
				ImageView iv = (ImageView) findViewById(R.id.headerHowToButton);
				if (dl.isDrawerVisible(Gravity.START)) {
					iv.setImageResource(R.drawable.pink_btn);
				}
				else {
					iv.setImageResource(R.drawable.pink_btn_dark);
				}
			}
			public void onDrawerSlide(View arg0, float arg1) { }
			public void onDrawerOpened(View arg0) { }
			public void onDrawerClosed(View arg0) { }
		});
		
		
		/**
		 * Lower banner setup
		 */
		if (isAmazon() && !isAmazonPhone()) {
			AdColony.configure(this, "version:2,store:amazon", getString(R.string.AdColonyAppIdAmazon), getString(R.string.AdColonyValueExchangeZoneIdAmazon));
			amazonStuff();
		}
        else if (isAmazon() && isAmazonPhone()) {
            AdColony.configure(this, "version:2,store:amazon", getString(R.string.AdColonyAppIdAmazon), getString(R.string.AdColonyValueExchangeZoneIdAmazon));
            googleAndroidStuff();
        }
		else {
			AdColony.configure(this, "version:2,store:google", getString(R.string.AdColonyAppId), getString(R.string.AdColonyValueExchangeZoneId));
			googleAndroidStuff();
		}
	}

    /**
     * Setup drawer menu
     * @param mDrawerList
     */
    private void addItemsToDrawer(ListView mDrawerList) {

        PackageInfo pInfo;
        String version = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionCode + " / " + pInfo.versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }

        if (mDrawerList != null) {
            DrawerAdapter ta = new DrawerAdapter(this, 0);

            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_my_profile, getString(R.string.SLIDING_DRAWER_MYPROFILE), DrawerItemFragments.MYPROFILE, R.color.drawercellBgColor));
            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_home, getString(R.string.SLIDING_DRAWER_HOME), DrawerItemFragments.HOME, R.color.drawercellBgColor));
            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_my_chart, getString(R.string.SLIDING_DRAWER_MYCHART), DrawerItemFragments.MYCHART, R.color.drawercellBgColor));
            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_howtoplay, getString(R.string.SLIDING_DRAWER_HOWTOPLAY), DrawerItemFragments.HOWTOPLAY, R.color.drawercellBgColor));
            ta.add(null);
            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_play, getString(R.string.SLIDING_DRAWER_PLAY), DrawerItemFragments.PLAY, R.color.drawercellBgColor));
            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_multiplayer, getString(R.string.SLIDING_DRAWER_MULTIPLAYER), DrawerItemFragments.MULTIPLAYER, R.color.drawercellBgColor));
            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_my_games, getString(R.string.SLIDING_DRAWER_MYGAMES), DrawerItemFragments.MYGAMES, R.color.drawercellBgColor));
            ta.add(null);
         // ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_my_profile, getString(R.string.SLIDING_DRAWER_MYPROFILE), DrawerItemFragments.MYPROFILE, R.color.drawercellBgColor)); //removed duplicate
            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_my_discs, getString(R.string.SLIDING_DRAWER_MYDISCS), DrawerItemFragments.MYDISCS, R.color.drawercellBgColor));
            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_leaderboards, getString(R.string.SLIDING_DRAWER_LEADERBOARD), DrawerItemFragments.LEADERBOARD, R.color.drawercellBgColor));
            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_my_music_basket, getString(R.string.SLIDING_DRAWER_MYMUSICBASKET), DrawerItemFragments.MYMUSICBASKET, R.color.drawercellBgColor));
            ta.add(null);
            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_tutorial, getString(R.string.SLIDING_DRAWER_RATEAPP), DrawerItemFragments.RATEAPP, R.color.drawercellBgColor, ""));
            ta.add(new DrawerItem(R.drawable.navigation_drawer_icon_question_mark, "Ver: " + version, DrawerItemFragments.APPVERSION, R.color.drawercellBgColor));

            mDrawerList.setAdapter(ta);
            mDrawerList.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    clearBackStackIfNeeded();
                    DrawerItem item = ((DrawerAdapter)arg0.getAdapter()).getItem(arg2);

                    if (item == null )
                        return;

                    if (item.getFragmentType() == DrawerItemFragments.PARTYPLAY) {
                        startActivity(new Intent(LoggedInMenu.this, PartyPlayActivity.class));
                        return;
                    }

					DrawerLayout dl = (DrawerLayout) findViewById(R.id.drawerLayout);
					if (dl != null) {
						if (dl.isDrawerOpen(Gravity.LEFT))
							dl.closeDrawer(Gravity.LEFT);
					}

                    Fragment f = item.getFragment();
                    if (f != null) {
                        int curr = getSupportFragmentManager().getBackStackEntryCount();
                        int prev = curr + 1;
                        while (curr != 0 && curr != prev) {
                            onBackPressed();
                            prev = curr;
                            curr = getSupportFragmentManager().getBackStackEntryCount();
                        }

                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
						Helpers.setFragmentAnimation(ft); // There was no animation. This works fine-ish, except it clears the "home" LoggedInFragment menu clumsily without calling it's proper exit animation
						ft.replace(R.id.frameLayout1, f);
                        ft.addToBackStack(null);
                        ft.commit();

                    }
                    else {
                        if (item.getToggleKey() != null) {
                            item.toggle();
                            ((DrawerAdapter)arg0.getAdapter()).notifyDataSetChanged();
                        }
                    }
                }
            });
        }
    }

    /**
     * Helper method to start the amazon leaderboard interface
     */
    public void showAmazonLeaderboards() {
		LeaderboardsClient lbClient = null;
		if (agsClient != null)
			lbClient = agsClient.getLeaderboardsClient();
		if (lbClient != null)
			lbClient.showLeaderboardsOverlay();
	}
	
	/**
	 * @return Returns true if the rate dialog was shown
	 */
	public boolean startRateOfApp() {
		Boolean dontShowRate = Storage.getStoredBoolean(this, Storage.DONT_SHOW_RATE);

		if (dontShowRate == null ||!dontShowRate ) {
			final Dialog d = DialogHelpers.getDialog(this, R.string.RATE_APP_TITLE, R.string.RATE_APP_BUTTON_RATE_TEXT, R.string.RATE_APP_BUTTON_REMIND_LATER, R.string.RATE_APP_BUTTON_NO_THANKS, R.string.RATE_APP_TEXT);
			d.findViewById(R.id.dialogButton1).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (isAmazon())
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=com.musicplode.beattheintro")));
					else
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.musicplode.beattheintro")));
					
					Storage.store(LoggedInMenu.this, Storage.DONT_SHOW_RATE, true);
					d.cancel();
				}
			});
			d.findViewById(R.id.dialogButton2).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					d.cancel();
				}
			});
			d.findViewById(R.id.dialogButton3).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Storage.store(LoggedInMenu.this, Storage.DONT_SHOW_RATE, true);
					d.cancel();
				}
			});
			d.show();
            return true;
		}
		return false;
	}

    /**
     * Google android specific stuff
     */
	private void googleAndroidStuff() {
		/**
		 * Configure mopub ads
		 */
		moPubView.setVisibility(View.VISIBLE);
	    moPubView.setAdUnitId(getString(R.string.mopubAdUnitId));
	    moPubView.setBannerAdListener(new BannerAdListener() {
			@Override
			public void onBannerLoaded(MoPubView arg0) {
				TrackerHelpers.bannerLoaded(LoggedInMenu.this);
			}
			public void onBannerFailed(MoPubView arg0, MoPubErrorCode arg1) { }
			public void onBannerExpanded(MoPubView arg0) { }
			public void onBannerCollapsed(MoPubView arg0) { }
			public void onBannerClicked(MoPubView arg0) {
				TrackerHelpers.bannerClicked(LoggedInMenu.this);
			}
		});
		moPubView.loadAd();
		
		
		View v = findViewById(R.id.adAmazonadview);
		if (v != null)
			v.setVisibility(View.GONE);
		
		JSONObject pushRegQuery = JSONRequestHelpers.getPushJSON(Storage.getUserProfile(this), Storage.getStoredString(this, Storage.GCMREGID), Helpers.getGCMPushTypeId(this));
		Storage.addRequestToQueue(this, Storage.PUSH_URL, pushRegQuery, null);
	}

    private void googleIAPSetup() {
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAn3nODzklV61KXsxQe5NgIKibo7H8RUTmp48zDvENeyCnmXBxvUouQpewy5c6XPvmECV+9MrUrD65fYNruX7rHHIcd/Q9MYaEvnnAlvybbtUH4hJ9tCuDEQiyLrHe3lZ6HpT+asVYxttd7H/tlpbM9EUEE/WTCWVSP0fwR7GPG0VhAjqU2a0hUTOhgv9C2Yi5wZ6iDz/4WuvcjseGqeDvdzIvHsSom8niYH8yf0DwYbwjVdJLeq+rg6cdDuVqb4GyAhGv0E9ECrbroVpgLiGAdHSpD/HDbCqF0fuHYeU/vhXKhQo5f2denINcEbgkfDy7I6EzAeunOxMc4tAca7B4wwIDAQAB";

        if (iabHelper == null || iabHelper.isDisposed()) {
            iabHelper = new IabHelper(this, base64EncodedPublicKey);
            iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                public void onIabSetupFinished(IabResult result) {
                    if (!result.isSuccess()) {
                        // Oh noes, there was a problem.
                        return;
                    }

                    // Have we been disposed of in the meantime? If so, quit.
                    if (iabHelper == null) return;

                    @SuppressWarnings("Convert2Diamond") List<String> additionalSkuList = new ArrayList<String>();

                    for (String sku : Storage.getAllSKUS(LoggedInMenu.this)) {
                        additionalSkuList.add(sku);
                    }
                    iabHelper.queryInventoryAsync(true, additionalSkuList, mGotInventoryListener);
                }
            });
        }
    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (iabHelper == null)
                return;

            if (result.isFailure())
                return;

			//noinspection Convert2Diamond
			googleItemData = new HashMap<String, JSONObject>();
            for (String sku : Storage.getAllSKUS(LoggedInMenu.this)) {

                SkuDetails prod = inventory.getSkuDetails(sku);

                if (prod != null) {
                    try {
                        googleItemData.put(sku, prod.getJSON());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                final Purchase purchase = inventory.getPurchase(sku);
                if (purchase != null) {
                    // 0 (purchased), 1 (canceled), or 2 (refunded).
                    if (purchase.getPurchaseState() == 0) {
                        InAppProducts iap = new InAppProducts(Storage.getInAppProducts(LoggedInMenu.this));
                        String spendTypeId = iap.getSpendTypeId(sku);

                        JSONObject o = JSONRequestHelpers.getSpendRequest(Storage.getUserProfile(LoggedInMenu.this), spendTypeId, purchase.getToken());
                        Storage.addRequestToQueue(LoggedInMenu.this, Storage.INAPPPURCHASE_URL, o, new OnNetworkResult() {
                            @Override
                            public void okResult(JSONObject response) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        DialogHelpers.showOkDialog(LoggedInMenu.this, R.string.INAPP_PURCHASE_SUCCESS_TITLE, R.string.INAPP_PURCHASE_SUCCESS_TEXT, R.string.OK);
                                    }
                                });

                                try {
                                    iabHelper.consumeAsync(purchase, null);
                                } catch(Exception e) {
                                    e.printStackTrace();
                                }
                                Storage.updateUserProfile(LoggedInMenu.this, response, true);
                            }

                            @Override
                            public void badResult(String errorMessage) {

                            }
                        });
                    }
                }
            }
        }
    };


	private void amazonStuff() {
		AdRegistration.setAppKey(AMAZON_APP_KEY);

        if (BuildConfig.DEBUG)
            AdRegistration.enableTesting(true);

        Helpers.setVisibilityIfExists(moPubView, View.GONE);

		amazonAdView = (AdLayout) findViewById(R.id.adAmazonadview);
		
		if (amazonAdView != null) {
			amazonAdView.loadAd(new AdTargetingOptions());
			amazonAdView.setVisibility(View.VISIBLE);
		}
		
		JSONObject pushRegQuery = JSONRequestHelpers.getPushJSON(Storage.getUserProfile(this), Storage.getStoredString(this, Storage.ADMREGID), Helpers.getADMPushTypeId(this));
		if (pushRegQuery != null) {
			Storage.addRequestToQueue(this, Storage.PUSH_URL, pushRegQuery, null);
		}
		
		if (isAmazon()) {
			interstitialAmazon = new InterstitialAd(this);
			interstitialAmazon.loadAd();
			interstitialAmazon.setListener(new AdListener() {
				@Override
				public void onAdLoaded(Ad arg0, AdProperties arg1) {
				}
				
				@Override
				public void onAdFailedToLoad(Ad arg0, AdError arg1) {
				}
				
				@Override
				public void onAdExpanded(Ad arg0) {
				}
				
				@Override
				public void onAdDismissed(Ad arg0) {
					afterInterstitial();
					interstitialAmazon.loadAd();
				}
				
				@Override
				public void onAdCollapsed(Ad arg0) {
				}
			});
		}
	}

	//reference to the agsClient
    private AmazonGamesClient agsClient;

    private AmazonGamesCallback callback = new AmazonGamesCallback() {
	        @Override
	        public void onServiceNotReady(AmazonGamesStatus status) {
	            //unable to use service
	        }
	        @Override
	        public void onServiceReady(AmazonGamesClient amazonGamesClient) {
	            agsClient = amazonGamesClient;
	            //ready to use GameCircle
	        }
	};

	public void showAdColony() {
		if (isAmazon()) {
			v4vc_ad = new AdColonyV4VCAd(getString(R.string.AdColonyValueExchangeZoneIdAmazon)).withListener(this);
		}
		else {
			v4vc_ad = new AdColonyV4VCAd(getString(R.string.AdColonyValueExchangeZoneId)).withListener(this);
		}

		AdColony.addV4VCListener(this);
		// Debug pop-up showing the number of plays today and
		// the playcap.
		v4vc_ad.show();
	}
	@Override
	public void onAdColonyAdAttemptFinished(AdColonyAd arg0) { }
	@Override
	public void onAdColonyAdStarted(AdColonyAd arg0) { }
	
	@Override
	public void onAdColonyV4VCReward(AdColonyV4VCReward reward) {
		if (reward.success()) {
			EarnDiscTypes earn = Storage.getEarnDiscTypes(LoggedInMenu.this);
			String id = earn.getNameToIdMap().get("AdColony");
			
			JSONObject jsonReq = JSONRequestHelpers.getFacebookShareJSON(LoggedInMenu.this, id);
			Storage.addRequestToQueue(LoggedInMenu.this, Storage.EARNDISC_URL, jsonReq, new OnNetworkResult() {
				@Override
				public void okResult(JSONObject response) {
					Helpers.refreshProfile(LoggedInMenu.this);
				}
				@Override
				public void badResult(String errorMessage) {
				}
			});
		}
	}
	
	public boolean isAmazon() {
		String s = android.os.Build.MANUFACTURER ;
		return (s != null && s.contentEquals("Amazon"));
	}

    public boolean isAmazonPhone() {
        String m = Build.MODEL;
        return (m != null && m.contentEquals("SD4930UR")); // Amazon fire phone
    }


    /**
     * Progress bar helper
     * @param from
     * @param to
     * @param max
     */
	public void increase(int from, int to, int max) {
		if (from == to && to != 0)
			return;
		to *= pb.getMax() / max;
		
		ValueAnimator va = ValueAnimator.ofInt(from, to);
	    va.setDuration(2000);
	    va.setInterpolator(new DecelerateInterpolator(2f));
	    va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
	        @Override
			public void onAnimationUpdate(ValueAnimator animation) {
	            Integer newProgress = (Integer) animation.getAnimatedValue(); // strangely, getAnimatedValue returns 0 sometimes - but it shouldn't

                pb.setProgress(newProgress);
	            pbRound.setProgress(newProgress);
	    		
	    		pbWidth = Math.max(pb.getWidth(), pbWidth);
	    		pickWidth = Math.max(pickLayout.getWidth(), pickWidth);
	    		
	    		int xPos = 0;
	    		if (newProgress != 0) {
	    			xPos = (int) (newProgress / (double)pb.getMax()*pbWidth);
	    		}
	    		
	    		xPos += pb.getX();
	    		tv.setText("" + Helpers.getChartPositionFromDiscs(newProgress));
	    		pickLayout.setX(xPos - pickWidth/2f);
	        }
	    });
	    va.start();
	}
	
	private enum DrawerItemFragments {
		MYPROFILE, HOME, MYCHART, HOWTOPLAY, PLAY, MULTIPLAYER, PARTYPLAY, MYGAMES, MYDISCS, LEADERBOARD, MYMUSICBASKET, TUTORIALTOGGLE, RATEAPP, APPVERSION, STRESSTEST
	}
	public class DrawerItem {
		private int icon;
		private String title;
		private DrawerItemFragments fragment;
		private int bgColor;
		private String toggleKey;
		
		public int getBGColor() {
			return bgColor;
		}
		
		public DrawerItem(int icon, String title, DrawerItemFragments fragment, int bgColor) {
			this(icon, title, fragment, bgColor, null);
		}
		
		public DrawerItem(int icon, String title, DrawerItemFragments fragment, int bgColor, String toggleKey) {
			this.icon = icon;
			this.title = title;
			this.fragment = fragment;
			this.bgColor = bgColor;
			this.toggleKey = toggleKey;
		}
		
		public String getToggleKey() {
			return toggleKey;
		}

		public DrawerItemFragments getFragmentType() {
			return fragment;
		}

		public String getTitle() {
			return title;
		}
		public void toggle() {
			if (fragment == DrawerItemFragments.RATEAPP) {
				if (isAmazon())
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.AMAZON_RATE_URL))));
				else
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.GOOGLE_RATE_URL))));
			}
		}
		
		public Fragment getFragment() {
			if (getFragmentType() == DrawerItemFragments.MYPROFILE)
				return new ProfileFragment();
			if (getFragmentType() == DrawerItemFragments.HOME)
				return null;
				// return new LoggedInFragment(); // We are already here, we merely need to close the drawer. Return a null seems to work, fine.
			if (getFragmentType() == DrawerItemFragments.MYCHART)
				return new MyChartFragment();
			if (getFragmentType() == DrawerItemFragments.HOWTOPLAY)
				return new HTPFragment();
			if (getFragmentType() == DrawerItemFragments.PLAY)
				return PackSelectFragment.newInstance(false);
			if (getFragmentType() == DrawerItemFragments.MULTIPLAYER)
				return PackSelectFragment.newInstance(true);
			if (getFragmentType() == DrawerItemFragments.PARTYPLAY)
				return null;
			if (getFragmentType() == DrawerItemFragments.MYGAMES)
				return new MyGamesFragment();
			if (getFragmentType() == DrawerItemFragments.MYDISCS)
				return new MyDiscsFragment();
			if (getFragmentType() == DrawerItemFragments.LEADERBOARD)
				return LeaderboardFragment.newInstance(0);
			if (getFragmentType() == DrawerItemFragments.MYMUSICBASKET)
				return new MusicBasketFragment();
			
			return null;
		}

		public int getIcon() {
			return icon;
		}
	}
	
	public class DrawerAdapter extends ArrayAdapter<DrawerItem> {
		public DrawerAdapter(Context context, int resource) {
			super(context, resource);
		}
		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}
		
		@Override
		public int getItemViewType(int position) {
		    if (getItem(position) == null)
		    	return 1;
		    return 0;
		}

		@Override
		public int getViewTypeCount() {
		    return 2;
		}
		
		@Override
		public boolean isEnabled(int position) {
			return getItem(position) != null;
		}
		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (position == 0) {
				LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.drawerprofilecell, null);

				String playerName = "Test";
				String imageURL = null;
				try {
					if (Storage.getUserProfile(getContext()) != null)
						playerName = Storage.getUserProfile(getContext()).getJSONObject("UserProfile").optString("Name", "");
					
					String fbId = Storage.getUserProfile(getContext()).getJSONObject("UserProfile").optString("FacebookId", null);
					if (fbId != null && fbId.length() > 0) {
						imageURL = Helpers.getFacebookProfileImage(fbId);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				((TextView)view.findViewById(R.id.profileCellNameTextView)).setText(playerName);

				if (imageURL != null) {
					Helpers.displayImage(getContext(), imageURL, (ImageView) view.findViewById(R.id.profileImageView));
				}

				Helpers.setAllFonts(view, getContext());
				
				return view;
			}
			
			
			if (getItem(position) == null) {
				LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.drawercellseparator, null);
				return view;
			}
			
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.drawercell, null);
			
			TextView tv = (TextView) view.findViewById(R.id.titleTextView);
			ImageView iv = (ImageView) view.findViewById(R.id.iconImageView);
			
			TextView toggleTextView = (TextView) view.findViewById(R.id.toggleTextView);
			if (toggleTextView != null) {
				if (getItem(position).getToggleKey() != null && getItem(position).getToggleKey().length() > 0) {
					toggleTextView.setVisibility(View.VISIBLE);
				}
				else {
					toggleTextView.setVisibility(View.INVISIBLE);
				}
			}
			
			tv.setText(getItem(position).getTitle());
			iv.setImageResource(getItem(position).getIcon());
			
			Helpers.setAllFonts(view, getContext());
			
			view.setBackgroundResource(getItem(position).getBGColor());
			
			return view;
		}
		
	}
	
	public void publishAmazonLeaderBoardScore(String leaderboardId, long score) {
		if (leaderboardId == null)
			return;
		
		LeaderboardsClient lbClient = null;
		if (agsClient != null)
			lbClient = agsClient.getLeaderboardsClient();
		
		if (lbClient != null)
			lbClient.submitScore(leaderboardId, score);
	}
	
	public void setPackAchievement(String packId, int stars) {
		if (packId == null)
			return;

		AchievementsClient acClient = null;
		if (agsClient != null)
			acClient = agsClient.getAchievementsClient();

		if (acClient != null) {
			float progress = stars/9f * 100f;

			AGResponseHandle<UpdateProgressResponse> handle = acClient.updateProgress(packId.replaceAll("-",  "_"), progress);
			handle.setCallback(new AGResponseCallback<UpdateProgressResponse>() {
				@Override
				public void onComplete(UpdateProgressResponse result) { }
			});
		}	
	}
	
	public void updateDiscAchievement() {
		AchievementsClient acClient = null;
		
		if (agsClient != null)
			acClient = agsClient.getAchievementsClient();

		if (acClient != null) {
			
			JSONObject userProfile = Storage.getUserProfile(this);
			//noinspection StatementWithEmptyBody
			if (userProfile == null || userProfile.optJSONObject("UserProfile") == null) {
				
			}
			else {
				int discs = Helpers.getPlayerDiscs(userProfile);
				
				if (discs > 500000)
					acClient.updateProgress(getString(R.string.AMAZON_ACHIEVEMENT_SILVER_DISC), 100f);
				else
					acClient.updateProgress(getString(R.string.AMAZON_ACHIEVEMENT_SILVER_DISC), discs/500000f*100f);
				
				if (discs > 1000000)
					acClient.updateProgress(getString(R.string.AMAZON_ACHIEVEMENT_GOLD_DISC), 100f);
				else
					acClient.updateProgress(getString(R.string.AMAZON_ACHIEVEMENT_GOLD_DISC), (discs-500000)/500000f*100f);
					
				if (discs > 1500000)
					acClient.updateProgress(getString(R.string.AMAZON_ACHIEVEMENT_PLATINUM_DISC), 100f);
				else
					acClient.updateProgress(getString(R.string.AMAZON_ACHIEVEMENT_PLATINUM_DISC), (discs-1000000)/500000f*100f);
				
				if (discs > 2000000)
					acClient.updateProgress(getString(R.string.AMAZON_ACHIEVEMENT_DIAMOND_DISC), 100f);
				else
					acClient.updateProgress(getString(R.string.AMAZON_ACHIEVEMENT_DIAMOND_DISC), (discs-1500000)/500000f*100f);
			}
		}	
	}
	
	@Override
	public void onPause() {
		super.onPause();
		AdColony.pause();
		if (agsClient != null) {
			AmazonGamesClient.release();
		}
	}

	public void disableAds() {
		View v = findViewById(R.id.adlayout);
		if (v != null)
			v.setVisibility(View.GONE);
	}
	public void enableAds() {
		View v = findViewById(R.id.adlayout);
		if (v != null)
			v.setVisibility(View.VISIBLE);
		
		View prog = findViewById(R.id.progressLayout);
		if (prog != null && prog.getVisibility() != View.GONE)
			prog.setVisibility(View.GONE);
	}
	public void enableLowerChart() {
		View v = findViewById(R.id.adlayout);
		if (v != null)
			v.setVisibility(View.GONE);

		View prog = findViewById(R.id.progressLayout);
		if (prog != null) {
			prog.setVisibility(View.VISIBLE);

			/**
			 * Progression views
			 */
			pb = (ProgressBar) findViewById(R.id.progressBar1);
	        pb.setMax(2000000);

	        pbRound = (ProgressBar) findViewById(R.id.progressBarRound);
	        pickLayout = (RelativeLayout) findViewById(R.id.pickLayout);
	        pbRound.setMax(2000000);

	        tv = (TextView) findViewById(R.id.pickTextView);

	        UserProfile up = new UserProfile(Storage.getUserProfile(this));
	        int discs = up.getNumberOfDiscs();

			increase(pb.getProgress() / 20000, Helpers.getPercentageCompletedFromDiscs(discs), 100);
		}
	}
	public void disableLowerChart() {
		View prog = findViewById(R.id.progressLayout);
		if (prog != null && prog.getVisibility() != View.GONE)
			prog.setVisibility(View.GONE);
	}
	
	public boolean showInterstitial() {
		UserProfile up = new UserProfile(Storage.getUserProfile(this));
		int totalPlayed = up.getTotalPlayedGames();
		
		if (totalPlayed >= 10 && totalPlayed % 10 == 0 && startRateOfApp())
			return true;
		
		if (isAmazon() && !isAmazonPhone()) {
			boolean amazonResult = interstitialAmazon.showAd();
			
			if (!amazonResult)
				interstitialAmazon.loadAd();
			
			return amazonResult;
		}
		
		if (interstitial != null && interstitial.isReady()) {
			interstitial.show();
			return true;
		}
		
		if (interstitial != null)
			interstitial.load();
		
		return false;
    }
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		unregisterReceiver(mHandleMessageReceiver);

        if (moPubView != null)
			moPubView.destroy();

		if (interstitial != null)
			interstitial.destroy();
		
		if (amazonAdView != null)
			amazonAdView.destroy();

        if (iabHelper != null)
            iabHelper.dispose();
	}
	
	@Override
    protected void onStop() {
        super.onStop();

        stopMediaPlayers();
    }
	
	/**
	 * Updates the coins, locks and musicBasket entries in the topBar
	 * 
	 * Also starts onUpdate for all visible fragments if they are instances of {@link IUpdateableFragment}
	 */
	public void updateUI() {
		TextView playerCoins = (TextView) findViewById(R.id.playerCoins);
		TextView headerBasketTextView = (TextView) findViewById(R.id.headerBasketTextView);
		TextView playerLocks = (TextView) findViewById(R.id.playerLocks);
		ImageView lv = (ImageView) findViewById(R.id.headerLocksIcon);

		int basketCount = 0;
		int coins = 0;

		JSONObject userProfile = Storage.getUserProfile(this);
		if (userProfile != null && userProfile.optJSONObject("UserProfile") != null) {
			coins = Helpers.getPlayerDiscs(userProfile);
			try {
				basketCount = userProfile.getJSONObject("UserProfile").getJSONArray("Likes").length();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (headerBasketTextView != null)
			headerBasketTextView.setText("" + basketCount);
		
		if (playerCoins != null)
			playerCoins.setText(Helpers.getDiscString(coins));

		int locks = Helpers.getNumberOfUnlocks(userProfile);
		if (playerLocks != null) {
			playerLocks.setText("" + locks);
			if (locks > 0)  // swap out the locked icon for unlocked
				lv.setImageResource(R.drawable.padlock_unlock_solo);
			else lv.setImageResource(R.drawable.padlock_solo); // reset incase we've just spent our last lock
		}

		for (Fragment f : getVisibleFragments()) {
			if (f != null && f instanceof IUpdateableFragment) {
				((IUpdateableFragment) f).onUpdate();
			}
		}
	}
	
	/**
	 * @return Returns a List of all visible fragments, or size == 0 if non exist
	 */
	public List<Fragment> getVisibleFragments(){
		@SuppressWarnings("Convert2Diamond") List<Fragment> visibleFragments = new ArrayList<Fragment>();
		
	    FragmentManager fragmentManager = getSupportFragmentManager();
	    List<Fragment> fragments = fragmentManager.getFragments();
	    for(Fragment fragment : fragments){
	        if(fragment != null && fragment.isVisible())
	        	visibleFragments.add(fragment);
	    }
	    
	    return visibleFragments;
	}
	
	@Override
	public void onBackPressed() {
		if (shouldBlockBack)
			return;
		
		if (offlineMode) {
			finish();
			return;
		}
		
		if (backstackKey != null) {
			if (skip) {
				super.onBackPressed();
				skip = false;
			}
			else {
				getSupportFragmentManager().popBackStackImmediate(backstackKey, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				backstackKey = null;
			}
		}
		else {
			super.onBackPressed();
		}
	}

	/**
	 * Volumebuttons should always change the music volume and not the ringlevels
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (audio == null)
			audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		
	    switch (keyCode) {
		    case KeyEvent.KEYCODE_VOLUME_UP:
		        audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
		                AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
		        return true;
		    case KeyEvent.KEYCODE_VOLUME_DOWN:
		        audio.adjustStreamVolume(AudioManager.STREAM_MUSIC,
		                AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
		        return true;
		        
		    case KeyEvent.KEYCODE_BACK:
		    	onBackPressed();
		    	return true;
		    default:
		    	
	        return false;
	    }
	}
	
	
	private static void setEnabledIfNotNull(View v, boolean value) {
		if (v != null)
			v.setEnabled(value);
	}
	
	/**
	 * Changes the default backstack pruning
	 * @param value if true back will be blocked, false back will be unblocked
	 */
	public void setShouldBlockBackButton(boolean value) {
		shouldBlockBack = value;
		
		setEnabledIfNotNull(findViewById(R.id.headerBasketButton), !value);
		setEnabledIfNotNull(findViewById(R.id.headerHowToButton), !value);
		setEnabledIfNotNull(findViewById(R.id.headerLocksButton), !value);
		setEnabledIfNotNull(findViewById(R.id.headerCoinBackground), !value);
		
		if (offlineMode)
			return;

		DrawerLayout dl = (DrawerLayout) findViewById(R.id.drawerLayout);
		if (dl != null) {
			dl.setEnabled(!value);
			
			if (value && dl.isDrawerOpen(Gravity.LEFT))
				dl.closeDrawer(Gravity.LEFT);
			
			if (value) {
				dl.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);	
			}
			else {
				dl.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
			}
		}
	}

    /**
     * On back button pressed exit to a given backstack key
     * @param backstackKey
     */
	public void setOnBackExitTo(String backstackKey) {
		this.backstackKey = backstackKey;
	}
	
	public void closeDrawerIfOpen() {
		DrawerLayout dl = (DrawerLayout) findViewById(R.id.drawerLayout);
		if (dl != null && dl.isDrawerOpen(Gravity.LEFT))
			dl.closeDrawer(Gravity.LEFT);
	}

    public void increaseBasketCount() {
        TextView headerBasketTextView = (TextView) findViewById(R.id.headerBasketTextView);

        if (headerBasketTextView != null) {
            try {
                String s = headerBasketTextView.getText().toString();
                if (s != null) {
                    int i = Integer.parseInt(s);
                    i++;
                    headerBasketTextView.setText("" + i);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    public void startGame(String opponent, String packId, String levelId, String playerNames[], boolean showInterstitial) {
        if (!showInterstitial) {
            startGameOpponent = startGameLevelId = startGamePackId = null;
            startGamePlayerNames = null;

            if (opponent == null)
                opponent = "";

            if (playerNames == null)
                Helpers.startGame(LoggedInMenu.this, R.id.frameLayout1, opponent, packId, levelId);
            else
                Helpers.startPartyPlayGame(LoggedInMenu.this, R.id.frameLayout1, packId, levelId, playerNames);
        }
        else {
            startGameLevelId = levelId;
            startGameOpponent = opponent;
            startGamePackId = packId;
            startGamePlayerNames = playerNames;

            if (!showInterstitial()) {
                startGame(startGameOpponent, startGamePackId, startGameLevelId, startGamePlayerNames, false);
            }
        }
    }

    private void afterInterstitial() {
        if (interstitial != null)
            interstitial.load();

        if (startGamePackId != null && startGameLevelId != null)
            startGame(startGameOpponent, startGamePackId, startGameLevelId, startGamePlayerNames, false);
    }
    public void setClearStackBeforeNext() {
        clearStackBeforeNext = true;
    }

    public void clearBackStackIfNeeded() {
        if (!clearStackBeforeNext)
            return;
        clearStackBeforeNext = false;
        int curr = getSupportFragmentManager().getBackStackEntryCount();
        int prev = curr + 1;
        while (curr != 0 && curr != prev) {
            onBackPressed();
            prev = curr;
            curr = getSupportFragmentManager().getBackStackEntryCount();
        }
    }
	
	
	/**********************************************************************
	 * Amazon IAP stuffs
	 *********************************************************************/
	@Override
	public void onGetUserIdResponseSuccessful(String userId, boolean userChanged) {
		Set<String> requestIds = purchaseDataStorage.getAllRequestIds();
		for (String requestId : requestIds) {
			PurchaseData purchaseData = purchaseDataStorage.getPurchaseData(requestId);
			if (purchaseData == null || purchaseData.getSKU() == null) {
				continue;
			}

			final String sku = purchaseData.getSKU();
			SKUData skuData = purchaseDataStorage.getSKUData(sku);
		}
	}

	public Map<String, Item> getAllItems() {
		if (itemData == null)
			//noinspection Convert2Diamond
			return new HashMap<String, Item>();
		
		return itemData;
	}

    public Map<String, JSONObject> getAllGoogleItems() {
        if (googleItemData == null)
			//noinspection Convert2Diamond
			return new HashMap<String, JSONObject>();

        return googleItemData;
    }

    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 10001;

	public void buy(String sku) {
        if (isAmazon()) {
            String requestId = PurchasingManager.initiatePurchaseRequest(sku);
            PurchaseData purchaseData = purchaseDataStorage.newPurchaseData(requestId);
            Log.i("", "buy: requestId (" + requestId + ") requestState (" + purchaseData.getRequestState() + ")");
        }
        else {
            iabHelper.launchPurchaseFlow(this, sku, RC_REQUEST, mPurchaseFinishedListener, "");
        }
	}


    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, final Purchase purchase) {
            // if we were disposed of in the meantime, quit.
            if (iabHelper == null) return;

            if (result.isFailure()) {
                return;
            }

            InAppProducts iap = new InAppProducts(Storage.getInAppProducts(LoggedInMenu.this));
            String spendTypeId = iap.getSpendTypeId(purchase.getSku());

            JSONObject o = JSONRequestHelpers.getSpendRequest(Storage.getUserProfile(LoggedInMenu.this), spendTypeId, purchase.getToken());

            Storage.addRequestToQueue(LoggedInMenu.this, Storage.INAPPPURCHASE_URL, o, new OnNetworkResult() {
                @Override
                public void okResult(JSONObject response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DialogHelpers.showOkDialog(LoggedInMenu.this, R.string.INAPP_PURCHASE_SUCCESS_TITLE, R.string.INAPP_PURCHASE_SUCCESS_TEXT, R.string.OK);
                        }
                    });
                    iabHelper.consumeAsync(purchase, null);
                    Storage.updateUserProfile(LoggedInMenu.this, response, true);
                }

                @Override
                public void badResult(String errorMessage) {
                }
            });
        }
    };

	
	@Override
	public void onItemDataResponseSuccessful(Map<String, Item> itemData) {
		this.itemData = itemData;
	}

	@Override
	public void onPurchaseResponseSuccess(String userId, final String sku, String purchaseToken) {
		InAppProducts iap = new InAppProducts(Storage.getInAppProducts(this));
		String spendTypeId = iap.getSpendTypeId(sku);

		JSONObject o = JSONRequestHelpers.getSpendRequest(Storage.getUserProfile(this), spendTypeId, purchaseToken);

		Storage.addRequestToQueue(this, Storage.INAPPPURCHASE_URL, o, new OnNetworkResult() {
			@Override
			public void okResult(JSONObject response) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						DialogHelpers.showOkDialog(LoggedInMenu.this, R.string.INAPP_PURCHASE_SUCCESS_TITLE, R.string.INAPP_PURCHASE_SUCCESS_TEXT, R.string.OK);
					}
				});
		
				SKUData skuData = purchaseDataStorage.getSKUData(sku);
				if (skuData != null) {
					skuData.consume(1);
					purchaseDataStorage.saveSKUData(skuData);
				}
				
				Storage.updateUserProfile(LoggedInMenu.this, response, true);
			}
			
			@Override
			public void badResult(String errorMessage) {
			}
		});
	}

    @Override
    public void onGetUserIdResponseFailed(String requestId) { }
    @Override
    public void onItemDataResponseSuccessfulWithUnavailableSkus(Set<String> unavailableSkus) { }
    @Override
    public void onItemDataResponseFailed(String requestId) { }
    @Override
    public void onPurchaseResponseAlreadyEntitled(String userId, String sku) { }
    @Override
    public void onPurchaseResponseInvalidSKU(String requestId, String sku) { }
    @Override
    public void onPurchaseResponseFailed(String requestId, String sku) { }
    @Override
    public void onPurchaseUpdatesResponseSuccess(String userId, String sku, String purchaseToken) { }
    @Override
    public void onPurchaseUpdatesResponseSuccessRevokedSku(String userId, String revokedSku) { }
    @Override
    public void onPurchaseUpdatesResponseFailed(String requestId) { }


    /**
     * Mopub interstitial
     */
	@Override
	public void onInterstitialClicked(MoPubInterstitial arg0) {	}
	@Override
	public void onInterstitialDismissed(MoPubInterstitial arg0) {
		afterInterstitial();
	}
	@Override
	public void onInterstitialFailed(MoPubInterstitial arg0, MoPubErrorCode arg1) { }
	@Override
	public void onInterstitialLoaded(MoPubInterstitial arg0) { }
	@Override
	public void onInterstitialShown(MoPubInterstitial arg0) { }
}
