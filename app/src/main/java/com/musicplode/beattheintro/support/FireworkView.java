package com.musicplode.beattheintro.support;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
// import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;

import com.musicplode.beattheintro.storage.Storage;

import java.util.ArrayList;
import java.util.List;

public class FireworkView extends View {
	public static class Explosion {
		int currentStep = 0;
		private int maxX, maxY, fromColor, toColor, steps, numberOfParticles, numberOfTrailPartiles;

		
		/**
		 * particle
		 * float [particleNumber][trailNumber][xOrY]
		 * 
		 * acceleration
		 * float [particleNumber][accelerationXorY]
		 * 
		 */
		
		
		float particle[][][];
		float acceleration[][];
		
		
		int fromRed, fromGreen, fromBlue, toRed, toGreen, toBlue;
		private int waitFor;

		public Explosion(int x, int y, int steps, int numberOfParticles, int numberOfTrailPartiles) {
			maxX = x;
			maxY = y;
			this.steps = steps;
			this.numberOfParticles = numberOfParticles;
			this.numberOfTrailPartiles = numberOfTrailPartiles; 
			
			particle = new float[numberOfParticles][numberOfTrailPartiles][2];
			acceleration = new float[numberOfParticles][2];
			
			resetFirework();
		}

		private void resetFirework() {
			currentStep = 0;
			waitFor = Storage.random.nextInt(steps/4);

            particle = new float[numberOfParticles][numberOfTrailPartiles][2];
            acceleration = new float[numberOfParticles][2];

            fromColor = Color.rgb(Storage.random.nextInt(255), Storage.random.nextInt(255), Storage.random.nextInt(255));
            toColor = Color.rgb(Storage.random.nextInt(255), Storage.random.nextInt(255), Storage.random.nextInt(255));

            fromRed = Color.red(fromColor);
            fromGreen = Color.green(fromColor);
            fromBlue = Color.blue(fromColor);

            toRed = Color.red(toColor);
			toGreen = Color.green(toColor);
			toBlue = Color.blue(toColor);
			
			
			int x = Storage.random.nextInt(maxX - 200) + 100;
			int y = Storage.random.nextInt(maxY/2);
			
			for (int i = 0; i < numberOfParticles; i++) {
				for (int j = 0; j < numberOfTrailPartiles; j++) {
					particle[i][j][0] = x;
					particle[i][j][1] = y;
				}
				acceleration[i][0] = (Storage.random.nextFloat() - 0.5f)*10;
				acceleration[i][1] = (Storage.random.nextFloat() - 0.5f)*10;
			}
		}
		
		public void update() {
			waitFor--;
			if (waitFor > 0) {
				currentStep = 0;
				return;
			}
		
			if (currentStep > steps) {
				resetFirework();
			}
			
			currentStep++;
			
			int trailPlaceToReplace = (currentStep+1) % numberOfTrailPartiles;
			int newestCalculatedTrailPlace = currentStep % numberOfTrailPartiles;

			for (int i = 0; i < numberOfParticles; i++) {
				particle[i][trailPlaceToReplace][0] =  particle[i][newestCalculatedTrailPlace][0] + acceleration[i][0];
				particle[i][trailPlaceToReplace][1] =  (float) (particle[i][newestCalculatedTrailPlace][1] + acceleration[i][1] + Math.pow(currentStep/20f, 2));
			}
		}

		public int getColor(int trail) {
			double frac = currentStep / (double)steps;
			int blueDiff = toBlue - fromBlue;
			int redDiff = toRed - fromRed ;
			int greenDiff = toGreen - fromGreen;
			
			return Color.argb(Math.max(255 - currentStep*3, 0), (int)(fromRed + redDiff*frac), (int)(fromGreen + greenDiff*frac), (int)(fromBlue + blueDiff*frac));
		}
		
		public void drawFireWork(Canvas c) {
			if (waitFor > 0) {
				return;
			}
			
			int newestTrailPlace = currentStep % numberOfTrailPartiles;

			for (int i = (newestTrailPlace + 1) % numberOfTrailPartiles; i != newestTrailPlace; i = (i + 1) % numberOfTrailPartiles) {
				Paint p = new Paint();
				p.setColor(getColor(currentStep));
				for (int particleId = 0; particleId < numberOfParticles; particleId++) {
					c.drawRect(new RectF(particle[particleId][i][0], particle[particleId][i][1], particle[particleId][i][0] + 3, particle[particleId][i][1] + 3), p);
				}
			}
			
		}
	}
	
	public FireworkView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	int width, height;
	
	private List<Explosion> explosions = new ArrayList<Explosion>();
	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		if (canvas.getWidth() <= 0)
			return;
		
		if (explosions.isEmpty()) {
			for (int i = 0; i < 5; i++) {
				explosions.add(new Explosion(canvas.getWidth(), canvas.getHeight(), 150, 30, 7));
			}
		}
		

		for (Explosion e : explosions) {
			e.update();
			e.drawFireWork(canvas);
		}
		
	}

    private Handler mHandler = new Handler();
    private Runnable mTick = new Runnable() {
	    @Override
		public void run() {
	        invalidate();
	        mHandler.postDelayed(this, 20);
	    }
	};

	public void startAnimation() {
	    mHandler.removeCallbacks(mTick);
	    mHandler.post(mTick);
	}

	public void stopAnimation() {
		if (mHandler != null)
			mHandler.removeCallbacks(mTick);
	}
	
	
	@Override
	public void onVisibilityChanged(View changedView, int visibility) {
		super.onVisibilityChanged(changedView, visibility);
		if (visibility == View.VISIBLE)
			startAnimation();
		else
			stopAnimation();
	}
	
	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();

		startAnimation();
	}

	@Override
	public void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		stopAnimation();
	}
}