package com.musicplode.beattheintro.support;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;

import com.musicplode.beattheintro.R;

public class RingView extends View {
	private RectF rectF;
	private Bitmap bmp;
	private Canvas currentCircleCanvas;
	private Paint paint = new Paint();
	private int currentCircle;
	private Options o = new Options();
	
	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		if (bmp != null && bmp.isRecycled())
			return;
		
		if (rectF == null) {
			o.inMutable = true;
			bmp = BitmapFactory.decodeResource( getResources(), R.drawable.counter_circle_3 , o);
			if (bmp == null)
				return;
			currentCircleCanvas = new Canvas(bmp);
			rectF = new RectF(0, 0, bmp.getWidth(), bmp.getHeight());
		    paint = new Paint();
		    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		    
		    currentCircle = 0;
		}
		
		long elapsedTime = SystemClock.uptimeMillis() - mAnimStartTime;
		
		if (elapsedTime > 3000) {
			bmp.recycle();
			return;
		}
		else if (elapsedTime > 2000 && currentCircle < 2) {
			currentCircle = 2;
			bmp.recycle();
			
			bmp = BitmapFactory.decodeResource( getResources(), R.drawable.counter_circle_1 , o);
			currentCircleCanvas = new Canvas(bmp);
		}
		else if (elapsedTime > 1000 && currentCircle < 1) {
			currentCircle = 1;
			
			bmp = BitmapFactory.decodeResource( getResources(), R.drawable.counter_circle_2 , o);
			currentCircleCanvas = new Canvas(bmp);
		}
		int bla = (int) (360 * (elapsedTime%1000 / (double) 1000));
		punchHole(bmp, bla);
		canvas.drawBitmap(bmp, canvas.getWidth()/2 - bmp.getWidth()/2, canvas.getHeight()/2 - bmp.getHeight()/2, null);
	}
	public void punchHole(Bitmap bitmap, float sweepDegrees) {
		currentCircleCanvas.drawArc(rectF, -90, sweepDegrees, true, paint);
	}
	
	Handler mHandler = new Handler();
	Runnable mTick = new Runnable() {
	    @Override
		public void run() {
	        invalidate();
	        mHandler.postDelayed(this, 50);
	    }
	};
	private long mAnimStartTime;
	
	public RingView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public void startAnimation() {
	    mAnimStartTime = SystemClock.uptimeMillis();
	    mHandler.removeCallbacks(mTick);
	    mHandler.post(mTick);
	}

	public void stopAnimation() {
	    mHandler.removeCallbacks(mTick);
	}
	
	@Override
	public void onVisibilityChanged(View changedView, int visibility) {
		super.onVisibilityChanged(changedView, visibility);
		if (visibility == View.VISIBLE)
			startAnimation();
		else
			stopAnimation();
	}	
	
	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();

		startAnimation();
	}

	@Override
	public void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		stopAnimation();
	}
}
