package com.musicplode.beattheintro.support;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.musicplode.beattheintro.R;

public class PieProgressBar extends ProgressBar {
	private RectF rectF;
	private Bitmap bmp;
	private Canvas currentCircleCanvas;
	private Paint paint = new Paint();
    private int currentImageResource;
	private Options o = new Options();
    private boolean recreateBitmap;

    public PieProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void loadImageResource() {
        if (0 < getProgress() && getProgress() < 33 && currentImageResource != R.drawable.partyplay_counter_blue) {
            if (bmp != null)
                bmp.recycle();
            bmp = BitmapFactory.decodeResource( getResources(), R.drawable.partyplay_counter_blue , o);
            currentImageResource = R.drawable.partyplay_counter_blue;
            currentCircleCanvas = new Canvas(bmp);
        }
        else if (33 < getProgress() && getProgress() < 66 && currentImageResource != R.drawable.partyplay_counter_yellow) {
            if (bmp != null)
                bmp.recycle();
            bmp = BitmapFactory.decodeResource( getResources(), R.drawable.partyplay_counter_yellow , o);
            currentImageResource = R.drawable.partyplay_counter_yellow;
            currentCircleCanvas = new Canvas(bmp);
        }
        else if (66 < getProgress() && getProgress() < 100 && currentImageResource != R.drawable.partyplay_counter_red) {
            if (bmp != null)
                bmp.recycle();
            bmp = BitmapFactory.decodeResource( getResources(), R.drawable.partyplay_counter_red , o);
            currentImageResource = R.drawable.partyplay_counter_red;
            currentCircleCanvas = new Canvas(bmp);
        }
    }


    @Override
	public void onDraw(Canvas canvas) {
		//super.onDraw(canvas);

		if (bmp != null && bmp.isRecycled())
			return;

        loadImageResource();

		if (rectF == null || recreateBitmap) {
            if (recreateBitmap && bmp != null)
                bmp.recycle();

            recreateBitmap = false;

			o.inMutable = true;
            bmp = BitmapFactory.decodeResource( getResources(), R.drawable.partyplay_counter_blue , o);
            currentImageResource = R.drawable.partyplay_counter_blue;
            if (bmp == null)
                return;

			rectF = new RectF(0, 0, bmp.getWidth(), bmp.getHeight());
		    paint = new Paint();
            currentCircleCanvas = new Canvas(bmp);
		    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		}

		int bla = (int) ((double)360 * ((double)getProgress()/(double)getMax()));
        punchHole(bmp, bla);
		canvas.drawBitmap(bmp, canvas.getWidth()/2 - bmp.getWidth()/2, canvas.getHeight()/2 - bmp.getHeight()/2, null);
	}

    private int progress = 0;

    @Override
    public synchronized void setProgress(int progress) {
        if (this.progress > progress) {
            recreateBitmap = true;
            System.out.println("recreate bmp");
        }

        this.progress = progress;
        invalidate();
    }

    @Override
    public synchronized int getProgress() {
        return progress;
    }

    public void punchHole(Bitmap bitmap, float sweepDegrees) {
		currentCircleCanvas.drawArc(rectF, -90, sweepDegrees, true, paint);
	}
}
