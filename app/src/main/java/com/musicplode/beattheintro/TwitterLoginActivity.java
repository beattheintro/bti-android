package com.musicplode.beattheintro;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.musicplode.beattheintro.fragments.TwitterLoginFragment;

/**
 * Activity that handles TwitterLogin, started from SocialHelperFragment
 */
public class TwitterLoginActivity extends FragmentActivity {
	@Override
	public void onCreate(Bundle savedStateInstance) {
		super.onCreate(savedStateInstance);
		
		setContentView(R.layout.emptyframe);
		
		if (savedStateInstance == null) {
			TwitterLoginFragment tlf = new TwitterLoginFragment();
			tlf.setBackOnDone();
			
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			ft.add(R.id.frameLayout2, tlf);
			ft.commit();
		}
	}
}