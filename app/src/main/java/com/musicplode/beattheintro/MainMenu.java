package com.musicplode.beattheintro;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
// import android.widget.ImageView;

import com.mopub.mobileads.MoPubView;
import com.musicplode.beattheintro.fragments.HTPFragment;
import com.musicplode.beattheintro.fragments.LoginFragment;
import com.musicplode.beattheintro.helpers.Helpers;

public class MainMenu extends FragmentActivity {
	private static final String INFO ="info";
	
	private MoPubView moPubView;
	private boolean isTablet;

	private boolean isSignup;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		isTablet = getResources().getBoolean(R.bool.tablet);

		if(isTablet){
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	    }
		else
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		setContentView(R.layout.headerandfootermainmenu);

		isSignup = getIntent() != null ? getIntent().getBooleanExtra("signup", false) : false;
		
		/**
		 * If app is reinstantiated dont readd the fragments
		 */
		if (savedInstanceState == null) {
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			Helpers.setFragmentAnimation(ft);
			Fragment f = LoginFragment.newInstance(isSignup);
			ft.add(R.id.frameLayout1, f);
			ft.commit();
		}
		

		findViewById(R.id.headerHowToButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
				getSupportFragmentManager().popBackStack(INFO, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				Helpers.setFragmentAnimation(ft);
				Fragment f = new HTPFragment();
				ft.replace(R.id.frameLayout1, f);
				ft.addToBackStack(INFO);
				ft.commit();
			}
		});
		
		moPubView = (MoPubView) findViewById(R.id.adMopubview);
		moPubView.setAdUnitId(getString(R.string.mopubAdUnitId));
		moPubView.loadAd();
		
		Helpers.addPulse(findViewById(R.id.headerHowToButton), 0);
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		moPubView.destroy();
	}
}
