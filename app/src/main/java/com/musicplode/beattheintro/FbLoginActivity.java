package com.musicplode.beattheintro;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Social login fragment that can be launched from SocialHelperFragment 
 */
public class FbLoginActivity extends Activity {
	private static List<String> permissions;
    private Session.StatusCallback statusCallback = new SessionStatusCallback();
    private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/***** FB Permissions *****/
		//noinspection Convert2Diamond
		permissions = new ArrayList<String>();
		permissions.add("email");
		/***** End FB Permissions *****/
		
		Session session = Session.getActiveSession();

		Session.openActiveSession(FbLoginActivity.this, true, statusCallback);
		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(this, null, statusCallback,
						savedInstanceState);
			}
			if (session == null) {
				session = new Session(this);
			}
			Session.setActiveSession(session);
			session.addCallback(statusCallback);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				session.openForRead(new Session.OpenRequest(this).setCallback(
						statusCallback).setPermissions(permissions));
			}
		}
	}

	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			// Check if Session is Opened or not
			processSessionStatus(session, state, exception);
		}

	}

	public void processSessionStatus(Session session, SessionState state,
			Exception exception) {

		if (session != null && session.isOpened()) {

			if (session.getPermissions().contains("email")) {
				// Show Progress Dialog
				dialog = new ProgressDialog(FbLoginActivity.this);
				dialog.setMessage("Loggin in..");
				dialog.show();
				
				Request.newMeRequest(session,
						new Request.GraphUserCallback() {

							@Override
							public void onCompleted(GraphUser user,
									Response response) {

								if (dialog != null && dialog.isShowing()) {
									dialog.dismiss();
								}
								if (user != null) {
									@SuppressWarnings("Convert2Diamond") Map<String, Object> responseMap = new HashMap<String, Object>();
									GraphObject graphObject = response.getGraphObject();
									responseMap = graphObject.asMap();
									Log.i("FbLogin", "Response Map KeySet - " + responseMap.keySet());
									// TODO : Get Email
									// responseMap.get("email");

									if (responseMap.get("email") != null) {
										String facebookTitle = getIntent() != null ? getIntent().getStringExtra("facebookTitle") : null;
										String facebookDesc = getIntent() != null ? getIntent().getStringExtra("facebookDesc") : null;
										String facebookCaption = getIntent() != null ? getIntent().getStringExtra("facebookCaption") : null;
										String facebookLink = getIntent() != null ? getIntent().getStringExtra("facebookLink") : null;
										String facebookPicture = getIntent() != null ? getIntent().getStringExtra("facebookPicture") : null;
										
										Intent i = new Intent();
										if (facebookTitle != null)
											i.putExtra("facebookTitle", facebookTitle);
										if (facebookDesc != null)
											i.putExtra("facebookDesc", facebookDesc);
										if (facebookCaption != null)
											i.putExtra("facebookCaption", facebookCaption);
										if (facebookLink != null)
											i.putExtra("facebookLink", facebookLink);
										if (facebookPicture != null)
											i.putExtra("facebookPicture", facebookPicture);
										
										setResult(RESULT_OK, i);
										finish();
										
									} else {
										// Clear all session info & ask user to
										// login again
										Session session = Session
												.getActiveSession();
										if (session != null) {
											session.closeAndClearTokenInformation();
										}
									}
								}
							}
						}).executeAsync();
			} else {
				session.requestNewReadPermissions(new Session.NewPermissionsRequest(
						FbLoginActivity.this, permissions));
			}
		}
	}

	/********** Activity Methods **********/

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d("FbLogin", "Result Code is - " + resultCode + "");
		Session.getActiveSession().onActivityResult(FbLoginActivity.this,
				requestCode, resultCode, data);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Session session = Session.getActiveSession();
		Session.saveSession(session, outState);
	}

	@Override
	protected void onStart() {
		super.onStart();
		Session.getActiveSession().addCallback(statusCallback);
	}

	@Override
	protected void onStop() {
		super.onStop();
		Session.getActiveSession().removeCallback(statusCallback);
	}
}