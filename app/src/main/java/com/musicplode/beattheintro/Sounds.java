package com.musicplode.beattheintro;

public class Sounds {
	public enum Sound {
		CORRECT,
		WRONG, 
		COUNTDOWN, 
		CARDTURN, 
		CLOSE, 
		CREDITSCORE, 
		LARGECOINS,
		LOSE, 
		OFF, 
		RIGHTANSWER, 
		SMALLAPPLAUSE, 
		WAHOO, 
		SMALLCOINS, 
		OPEN, 
		STAR, 
		STOP,
	}
}
